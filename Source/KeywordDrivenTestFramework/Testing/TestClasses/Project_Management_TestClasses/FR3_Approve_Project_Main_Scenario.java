/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_PageObject.Project_Management_PageObjects;
import KeywordDrivenTestFramework.Entities.TestResult;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR3-Approve Project Main Scenario",
        createNewBrowserInstance = false
)
public class FR3_Approve_Project_Main_Scenario extends BaseClass
{

    String error = "";

    public FR3_Approve_Project_Main_Scenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!ApproveProject())
        {
            return narrator.testFailed("Approve Project Failed Due To :" + error);
        }

        return narrator.finalizeTest("Project Management record is saved");

    }
    
    public boolean ApproveProject()
    {
        
        //Approve / park project (entity level)
         if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Approve_ParkProject_dropdown()))
        {
            error = "Failed to wait for Approve / park project (entity level) dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Approve_ParkProject_dropdown()))
        {
            error = "Failed to click on Approve / park project (entity level) dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Approve / park project (entity level) : " + getData("Approve / park project (entity level)");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.TypeSearch2(), (getData("Approve / park project (entity level)"))))
        {
            error = "Failed to click the Approve / park project (entity level): " + getData("Approve / park project (entity level)");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Project_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Approve / park project (entity level)"))))
        {
            error = "Failed to wait for Approve / park project (entity level) option : " + getData("Approve / park project (entity level)");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Text2(getData("Approve / park project (entity level)"))))
        {
            error = "Failed to click Approve / park project (entity level) option : " + getData("Approve / park project (entity level)");
            return false;
        }

        narrator.stepPassedWithScreenShot("Approve / park project (entity level): " + getData("Approve / park project (entity level)"));
        
        
         //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.SaveBtn()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.SaveBtn()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Project_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Project_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Record saved");

        return true;
        
    }
}
