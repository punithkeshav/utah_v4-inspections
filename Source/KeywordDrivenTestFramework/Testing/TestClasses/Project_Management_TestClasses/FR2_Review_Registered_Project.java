/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_PageObject.Project_Management_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR2- Review Registered Project - Main Scenario",
        createNewBrowserInstance = false
)
/**
 *
 * @author smabe
 */
public class FR2_Review_Registered_Project extends BaseClass
{

    String error = "";

    public FR2_Review_Registered_Project()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!ProjectApprova())
        {
            return narrator.testFailed("Project Approva Failed Due To :" + error);
        }

        return narrator.finalizeTest("Project Approva record is saved");
    }

    public boolean ProjectApprova()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.projectApprovalTab()))
        {
            error = "Failed to wait for the Project Approval Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.projectApprovalTab()))
        {
            error = "Failed to click the Project Approval Tab.";
            return false;
        }

        //Business unit verification and approval
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.BusinessUnitVerificationAndApproval_dropdown()))
        {
            error = "Failed to wait for Business unit verification and approval dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.BusinessUnitVerificationAndApproval_dropdown()))
        {
            error = "Failed to click on Business unit verification and approval dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Business unit verification and approval type search";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.TypeSearch(), getData("Business unit verification and approval")))
        {
            error = "Failed to enter  Business unit verification and approval: " + getData("Business unit verification and approval");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Project_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Business unit verification and approval"))))
        {
            error = "Failed to wait for Business unit verification and approval drop down option : " + getData("Business unit verification and approval");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Text2(getData("Business unit verification and approval"))))
        {
            error = "Failed to click Business unit verification and approval drop down option : " + getData("Business unit verification and approval");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit verification and approval: " + getData("Business unit verification and approval"));

        if (getData("Tick Business unit approval").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.BusinesssUnit_Checkbox()))
            {
                error = "Failed to wait for Business unit approval Checkbox.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.BusinesssUnit_Checkbox()))
            {
                error = "Failed to click the  Business unit approval  Checkbox.";
                return false;
            }
            narrator.finalizeTest("Successfully ticked   Business unit  approval  Checkbox");

        }
        
        //Group verification and approval
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.GroupVerificationAndApproval_dropdown()))
        {
            error = "Failed to wait for Group verification and approval dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.GroupVerificationAndApproval_dropdown()))
        {
            error = "Failed to click on Group verification and approval dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Group verification and approval type search";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.TypeSearch(), getData("Group verification and approval")))
        {
            error = "Failed to enter  Business unit verification and approval: " + getData("Group verification and approval");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Project_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Group verification and approval"))))
        {
            error = "Failed to wait for Group verification and approval drop down option : " + getData("Group verification and approval");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Text2(getData("Group verification and approval"))))
        {
            error = "Failed to click Group verification and approval drop down option : " + getData("Group verification and approval");
            return false;
        }

        narrator.stepPassedWithScreenShot("Group verification and approval: " + getData("Group verification and approval"));
        
        //Group approval
         if (getData("Group approval").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.BusinesssUnit_Checkbox()))
            {
                error = "Failed to wait for Group approval Checkbox.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Group_Approval_Checkbox()))
            {
                error = "Failed to click the Group approval Checkbox.";
                return false;
            }
            narrator.finalizeTest("Successfully ticked Group approval Checkbox");

        }


        
        

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.SaveBtn()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.SaveBtn()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Project_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Project_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        return true;
    }

}
