/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_PageObject.Project_Management_PageObjects;
import KeywordDrivenTestFramework.Entities.TestResult;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR9-Edit Project Management - Main Scenario",
        createNewBrowserInstance = false
)
public class FR9_Edit_Project_Management_Main_Scenario extends BaseClass
{

    String error = "";

    public FR9_Edit_Project_Management_Main_Scenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!ApproveProject())
        {
            return narrator.testFailed("Approve Project Failed Due To :" + error);
        }

        return narrator.finalizeTest("Project Management record is saved");

    }

    public boolean ApproveProject()
    {
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }
        
//        if (!SeleniumDriverInstance.switchToTabOrWindow())
//        {
//            error = "Failed to switch tab.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ButtonConfirm()))
//        {
//            error = "Failed to wait for the yes button";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.ButtonConfirm()))
//        {
//            error = "Failed to click the yes button";
//            return false;
//        }

//        if (!SeleniumDriverInstance.switchToTabOrWindow())
//        {
//            error = "Failed to switch tab.";
//            return false;
//        }

       // pause(3000);
//         if (!SeleniumDriverInstance.switchToFrameByName(Project_Management_PageObjects.iframe()))
//        {
//            error = "Failed to switch frame.";
//            return false;
//        }

        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ContainsTextBox()))
        {
            error = "Failed to wait for contains";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.SearchButton()))
        {
            error = "Failed wait for Search Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        pause(5000);

        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ProcessFlow_Button()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.ProcessFlow_Button()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        
         if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.technicalFeasibilityComments()))
        {
            error = "Failed to wait for comments text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.technicalFeasibilityComments(), getData("comments")))
        {
            error = "Failed to enter comments";
            return false;
        }

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.SaveBtn()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.SaveBtn()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Project_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Project_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Record saved");

        return true;

    }
}
