/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_PageObject.Project_Management_PageObjects;
import KeywordDrivenTestFramework.Entities.TestResult;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR6-Capture ECO2Man Project Savings - Main Scenario",
        createNewBrowserInstance = false
)
public class FR6_CaptureECO2ManProjectSavings_Main_Scenario extends BaseClass
{

    String error = "";

    public FR6_CaptureECO2ManProjectSavings_Main_Scenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CaptureECO2ManProjectSavings())
        {
            return narrator.testFailed("Capture ECO2Man Project Savings Failed Due To :" + error);
        }

        return narrator.finalizeTest("Project Management record is saved");

    }

    public boolean CaptureECO2ManProjectSavings()
    {
        //ECO2Man
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ECO2ManTab()))
        {
            error = "Failed to wait for the ECO2Man Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.ECO2ManTab()))
        {
            error = "Failed to click the ECO2Man Tab.";
            return false;
        }

       

        //ECO2Man Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ECO2ManAdd()))
        {
            error = "Failed to wait for ECO2Man add.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.ECO2ManAdd()))
        {
            error = "Failed to click ECO2Man add.";
            return false;
        }

        //Year
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ECO2Man_Year_dropdown()))
        {
            error = "Failed to wait for Year dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.ECO2Man_Year_dropdown()))
        {
            error = "Failed to click on Year dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Year : " + getData("Year");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.TypeSearch2(), (getData("Year"))))
        {
            error = "Failed to click the Year: " + getData("Year");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Project_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Year"))))
        {
            error = "Failed to wait for Year option : " + getData("Year");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Text2(getData("Year"))))
        {
            error = "Failed to click Year option : " + getData("Year");
            return false;
        }

        narrator.stepPassedWithScreenShot("Year: " + getData("Year"));

        //Month
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ECO2Man_Month_dropdown()))
        {
            error = "Failed to wait for Month dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.ECO2Man_Month_dropdown()))
        {
            error = "Failed to click on Month dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Month : " + getData("Month");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.TypeSearch2(), (getData("Month"))))
        {
            error = "Failed to click the Month: " + getData("Month");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Project_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Month"))))
        {
            error = "Failed to wait for Month option : " + getData("Month");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Text2(getData("Month"))))
        {
            error = "Failed to click Month option : " + getData("Month");
            return false;
        }

        narrator.stepPassedWithScreenShot("Month: " + getData("Month"));

        //Quarter
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ECO2Man_Quarter_dropdown()))
        {
            error = "Failed to wait for Quarter dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.ECO2Man_Quarter_dropdown()))
        {
            error = "Failed to click on Quarter dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Quarter : " + getData("Quarter");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.TypeSearch2(), (getData("Quarter"))))
        {
            error = "Failed to click the Quarter: " + getData("Quarter");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Project_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Quarter"))))
        {
            error = "Failed to wait for Quarter option : " + getData("Quarter");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Text2(getData("Quarter"))))
        {
            error = "Failed to click Quarter option : " + getData("Quarter");
            return false;
        }

        narrator.stepPassedWithScreenShot("Quarter: " + getData("Quarter"));
        

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.SaveBtn()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.SaveBtn()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Project_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Project_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Record saved");

        return true;

    }
}
