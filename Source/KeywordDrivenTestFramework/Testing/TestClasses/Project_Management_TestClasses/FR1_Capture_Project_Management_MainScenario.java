/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_PageObject.Project_Management_PageObjects;
import KeywordDrivenTestFramework.Entities.TestResult;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR1- Capture Project Managemen - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Project_Management_MainScenario extends BaseClass
{

    String error = "";

    public FR1_Capture_Project_Management_MainScenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!Capture_Project_Management())
        {
            return narrator.testFailed("Capture Project Managemen Failed Due To :" + error);
        }

//        if (testData.getData("Supporting Documents").equalsIgnoreCase("Yes"))
//        {
//            if (!Supporting_Documents())
//            {
//                return narrator.testFailed("Failed due - " + error);
//            }
//        }
        return narrator.finalizeTest("Project Management record is saved");
    }

    public boolean Capture_Project_Management()
    {

        //Project Management
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.project_Management()))
        {
            error = "Failed to wait for Project Managenment Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.project_Management()))
        {
            error = "Failed to click Projects Managenment Tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Projects Managenment Tab");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Add_Button()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Add_Button()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Process flow
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ProcessFlow_Button()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.ProcessFlow_Button()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Entity dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Entity_dropdown()))
        {
            error = "Failed to wait for 'Entity' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Entity_dropdown()))
        {
            error = "Failed to click on 'Entity' dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.TypeSearch2(), getData("Entity option")))
        {
            error = "Failed to enter the Entity : " + getData("Entity option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Project_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        pause(3000);

        //Entity TickBox
//        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.businessUnitOption1(getData("Entity 1"))))
//        {
//            error = "Failed to wait for Entity option : " + getData("Entity 1");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.businessUnitOption1(getData("Entity 1"))))
//        {
//            error = "Failed to wait for Entity option : " + getData("Entity 1");
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.businessUnitOption1(getData("Entity 2"))))
        {
            error = "Failed to wait for Entity option : " + getData("Entity 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.businessUnitOption1(getData("Entity 2"))))
        {
            error = "Failed to wait for Entity option : " + getData("Entity 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.businessUnitOption1(getData("Entity 3"))))
        {
            error = "Failed to wait for Entity option : " + getData("Entity 3");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.businessUnitOption1(getData("Entity 3"))))
        {
            error = "Failed to wait for Entity option : " + getData("Entity 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.businessUnitOption1(getData("Entity 4"))))
        {
            error = "Failed to wait for Entity option : " + getData("Entity 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.businessUnitOption1(getData("Entity 4"))))
        {
            error = "Failed to wait for Entity option : " + getData("Entity 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.businessUnitOption1(getData("Entity 5"))))
        {
            error = "Failed to wait for Entity option : " + getData("Entity 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.businessUnitOption1(getData("Entity 5"))))
        {
            error = "Failed to wait for Entity option : " + getData("Entity 5");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Select_EntityOption(getData("Entity option"))))
        {
            error = "Failed to wait for Entity option : " + getData("Entity option");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Select_EntityOption(getData("Entity option"))))
        {
            error = "Failed to wait for Entity option : " + getData("Entity option");
            return false;
        }
        
          if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Entity_dropdown_2()))
        {
            error = "Failed to wait for 'Entity' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Entity_dropdown_2()))
        {
            error = "Failed to click on 'Entity' dropdown.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Entity: " + getData("Entity option"));

        //Planned start date
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.PlannedStartDate(), startDate))
        {
            error = "Failed to enter the planned start date";
            return false;
        }
        narrator.stepPassedWithScreenShot("Planned start date: " + startDate);

        //Project
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.Project_Input(), getData("Project")))
        {
            error = "Failed to enter the Project field: " + getData("Project");
            return false;
        }
        narrator.stepPassedWithScreenShot("Project: " + getData("Project"));

        //Project description
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.Project_Description(), getData("Project description")))
        {
            error = "Failed to enter the Project description field: " + getData("Project description");
            return false;
        }
        narrator.stepPassedWithScreenShot("Project description: " + getData("Project description"));

        //Theme DropDown
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Theme_Dropdown()))
        {
            error = "Failed to wait for the 'Theme' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Theme_Dropdown()))
        {
            error = "Failed to click the 'Theme' dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Theme_Option(getData("Theme"))))
        {
            error = "Failed to wait for the 'Theme Select'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Theme_Option(getData("Theme"))))
        {
            error = "Failed to click the 'Theme Select'.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Theme Option Selected from Dropdown Options :" + getData("Theme"));

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Theme_Dropdown()))
        {
            error = "Failed to wait for the 'Theme' dropdown.";
            return false;
        }

        if (getData("Theme").equalsIgnoreCase("Energy and Carbon"))
        {
            //Function
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Function_dropdown()))
            {
                error = "Failed to wait for 'Function' dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Function_dropdown()))
            {
                error = "Failed to click on 'Function' dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Function type search";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.TypeSearch2(), getData("Function")))
            {
                error = "Failed to enter  Function: " + getData("Function");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Function"))))
            {
                error = "Failed to wait for Function drop down option : " + getData("Function");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Text2(getData("Function"))))
            {
                error = "Failed to click Function drop down option : " + getData("Function");
                return false;
            }

            narrator.stepPassedWithScreenShot("Function: " + getData("Function"));

        }

        //Project Category
//        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.projectCategoryDD()))
//        {
//            error = "Failed to wait for 'Project category' dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.projectCategoryDD()))
//        {
//            error = "Failed to click on 'Project category' dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.text_Search(getData("Project category"))))
//        {
//            error = "Failed to wait for Project category: " + getData("Project category");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Project category"))))
//        {
//            error = "Failed to wait for Project category drop down option : " + getData("Project category");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Text2(getData("Project category"))))
//        {
//            error = "Failed to click Project category drop down option : " + getData("Project category");
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Project category: " + getData("Project category"));
//
//        //Project type
//        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.projectTypeDD()))
//        {
//            error = "Failed to wait for 'Project type' dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.projectTypeDD()))
//        {
//            error = "Failed to click on 'Project type' dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.text_Search(getData("Project type"))))
//        {
//            error = "Failed to wait for Project type: " + getData("Project type");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Project type"))))
//        {
//            error = "Failed to wait for Project type drop down option : " + getData("Project type");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Text2(getData("Project type"))))
//        {
//            error = "Failed to click Project type drop down option : " + getData("Project type");
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Project type: " + getData("Project type"));
        //Planned completion date
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.PlannedCompletionDate(), endDate))
        {
            error = "Failed to enter the planned completion date";
            return false;
        }
        narrator.stepPassedWithScreenShot("Planned completion date: " + endDate);

        //Objectives and proposed activities
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.ObjectivesAndProposedActivities(), getData("Objectives and proposed activities")))
        {
            error = "Failed to enter the Objectives and proposed activities";
            return false;
        }
        narrator.stepPassedWithScreenShot("Objectives and proposed activities: " + getData("Objectives and proposed activities"));

        //Due diligence notes
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.DueDiligenceNotes(), getData("Due diligence notes")))
        {
            error = "Failed to enter the Due diligence notes";
            return false;
        }
        narrator.stepPassedWithScreenShot("Due diligence end date: " + getData("Due diligence notes"));

        //Project originator
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ProjectOriginator_dropdown()))
        {
            error = "Failed to wait for 'Project originator' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.ProjectOriginator_dropdown()))
        {
            error = "Failed to click on 'Project originator' dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Project originator: " + getData("Project originator");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.TypeSearch(), (getData("Project originator"))))
        {
            error = "Failed to click the Project originator: " + getData("Project originator");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Project_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Project originator"))))
        {
            error = "Failed to wait for Project originator option : " + getData("Project originator");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Text2(getData("Project originator"))))
        {
            error = "Failed to click Project originator option : " + getData("Project originator");
            return false;
        }

        narrator.stepPassedWithScreenShot("Project originator: " + getData("Project originator"));

        if (getData("Supporting Documents").equalsIgnoreCase("Yes"))
        {
            if (!Supporting_Documents())
            {
                error = "Failed due to upload Supporting Documents";
                return false;
            }
        }

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.SaveBtn()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.SaveBtn()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Project_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = Project_Management_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Capture a Relationship Based Safety record for SLP Observation record saved");

        return true;
    }

    public boolean Supporting_Documents()
    {

        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to wait for Supporting Documents'";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to click on Supporting Documents";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Supporting Documents Tab");

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.linkADoc_button()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.linkADoc_button()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.urlInput_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.urlInput_TextAreaxpath(), getData("Document Link")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.tile_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.tile_TextAreaxpath(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Project_Management_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
//        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.SaveBtn()))
//        {
//            error = "Failed to wait for Save and Continue.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.SaveBtn()))
//        {
//            error = "Failed to click Save and Continue.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");
//        //Check if the record has been Saved
//        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.recordSaved_popup()))
//        {
//            error = "Failed to wait for 'Record Saved' popup.";
//            return false;
//        }
//        String saved = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.recordSaved_popup());
//
//        if (saved.equals("Record saved"))
//        {
//            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//
//        } else
//        {
//            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.failed()))
//            {
//                error = "Failed to wait for error message.";
//                return false;
//            }
//
//            String failed = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.failed());
//
//            if (failed.equals("ERROR: Record could not be saved"))
//            {
//                error = "Failed to save record.";
//                return false;
//            }
//        }
//
//        //Getting the action No
//        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.getActionRecord());
//        String[] record = acionRecord.split(" ");
//        Project_Management_PageObjects.setRecord_Number(record[2]);
//        String record_ = Project_Management_PageObjects.getRecord_Number();
//        narrator.stepPassed("Record number :" + acionRecord);
//        narrator.stepPassedWithScreenShot("Record saved");
        return true;

    }
}
