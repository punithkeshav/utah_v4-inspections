/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.BaselineDataCollection_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.RelationshipBasedSafety_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.BaselineDataCollection_PageObjects.BaselineDataCollection_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_PageObject.Project_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR8-Capture Actions Main Scenario",
        createNewBrowserInstance = false
)

public class FR8_CaptureActions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR8_CaptureActions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CaptureActions())
        {
            return narrator.testFailed("Capture Actions Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Capture Actions");
    }

    //Enter data
    public boolean CaptureActions()
    {
        
           if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ActionsTab()))
        {
            error = "Failed to wait for Actions tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.ActionsTab()))
        {
            error = "Failed to click on Actions tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Actions tab");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Actions_Add()))
        {
            error = "Failed to wait for  Actions  Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Actions_Add()))
        {
            error = "Failed to click on Safety  Actions Add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click  Actions Add button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Actions_processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Actions_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        //Type of action
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to wait for Type of action.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to click Type of action";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Action description text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.TypeSearch2(), getData("Type of action")))

        {
            error = "Failed to enter Type of action :" + getData("Type of action");
            return false;
        }

         if (!SeleniumDriverInstance.pressEnter_2(Project_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        //
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Project_Management_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to click Project drop down option : " + getData("Type of action");
            return false;
        }
        narrator.stepPassedWithScreenShot("Type of action :" + getData("Type of action"));
        
        
        ///Action Description

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ActionDescription()))
        {
            error = "Failed to wait for Action description text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.ActionDescription(), getData("Action description")))

        {
            error = "Failed to enter Action description :" + getData("Action description");
            return false;
        }

        //Entity
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Entity_Dropdown()))
        {
            error = "Failed to wait for Entity dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Entity_Dropdown()))
        {
            error = "Failed to click Entity dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Business Unit"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to wait to expand Entity";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to expand Entity";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Entity:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity drop down :" + getData("Business unit 1");
            return false;
        }
        
//          if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.businessUnitOption1(getData("Business unit 2"))))
//        {
//            error = "Failed to wait for Entity:" + getData("Business unit 2");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.businessUnitOption1(getData("Business unit 2"))))
//        {
//            error = "Failed to click Entity drop down :" + getData("Business unit 2");
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Entity option  :" + getData("Business unit option"));
        
        
        
        //Responsible person
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ResponsiblePerson_dropdown()))
        {
            error = "Failed to wait for Responsible person dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.ResponsiblePerson_dropdown()))
        {
            error = "Failed to click Responsible person dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible person text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.TypeSearch(), getData("Responsible person")))
        {
            error = "Failed to enter Responsible person option :" + getData("Responsible person");
            return false;
        }

         if (!SeleniumDriverInstance.pressEnter_2(BaselineDataCollection_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to wait forResponsible person options.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person :" + getData("Responsible person");
            return false;
        }

        narrator.stepPassedWithScreenShot("Responsible person :" + getData("Responsible person"));
        
        
        
        
        
        //Action due date

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.ActionDueDate()))
        {
            error = "Failed to wait for Action due date input field.";
            return false;

        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObjects.ActionDueDate(), startDate))
        {
            error = "Failed to enter Action due date :" + startDate;
            return false;
        }

        narrator.stepPassedWithScreenShot("Action due date :" + startDate);
        
        

       
        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.Actions_save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObjects.Actions_save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.recordSaved_popup2());

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.recordSaved_popup2()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Project_Management_PageObjects.setRecord_Number(record[2]);
        String record_ = BaselineDataCollection_PageObjects.getRecord_Number();
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}
