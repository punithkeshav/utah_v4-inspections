/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.BaselineDataCollection_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.RelationshipBasedSafety_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.BaselineDataCollection_PageObjects.BaselineDataCollection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR3-Capture Safety Leadership Actions - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_CaptureSafetyLeadershipActions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR3_CaptureSafetyLeadershipActions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CaptureSafetyLeadershipActions())
        {
            return narrator.testFailed("Capture Safety Leadership Actions Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Safety Leadership Actions");
    }

    //Enter data
    public boolean CaptureSafetyLeadershipActions()
    {
        pause(3000);

        //StartButton_2
        //Start button
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.StartButton()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.StartButton_2()))
            {
                error = "Failed to wait for Start button.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.StartButton()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.StartButton_2()))
            {
                error = "Failed to click on Start button.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully click Start button.");

        //Next button 
        //NextButton_2
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.NextButton()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.NextButton_2()))
            {
                error = "Failed to wait for Next button";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.NextButton()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.NextButton_2()))
            {
                error = "Failed to click Next button";
                return false;
            }
        }

        //Answer 1
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.AnswerDropDown()))
        {
            error = "Failed to wait for Answer 1.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.AnswerDropDown()))
        {
            error = "Failed to click Answer 1";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Action description text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.TypeSearch2(), getData("Answer")))

        {
            error = "Failed to enter Answer 1:" + getData("Answer");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(BaselineDataCollection_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Text2(getData("Answer"))))
        {
            error = "Failed to wait for Answer drop down option : " + getData("Answer");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(BaselineDataCollection_PageObjects.Text2(getData("Answer"))))
        {
            error = "Failed to click Project drop down option : " + getData("Answer");
            return false;
        }
        narrator.stepPassedWithScreenShot("Have you read all the requirements before conducting this survey? :" + getData("Answer"));

        if (getData("Execute Main scenario").equalsIgnoreCase("True"))
        {
            //Save current section
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.SaveCurrentSectionButton()))
            {
                error = "Failed to wait for Save current section button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.SaveCurrentSectionButton()))
            {
                error = "Failed to click Save current section button";
                return false;
            }

            pause(20000);
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.CloseCurrentModule()))
            {
                error = "Failed to wait for the cross to close current module";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.CloseCurrentModule()))
            {
                error = "Failed to close current module";
                return false;
            }
            narrator.stepPassedWithScreenShot("Closed the survey using the cross in the top right-hand corner");

        }

        if (getData("Execute Optional scenario 1").equalsIgnoreCase("True"))
        {
            //Next button 
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.NextButton()))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.NextButton_2()))
                {
                    error = "Failed to wait for Next button";
                    return false;
                }
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.NextButton()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.NextButton_2()))
                {
                    error = "Failed to click Next button";
                    return false;
                }
            }
            //Answer 2
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.AnswerDropDown2()))
            {
                error = "Failed to wait for Answer 2";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.AnswerDropDown2()))
            {
                error = "Failed to click Answer 2";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Action 2  text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.TypeSearch2(), getData("Answer")))

            {
                error = "Failed to enter Answer 2:" + getData("Answer");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(BaselineDataCollection_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Text2(getData("Answer"))))
            {
                error = "Failed to wait for Answer 2 drop down option : " + getData("Answer");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(BaselineDataCollection_PageObjects.Text2(getData("Answer"))))
            {
                error = "Failed to click Answer 2 drop down option : " + getData("Answer");
                return false;
            }
            narrator.stepPassedWithScreenShot("Answer 2 :" + getData("Answer"));

            //PREVIOUS
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.PreviousButton()))
            {
                error = "Failed to wait for Previous button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.PreviousButton()))
            {
                error = "Failed to click Previous button";
                return false;
            }
            narrator.stepPassedWithScreenShot("Previous button clicked :");

            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.PreviousButton()))
            {
                error = "Failed to wait for Previous button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.PreviousButton()))
            {
                error = "Failed to click Previous button";
                return false;
            }
            narrator.stepPassedWithScreenShot("Previous button clicked :");

        }

        if (getData("Execute Optional scenario 3").equalsIgnoreCase("True"))
        {

            narrator.stepPassedWithScreenShot("CONTINUE button");
            //CONTINUE
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.ContinueButton()))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.ContinueButton_2()))
                {
                    error = "Failed to wait for CONTINUE button";
                    return false;
                }
            }

            pause(3000);
            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.ContinueButton()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.ContinueButton_2()))
                {
                    error = "Failed to click CONTINUE button";
                    return false;
                }
            }

            //Next button 
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.NextButton()))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.NextButton_2()))
                {
                    error = "Failed to wait for Next button";
                    return false;
                }
            }

        }

        if (getData("Optional scenario 2").equalsIgnoreCase("True"))
        {
            //Next button 
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.NextButton()))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.NextButton_2()))
                {
                    error = "Failed to wait for Next button";
                    return false;
                }
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.NextButton()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.NextButton_2()))
                {
                    error = "Failed to click Next button";
                    return false;
                }
            }

            //Answer 2
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.AnswerDropDown2()))
            {
                error = "Failed to wait for Answer 2";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.AnswerDropDown2()))
            {
                error = "Failed to click Answer 2";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Action 2  text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.TypeSearch2(), getData("Answer")))

            {
                error = "Failed to enter Answer 2:" + getData("Answer");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(BaselineDataCollection_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Text2(getData("Answer"))))
            {
                error = "Failed to wait for Answer 2 drop down option : " + getData("Answer");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(BaselineDataCollection_PageObjects.Text2(getData("Answer"))))
            {
                error = "Failed to click Answer 2 drop down option : " + getData("Answer");
                return false;
            }
            narrator.stepPassedWithScreenShot("Answer 2 :" + getData("Answer"));

            //Finish
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.FinishButton()))
            {
                error = "Failed to wait for Finish button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.FinishButton()))
            {
                error = "Failed to click Finish button";
                return false;
            }

            narrator.stepPassedWithScreenShot("Finish button clicked");

            pause(10000);

            narrator.stepPassedWithScreenShot("Capture the Survey record saved");

        }

        return true;
    }

}
