/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.BaselineDataCollection_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.RelationshipBasedSafety_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.BaselineDataCollection_PageObjects.BaselineDataCollection_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.RelationshipBasedSafety_PageObjects.RelationshipBasedSafety_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Baseline Data Collection Survey - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_CaptureBaselineDataCollectionSurvey_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR2_CaptureBaselineDataCollectionSurvey_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CaptureARelationshipBasedSafetyRecord())
        {
            return narrator.testFailed("Capture a Relationship Based Safety record Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture a Relationship Based Safety record");
    }

    //Enter data
    public boolean CaptureARelationshipBasedSafetyRecord()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.SurveyTab()))
        {
            error = "Failed to wait for Survey tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.SurveyTab()))
        {
            error = "Failed to click on Survey tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Survey tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Survey_Add()))
        {
            error = "Failed to wait for Survey Add button .";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.Survey_Add()))
        {
            error = "Failed to click on Survey Add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Survey Add button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Survey_processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.Survey_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        //Survey name
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.SurveyNameDropDown()))
        {
            error = "Failed to wait for Survey name.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.SurveyNameDropDown()))
        {
            error = "Failed to click Survey name";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.TypeSearch2(), getData("Survey name")))

        {
            error = "Failed to enter Survey name :" + getData("Survey name");
            return false;
        }

         if (!SeleniumDriverInstance.pressEnter_2(BaselineDataCollection_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Text2(getData("Survey name"))))
        {
            error = "Failed to wait for Survey name drop down option : " + getData("Survey name");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(BaselineDataCollection_PageObjects.Text2(getData("Survey name"))))
        {
            error = "Failed to click Survey name drop down option : " + getData("Survey name");
            return false;
        }
        narrator.stepPassedWithScreenShot("Survey name");

        //Survey conducted by
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.SurveyConducteByDropDown()))
        {
            error = "Failed to wait for Survey conducted by.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.SurveyConducteByDropDown()))
        {
            error = "Failed to click Survey conducted by";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.TypeSearch(), getData("Survey conducted by")))

        {
            error = "Failed to enter Survey conducted by :" + getData("Survey conducted by");
            return false;
        }

         if (!SeleniumDriverInstance.pressEnter_2(BaselineDataCollection_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Text2(getData("Survey conducted by"))))
        {
            error = "Failed to wait for Status drop down option : " + getData("Survey conducted by");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(BaselineDataCollection_PageObjects.Text2(getData("Survey conducted by"))))
        {
            error = "Failed to click Survey conducted by drop down option : " + getData("Survey conducted by");
            return false;
        }
        narrator.stepPassedWithScreenShot("Survey conducted by");

        //Geographic location
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.GeographicLocationDropDown()))
        {
            error = "Failed to wait for Geographic location.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.GeographicLocationDropDown()))
        {
            error = "Failed to click Geographic location";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.TypeSearch2(), getData("Geographic location")))

        {
            error = "Failed to enter Geographic location :" + getData("Geographic location");
            return false;
        }

         if (!SeleniumDriverInstance.pressEnter_2(BaselineDataCollection_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Text2(getData("Geographic location"))))
        {
            error = "Failed to wait for Geographic location drop down option : " + getData("Geographic location");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(BaselineDataCollection_PageObjects.Text2(getData("Geographic location"))))
        {
            error = "Failed to click Geographic location drop down option : " + getData("Geographic location");
            return false;
        }
        narrator.stepPassedWithScreenShot("Geographic location");

        if (getData("Exclude from results Execute").equalsIgnoreCase("True"))
        {
            // Exclude from results
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.ExcludeFromResults()))
            {
                error = "Failed to wait for Exclude from results.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.ExcludeFromResults()))
            {
                error = "Failed to click Exclude from results";
                return false;
            }

            //Reason for excluding from results
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.ReasonForExcludingFromResults()))
            {
                error = "Failed to wait for Reason for excluding from results text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.ReasonForExcludingFromResults(), getData("Reason for excluding from results")))

            {
                error = "Failed to enter Reason for excluding from results :" + getData("Reason for excluding from results");
                return false;
            }

        }

        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Survey_save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.Survey_save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(BaselineDataCollection_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(BaselineDataCollection_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(BaselineDataCollection_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        BaselineDataCollection_PageObjects.setRecord_Number(record[2]);
        String record_ = BaselineDataCollection_PageObjects.getRecord_Number();
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;

    }

}
