/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.BaselineDataCollection_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.RelationshipBasedSafety_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.BaselineDataCollection_PageObjects.BaselineDataCollection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Baseline Data Collection - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_CaptureBaselineDataCollection_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR1_CaptureBaselineDataCollection_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToRelationshipBasedSafety())
        {
            return narrator.testFailed("Capture Baseline Data Collection Failed Due To: " + error);
        }
        if (!CreateRelationshipBasedSafety())
        {
            return narrator.testFailed("Capture Baseline Data Collection Failed Due To: " + error);
        }

        if (getData("Upload Document").equalsIgnoreCase("True"))
        {

            if (!UploadDocument())
            {
                return narrator.testFailed("Failed to upload document: " + error);
            }
        }

        return narrator.finalizeTest("Successfully Capture Baseline Data Collection");
    }

    public boolean NavigateToRelationshipBasedSafety()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.BaselineDataCollectionTab()))
        {
            error = "Failed to wait for Baseline Data Collection tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.BaselineDataCollectionTab()))
        {
            error = "Failed to click on Baseline Data Collection tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Baseline Data Collection tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.BaselineDataCollection_Add()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.BaselineDataCollectionTab()))
            {
                error = "Failed to wait for 'Add' button.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.BaselineDataCollection_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    //Enter data
    public boolean CreateRelationshipBasedSafety()
    {

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.BaselineDataCollection_processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.BaselineDataCollection_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        //Site
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Site_Dropdown()))
        {
            error = "Failed to wait for Site dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.Site_Dropdown()))
        {
            error = "Failed to click Site dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Text2(getData("Business unit"))))
        {
            error = "Failed to wait for Site drop down option : " + getData("Business unit");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to wait to expand Site";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to expand Site";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Site:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Site Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Site:" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click Site Option drop down :" + getData("Business unit 2");
            return false;
        }

//             if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.businessUnitOption1(getData("Business unit 3"))))
//        {
//            error = "Failed to wait for Site:" + getData("Business unit 3");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.businessUnitOption1(getData("Business unit 3"))))
//        {
//            error = "Failed to click Site Option drop down :" + getData("Business unit 3");
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to wait for Site:" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to click Site Option drop down :" + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Site option  :" + getData("Business unit option"));

        //Title 
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Title()))
        {
            error = "Failed to wait for Title .";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.Title(), getData("Title Title")))
        {
            error = "Failed to enter Title :" + getData("Title Title");
            return false;
        }

        //Data collection start date
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.DataCollectionStartDate()))
        {
            error = "Failed to wait for Data collection start date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.DataCollectionStartDate(), startDate))
        {
            error = "Failed to enter Data collection start date : " + startDate;
            return false;
        }

        //Data collection end date
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.DataCollectionEndDate()))
        {
            error = "Failed to wait for Data collection end date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.DataCollectionEndDate(), endDate))
        {
            error = "Failed to enter Data collection end date : " + endDate;
            return false;
        }

        //Owner
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.OwnerDropDown()))
        {
            error = "Failed to wait for Owner.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.OwnerDropDown()))
        {
            error = "Failed to click Owner";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.TypeSearch(), getData("Owner")))

        {
            error = "Failed to enter Owner :" + getData("Owner");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(BaselineDataCollection_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Text2(getData("Owner"))))
        {
            error = "Failed to wait for Owner drop down option : " + getData("Owner");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(BaselineDataCollection_PageObjects.Text2(getData("Owner"))))
        {
            error = "Failed to click Owner drop down option : " + getData("Owner");
            return false;
        }
        narrator.stepPassedWithScreenShot("Owner");

        //Linked Stakeholder Groups / Communities
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.LinkedStakeholderGroupsCommunitiesDropDown()))
        {
            error = "Failed to wait for Linked Stakeholder Groups / Communities drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.LinkedStakeholderGroupsCommunitiesDropDown()))
        {
            error = "Failed to click Linked Stakeholder Groups / Communities drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Linked Stakeholder Groups / Communities text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.TypeSearch(), getData("Linked Stakeholder Groups / Communities")))

        {
            error = "Failed to enter Linked Stakeholder Groups / Communities option :" + getData("Linked Stakeholder Groups / Communities");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(BaselineDataCollection_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearchCheckBox(getData("Linked Stakeholder Groups / Communities"))))
        {
            error = "Failed to wait for Linked Stakeholder Groups / Communities option :" + getData("Linked Stakeholder Groups / Communities");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.TypeSearchCheckBox(getData("Linked Stakeholder Groups / Communities"))))
        {
            error = "Failed to select Linked Stakeholder Groups / Communities option :" + getData("Linked Stakeholder Groups / Communities");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.LinkedStakeholderGroupsCommunitiesDropDown()))
        {
            error = "Failed to click Linked Stakeholder Groups / Communities";
            return false;
        }

        narrator.stepPassedWithScreenShot("Linked Stakeholder Groups / Communities :" + getData("Linked Stakeholder Groups / Communities"));

        ///Applicable survey to be used
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.ApplicableSurveyToBeUsedDropDown()))
        {
            error = "Failed to wait for Applicable survey to be used drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.ApplicableSurveyToBeUsedDropDown()))
        {
            error = "Failed to click Applicable survey to be used drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Type text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.TypeSearch2(), getData("Applicable survey to be used")))

        {
            error = "Failed to enter Applicable survey to be used option :" + getData("Applicable survey to be used");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(BaselineDataCollection_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearchCheckBox(getData("Applicable survey to be used"))))
        {
            error = "Failed to wait for Applicable survey to be used option :" + getData("Applicable survey to be used");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.TypeSearchCheckBox(getData("Applicable survey to be used"))))
        {
            error = "Failed to select Applicable survey to be used option :" + getData("Applicable survey to be used");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.ApplicableSurveyToBeUsedDropDown()))
        {
            error = "Failed to click Applicable survey to be used";
            return false;
        }

        narrator.stepPassedWithScreenShot("Applicable survey to be used :" + getData("Applicable survey to be used"));

        //Team
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TeamDropDown()))
        {
            error = "Failed to wait for Team drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.TeamDropDown()))
        {
            error = "Failed to click Team drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Team text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.TypeSearch(), getData("Team")))

        {
            error = "Failed to enter Team option :" + getData("Team");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(BaselineDataCollection_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearchCheckBox(getData("Team"))))
        {
            error = "Failed to wait for Team option :" + getData("Team");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.TypeSearchCheckBox(getData("Team"))))
        {
            error = "Failed to select Team option :" + getData("Team");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.TeamDropDown()))
        {
            error = "Failed to click Team";
            return false;
        }

        narrator.stepPassedWithScreenShot("Team :" + getData("Team"));

        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.BaselineDataCollection_save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.BaselineDataCollection_save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(BaselineDataCollection_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(BaselineDataCollection_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(BaselineDataCollection_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        BaselineDataCollection_PageObjects.setRecord_Number(record[2]);
        String record_ = BaselineDataCollection_PageObjects.getRecord_Number();
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        if (getData("Execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.StatusDropDown()))
            {
                error = "Failed to wait for Status.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.StatusDropDown()))
            {
                error = "Failed to click Status";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for search text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.TypeSearch2(), getData("Status")))

            {
                error = "Failed to enter Status :" + getData("Status");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(BaselineDataCollection_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.Text2(getData("Status"))))
            {
                error = "Failed to wait for Status drop down option : " + getData("Status");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(BaselineDataCollection_PageObjects.Text2(getData("Status"))))
            {
                error = "Failed to click Status drop down option : " + getData("Status");
                return false;
            }
            narrator.stepPassedWithScreenShot("Status");

            //Save and Continue
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.BaselineDataCollection_save()))
            {
                error = "Failed to wait for Save and Continue.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.BaselineDataCollection_save()))
            {
                error = "Failed to click Save and Continue.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
            saved = SeleniumDriverInstance.retrieveTextByXpath(BaselineDataCollection_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(BaselineDataCollection_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            //Getting the action No
            acionRecord = SeleniumDriverInstance.retrieveTextByXpath(BaselineDataCollection_PageObjects.getActionRecord());
            record = acionRecord.split(" ");
            BaselineDataCollection_PageObjects.setRecord_Number(record[2]);
            record_ = BaselineDataCollection_PageObjects.getRecord_Number();
            narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        }

        return true;
    }

    public boolean UploadDocument()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to wait for Supporting Documents'";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to click on Supporting Documents";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Supporting Documents Tab");

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.linkADoc_button()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.linkADoc_button()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.urlInput_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.urlInput_TextAreaxpath(), getData("Document Link")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.tile_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(BaselineDataCollection_PageObjects.tile_TextAreaxpath(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(BaselineDataCollection_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.BaselineDataCollection_save()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(BaselineDataCollection_PageObjects.BaselineDataCollection_save()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(BaselineDataCollection_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(BaselineDataCollection_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(BaselineDataCollection_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(BaselineDataCollection_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        BaselineDataCollection_PageObjects.setRecord_Number(record[2]);
        String record_ = BaselineDataCollection_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;

    }

}
