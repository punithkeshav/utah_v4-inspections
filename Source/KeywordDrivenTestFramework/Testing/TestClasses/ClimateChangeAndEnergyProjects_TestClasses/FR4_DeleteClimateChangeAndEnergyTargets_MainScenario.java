/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyProjects_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyTargets_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyTargets_PageObjects.ClimateChangeAndEnergyTargets_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR4-Delete Climate Change and Energy Targets",
        createNewBrowserInstance = false
)

public class FR4_DeleteClimateChangeAndEnergyTargets_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR4_DeleteClimateChangeAndEnergyTargets_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!deleteStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully deleted Stakeholder record ");
    }

    public boolean deleteStakeholderIndividual()
    {

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

     

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        

        //contains
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.ContainsTextBox()))
        {
            error = "Failed to wait for contains";
            return false;
        }


        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);
        pause(5000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.deleteRecordButton()))
        {
            error = "Failed to wait for delete button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.deleteRecordButton()))
        {
            error = "Failed to click delete Button.";
            return false;

        }
        pause(3500);

        narrator.stepPassedWithScreenShot("Successfully clicked delete button");

       if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);

        return true;
    }

}
