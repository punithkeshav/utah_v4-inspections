/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.RelationshipBasedSafety_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.RelationshipBasedSafety_PageObjects.RelationshipBasedSafety_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture a Relationship Based Safety record for VFL Observation - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_CaptureARelationshipBasedSafetyRecord_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR2_CaptureARelationshipBasedSafetyRecord_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CaptureARelationshipBasedSafetyRecord())
        {
            return narrator.testFailed("Capture a Relationship Based Safety record Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture a Relationship Based Safety record");
    }

    //Enter data
    public boolean CaptureARelationshipBasedSafetyRecord()
    {
//StartButton2
        //Start button
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.StartButton2()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.StartButton()))
            {
                error = "Failed to wait for Start button.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.StartButton2()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.StartButton()))
            {
                error = "Failed to click on Start button.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully click Start button.");

        //Answer 1
//        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.AnswerDropDown()))
//        {
//            error = "Failed to wait for Answer 1.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.AnswerDropDown()))
//        {
//            error = "Failed to click Answer 1";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for Action description text box";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.TypeSearch2(), getData("Answer")))
//
//        {
//            error = "Failed to enter Answer 1:" + getData("Answer");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.pressEnter_2(RelationshipBasedSafety_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to press enter";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.Text2(getData("Answer"))))
//        {
//            error = "Failed to wait for Answer drop down option : " + getData("Answer");
//            return false;
//        }
//        if (!SeleniumDriverInstance.doubleClickElementbyXpath(RelationshipBasedSafety_PageObjects.Text2(getData("Answer"))))
//        {
//            error = "Failed to click Project drop down option : " + getData("Answer");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Answer :" + getData("Answer"));
//
//        pause(4000);
//        //Next button 
//        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.NextButton()))
//        {
//            error = "Failed to wait for Next button";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.NextButton()))
//        {
//            error = "Failed to click Next button";
//            return false;
//        }
//
//        //Answer 2
//        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.AnswerDropDown2()))
//        {
//            error = "Failed to wait for Answer 2";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.AnswerDropDown2()))
//        {
//            error = "Failed to click Answer 2";
//            return false;
//        }
//
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.Comments()))
        {
            error = "Failed to wait for Comments text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.Comments(), getData("Comments")))

        {
            error = "Failed to enter Comments :" + getData("Comments");
            return false;
        }
//
//        if (!SeleniumDriverInstance.pressEnter_2(RelationshipBasedSafety_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to press enter";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.Text2(getData("Answer"))))
//        {
//            error = "Failed to wait for Answer 2 drop down option : " + getData("Answer");
//            return false;
//        }
//        if (!SeleniumDriverInstance.doubleClickElementbyXpath(RelationshipBasedSafety_PageObjects.Text2(getData("Answer"))))
//        {
//            error = "Failed to click Answer 2 drop down option : " + getData("Answer");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Answer 2 :" + getData("Answer"));

        //Finish
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.FinishButton()))
        {
            error = "Failed to wait for Finish button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.FinishButton()))
        {
            error = "Failed to click Finish button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Finish button clicked");

        pause(7000);
        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.RelationshipBasedSafety_save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Capture a Relationship Based Safety record for VFL Observation record saved");
        return true;
    }

}
