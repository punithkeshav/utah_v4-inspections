/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.RelationshipBasedSafety_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.RelationshipBasedSafety_PageObjects.RelationshipBasedSafety_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture a Relationship Based Safety record for SLP Observation - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_A_RelationshipBasedSafety_Record_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR1_Capture_A_RelationshipBasedSafety_Record_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToRelationshipBasedSafety())
        {
            return narrator.testFailed("Capture a Relationship Based Safety record for SLP Observation Failed Due To: " + error);
        }
        if (!CreateRelationshipBasedSafety())
        {
            return narrator.testFailed("Capture a Relationship Based Safety record for SLP Observation Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual Record ");
    }

    public boolean NavigateToRelationshipBasedSafety()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.RelationshipBasedSafetyTab()))
        {
            error = "Failed to wait for Relationship Based Safety tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.RelationshipBasedSafetyTab()))
        {
            error = "Failed to click on Relationship Based Safety tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Relationship Based Safety tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.RelationshipBased_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.RelationshipBased_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    //Enter data
    public boolean CreateRelationshipBasedSafety()
    {

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.RelationshipBased_processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.RelationshipBased_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        //New Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.BusinessUnit_Dropdown()))
        {
            error = "Failed to wait for 'Business Unit' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.BusinessUnit_Dropdown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for type search";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.TypeSearch2(), getData("Business unit option")))

        {
            error = "Failed to enter business units :" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(RelationshipBasedSafety_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Business Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Business Option drop down :" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Business Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Business Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Business Option drop down :" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Business Option drop down:" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        if (getData("Link to project").equalsIgnoreCase("True"))
        {
            //Drop click escape
            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.LinkToProject()))
            {
                error = "Failed to wait for  Link to project";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.LinkToProject()))
            {
                error = "Failed to click  Link to project";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully clicked Link to project");

            //Project
            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.ProjectDropDown()))
            {
                error = "Failed to wait for Project.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.ProjectDropDown()))
            {
                error = "Failed to click Project";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for search text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.TypeSearch2(), getData("Project")))

            {
                error = "Failed to enter Project :" + getData("Project");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(RelationshipBasedSafety_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.Text(getData("Project"))))
            {
                error = "Failed to wait for Project:" + getData("Project");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.Text(getData("Project"))))
            {
                error = "Failed to click Project Option drop down :" + getData("Project");
                return false;
            }

            narrator.stepPassedWithScreenShot("Project:" + getData("Project"));

        }

        //Type of observation
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.TypeOfObservation_dropdown()))
        {
            error = "Failed to wait for Type of observation dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.TypeOfObservation_dropdown()))
        {
            error = "Failed to click Type of observation dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Type of observation 1"))))
        {
            error = "Failed to wait for Type of observation drop down option : " + getData("Type of observation 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.businessUnitOption1(getData("Type of observation 1"))))
        {
            error = "Failed to click Type of observation drop down option : " + getData("Type of observation 1");
            return false;
        }
        
            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.TypeOfObservation(getData("Type of observation 2"))))
            {
                error = "Failed to wait for Type of observation 2 drop down option : " + getData("Type of observation 2");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.TypeOfObservation(getData("Type of observation 2"))))
            {
                error = "Failed to click Type of observation 2 drop down option : " + getData("Type of observation 2");
                return false;
            }
        narrator.stepPassedWithScreenShot("Type of observation :" + getData("Type of observation 1"));

        //Date observed
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.DateObserved()))
        {
            error = "Failed to wait for Date observed input field.";
            return false;

        }

        if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.DateObserved(), startDate))
        {
            error = "Failed to enter Date observed :" + startDate;
            return false;
        }

        narrator.stepPassedWithScreenShot("Date observed :" + startDate);

        //Time observed
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.TimeObserved()))
        {
            error = "Failed to wait for Time observed input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.TimeObserved(), getData("Time observed")))
        {
            error = "Failed to enter Time observed";
            return false;
        }
        narrator.stepPassedWithScreenShot("Time observed :" + getData("Time observed"));

        //Observation
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.Observation()))
        {
            error = "Failed to wait for Observation input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.Observation(), getData("Observation")))
        {
            error = "Failed to enter Observation";
            return false;
        }

        narrator.stepPassedWithScreenShot("Observation :" + getData("Observation"));

        //Person observing
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.PersonObserving_dropdown()))
        {
            error = "Failed to wait for Person observing dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.PersonObserving_dropdown()))
        {
            error = "Failed to click Person observing dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Person observing text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.TypeSearch(), getData("Person observing")))
        {
            error = "Failed to enter Person observing option :" + getData("Person observing");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(RelationshipBasedSafety_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.Text(getData("Person observing"))))
        {
            error = "Failed to wait for Person observing options.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.Text(getData("Person observing"))))
        {
            error = "Failed to click Person observing :" + getData("Person observing");
            return false;
        }

        narrator.stepPassedWithScreenShot("Person observing :" + getData("Person observing"));

        //Observed Group / Individual
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.ObservedGroupIndividual_DropDown()))
        {
            error = "Failed to wait for Observed Group / Individual";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.ObservedGroupIndividual_DropDown()))
        {
            error = "Failed to click Observed Group / Individual";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.TypeSearch2(), getData("Observed Group / Individual")))
        {
            error = "Failed to enter Observed Group / Individual option :" + getData("Observed Group / Individual");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(RelationshipBasedSafety_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.Text3(getData("Observed Group / Individual"))))
        {
            error = "Failed to wait for Observed Group / Individual drop down option : " + getData("Observed Group / Individual");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.Text3(getData("Observed Group / Individual"))))
        {
            error = "Failed to click Observed Group / Individual drop down option : " + getData("Observed Group / Individual");
            return false;
        }

//        if (getData("Observed Group / Individual").equalsIgnoreCase("Individual"))
//        {
//            //Who was observed
//            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.WhoWasObserved_DropDown()))
//        {
//            error = "Failed to wait for Who was observed";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.WhoWasObserved_DropDown()))
//        {
//            error = "Failed to click Who was observedl";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.TypeSearch()))
//        {
//            error = "Failed to wait for search text box";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.TypeSearch(), getData("Who was observed")))
//        {
//            error = "Failed to enter Observed Group / Individual option :" + getData("Who was observed");
//            return false;
//        }
//
//         if (!SeleniumDriverInstance.pressEnter_2(RelationshipBasedSafety_PageObjects.TypeSearch()))
//        {
//            error = "Failed to press enter";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.Text2(getData("Who was observed"))))
//        {
//            error = "Failed to wait for Who was observed drop down option : " + getData("Who was observed");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.Text2(getData("Who was observed"))))
//        {
//            error = "Failed to click Who was observed drop down option : " + getData("Who was observed");
//            return false;
//        }
//
//        } else
//        {
//
//            //Who was observed
//            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.WhoWasObserved()))
//            {
//                error = "Failed to wait for Who was observed";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.WhoWasObserved(), getData("Who was observed")))
//            {
//                error = "Failed to enter Who was observed option :" + getData("Who was observed");
//                return false;
//            }
//
//        }
        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.RelationshipBasedSafety_save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.RelationshipBasedSafety_save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(RelationshipBasedSafety_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(RelationshipBasedSafety_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(RelationshipBasedSafety_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        RelationshipBasedSafety_PageObjects.setRecord_Number(record[2]);
        String record_ = RelationshipBasedSafety_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Capture a Relationship Based Safety record for SLP Observation record saved");
        return true;
    }

}
