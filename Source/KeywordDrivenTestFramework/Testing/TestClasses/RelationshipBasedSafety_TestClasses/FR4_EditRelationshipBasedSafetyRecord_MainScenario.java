/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.RelationshipBasedSafety_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.RelationshipBasedSafety_PageObjects.RelationshipBasedSafety_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR4-Edit Relationship Based Safety Record - Main scenario",
        createNewBrowserInstance = false
)

public class FR4_EditRelationshipBasedSafetyRecord_MainScenario extends BaseClass
{
    
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR4_EditRelationshipBasedSafetyRecord_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 2500);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }
    
    public TestResult executeTest()
    {
        if (!navigateToStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Stakeholder Individual updates saved");
    }
    
    public boolean navigateToStakeholderIndividual()
    {
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(RelationshipBasedSafety_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.SearchOption()))
        {
            error = "Failed to wait for search options";
            return false;
        }
        
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.SearchOption()))
        {
            error = "Failed to click search options";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);
        
        pause(5000);

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.RelationshipBased_processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.RelationshipBased_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }
        
        if (getData("Observation execute").equalsIgnoreCase("True"))
        {

            //Observation
            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.Observation()))
            {
                error = "Failed to wait for Observation input field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.Observation(), getData("Observation")))
            {
                error = "Failed to enter Observation";
                return false;
            }
            
        }
        
          if (getData("Status execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.StatusDropDown()))
            {
                error = "Failed to wait for Status.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.StatusDropDown()))
            {
                error = "Failed to click Status";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for search text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(RelationshipBasedSafety_PageObjects.TypeSearch2(), getData("Status")))

            {
                error = "Failed to enter Status :" + getData("Status");
                return false;
            }

              if (!SeleniumDriverInstance.pressEnter_2(RelationshipBasedSafety_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

            //
            if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.Text3(getData("Status"))))
            {
                error = "Failed to wait for Status drop down option : " + getData("Status");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(RelationshipBasedSafety_PageObjects.Text3(getData("Status"))))
            {
                error = "Failed to click Status drop down option : " + getData("Status");
                return false;
            }
            narrator.stepPassedWithScreenShot("Status :" + getData("Status"));
            
            
        }
        

        
        narrator.stepPassedWithScreenShot("Observation :" + getData("Observation"));

        //Save to continue
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.RelationshipBasedSafety_save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(RelationshipBasedSafety_PageObjects.RelationshipBasedSafety_save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(RelationshipBasedSafety_PageObjects.recordSaved_popup1()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(RelationshipBasedSafety_PageObjects.recordSaved_popup1());
        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            
            error = "Failed to save record.";
            return false;            
        }
        
        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(RelationshipBasedSafety_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        RelationshipBasedSafety_PageObjects.setRecord_Number(record[2]);
        String record_ = RelationshipBasedSafety_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        
        return true;
    }
    
}
