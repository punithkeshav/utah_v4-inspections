/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyTargets_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyTargets_PageObjects.ClimateChangeAndEnergyTargets_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR3-Edit Climate Change and Energy Targets - Main scenario",
        createNewBrowserInstance = false
)

public class FR3_EditClimateChangeAndEnergyTargets_MainScenario extends BaseClass
{
    
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR3_EditClimateChangeAndEnergyTargets_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 2500);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }
    
    public TestResult executeTest()
    {
        if (!navigateToStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Stakeholder Individual updates saved");
    }
    
    public boolean navigateToStakeholderIndividual()
    {
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        pause(3000);
        
        
        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.ContainsTextBox()))
        {
            error = "Failed to wait for contains";
            return false;
        }
        
     

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.SearchButton()))
        {
            error = "Failed wait for Search Button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);
        
        pause(5000);

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }
        
        
         //Target start date month
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TargetStartDateMonthDropDown()))
            {
                error = "Failed to wait for Target start date month drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.TargetStartDateMonthDropDown()))
            {
                error = "Failed to click Target start month date drop down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for search text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2(), getData("Target start date month")))

            {
                error = "Failed to enter Target start date month :" + getData("Target start date month");
                return false;
            }

                   if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

           
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Target start date month"))))
            {
                error = "Failed to wait for Target start date month drop down option : " + getData("Target start date month");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Target start date month"))))
            {
                error = "Failed to click Target start date month drop down option : " + getData("Target start date month");
                return false;
            }
            narrator.stepPassedWithScreenShot("Target start date month");
            
            
            
             //Target start date year
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TargetStartDateYearDropDown()))
            {
                error = "Failed to wait for Target start date year drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.TargetStartDateYearDropDown()))
            {
                error = "Failed to click Target start year date drop down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for search text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2(), getData("Target start date year")))

            {
                error = "Failed to enter Target start date year :" + getData("Target start date year");
                return false;
            }

                   if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

           
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Target start date year"))))
            {
                error = "Failed to wait for Target start date year drop down option : " + getData("Target start date year");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Target start date year"))))
            {
                error = "Failed to click Target start date year drop down option : " + getData("Target start date year");
                return false;
            }
            narrator.stepPassedWithScreenShot("Target start date year");
            

        

        

        //Save to continue
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Button_Save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Button_Save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.recordSaved_popup1()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.recordSaved_popup1());
        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            
            error = "Failed to save record.";
            return false;            
        }
        
        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ClimateChangeAndEnergyTargets_PageObjects.setRecord_Number(record[2]);
        String record_ = ClimateChangeAndEnergyTargets_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        
        return true;
    }
    
}
