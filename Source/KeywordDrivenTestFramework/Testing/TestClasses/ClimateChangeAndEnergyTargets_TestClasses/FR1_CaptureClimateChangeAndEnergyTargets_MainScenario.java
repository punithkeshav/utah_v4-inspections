/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyTargets_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyTargets_PageObjects.ClimateChangeAndEnergyTargets_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Climate Change and Energy Targets - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_CaptureClimateChangeAndEnergyTargets_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR1_CaptureClimateChangeAndEnergyTargets_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!ClimateChangeAndEnergyTargets())
        {
            return narrator.testFailed("Climate Change and Energy Targets Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Climate Change and Energy Targets");
    }

    public boolean ClimateChangeAndEnergyTargets()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.ECO2ManTab()))
        {
            error = "Failed to wait for ECO2Man tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.ECO2ManTab()))
        {
            error = "Failed to click on ECO2Man tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to ECO2Man tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.ClimateChangeAndEnergyTargetsTab()))
        {
            error = "Failed to wait for Climate Change and Energy Targets tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.ClimateChangeAndEnergyTargetsTab()))
        {
            error = "Failed to click on Climate Change and Energy Targets tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated toClimate Change and Energy Targets tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.ClimateChangeAndEnergyTargets_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.ClimateChangeAndEnergyTargets_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        //New Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.BusinessUnit_Dropdown()))
        {
            error = "Failed to wait for 'Business Unit' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.BusinessUnit_Dropdown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }
        
          if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for type search";
            return false;
        }
        

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2(), getData("Business unit option")))

        {
            error = "Failed to enter Applicable business units :" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Target start date month
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TargetStartDateMonthDropDown()))
        {
            error = "Failed to wait for Target start date month drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.TargetStartDateMonthDropDown()))
        {
            error = "Failed to click Target start month date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2(), getData("Target start date month")))

        {
            error = "Failed to enter Target start date month :" + getData("Target start date month");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Target start date month"))))
        {
            error = "Failed to wait for Target start date month drop down option : " + getData("Target start date month");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Target start date month"))))
        {
            error = "Failed to click Target start date month drop down option : " + getData("Target start date month");
            return false;
        }
        narrator.stepPassedWithScreenShot("Target start date month");

        //Target start date year
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TargetStartDateYearDropDown()))
        {
            error = "Failed to wait for Target start date year drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.TargetStartDateYearDropDown()))
        {
            error = "Failed to click Target start year date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2(), getData("Target start date year")))

        {
            error = "Failed to enter Target start date year :" + getData("Target start date year");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Target start date year"))))
        {
            error = "Failed to wait for Target start date year drop down option : " + getData("Target start date year");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Target start date year"))))
        {
            error = "Failed to click Target start date year drop down option : " + getData("Target start date year");
            return false;
        }
        narrator.stepPassedWithScreenShot("Target start date year");

        //Target start date month
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TargetStartDateMonthDropDown2()))
        {
            error = "Failed to wait for Target start date month drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.TargetStartDateMonthDropDown2()))
        {
            error = "Failed to click Target start month date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2(), getData("Target start date month")))

        {
            error = "Failed to enter Target start date month :" + getData("Target start date month");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Target start date month"))))
        {
            error = "Failed to wait for Target start date month drop down option : " + getData("Target start date month");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Target start date month"))))
        {
            error = "Failed to click Target start date month drop down option : " + getData("Target start date month");
            return false;
        }
        narrator.stepPassedWithScreenShot("Target start date month");

        //Target start date year
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TargetStartDateYearDropDown2()))
        {
            error = "Failed to wait for Target start date year drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.TargetStartDateYearDropDown2()))
        {
            error = "Failed to click Target start year date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2(), getData("Target start date year")))

        {
            error = "Failed to enter Target start date year :" + getData("Target start date year");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Target start date year"))))
        {
            error = "Failed to wait for Target start date year drop down option : " + getData("Target start date year");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Target start date year"))))
        {
            error = "Failed to click Target start date year drop down option : " + getData("Target start date year");
            return false;
        }
        narrator.stepPassedWithScreenShot("Target start date year");

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Button_Save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Button_Save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ClimateChangeAndEnergyTargets_PageObjects.setRecord_Number(record[2]);
        String record_ = ClimateChangeAndEnergyTargets_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Capture a Relationship Based Safety record for SLP Observation record saved");
        return true;
    }

}
