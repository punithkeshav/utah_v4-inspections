/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyTargets_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyTargets_PageObjects.ClimateChangeAndEnergyTargets_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR2- Capture Mineral Production Targets - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_CaptureMineralProductionTargets_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR2_CaptureMineralProductionTargets_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!ClimateChangeAndEnergyTargets())
        {
            return narrator.testFailed("Climate Change and Energy Targets Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Climate Change and Energy Targets");
    }

    public boolean ClimateChangeAndEnergyTargets()
    {

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.MineralProductionTargets_Add()))
        {
            error = "Failed to wait for Mineral Production Targets 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.MineralProductionTargets_Add()))
        {
            error = "Failed to click on Mineral Production Targets 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Mineral type
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.MineralType_Dropdown()))
        {
            error = "Failed to wait for Mineral type dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.MineralType_Dropdown()))
        {
            error = "Failed to click Mineral type dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.TypeSearch2(), getData("Mineral type")))

        {
            error = "Failed to enter Mineral type:" + getData("Mineral type");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Mineral type"))))
        {
            error = "Failed to wait for Mineral type drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Text2(getData("Mineral type"))))
        {
            error = "Failed to click Mineral type drop down option : " + getData("Mineral type");
            return false;
        }

        narrator.stepPassedWithScreenShot("Mineral type option  :" + getData("Mineral type"));

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.CO2e_MineralProduced()))
        {
            error = "Failed to wait for CO2e/ mineral produced text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.CO2e_MineralProduced(), getData("CO2e/ mineral produced")+122))
        {
            error = "Failed to enter CO2e/ mineral produced :" + getData("CO2e/ mineral produced");
            return false;
        }

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.Button_Save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyTargets_PageObjects.Button_Save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyTargets_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ClimateChangeAndEnergyTargets_PageObjects.setRecord_Number(record[2]);
        String record_ = ClimateChangeAndEnergyTargets_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Capture a Relationship Based Safety record for SLP Observation record saved");
        return true;
    }

}
