/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergy_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyMonitoring_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyMonitoring_PageObjects.ClimateChangeAndEnergyMonitoring_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR3 Capture Findings Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_Findings_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Capture_Findings_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!captureFindings())
        {
            return narrator.testFailed("Capture Findings Failed due - " + error);
        }

        return narrator.finalizeTest("Capture Findings");
    }

    public boolean captureFindings()
    {

        //Management Findings panel
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.ManagementFindingsPanel()))
        {
            error = "Failed to locate Management Findings panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.ManagementFindingsPanel()))
        {
            error = "Failed to expand Management Findings panel";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.findingsAddButton()))
        {
            error = "Failed to wait for Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.findingsAddButton()))
        {
            error = "Failed to click on Add button.";
            return false;
        }

        //Process Flow
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.findingsProcessflow()))
        {
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.findingsProcessflow()))
        {
            error = "Failed to click Process Flow button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Processflow in Add phase");

        //Findings Description
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Findings_desc()))
        {
            error = "Failed to wait for Description textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Findings_desc(), testData.getData("Description")))
        {
            error = "Failed to enter text into Description textarea.";
            return false;
        }

        //Findings Owner dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Findings_owner_dropdown()))
        {
            error = "Failed to wait for Findings Owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Findings_owner_dropdown()))
        {
            error = "Failed to click Findings Owner dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Measurement taken by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch(), getData("Findings Owner")))
        {
            error = "Failed to enter  Findings Owner :" + getData("Findings Owner");
            return false;
        }
     if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Findings Owner"))))
        {
            error = "Failed to wait for Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Findings Owner"))))
        {
            error = "Failed to click Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }

       

        //Findings Classification dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Findings_class_dropdown()))
        {
            error = "Failed to wait for Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Findings_class_dropdown()))
        {
            error = "Failed to click Findings Classification dropdown.";
            return false;
        }
        //Findings Classification select
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Findings Classification"))))
        {
            error = "Failed to wait for '" + testData.getData("Findings Classification") + "' in Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(testData.getData("Findings Classification"))))
        {
            error = "Failed to click '" + testData.getData("Findings Classification") + "' from Findings Classification dropdown.";
            return false;
        }
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.SaveButtonFindings()))
        {
            error = "Failed to locate Save button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.SaveButtonFindings()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        
 
           //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ClimateChangeAndEnergyMonitoring_PageObjects.setRecord_Number(record[2]);
        String record_ = ClimateChangeAndEnergyMonitoring_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
  

        
        

       

        return true;
    }

}
