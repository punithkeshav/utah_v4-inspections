/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergy_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyMonitoring_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyTargets_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyMonitoring_PageObjects.ClimateChangeAndEnergyMonitoring_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyTargets_PageObjects.ClimateChangeAndEnergyTargets_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Climate Change and Energy Monitoring - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_CaptureClimateChangeAndEnergyMonitoring_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR1_CaptureClimateChangeAndEnergyMonitoring_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!ClimateChangeAndEnergyMonitoring())
        {
            return narrator.testFailed("Climate Change and Energy Monitoring Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Climate Change and Energy Monitoring");
    }

    public boolean ClimateChangeAndEnergyMonitoring()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.ECO2ManTab()))
        {
            error = "Failed to wait for ECO2Man tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.ECO2ManTab()))
        {
            error = "Failed to click on ECO2Man tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to ECO2Man tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.ClimateChangeAndEnergyMonitoringTab()))
        {
            error = "Failed to wait for Climate Change and Energy Monitoring tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.ClimateChangeAndEnergyMonitoringTab()))
        {
            error = "Failed to click on Climate Change and Energy Monitoring tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated toClimate Change and Energy Monitoring tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Button_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Button_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        //New Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.BusinessUnit_Dropdown()))
        {
            error = "Failed to wait for 'Business Unit' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.BusinessUnit_Dropdown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
       
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Month
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.MonthDropDown()))
        {
            error = "Failed to wait for Target start date month drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.MonthDropDown()))
        {
            error = "Failed to click Target start month date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2(), getData("Month")))

        {
            error = "Failed to enter  month :" + getData("Month");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Month"))))
        {
            error = "Failed to wait for month drop down option : " + getData("Month");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Month"))))
        {
            error = "Failed to click month drop down option : " + getData("Month");
            return false;
        }
        narrator.stepPassedWithScreenShot("Month");

        //Year
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.YearDropDown()))
        {
            error = "Failed to wait for year drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.YearDropDown()))
        {
            error = "Failed to click year date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2(), getData("Year")))

        {
            error = "Failed to enter year :" + getData("Year");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Year"))))
        {
            error = "Failed to wait for year drop down option : " + getData("Year");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Year"))))
        {
            error = "Failed to click year drop down option : " + getData("Year");
            return false;
        }
        narrator.stepPassedWithScreenShot("year");

        //Date captured by 
//        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.DateCapturedByDropDown()))
//        {
//            error = "Failed to wait for Date captured by  drop down.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.DateCapturedByDropDown()))
//        {
//            error = "Failed to click Date captured by  drop down";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch()))
//        {
//            error = "Failed to wait for search text box";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch(), getData("Date captured by")))
//
//        {
//            error = "Failed to enter Target start date month :" + getData("Date captured by");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch()))
//        {
//            error = "Failed to press enter";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Date captured by"))))
//        {
//            error = "Failed to wait for Date captured by drop down option : " + getData("Date captured by");
//            return false;
//        }
//        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Date captured by"))))
//        {
//            error = "Failed to click Date captured by drop down option : " + getData("Date captured by");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Date captured by");
        if (getData("Link to environmental permit execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.LinkToEnvironmental()))
            {
                error = "Failed to wait for Link to environmental permit";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.LinkToEnvironmental()))
            {
                error = "Failed to click Link to environmental permit";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.LinkToEnvironmentalDropDown()))
            {
                error = "Failed to wait for Link to environmental permit drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.LinkToEnvironmentalDropDown()))
            {
                error = "Failed to click Link to environmental permit  drop down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait to type text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2(), getData("Link to environmental permit")))
            {
                error = "Failed to enter Link to environmental permit";
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Link to environmental permit"))))
            {
                error = "Failed to wait for Link to environmental permit drop down option : " + getData("Link to environmental permit");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Link to environmental permit"))))
            {
                error = "Failed to click Link to environmental permit drop down option : " + getData("Link to environmental permit");
                return false;
            }
            narrator.stepPassedWithScreenShot("Link to environmental permit");

        }

        if (getData("Link to projects execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.LinkToProjects()))
            {
                error = "Failed to wait for Link to projects";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.LinkToProjects()))
            {
                error = "Failed to click Link to projects";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.LinkToProjectsDropDown()))
            {
                error = "Failed to wait for Link to projects drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.LinkToProjectsDropDown()))
            {
                error = "Failed to click Link to projects drop down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait to type text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2(), getData("Link to projects")))
            {
                error = "Failed to enter Link to environmental permit";
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Link to projects"))))
            {
                error = "Failed to wait for Link to projects drop down option : " + getData("Link to projects");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Link to projects"))))
            {
                error = "Failed to click Link to projects drop down option : " + getData("Link to environmental permit");
                return false;
            }
            narrator.stepPassedWithScreenShot("Link to projects");

        }

        if (getData("Upload documents").equalsIgnoreCase("True"))
        {
            //Upload hyperlink to Supporting documents
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.linkToADocument()))
            {
                error = "Failed to wait for Supporting documents 'Link box' link.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.linkToADocument()))
            {
                error = "Failed to click on Supporting documents 'Link box' link.";
                return false;
            }
            pause(1000);
            narrator.stepPassedWithScreenShot("Successfully clicked Supporting documents 'Upload Hyperlink box'.");

            //switch to new window
            if (!SeleniumDriverInstance.switchToTabOrWindow())
            {
                error = "Failed to switch to new window or tab.";
                return false;
            }

            //URL https
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Link_URL()))
            {
                error = "Failed to wait for 'URL value' field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Link_URL(), getData("Document Link")))
            {
                error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

            //Title
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.urlTitle()))
            {
                error = "Failed to wait for 'Url Title' field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.urlTitle(), getData("URL Title")))
            {
                error = "Failed to enter '" + getData("URL Title") + "' into 'Url Title' field.";
                return false;
            }
            narrator.stepPassedWithScreenShot("URL Title : '" + getData("URL Title") + "'.");

            //Add button
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.urlAddButton()))
            {
                error = "Failed to wait for 'Add' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.urlAddButton()))
            {
                error = "Failed to click on 'Add' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
            narrator.stepPassed("Successfully uploaded '" + getData("URL Title") + "' document using '" + getData("Document Link") + "' Link.");

        }

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Button_Save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Button_Save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ClimateChangeAndEnergyMonitoring_PageObjects.setRecord_Number(record[2]);
        String record_ = ClimateChangeAndEnergyMonitoring_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        return true;
    }

}
