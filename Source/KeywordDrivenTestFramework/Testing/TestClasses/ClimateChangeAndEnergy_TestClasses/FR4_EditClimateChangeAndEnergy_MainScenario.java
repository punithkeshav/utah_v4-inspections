/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergy_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyMonitoring_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyTargets_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyMonitoring_PageObjects.ClimateChangeAndEnergyMonitoring_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyTargets_PageObjects.ClimateChangeAndEnergyTargets_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR4-Edit Climate Change and Energy - Main scenario",
        createNewBrowserInstance = false
)

public class FR4_EditClimateChangeAndEnergy_MainScenario extends BaseClass
{
    
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR4_EditClimateChangeAndEnergy_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 2500);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }
    
    public TestResult executeTest()
    {
        if (!navigateToStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Stakeholder Individual updates saved");
    }
    
    public boolean navigateToStakeholderIndividual()
    {
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        pause(3000);
        
        
        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.ContainsTextBox()))
        {
            error = "Failed to wait for contains";
            return false;
        }
        
     

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.SearchButton()))
        {
            error = "Failed wait for Search Button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);
        
        pause(5000);

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }
        
        //Month
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.MonthDropDown()))
        {
            error = "Failed to wait for Target start date month drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.MonthDropDown()))
        {
            error = "Failed to click Target start month date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2(), getData("Month")))

        {
            error = "Failed to enter  month :" + getData("Month");
            return false;
        }

       if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Month"))))
        {
            error = "Failed to wait for month drop down option : " + getData("Month");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Month"))))
        {
            error = "Failed to click month drop down option : " + getData("Month");
            return false;
        }
        narrator.stepPassedWithScreenShot("Month");

        //Year
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.YearDropDown()))
        {
            error = "Failed to wait for year drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.YearDropDown()))
        {
            error = "Failed to click year date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2(), getData("Year")))

        {
            error = "Failed to enter year :" + getData("Year");
            return false;
        }

       if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Year"))))
        {
            error = "Failed to wait for year drop down option : " + getData("Year");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Year"))))
        {
            error = "Failed to click year drop down option : " + getData("Year");
            return false;
        }
        narrator.stepPassedWithScreenShot("year");
        

        

        //Save to continue
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Button_Save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Button_Save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.recordSaved_popup1()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.recordSaved_popup1());
        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            
            error = "Failed to save record.";
            return false;            
        }
        
        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyTargets_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ClimateChangeAndEnergyMonitoring_PageObjects.setRecord_Number(record[2]);
        String record_ = ClimateChangeAndEnergyMonitoring_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        
        return true;
    }
    
}
