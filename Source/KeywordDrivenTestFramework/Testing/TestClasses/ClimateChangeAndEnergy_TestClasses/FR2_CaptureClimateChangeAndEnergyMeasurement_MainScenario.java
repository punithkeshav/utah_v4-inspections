/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergy_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyMonitoring_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyTargets_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyMonitoring_PageObjects.ClimateChangeAndEnergyMonitoring_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyTargets_PageObjects.ClimateChangeAndEnergyTargets_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Climate Change and Energy Measurement - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_CaptureClimateChangeAndEnergyMeasurement_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR2_CaptureClimateChangeAndEnergyMeasurement_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!ClimateChangeAndEnergyMeasurement())
        {
            return narrator.testFailed("Climate Change and Energy Measurement Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Climate Change and Measurement Monitoring");
    }

    public boolean ClimateChangeAndEnergyMeasurement()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.MeasurementsTab()))
        {
            error = "Failed to wait for Measurements tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.MeasurementsTab()))
        {
            error = "Failed to click on Measurements tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated toClimate Change and Energy Monitoring tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Measurements_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Measurements_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Emission source
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.EmissionSource_Dropdown()))
        {
            error = "Failed to wait for Emission source dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.EmissionSource_Dropdown()))
        {
            error = "Failed to click Emission source dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait to expand Emission source";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2(), getData("Emission source")))
        {
            error = "Failed to enter Emission source";
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.businessUnitOption1(getData("Emission source 1"))))
        {
            error = "Failed to wait for Emission source:" + getData("Emission source 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.businessUnitOption1(getData("Emission source 1"))))
        {
            error = "Failed to click Emission source Option drop down :" + getData("Emission source 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.businessUnitOption1(getData("Emission source 2"))))
        {
            error = "Failed to wait for Emission source:" + getData("Emission source 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.businessUnitOption1(getData("Emission source 2"))))
        {
            error = "Failed to click Emission source Option drop down :" + getData("Emission source 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text3(getData("Emission source"))))
        {
            error = "Failed to wait for Emission source drop down option : " + getData("Emission source");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text3(getData("Emission source"))))
        {
            error = "Failed to click Emission source drop down option : " + getData("Emission source");
            return false;
        }

        narrator.stepPassedWithScreenShot("Emission source option  :" + getData("Emission source"));

        //Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.UnitDropDown()))
        {
            error = "Failed to wait for Unit drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.UnitDropDown()))
        {
            error = "Failed to click Unit drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2(), getData("Unit")))

        {
            error = "Failed to enter  Unit :" + getData("Unit");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(ClimateChangeAndEnergyMonitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Unit"))))
        {
            error = "Failed to wait for Unit drop down option : " + getData("Unit");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Text2(getData("Unit"))))
        {
            error = "Failed to click Unit drop down option : " + getData("Unit");
            return false;
        }
        narrator.stepPassedWithScreenShot("Unit");

        //Measurement 
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Measurement()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Measurement(), 12 + ""))

        {
            error = "Failed to enter Measurement :" + 12;
            return false;
        }

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Button_Save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ClimateChangeAndEnergyMonitoring_PageObjects.Button_Save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.recordSaved_popup1()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.recordSaved_popup1());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ClimateChangeAndEnergyMonitoring_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ClimateChangeAndEnergyMonitoring_PageObjects.setRecord_Number(record[2]);
        String record_ = ClimateChangeAndEnergyMonitoring_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        return true;
    }

}
