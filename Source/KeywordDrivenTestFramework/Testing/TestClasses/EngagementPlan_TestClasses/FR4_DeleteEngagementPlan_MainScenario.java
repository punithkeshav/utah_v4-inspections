/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EngagementPlan_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.BaselineDataCollection_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.RelationshipBasedSafety_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EngagementPlan_PageObjects.EngagementPlan_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR4 - Delete Engagement Plan",
        createNewBrowserInstance = false
)

public class FR4_DeleteEngagementPlan_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR4_DeleteEngagementPlan_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!deleteStakeholderIndividual())
        {
            return narrator.testFailed("Delete Engagement Plan Failed due to :" + error);
        }

        return narrator.finalizeTest("Successfully Delete Engagement Plan");
    }

    public boolean deleteStakeholderIndividual()
    {

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

     

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        

        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.ContainsTextBox()))
        {
            error = "Failed to wait for s contains text box";
            return false;
        }

        pause(4000);

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);
        pause(5000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.EngagementPlan_processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.EngagementPlan_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.DeleteButton()))
        {
            error = "Failed to wait for delete button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.DeleteButton()))
        {
            error = "Failed to click delete Button.";
            return false;

        }
        pause(3500);

        narrator.stepPassedWithScreenShot("Successfully clicked delete button");

       if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);

        return true;
    }

}
