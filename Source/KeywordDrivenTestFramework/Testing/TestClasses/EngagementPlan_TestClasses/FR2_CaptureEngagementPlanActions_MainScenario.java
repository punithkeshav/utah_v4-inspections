/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EngagementPlan_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.RelationshipBasedSafety_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EngagementPlan_PageObjects.EngagementPlan_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.RelationshipBasedSafety_PageObjects.RelationshipBasedSafety_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Engagement Plan Actions - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_CaptureEngagementPlanActions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR2_CaptureEngagementPlanActions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CaptureSafetyLeadershipActions())
        {
            return narrator.testFailed("Capture Engagement Plan Actions Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Engagement Plan Actions");
    }

    //Enter data
    public boolean CaptureSafetyLeadershipActions()
    {
        
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.ActionsTab()))
        {
            error = "Failed to wait for Actions Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.ActionsTab()))
        {
            error = "Failed to click on Actions Tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click  Actions Tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Actions_Add()))
        {
            error = "Failed to wait for  Actions Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Actions_Add()))
        {
            error = "Failed to click on  Actions Add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Safety Leadership Actions Add button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Actions_processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Actions_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        //Type of action
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to wait for Type of action.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to click Type of action";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Action description text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch2(), getData("Type of action")))

        {
            error = "Failed to enter Type of action :" + getData("Type of action");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        //
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to click Project drop down option : " + getData("Type of action");
            return false;
        }
        narrator.stepPassedWithScreenShot("Type of action :" + getData("Type of action"));
        
        
        ///Action Description

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.ActionDescription()))
        {
            error = "Failed to wait for Action description text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.ActionDescription(), getData("Action description")))

        {
            error = "Failed to enter Action description :" + getData("Action description");
            return false;
        }

        //Entity
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Entity_Dropdown()))
        {
            error = "Failed to wait for Entity dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Entity_Dropdown()))
        {
            error = "Failed to click Entity dropdown.";
            return false;
        }

       
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Entity text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch2(), getData("Business unit option")))

        {
            error = "Failed to enter Entity option :" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);
        

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to wait to expand Entity";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to expand Entity";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Entity:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity drop down :" + getData("Business unit 1");
            return false;
        }
        
//          if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.businessUnitOption1(getData("Business unit 2"))))
//        {
//            error = "Failed to wait for Entity:" + getData("Business unit 2");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.businessUnitOption1(getData("Business unit 2"))))
//        {
//            error = "Failed to click Entity drop down :" + getData("Business unit 2");
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Entity option  :" + getData("Business unit option"));
        
        
        
        //Responsible person
        
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.ResponsiblePerson_dropdown()))
        {
            error = "Failed to wait for Responsible person dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.ResponsiblePerson_dropdown()))
        {
            error = "Failed to click Responsible person dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible person text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch(), getData("Responsible person")))
        {
            error = "Failed to enter Responsible person option :" + getData("Responsible person");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to wait forResponsible person options.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person :" + getData("Responsible person");
            return false;
        }

        narrator.stepPassedWithScreenShot("Responsible person :" + getData("Responsible person"));
        
        
        
        
        
        //Action due date

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.ActionDueDate()))
        {
            error = "Failed to wait for Action due date input field.";
            return false;

        }

        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.ActionDueDate(), startDate))
        {
            error = "Failed to enter Action due date :" + startDate;
            return false;
        }

        narrator.stepPassedWithScreenShot("Action due date :" + startDate);
        
        

       
        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Actions_save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Actions_save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EngagementPlan_PageObjects.setRecord_Number(record[2]);
        String record_ = EngagementPlan_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Capture a Relationship Based Safety record for SLP Observation record saved");
        return true;
    }

}
