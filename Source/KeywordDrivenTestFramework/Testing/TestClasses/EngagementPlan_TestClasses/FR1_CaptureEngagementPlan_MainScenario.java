/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EngagementPlan_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.BaselineDataCollection_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.RelationshipBasedSafety_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EngagementPlan_PageObjects.EngagementPlan_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Engagement Plan - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_CaptureEngagementPlan_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR1_CaptureEngagementPlan_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToEngagementPlan())
        {
            return narrator.testFailed("Capture Engagement Plan Failed Due To: " + error);
        }
        if (!CreateEngagementPlan())
        {
            return narrator.testFailed("Capture Engagement PlanFailed Due To: " + error);
        }

        if (getData("Upload Document").equalsIgnoreCase("True"))
        {

            if (!UploadDocument())
            {
                return narrator.testFailed("Failed to upload document: " + error);
            }
        }

        return narrator.finalizeTest("Successfully Capture Engagement Plan");
    }

    public boolean NavigateToEngagementPlan()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.EngagementPlanTab()))
        {
            error = "Failed to wait for Engagement Plan tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.EngagementPlanTab()))
        {
            error = "Failed to click on Engagement Plan tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Engagement Plan tab.");

        pause(5000);
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.EngagementPlan_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.EngagementPlan_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    //Enter data
    public boolean CreateEngagementPlan()
    {

        pause(5000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.EngagementPlan_processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.EngagementPlan_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        //Business unit
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.BusinessUnit_Dropdown()))
        {
            error = "Failed to wait for Business unit dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.BusinessUnit_Dropdown()))
        {
            error = "Failed to click Business unit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business unit text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch2(), getData("Business unit option")))

        {
            error = "Failed to enter Business unit  option :" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to wait to expand Business unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to expand Business unit";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business unit:" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click Business unit Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to wait for Business unit:" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to click Business unit drop down option :" + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Engagement plan title 
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.EngagementPlanTitle()))
        {
            error = "Failed to wait for Engagement plan title .";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.EngagementPlanTitle(), getData("Engagement plan title")))
        {
            error = "Failed to enter Engagement plan title :" + getData("Engagement plan title");
            return false;
        }

        //Engagement start date
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.EngagementStartDate()))
        {
            error = "Failed to wait for Engagement start date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.EngagementStartDate(), startDate))
        {
            error = "Failed to enter Engagement start date : " + startDate;
            return false;
        }

        int number = Integer.parseInt(getData("Show in advance"));
        //Show in advance (Number)
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.ShowInAdvance()))
        {
            error = "Failed to wait for Show in advance";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.ShowInAdvance()))
        {
            error = "Failed to wait click Show in advance";
            return false;
        }
        if (!SeleniumDriverInstance.enterNumberByXpath(EngagementPlan_PageObjects.ShowInAdvance(), number))
        {
            error = "Failed to enter Show in advance : " + endDate;
            return false;
        }
        narrator.stepPassedWithScreenShot("Show in advance :" + getData("Show in advance"));

        if (getData("Link to project").equalsIgnoreCase("True"))
        {
            //Link to project
            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.LinkToProject()))
            {
                error = "Failed to wait for /Link to project";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.LinkToProject()))
            {
                error = "Failed to click /Link to project ";
                return false;
            }

            //Project
            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.ProjectDropDown()))
            {
                error = "Failed to wait for Project drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.ProjectDropDown()))
            {
                error = "Failed to click Project drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Project text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch2(), getData("Project")))

            {
                error = "Failed to enter Project option :" + getData("Project");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("Project"))))
            {
                error = "Failed to wait for Project option :" + getData("Project");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("Project"))))
            {
                error = "Failed to select Project option :" + getData("Project");
                return false;
            }

            narrator.stepPassedWithScreenShot("Project :" + getData("Project"));

        }

        //Frequency
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.FrequencyDropDown()))
        {
            error = "Failed to wait for Frequencys drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.FrequencyDropDown()))
        {
            error = "Failed to click Frequency drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Frequency text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch2(), getData("Frequency")))

        {
            error = "Failed to enter Frequency option :" + getData("Frequency");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("Frequency"))))
        {
            error = "Failed to wait for Frequency option :" + getData("Frequency");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("Frequency"))))
        {
            error = "Failed to select Frequency option :" + getData("Frequency");
            return false;
        }

        narrator.stepPassedWithScreenShot("Frequency :" + getData("Frequency"));

        if (getData("Frequency").equalsIgnoreCase("Daily") || getData("Frequency").equalsIgnoreCase("Monthly") || getData("Frequency").equalsIgnoreCase("Weekly") || getData("Frequency").equalsIgnoreCase("Bi-Annually") || getData("Frequency").equalsIgnoreCase("Quarterly") || getData("Frequency").equalsIgnoreCase("Annually"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.EngagementEndDate()))
            {
                error = "Failed to wait for Engagement end date";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.EngagementEndDate(), endDate))
            {
                error = "Failed to enter Engagement end date :" + endDate;
                return false;
            }

        }

        if (getData("Frequency").equalsIgnoreCase("Monthly") || getData("Frequency").equalsIgnoreCase("Weekly"))
        {
            //On which day of the week
            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.OnWhichDayOfTheWeekDropDown()))
            {
                error = "Failed to wait for On which day of the week drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.OnWhichDayOfTheWeekDropDown()))
            {
                error = "Failed to click On which day of the week drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for On which day of the week text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch2(), getData("On which day of the week")))

            {
                error = "Failed to enter On which day of the week option :" + getData("On which day of the week");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("On which day of the week"))))
            {
                error = "Failed to wait for On which day of the week option :" + getData("On which day of the week");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("On which day of the week"))))
            {
                error = "Failed to select On which day of the week option :" + getData("On which day of the week");
                return false;
            }

            narrator.stepPassedWithScreenShot("On which day of the week :" + getData("On which day of the week"));

        }

        //Responsible person
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.ResponsiblePersonDropDown()))
        {
            error = "Failed to wait for Linked Responsible persons drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.ResponsiblePersonDropDown()))
        {
            error = "Failed to click Linked Responsible person drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible person text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch(), getData("Responsible person")))

        {
            error = "Failed to enter Responsible person option :" + getData("Responsible person");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person option :" + getData("Responsible person");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to select Responsible person option :" + getData("Responsible person");
            return false;
        }

        narrator.stepPassedWithScreenShot("Responsible person :" + getData("Responsible person"));

        if (getData("Frequency").equalsIgnoreCase("Bi-Annually") || getData("Frequency").equalsIgnoreCase("Quarterly") || getData("Frequency").equalsIgnoreCase("Annually"))
        {

            //Frequency from      
            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.FrequencyFromDropDown()))
            {
                error = "Failed to wait for Frequency drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.FrequencyFromDropDown()))
            {
                error = "Failed to click Frequency from drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Frequency from text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch2(), getData("Frequency from")))

            {
                error = "Failed to enter Frequency from option :" + getData("Frequency from");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("Frequency from"))))
            {
                error = "Failed to wait for Frequency from option :" + getData("Frequency from");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("Frequency from"))))
            {
                error = "Failed to select Frequency from option :" + getData("Frequency from");
                return false;
            }

            narrator.stepPassedWithScreenShot("Frequency from :" + getData("Frequency from"));

            //On which day of the week
            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.OnWhichDayOfTheWeekDropDown()))
            {
                error = "Failed to wait for On which day of the week drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.OnWhichDayOfTheWeekDropDown()))
            {
                error = "Failed to click On which day of the week drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for On which day of the week text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch2(), getData("On which day of the week")))

            {
                error = "Failed to enter On which day of the week option :" + getData("On which day of the week");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("On which day of the week"))))
            {
                error = "Failed to wait for On which day of the week option :" + getData("On which day of the week");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("On which day of the week"))))
            {
                error = "Failed to select On which day of the week option :" + getData("On which day of the week");
                return false;
            }

            narrator.stepPassedWithScreenShot("On which day of the week :" + getData("On which day of the week"));

            //Week of month
            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.WeekOfMonthDropDown()))
            {
                error = "Failed to wait for Week of month drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.WeekOfMonthDropDown()))
            {
                error = "Failed to click Week of month from drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Week of month from text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch2(), getData("Week of month")))

            {
                error = "Failed to enter Week of month from option :" + getData("Week of month");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("Week of month"))))
            {
                error = "Failed to wait for Week of month from option :" + getData("Week of month");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("Week of month"))))
            {
                error = "Failed to select Week of month from option :" + getData("Week of month");
                return false;
            }

            narrator.stepPassedWithScreenShot("Week of month  :" + getData("Week of month"));

        }

        if (getData("Frequency").equalsIgnoreCase("Monthly"))
        {

            //Week of month
            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.WeekOfMonthDropDown()))
            {
                error = "Failed to wait for Week of month drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.WeekOfMonthDropDown()))
            {
                error = "Failed to click Week of month from drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Week of month from text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch2(), getData("Week of month")))

            {
                error = "Failed to enter Week of month from option :" + getData("Week of month");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("Week of month"))))
            {
                error = "Failed to wait for Week of month from option :" + getData("Week of month");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("Week of month"))))
            {
                error = "Failed to select Week of month from option :" + getData("Week of month");
                return false;
            }

            narrator.stepPassedWithScreenShot("Week of month  :" + getData("Week of month"));

        }

        //Purpose of engagement
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.PurposeOfEngagementDropDown()))
        {
            error = "Failed to wait for Purpose of engagement.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.PurposeOfEngagementDropDown()))
        {
            error = "Failed to click Purpose of engagement";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch2(), getData("Purpose of engagement")))

        {
            error = "Failed to enter Purpose of engagement :" + getData("Purpose of engagement");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearchCheckBox(getData("Purpose of engagement"))))
        {
            error = "Failed to wait for Purpose of engagement drop down option : " + getData("Purpose of engagement");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(EngagementPlan_PageObjects.TypeSearchCheckBox(getData("Purpose of engagement"))))
        {
            error = "Failed to click Purpose of engagement drop down option : " + getData("Purpose of engagement");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.PurposeOfEngagementDropDown()))
        {
            error = "Failed to click Purpose of engagement";
            return false;
        }
        narrator.stepPassedWithScreenShot("Purpose of engagement");

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.PurposeOfEngagementDropDown()))
        {
            error = "Failed to wait for Purpose of engagement.";
            return false;
        }

        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.EngagementPlan_save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.EngagementPlan_save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EngagementPlan_PageObjects.setRecord_Number(record[2]);
        String record_ = EngagementPlan_PageObjects.getRecord_Number();
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

    public boolean UploadDocument()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to wait for Supporting Documents'";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to click on Supporting Documents";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Supporting Documents Tab");

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.linkADoc_button()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.linkADoc_button()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.urlInput_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.urlInput_TextAreaxpath(), getData("Document Link")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.tile_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.tile_TextAreaxpath(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(EngagementPlan_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.EngagementPlan_save()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.EngagementPlan_save()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EngagementPlan_PageObjects.setRecord_Number(record[2]);
        String record_ = EngagementPlan_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;

    }

}
