/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EngagementPlan_TestClasses;

/**
 *
 * @author smabe
 */


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EngagementPlan_PageObjects.EngagementPlan_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;


@KeywordAnnotation(
        Keyword = "UC EDP 01-03",
        createNewBrowserInstance = false
)
public class UC_EDP_01_03 extends BaseClass
{
    
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
     public UC_EDP_01_03()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }
     
       public TestResult executeTest()
    {
        if (!Attendees())
        {
            return narrator.testFailed("Capture Mark an Engagement Plan as Inactive  Failed Due To: " + error);
        }
    

        return narrator.finalizeTest("Successfully Mark an Engagement Plan as Inactive");
    }
       
       
          public boolean Attendees()
    {
       
        
           //Stakeholders
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.ActiveInactiveDropDown()))
        {
            error = "Failed to wait for Active / Inactive";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.ActiveInactiveDropDown()))
        {
            error = "Failed to click Active / Inactive";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EngagementPlan_PageObjects.TypeSearch2(), getData("Active / Inactive")))

        {
            error = "Failed to enter Active / Inactive :" + getData("Active / Inactive");
            return false;
        }

       if (!SeleniumDriverInstance.pressEnter_2(EngagementPlan_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.Text2(getData("Active / Inactive"))))
        {
            error = "Failed to wait for Stakeholders drop down option : " + getData("Active / Inactive");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(EngagementPlan_PageObjects.Text2(getData("Active / Inactive"))))
        {
            error = "Failed to click Active / Inactive drop down option : " + getData("Active / Inactive");
            return false;
        }
      
        narrator.stepPassedWithScreenShot("Active / Inactive :" + getData("Active / Inactive"));
        

         //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.EngagementPlan_save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.EngagementPlan_save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EngagementPlan_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EngagementPlan_PageObjects.setRecord_Number(record[2]);
        String record_ = EngagementPlan_PageObjects.getRecord_Number();
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        
        return true;
    }
       
       
       
    
}
