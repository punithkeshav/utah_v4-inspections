/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */

@KeywordAnnotation(
        Keyword = "Capture Vulnerability",
        createNewBrowserInstance = false
)

public class FR5_CaptureOrUpdateVulnerablity_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR5_CaptureOrUpdateVulnerablity_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if (!updateStakeholderAnalysis())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Captured Stakeholder analysis");
    }

    public boolean updateStakeholderAnalysis(){
     
         
    SeleniumDriverInstance.scrollToElement(Stakeholder_Individual_PageObjects.vulnerabilityTab());
    
        //Vulnerability tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.vulnerabilityTab())){
            error = "Failed to wait for Vulnerability tab";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.vulnerabilityTab())){
            error = "Failed to click Vulnerability tab";
            return false;
        } 
        
        //Is Stakeholder Vulnerable
        
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholderVulnerable())){
            error = "Failed to wait for Vulnerability confirmed dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholderVulnerable())){
            error = "Failed to click Vulnerability confirmed dropdown";
            return false;
        } 
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text2(getData("Stakeholder Vulnerable"))))
        {
            error = "Failed to wait for Stakeholder Vulnerable drop down option : " + getData("Stakeholder Vulnerable");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text2(getData("Stakeholder Vulnerable"))))
        {
            error = "Failed to click Stakeholder Vulnerable drop down option : " + getData("Stakeholder Vulnerable");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully captured Stakeholder Vulnerable");
        
        //Vulnerability confirmed dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.vulnerabilityConfirmed())){
            error = "Failed to wait for Vulnerability confirmed dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.vulnerabilityConfirmed())){
            error = "Failed to click Vulnerability confirmed dropdown";
            return false;
        } 
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.singleSelect(testData.getData("Vulnerability confirmed")))){
            error = "Failed to wait for Vulnerability confirmed dropdown value";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.singleSelect(testData.getData("Vulnerability confirmed")))){
            error = "Failed to select Vulnerability confirmed dropdown value";
            return false;
        }
        
        if(getData("Vulnerability confirmed").equalsIgnoreCase("Yes")){
        //Vulnerability categorization dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.vulnerabilityCategorization())){
            error = "Failed to wait for Vulnerability categorization dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.vulnerabilityCategorization())){
            error = "Failed to click Vulnerability categorization dropdown";
            return false;
        } 
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.singleSelect(testData.getData("Vulnerability categorization")))){
            error = "Failed to wait for Vulnerability categorization dropdown value";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.singleSelect(testData.getData("Vulnerability categorization")))){
            error = "Failed to select Vulnerability categorization dropdown value";
            return false;
        }
        
         //Vulnerability type checklist       
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.vulnerabiltyTypeDD())){
            error = "Failed to wait for Vulnerability type checklist value";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.vulnerabiltyTypeDD())){
            error = "Failed to select Vulnerability type checklist value";
            return false;
        } 
        
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Vulnerability type")))
            {
                 error = "Failed to enter Vulnerability type :" + getData("Vulnerability type");
                return false;
            }

              if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Individual_PageObjects.text_Search1()))
        {
            error = "Failed to press enter";
            return false;
        }
      
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Vulnerability type")))){
            error = "Failed to wait for 'Vulnerability type' options.";
            return false;
        }
        
        //Try this
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TypeSearchCheckBox(getData("Vulnerability type")))) {
         error = "Failed to select '"+testData.getData("Vulnerability type")+"' from 'Vulnerability type' options.";
            return false;
        }
        
        //Vulnerability description
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.vulnerabilityDescription())){
            error = "Failed to wait for Vulnerability description textbox";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.vulnerabilityDescription(), testData.getData("Vulnerability description"))){
            error = "Failed to click Vulnerability description textbox";
            return false;
        } 
        }
        
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to wait for Save button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to click Save button";
            return false;
        } 
        
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        
          //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Stakeholder_Individual_PageObjects.setRecord_Number(record[2]);
            String record_ = Stakeholder_Individual_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully saved the record");
        return true;
    }

}
