/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Stakeholder Individual record deleted",
        createNewBrowserInstance = false
)

public class FR12_DeleteStakeholderIndividual_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR12_DeleteStakeholderIndividual_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!deleteStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully deleted Stakeholder record ");
    }

    public boolean deleteStakeholderIndividual()
    {

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual()))
        {
            error = "Failed to wait for 'Stakeholder individual' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual()))
        {
            error = "Failed to click on 'Stakeholder individual' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholder individual' tab.");

        pause(7000);
        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.SearchOption()))
        {
            error = "Failed to wait for search options";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.SearchOption()))
        {
            error = "Failed to click search options";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);
        pause(5000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_processflow(), 5000))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.deleteRecordButton()))
        {
            error = "Failed to wait for delete button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.deleteRecordButton()))
        {
            error = "Failed to click delete Button.";
            return false;

        }
        pause(3500);

        narrator.stepPassedWithScreenShot("Successfully clicked delete button");

       if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);

        return true;
    }

}
