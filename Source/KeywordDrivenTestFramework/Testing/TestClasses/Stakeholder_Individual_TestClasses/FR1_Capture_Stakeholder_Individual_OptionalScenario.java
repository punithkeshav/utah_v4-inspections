/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Stakeholder Individual - Optional Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Stakeholder_Individual_OptionalScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR1_Capture_Stakeholder_Individual_OptionalScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!uploadSupportingDocs())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Record is saved.");
    }

    public boolean uploadSupportingDocs()
    {

        //Supporting Documents tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.supporting_tab()))
        {
            error = "Failed to wait for Supporting Documents tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.supporting_tab()))
        {
            error = "Failed to click Supporting Documents tab.";
            return false;
        }
        //Add support document - Hyperlink
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.linkbox()))
        {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.linkbox()))
        {
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.LinkURL()))
        {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.LinkURL(), getData("Document Link")))
        {
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.urlTitle()))
        {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.urlTitle(), getData("URL Title")))
        {
            error = "Failed to enter '" + getData("URL Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("URL Title : '" + getData("URL Title") + "'.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.urlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.urlAddButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Individual_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
//        Save to continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save()))
        {
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save()))
        {
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }

        //
        if (SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.ValidationPopUp()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.designation()))
            {
                error = "Failed to wait for Designation input field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.designation(), getData("Designation")))
            {
                error = "Failed to enter Designation input field.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.PrimaryContactNumber()))
            {
                error = "Failed to wait for Primary contact number  input field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.PrimaryContactNumber(), getData("Primary contact number")))
            {
                error = "Failed to enter Primary contact number input field.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.StreetAddress()))
            {
                error = "Failed to wait for Street address input field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.StreetAddress(), getData("Street address")))
            {
                error = "Failed to enter Street address input field.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.PostalCode()))
            {
                error = "Failed to wait for Zip/Postal code input field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.PostalCode(), getData("Zip/Postal code")))
            {
                error = "Failed to enter Zip/Postal code input field.";
                return false;
            }

            //Geographic location
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.GeographicLocationDropDown()))
            {
                error = "Failed to wait for Geographic location drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.GeographicLocationDropDown()))
            {
                error = "Failed to click Applicable business units.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.text_Search1(), getData("Geographic location")))

            {
                error = "Failed to enter Geographic location :" + getData("Geographic location");
                return false;
            }

                       if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Individual_PageObjects.text_Search1()))
        {
            error = "Failed to press enter";
            return false;
        }

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Text3(getData("Geographic location"))))
            {
                error = "Failed to wait for Geographic location drop down option : " + getData("Business unit option");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Text3(getData("Geographic location"))))
            {
                error = "Failed to click Geographic locationdrop down option : " + getData("Geographic location");
                return false;
            }

            narrator.stepPassedWithScreenShot("Geographic location  :" + getData("Geographic location"));

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save()))
            {
                error = "Failed to wait for Save and Continue.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save()))
            {
                error = "Failed to click Save and Continue.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
            String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Stakeholder_Individual_PageObjects.setRecord_Number(record[2]);
            String record_ = Stakeholder_Individual_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
        }

        return true;
    }

}
