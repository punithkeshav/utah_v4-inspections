/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR6_AddViewEngagements_MainScenario",
        createNewBrowserInstance = false
)

public class FR6_AddViewEngagements_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR6_AddViewEngagements_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToEnagements())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }

    public boolean navigateToEnagements()
    {

        //Navigate to the Enagements tab.
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementsTab()))
        {
            error = "Failed to wait for Navigate to the Commitments tab tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementsTab()))
        {
            error = "Failed to click Navigate to the Commitments tab";
            return false;
        }

        //Search engagements
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.searchEngagements()))
        {
            error = "Failed to wait for Search engagements to be opened";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.searchEngagements()))
        {
            error = "Failed to click Search engagements to be opened";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Search engagements");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.searchEngagements1()))
        {
            error = "Failed to wait for Search engagements to be opened";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.searchEngagements1()))
        {
            error = "Failed to click Search engagements to be opened";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked search engagements button");

        String results = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.RecordSearchResults());

        if (results.equalsIgnoreCase("No results returned"))
        {
            narrator.stepPassedWithScreenShot("No results returned");

        }

        return true;
    }

}
