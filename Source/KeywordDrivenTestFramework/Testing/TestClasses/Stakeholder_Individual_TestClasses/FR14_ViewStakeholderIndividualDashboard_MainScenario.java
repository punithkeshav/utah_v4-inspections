/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Dashboards viewed.",
        createNewBrowserInstance = false
)

public class FR14_ViewStakeholderIndividualDashboard_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR14_ViewStakeholderIndividualDashboard_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if (!viewDashBaord())
        {
            return narrator.testFailed("Failed due - " + error);
        }
         

        return narrator.finalizeTest("Successfully viewed Dashboard");
    }

    public boolean viewDashBaord(){
         
      //ViewDasboard
        pause(3500);
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.viewDashboard())) {
            error = "Failed to wait for viewDasboard.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.viewDashboard())) {
            error = "Failed to click view Dasboard.";
            return false;
        }
  
   narrator.stepPassedWithScreenShot("Successfully clicked on View Dasboard");
   
      pause(9500);
      
      //navigate  button
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigateButton())) {
            error = "Failed to wait for view Dasboard.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigateButton())) {
            error = "Failed to click view Dasboard.";
            return false;
        }
      narrator.stepPassedWithScreenShot("Successfully clicked on navigate  button");
        
        //App OverView
           if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.appOverView())) {
            error = "Failed to wait for view App OverView";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.appOverView())) {
            error = "Failed to click view App OverView.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked on App OverView button");
        pause(5500);
       
        return true;
    }

}
