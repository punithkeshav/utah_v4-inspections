/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Stakeholder Individual updates saved",
        createNewBrowserInstance = false
)

public class FR11_EditStakeholderIndividual_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR11_EditStakeholderIndividual_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 2500);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Stakeholder Individual updates saved");
    }

    public boolean navigateToStakeholderIndividual()
    {
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual()))
        {
            error = "Failed to wait for 'Stakeholder individual' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual()))
        {
            error = "Failed to click on 'Stakeholder individual' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholder individual' tab.");

        //Search
        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.SearchOption()))
        {
            error = "Failed to wait for search options";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.SearchOption()))
        {
            error = "Failed to click search options";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);
        pause(9000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_processflow(),5000))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }
        //Stakeholder Individual first name
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_fname()))
        {
            error = "Failed to wait for 'Stakeholder Individual first name' input field.";
            return false;

        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_fname(), testData.getData("First name")))
        {
            error = "Failed to click 'Stakeholder Individual first name' input field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered Individual first name.");

        //Stakeholder Individual last name
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_lname()))
        {
            error = "Failed to wait for 'Stakeholder Individual last name' input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_lname(), testData.getData("Last name")))
        {
            error = "Failed to click 'Stakeholder Individual last name' input field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Stakeholder Individual last name.");

        //Stakeholder Individual known as
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_knownas()))
        {
            error = "Failed to wait for 'Stakeholder Individual Known as' input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_knownas(), testData.getData("Known as")))
        {
            error = "Failed to click 'Stakeholder Individual Known as' input field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered Stakeholder Individual known as.");

        //Stakeholder Individual title dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_title_dropdown()))
        {
            error = "Failed to wait for 'Stakeholder title' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_title_dropdown()))
        {
            error = "Failed to click 'Stakeholder title' dropdown.";
            return false;
        }
        //Stakeholder Individual title select
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_title(testData.getData("Title"))))
        {
            error = "Failed to wait for 'Stakeholder title' options.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_title(testData.getData("Title"))))
        {
            error = "Failed to select '" + testData.getData("Title") + "' from 'Title' options.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered 'text fields' .");


        //Save to continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save()))
        {
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save()))
        {
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {

                error = "Failed to save record.";
                return false;        
        }

        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Individual_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Individual_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
  
        return true;
    }

}
