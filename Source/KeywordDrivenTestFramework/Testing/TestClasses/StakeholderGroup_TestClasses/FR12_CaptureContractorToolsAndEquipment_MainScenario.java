/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR12 – Capture Contractor Tools and Equipment Main Scenario",
        createNewBrowserInstance = false
)

public class FR12_CaptureContractorToolsAndEquipment_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR12_CaptureContractorToolsAndEquipment_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CaptureContractor())
        {
            return narrator.testFailed("Capture Contractor Tools and Equipment Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Contractor Tools and Equipment");
    }

    public boolean CaptureContractor()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.AddToolsEquipmentTab()))
        {
            error = "Failed to wait for Tools & Equipment Tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.AddToolsEquipmentTab()))
        {
            error = "Failed to click Tools & Equipment Tab";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.AddToolsEquipment()))
        {
            error = "Failed to wait for Add Tools & Equipment button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.AddToolsEquipment()))
        {
            error = "Failed to click Add Tools & Equipment button";
            return false;
        }

        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Tools_processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Tools_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }


        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.DescriptionOfEquipment()))
        {
            error = "Failed to wait for Vehicle type / make";
            return false;

        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.DescriptionOfEquipment(), getData("Description of equipment")))
        {
            error = "Failed to enter Description of equipment";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered Description of equipment"+ getData("Description of equipment"));
        

       


        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Tools_save()))
        {
            error = "Failed to wait for Save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Tools_save()))
        {
            error = "Failed to click Save";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;
    }
}
