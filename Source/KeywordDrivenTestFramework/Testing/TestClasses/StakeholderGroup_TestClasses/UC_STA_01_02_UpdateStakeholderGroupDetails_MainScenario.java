/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "UC STA 01 02  Main Scenario",
        createNewBrowserInstance = false
)

public class UC_STA_01_02_UpdateStakeholderGroupDetails_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public UC_STA_01_02_UpdateStakeholderGroupDetails_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 2500);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest()
    {

        if (!UpdateStakeholderGroupDetails())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }

    public boolean UpdateStakeholderGroupDetails()
    {
        //Stakeholder Details
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StakeholderDetailsTab()))
        {
            error = "Failed to wait for Stakeholder Details Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.StakeholderDetailsTab()))
        {
            error = "Failed to click on Stakeholder Details Tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Stakeholder Details");
        // Phone number
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.PhoneNumber()))
//        {
//            error = "Failed to wait for Phone number field.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.PhoneNumber(), getData("Phone number")))
//        {
//            error = "Failed to enter Phone number :" + getData("Phone number");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Phone number : '" + getData("Phone number"));

        //Street address
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StreetAddress()))
        {
            error = "Failed to wait for Street address field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.StreetAddress(), getData("Phone number")))
        {
            error = "Failed to enter Street address :" + getData("Phone number");
            return false;
        }
        narrator.stepPassedWithScreenShot("Street address : '" + getData("Phone number"));

        //Email address
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.EmailAddress()))
//        {
//            error = "Failed to wait for Email address field.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.EmailAddress(), getData("Email address")))
//        {
//            error = "Failed to enter Email address :" + getData("Email address");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Email address : '" + getData("Email address"));
        //Street Address
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StreetAddress()))
        {
            error = "Failed to wait for Street address field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.StreetAddress(), getData("Street address")))
        {
            error = "Failed to enter Street address :" + getData("Street address");
            return false;
        }
        narrator.stepPassedWithScreenShot("Street address : '" + getData("Street address"));

        //Zip/Postal code
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ZipPostalCode()))
        {
            error = "Failed to wait for Zip/Postal code field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.ZipPostalCode(), getData("Zip/Postal code")))
        {
            error = "Failed to enter Zip/Postal code :" + getData("Zip/Postal code");
            return false;
        }
        narrator.stepPassedWithScreenShot("Zip/Postal code : '" + getData("Zip/Postal code"));

        //Location
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.LocationDropDown()))
        {
            error = "Failed to wait for Location drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.LocationDropDown()))
        {
            error = "Failed to click Location drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Location"))))
        {
            error = "Failed to wait for Location option :" + getData("Location");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Location"))))
        {
            error = "Failed to select Location option :" + getData("Location");
            return false;
        }

        narrator.stepPassedWithScreenShot("Location : '" + getData("Location"));

        //Phone number
        if (SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.PhoneNumber()))
        {
            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.PhoneNumber(), getData("Phone number")))
            {
                error = "Failed to enter Phone number :" + getData("Phone number");
                return false;
            }
            narrator.stepPassedWithScreenShot("Phone number : '" + getData("Phone number"));

        }

        //Email address
        if (SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.EmailAddress()))
        {
            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.EmailAddress(), getData("Email address")))
            {
                error = "Failed to enter Email address :" + getData("Email address");
                return false;
            }
            narrator.stepPassedWithScreenShot("Email address : '" + getData("Email address"));

        }

        if (getData("Save").equalsIgnoreCase("True"))
        {
            //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SaveButton()))
            {
                error = "Failed to wait for Save button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SaveButton()))
            {
                error = "Failed to click Save button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
            String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
            String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record saved");

        }

        if (getData("Execute").equalsIgnoreCase("True"))
        {
            //Correspondence Address
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.CorrespondenceAddressDropDown()))
            {
                error = "Failed to wait for Correspondence Address drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.CorrespondenceAddressDropDown()))
            {
                error = "Failed to click Correspondence Address drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.CorrespondenceAddress()))
            {
                error = "Failed to wait for Correspondence address field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.CorrespondenceAddress(), getData("Correspondence address")))
            {
                error = "Failed to enter Correspondence address :" + getData("Correspondence address");
                return false;
            }
            narrator.stepPassedWithScreenShot("Correspondence address : '" + getData("Correspondence address"));

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ZipPostalCode2()))
            {
                error = "Failed to wait for Zip/Postal code field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.ZipPostalCode2(), getData("Zip/Postal code")))
            {
                error = "Failed to enter Zip/Postal code :" + getData("Zip/Postal code");
                return false;
            }
            narrator.stepPassedWithScreenShot("Zip/Postal code : '" + getData("Zip/Postal code"));

            //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SaveButton()))
            {
                error = "Failed to wait for Save button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SaveButton()))
            {
                error = "Failed to click Save button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
            String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
            String record[] = acionRecord.split(" ");
            Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
            String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record saved");

        }

        return true;
    }

}
