/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR8 – Capture Contractor Orders Main Scenario",
        createNewBrowserInstance = false
)

public class FR8_CaptureContractorOrders_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR8_CaptureContractorOrders_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CaptureContractorOrders())
        {
            return narrator.testFailed("Capture Contractor Orders Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Contractor Orders");
    }

    public boolean CaptureContractorOrders()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.OrdersDropDown()))
        {
            error = "Failed to wait for Orders Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.OrdersDropDown()))
        {
            error = "Failed to click Orders Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.OrdersAddButton()))
        {
            error = "Failed to wait for Orders add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.OrdersAddButton()))
        {
            error = "Failed to click Orders add button";
            return false;
        }

        pause(5000);
        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ProcessFlow1()))
        {
            error = "Failed to locate process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ProcessFlow1()))
        {
            error = "Failed to click on process flow button";
            return false;
        }

        //Order status
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.OrderStatusDropDown()))
        {
            error = "Failed to wait Order status";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.OrderStatusDropDown()))
        {
            error = "Failed to click Order status Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Order status")))
        {
            error = "Failed to enter Document uploaded :" + getData("Document uploaded");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Order status"))))
        {
            error = "Failed to wait for Order statusdrop down option : " + getData("Order status");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Order status"))))
        {
            error = "Failed to click Order status drop down option : " + getData("Order status");
            return false;
        }

        narrator.stepPassedWithScreenShot("Order status :" + getData("Order status"));

        //Approved by 
        if (getData("Order status").equalsIgnoreCase("Approved"))
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ApprovedByDropDown()))
            {
                error = "Failed to wait for Approved by Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ApprovedByDropDown()))
            {
                error = "Failed to click Approved by Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait Approved by";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Approved by")))
            {
                error = "Failed to enter Approved by :" + getData("Approved by");
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Approved by"))))
            {
                error = "Failed to wait for Approved by drop down option : " + getData("Approved by");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Approved by"))))
            {
                error = "Failed to click Approved by drop down option : " + getData("Approved by");
                return false;
            }

            narrator.stepPassedWithScreenShot("Approved by :" + getData("Approved by"));

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.DateApproved()))
            {
                error = "Failed to wait Date approved";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.DateApproved(), startDate))
            {
                error = "Failed to enter Date approved :" + startDate;
                return false;
            }
        }

        //Scope of Work
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ScopeOfWork_Tab()))
        {
            error = "Failed to wait for Scope of Work tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ScopeOfWork_Tab()))
        {
            error = "Failed to click on Scope of Work tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Scope of Work tab.");

        //Area where work will be conducted
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.AreaWhereWorkWillBeConductedDropDown()))
        {
            error = "Failed to wait for Area where work will be conducted Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.AreaWhereWorkWillBeConductedDropDown()))
        {
            error = "Failed to click Area where work will be conducted Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait Approved by";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch(), getData("Business unit option")))
        {
            error = "Failed to enter Area where work will be conducted :" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.businessUnitexpandButton()))
        {
            error = "Failed to wait to expand Business unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.businessUnitexpandButton()))
        {
            error = "Failed to expand Business unit";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Business unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Business unit option"))))
        {
            error = "Failed to wait for Business unit option option :" + getData("Type");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Business unit option"))))
        {
            error = "Failed to select Business unit option option :" + getData("Business unit option");
            return false;
        }

        //Project
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Project()))
        {
            error = "Failed to wait Project";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.Project(), getData("Project")))
        {
            error = "Failed to enter Project :" + startDate;
            return false;
        }

        //Start date
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StartDate()))
        {
            error = "Failed to wait Start date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.StartDate(), startDate))
        {
            error = "Failed to enter Start date :" + startDate;
            return false;
        }

        //End date
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.EndDate()))
        {
            error = "Failed to wait End date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.EndDate(), endDate))
        {
            error = "Failed to enter End date :" + endDate;
            return false;
        }

        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SaveButton2()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SaveButton2()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;
    }
}
