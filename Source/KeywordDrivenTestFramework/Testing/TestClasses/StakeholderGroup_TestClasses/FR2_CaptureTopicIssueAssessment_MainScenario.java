/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.Stakeholder_Individual_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR2 - Capture Topic / Issue Assessment Main Scenario",
        createNewBrowserInstance = false
)
public class FR2_CaptureTopicIssueAssessment_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR2_CaptureTopicIssueAssessment_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!CaptureStakeholderAnalysis())
        {
            return narrator.testFailed(" Capture Topic / Issue Assessment Failed due To :" + error);
        }

        return narrator.finalizeTest("Successfully  Capture Topic / Issue Assessment");
    }

    public boolean CaptureStakeholderAnalysis()
    {

        //Topic/Issue Assessment dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TopicIssueAssessmentDropdown()))
        {
            error = "Failed to wait for Topic/Issue Assessment dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.TopicIssueAssessmentDropdown()))
        {
            error = "Failed to click Topic/Issue Assessment dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TopicIssueAssessmentAdd()))
        {
            error = "Failed to wait for Topic/Issue Assessment Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.TopicIssueAssessmentAdd()))
        {
            error = "Failed to click Topic/Issue Assessment Add Button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Topic/Issue Assessment Add Button");

        // Topic / Issue
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Topic_IssueAssessmentDropdown()))
        {
            error = "Failed to wait for Topic/Issue  dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Topic_IssueAssessmentDropdown()))
        {
            error = "Failed to click Topic/Issue  dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Topic / Issue"))))
        {
            error = "Failed to wait for Topic / Issue drop down option : " + getData("Topic / Issue");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Topic / Issue"))))
        {
            error = "Failed to click Topic / Issue drop down option : " + getData("Topic / Issue");
            return false;
        }

        narrator.stepPassed("Topic / Issue :" + getData("Topic / Issue"));

        //Influence
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.InfluenceDropdown2()))
        {
            error = "Failed to wait for Influence dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.InfluenceDropdown2()))
        {
            error = "Failed to click Influence dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Influence"))))
        {
            error = "Failed to wait for Applicable business units source drop down option : " + getData("Influence");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Influence"))))
        {
            error = "Failed to click Influence drop down option : " + getData("Influence");
            return false;
        }
        narrator.stepPassed("Influence :" + getData("Influence"));

        //Interest
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.InterestDropdown2()))
        {
            error = "Failed to wait for Interest dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.InterestDropdown2()))
        {
            error = "Failed to click Interest dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Interest"))))
        {
            error = "Failed to wait for Interestsource drop down option : " + getData("Interest");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Interest"))))
        {
            error = "Failed to click Interestdrop down option : " + getData("Interest");
            return false;
        }
        narrator.stepPassed("Interest :" + getData("Interest"));

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SaveButton3()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SaveButton3()))
        {
            error = "Failed to click Save button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Successfully saved the record");
        return true;
    }
}
