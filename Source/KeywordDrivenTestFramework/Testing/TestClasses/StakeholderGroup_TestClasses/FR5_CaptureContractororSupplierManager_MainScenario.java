/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR5 – Capture Contractor or Supplier Manager Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_CaptureContractororSupplierManager_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR5_CaptureContractororSupplierManager_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {

        if (!CaptureAssociatedGroups())
        {
            return narrator.testFailed("Capture Contractor or Supplier Manager Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Associated Groups");
    }

    public boolean CaptureAssociatedGroups()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ContractororSupplierManager_Tab()))
        {
            error = "Failed to wait for Contractor or Supplier Manager tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ContractororSupplierManager_Tab()))
        {
            error = "Failed to click on Contractor or Supplier Manager tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Contractor or Supplier Manager tab.");

        //Group  Name 
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StatusDropDown()))
        {
            error = "Failed to wait for Status Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.StatusDropDown()))
        {
            error = "Failed to click Status Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Status")))
        {
            error = "Failed to enter Status :" + getData("Status");
            return false;
        }

      if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Status"))))
        {
            error = "Failed to wait for Status drop down option : " + getData("Status");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Status"))))
        {
            error = "Failed to click Status drop down option : " + getData("Status");
            return false;
        }

        narrator.stepPassedWithScreenShot("Status :"+getData("Status"));
        
        
        //Main contact
           if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.MainContact()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.MainContact(), getData("Main contact")))
        {
            error = "Failed to enter Main contact  :" + getData("Main contact");
            return false;
        }
        
        //Main contact phone
           if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.MainContactPhone()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.MainContactPhone(), getData("Main contact phone")))
        {
            error = "Failed to enter Main contact phone :" + getData("Main contact phone");
            return false;
        }
        
        //Main contact email
           if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.MainContactEmail()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.MainContactEmail(), getData("Main contact email")))
        {
            error = "Failed to enter Main contact email :" + getData("Main contact email");
            return false;
        }
        

        //Main contact individual
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.MainContactIndividualDropDown()))
        {
            error = "Failed to wait for Main contact individual Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.MainContactIndividualDropDown()))
        {
            error = "Failed to click Main contact individual Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch(), getData("Main contact individual")))
        {
            error = "Failed to enter Main contact individual :" + getData("Main contact individual");
            return false;
        }

       if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Main contact individual"))))
        {
            error = "Failed to wait for Main contact individual drop down option : " + getData("Main contact individual");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Main contact individual"))))
        {
            error = "Failed to click Main contact individual drop down option : " + getData("Main contact individual");
            return false;
        }

        narrator.stepPassedWithScreenShot("Main contact individual :"+ getData("Main contact individual"));
        
        

        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SaveButton2()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SaveButton2()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");
        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());


        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
       String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");


        return true;
    }
}
