/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR6 – Capture Contractor Questionnaire Main Scenario",
        createNewBrowserInstance = false
)

public class FR6_CaptureContractorQuestionnaire_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR6_CaptureContractorQuestionnaire_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {

        if (!CaptureAssociatedGroups())
        {
            return narrator.testFailed("Capture Contractor Questionnaire Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Contractor Questionnaire");
    }

    public boolean CaptureAssociatedGroups()
    {
   

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.QuestionnaireDropDown()))
        {
            error = "Failed to wait for Questionnaire Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.QuestionnaireDropDown()))
        {
            error = "Failed to click Questionnaire Drop Down";
            return false;
        }
        
          if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.QuestionnaireAddButton()))
        {
            error = "Failed to wait for Questionnaire button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.QuestionnaireAddButton()))
        {
            error = "Failed to click Questionnaire button";
            return false;
        }
        
         //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ProcessFlowQ()))
        {
            error = "Failed to locate process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ProcessFlowQ()))
        {
            error = "Failed to click on process flow button";
            return false;
        }
        
        //Provide your relevant Stakeholder group's Registration Number
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.RelevantStakeholder()))
        {
            error = "Failed to wait Provide your relevant Stakeholder group's Registration Number";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.RelevantStakeholder(), getData("Provide your relevant Stakeholder group's Registration Number")))
        {
            error = "Failed to enter Provide your relevant Stakeholder group's Registration Number :" + getData("Provide your relevant Stakeholder group's Registration Number");
            return false;
        }


        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SaveButton2()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SaveButton2()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());


        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
       String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");


        return true;
    }
}
