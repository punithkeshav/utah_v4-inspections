/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR4 Capture Associated Groups Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_CaptureAssociatedGroups_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR4_CaptureAssociatedGroups_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {

        if (!CaptureAssociatedGroups())
        {
            return narrator.testFailed("Capture Associated Groups Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Associated Groups");
    }

    public boolean CaptureAssociatedGroups()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Members_Tab()))
        {
            error = "Failed to wait for Associated Stakeholder Groups tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.AssociatedStakeholderGroups_Tab()))
        {
            error = "Failed to click on Associated Stakeholder Groups tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Associated Stakeholder Groups tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.AssociatedStakeholderGroupsAdd()))
        {
            error = "Failed to wait for AssociatedStakeholderGroups Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.AssociatedStakeholderGroupsAdd()))
        {
            error = "Failed to click Members AssociatedStakeholderGroups Button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully AssociatedStakeholderGroups Add Button");

        //Group  Name 
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.GroupNameDropDown()))
        {
            error = "Failed to wait for Group  Name Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.GroupNameDropDown()))
        {
            error = "Failed to click Group Name Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch(), getData("Group name")))
        {
            error = "Failed to enter Group name :" + getData("Group name");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Group name"))))
        {
            error = "Failed to wait for Group name drop down option : " + getData("Group name");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Group name"))))
        {
            error = "Failed to click Group name drop down option : " + getData("Group name");
            return false;
        }

        narrator.stepPassedWithScreenShot("Group name");
        
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.RelationshipDropDown()))
        {
            error = "Failed to wait for Relationship Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.RelationshipDropDown()))
        {
            error = "Failed to click Relationship Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Relationship")))
        {
            error = "Failed to enter Relationship :" + getData("Relationship");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Relationship"))))
        {
            error = "Failed to wait for Relationship drop down option : " + getData("Relationship");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Relationship"))))
        {
            error = "Failed to click Relationship drop down option : " + getData("Relationship");
            return false;
        }

        narrator.stepPassedWithScreenShot("Group name");

        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SaveButton2()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SaveButton2()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());


        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
       String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        narrator.stepPassedWithScreenShot("Recored Edited");

        return true;
    }
}
