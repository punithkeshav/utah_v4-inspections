/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR22 - Edit Stakeholder Group MainScenario",
        createNewBrowserInstance = false
)
public class FR22_EditStakeholderGroup_MainScenario extends BaseClass
{

    String error = "";

    public FR22_EditStakeholderGroup_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!SearchRecord())
        {
            return narrator.testFailed("Failed to edit record " + error);
        }

        return narrator.finalizeTest("Successfully edited Waste Management record");
    }

    public boolean SearchRecord() throws InterruptedException
    {

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.AddButton()))
        {
            error = "Failed to locate add button";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter contains to search";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

                pause(6000);

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ProcessFlow()))
        {
            error = "Failed to locate process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ProcessFlow()))
        {
            error = "Failed to click on process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.GroupName()))
        {
            error = "Failed to wait for Group name";
            return false;

        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.GroupName(), getData("Group name")))
        {
            error = "Failed to enter Group name :" + getData("Group name");
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered stakeholder group name :" + getData("Group name"));

        //Stakeholder categorie
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StakeholdCategoriDropDown()))
        {
            error = "Failed to wait for Stakeholder categorie.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.StakeholdCategoriDropDown()))
        {
            error = "Failed to click Stakeholder Categorie .";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Stakeholder Categorie")))

        {
            error = "Failed to enter Stakeholder group option :" + getData("Stakeholder Categorie");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Stakeholder Categorie"))))
        {
            error = "Failed to wait for Stakeholder Categorie option :" + getData("Stakeholder Categorie");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Stakeholder Categorie"))))
        {
            error = "Failed to select Stakeholder Categorie option :" + getData("Stakeholder Categorie");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.StakeholdCategoriDropDown()))
        {
            error = "Failed to click Stakeholder Categorie .";
            return false;
        }

        narrator.stepPassedWithScreenShot("Stakeholder Categorie :" + getData("Stakeholder Categorie"));

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SaveButton()))
        {
            error = "Failed to wait for Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        narrator.stepPassedWithScreenShot("Recored Edited");
        pause(5000);

        return true;
    }

}
