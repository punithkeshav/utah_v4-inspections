/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "UC STE 01 03 Main Scenario",
        createNewBrowserInstance = false
)

public class UCSTE0103_ConductStakeholderGroupAnalysis_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public UCSTE0103_ConductStakeholderGroupAnalysis_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!updateStakeholderAnalysis())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder analysis");
    }

    public boolean updateStakeholderAnalysis()
    {

        //Stakeholder Analysis
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StakeholerAnalysis_Tab()))
        {
            error = "Failed to wait for StakeHolder Analysis Tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.StakeholerAnalysis_Tab()))
        {
            error = "Failed to click StakeHolder Analysis Tab";
            return false;
        }

        //Interest dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.InterestDropdown()))
        {
            error = "Failed to wait for Interest dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.InterestDropdown()))
        {
            error = "Failed to click Interest dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Stakeholder interest"))))
        {
            error = "Failed to wait for Interest dropdown drop down option : " + getData("Stakeholder interest");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Stakeholder interest"))))
        {
            error = "Failed to click Interest dropdown drop down option : " + getData("Stakeholder interest");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Interest dropdown");

        //Influence dropdown
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.InfluenceDropdown()))
//        {
//            error = "Failed to wait for influence dropdown";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.InfluenceDropdown()))
//        {
//            error = "Failed to click influence dropdown";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Stakeholder influence"))))
//        {
//            error = "Failed to wait for Stakeholder influence option : " + getData("Stakeholder influence");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Stakeholder influence"))))
//        {
//            error = "Failed to click IStakeholder influence option : " + getData("Stakeholder influence");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully Influence dropdown");
//        //
//        //Stakeholder support
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StakeholderSupportDD()))
//        {
//            error = "Failed to wait for influence dropdown";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.StakeholderSupportDD()))
//        {
//            error = "Failed to click influence dropdown";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Stakeholder support"))))
//        {
//            error = "Failed to wait for Stakeholder support: " + getData("Stakeholder support");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Stakeholder support"))))
//        {
//            error = "Failed to click Stakeholder support : " + getData("Stakeholder support");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully captured stakeholder support");

        //Comments
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StakeholderAnalysisComments()))
        {
            error = "Failed to wait for influence dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.StakeholderAnalysisComments(), getData("Comments")))
        {
            error = "Failed to click influence dropdown";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully captured comments");

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SaveButton2()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SaveButton2()))
        {
            error = "Failed to click Save button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Successfully saved the record");

        if (getData("Execute").equalsIgnoreCase("True"))
        {
            //Guidelines
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.GuidelinesDropDown()))
            {
                error = "Failed to wait for Guidelines drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.GuidelinesDropDown()))
            {
                error = "Failed to click Guidelines drop down";
                return false;
            }

           
            if (!SeleniumDriverInstance.scrollToElement("//div[text()='Stakeholder Analysis Matrix']"))
            {
                error = "Failed to scroll downn";
                return false;
            }
           

            narrator.stepPassedWithScreenShot("Guidelines");

        }
        return true;
    }

}
