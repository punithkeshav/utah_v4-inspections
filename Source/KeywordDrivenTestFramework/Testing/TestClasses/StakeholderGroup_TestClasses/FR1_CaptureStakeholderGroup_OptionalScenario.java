/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR1_CaptureStakeholderGroup_OptionalScenario",
        createNewBrowserInstance = false
)

public class FR1_CaptureStakeholderGroup_OptionalScenario extends BaseClass
{

    String error = "";
    private String textbox;

    public FR1_CaptureStakeholderGroup_OptionalScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!uploadSupportingDocuments())
        {
            return narrator.testFailed("Failed to upload Supporting Documents: " + error);
        }
        return narrator.finalizeTest("Successfully uploaded Supporting Documents");
    }

    public boolean uploadSupportingDocuments() throws InterruptedException
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to wait for Supporting Documents Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to click on Supporting Documents Tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Supporting Documents Tab");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.uploadLinkbox()))
        {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.uploadLinkbox()))
        {
            error = "Failed to click on 'Link box' link.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.LinkURL()))
        {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.LinkURL(), getData("Document Link")))
        {
            error = "Failed to enter Document Link :" + getData("Document Link");
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : " + getData("Document Link"));

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.urlTitle()))
        {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.urlTitle(), getData("Title")))
        {
            error = "Failed to enter Title :" + getData("Title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title"));

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.urlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.urlAddButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Group_PageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
            return false;

        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");

        //Stakeholder Details
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StakeholderDetailsTab()))
        {
            error = "Failed to wait for Stakeholder Details Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.StakeholderDetailsTab()))
        {
            error = "Failed to click on Stakeholder Details Tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Stakeholder Details");
        // Phone number
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.PhoneNumber()))
        {
            error = "Failed to wait for Phone number field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.PhoneNumber(), getData("Phone number")))
        {
            error = "Failed to enter Phone number :" + getData("Phone number");
            return false;
        }
        narrator.stepPassedWithScreenShot("Phone number : '" + getData("Phone number"));

        //Email address
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.EmailAddress()))
        {
            error = "Failed to wait for Email address field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.EmailAddress(), getData("Email address")))
        {
            error = "Failed to enter Email address :" + getData("Email address");
            return false;
        }
        narrator.stepPassedWithScreenShot("Email address : '" + getData("Email address"));

        //Street Address
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StreetAddress()))
        {
            error = "Failed to wait for Street address field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.StreetAddress(), getData("Street address")))
        {
            error = "Failed to enter Street address :" + getData("Street address");
            return false;
        }
        narrator.stepPassedWithScreenShot("Street address : '" + getData("Street address"));

        //Zip/Postal code
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ZipPostalCode()))
        {
            error = "Failed to wait for Zip/Postal code field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.ZipPostalCode(), getData("Zip/Postal code")))
        {
            error = "Failed to enter Zip/Postal code :" + getData("Zip/Postal code");
            return false;
        }
        narrator.stepPassedWithScreenShot("Zip/Postal code : '" + getData("Zip/Postal code"));

        //Location
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.LocationDropDown()))
        {
            error = "Failed to wait for Location drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.LocationDropDown()))
        {
            error = "Failed to click Location drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Location"))))
        {
            error = "Failed to wait for Location option :" + getData("Location");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Location"))))
        {
            error = "Failed to select Location option :" + getData("Location");
            return false;
        }
   

        narrator.stepPassedWithScreenShot("Location : '" + getData("Location"));

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SaveButton()))
        {
            error = "Failed to wait for Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;

    }

}
