/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR9 – Capture Contractor Risk Assessment Main Scenario",
        createNewBrowserInstance = false
)

public class FR9_CaptureContractorRiskAssessment_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR9_CaptureContractorRiskAssessment_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CaptureContractorOrders())
        {
            return narrator.testFailed("Capture Contractor Risk Assessment Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Contractor Risk Assessment");
    }

    public boolean CaptureContractorOrders()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ContractorRiskAssessmentTab()))
        {
            error = "Failed to wait for Contractor Risk Assessment Tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ContractorRiskAssessmentTab()))
        {
            error = "Failed to click Contractor Risk Assessment Tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ContractorRiskAssessmentAddButton()))
        {
            error = "Failed to wait for Contractor Risk Assessment add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ContractorRiskAssessmentAddButton()))
        {
            error = "Failed to click Contractor Risk Assessment add button";
            return false;
        }

        //process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ContractorRiskAssessmentProcessFlow()))
        {
            error = "Failed to locate process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ContractorRiskAssessmentProcessFlow()))
        {
            error = "Failed to click on process flow button";
            return false;
        }

        //Capturer
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.CapturerDropDown()))
        {
            error = "Failed to Capturer";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.CapturerDropDown()))
        {
            error = "Failed to click Capturer Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch(), getData("Capturer")))
        {
            error = "Failed to enter Capturer :" + getData("Capturer");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Capturer"))))
        {
            error = "Failed to wait for Capturer drop down option : " + getData("Capturer");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Capturer"))))
        {
            error = "Failed to click Capturer drop down option : " + getData("Capturer");
            return false;
        }

        narrator.stepPassedWithScreenShot("Capturer :" + getData("Capturer"));

        //Risk ranking date
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.RiskRankingDate()))
        {
            error = "Failed to wait Risk ranking date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.RiskRankingDate(), startDate))
        {
            error = "Failed to enter Risk ranking date :" + startDate;
            return false;
        }

        //Description of scope
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.DescriptionOfScope()))
        {
            error = "Failed to wait Description of scope";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.DescriptionOfScope(), getData("Description of scope")))
        {
            error = "Failed to enter Description of scope :" + startDate;
            return false;
        }

        //Blasting activities including explosives
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.BlastingActivitiesIncludingExplosivesDropDown()))
        {
            error = "Failed to wait for Blasting activities including explosivesDrop Down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.BlastingActivitiesIncludingExplosivesDropDown()))
        {
            error = "Failed to click Blasting activities including explosives Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait Blasting activities including explosives";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Blasting activities including explosives")))
        {
            error = "Failed to enter Blasting activities including explosives :" + getData("Blasting activities including explosives");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Blasting activities including explosives"))))
        {
            error = "Failed to wait for Blasting activities including explosives drop down option : " + getData("Blasting activities including explosives");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Blasting activities including explosives"))))
        {
            error = "Failed to click Blasting activities including explosives drop down option : " + getData("Blasting activities including explosives");
            return false;
        }

        narrator.stepPassedWithScreenShot("Blasting activities including explosives :" + getData("Blasting activities including explosives"));

        //Community impact
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.CommunityImpactDropDown()))
        {
            error = "Failed to wait for Community impact Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.CommunityImpactDropDown()))
        {
            error = "Failed to click Community impact Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait Community impact";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Community impact")))
        {
            error = "Failed to enter Community impact :" + getData("Community impact");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Community impact"))))
        {
            error = "Failed to wait for Community impact drop down option : " + getData("Community impact");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Community impact"))))
        {
            error = "Failed to click Community impact drop down option : " + getData("Community impact");
            return false;
        }

        narrator.stepPassedWithScreenShot("Community impact :" + getData("Community impact"));

        if (getData("Execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.RelatedDocumentsTab()))
            {
                error = "Failed to wait for Related Documents Tab ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.RelatedDocumentsTab()))
            {
                error = "Failed to click Related Documents Tab";

            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.uploadLinkbox()))
            {
                error = "Failed to wait for 'Link box' link.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.uploadLinkbox()))
            {
                error = "Failed to click on 'Link box' link.";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

            //switch to new window
            if (!SeleniumDriverInstance.switchToTabOrWindow())
            {
                error = "Failed to switch to new window or tab.";
                return false;
            }

            //URL https
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.LinkURL()))
            {
                error = "Failed to wait for 'URL value' field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.LinkURL(), getData("Document Link")))
            {
                error = "Failed to enter Document Link :" + getData("Document Link");
                return false;
            }
            narrator.stepPassedWithScreenShot("Document link : " + getData("Document Link"));

            //Title
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.urlTitle()))
            {
                error = "Failed to wait for 'Url Title' field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.urlTitle(), getData("Title")))
            {
                error = "Failed to enter Title :" + getData("Title");
                return false;
            }
            narrator.stepPassedWithScreenShot("Title : '" + getData("Title"));

            //Add button
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.urlAddButton()))
            {
                error = "Failed to wait for 'Add' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.urlAddButton()))
            {
                error = "Failed to click on 'Add' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
            narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

            if (!SeleniumDriverInstance.switchToTabOrWindow())
            {
                error = "Failed to switch tab.";
                return false;
            }
            if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Group_PageObjects.iframeName()))
            {
                error = "Failed to switch to frame ";
                return false;

            }
            narrator.stepPassedWithScreenShot("Successfully switched the iframe.");

        }

        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.RiskAssSave()))
        {
            error = "Failed to wait for Save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.RiskAssSave()))
        {
            error = "Failed to click Save button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;
    }
}
