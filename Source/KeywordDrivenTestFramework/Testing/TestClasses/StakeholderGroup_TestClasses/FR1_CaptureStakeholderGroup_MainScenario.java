/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR1 - Capture Stakeholder Group MainScenario",
        createNewBrowserInstance = false
)

public class FR1_CaptureStakeholderGroup_MainScenario extends BaseClass
{

    String error = "";

    public FR1_CaptureStakeholderGroup_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!StakeholderGroup())
        {
            return narrator.testFailed("Failed to navigate to a module Stakeholder Group :" + error);
        }
        if (!StakeholderGroupRecord())
        {
            return narrator.testFailed("Failed to add Stakeholder Group :" + error);
        }
        return narrator.finalizeTest("Successfully added Stakeholder Group record");
    }

    public boolean StakeholderGroup()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StakeholderGroupTab()))
        {
            error = "Failed to locate the module : Stakeholder Group ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.StakeholderGroupTab()))
        {
            error = "Failed to navigate to the module : Stakeholder Group ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Stakeholder Group Tab Clicked Successfully");

        return true;

    }

    public boolean StakeholderGroupRecord() throws InterruptedException
    {
        //Add button

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.AddButton()))
        {
            error = "Failed to locate Stakeholder Group add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.AddButton()))
        {
            error = "Failed to click on Stakeholder Group add button";
            return false;
        }

        pause(5000);
        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ProcessFlow()))
        {
            error = "Failed to locate process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ProcessFlow()))
        {
            error = "Failed to click on process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.GroupName()))
        {
            error = "Failed to wait for Group name";
            return false;

        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.GroupName(), getData("Group name")))
        {
            error = "Failed to enter Group name :" + getData("Group name");
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered stakeholder group name :" + getData("Group name"));

        //Stakeholder categorie
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.StakeholdCategoriDropDown()))
        {
            error = "Failed to wait for Stakeholder categorie.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.StakeholdCategoriDropDown()))
        {
            error = "Failed to click Stakeholder Categorie .";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Stakeholder Categorie")))

        {
            error = "Failed to enter Stakeholder group option :" + getData("Stakeholder Categorie");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (getData("Stakeholder Categorie").equalsIgnoreCase("Contractor"))
        {
            //businessUnitexpandButton

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.businessUnitexpandButton()))
            {
                error = "Failed to wait for expand drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.businessUnitexpandButton()))
            {
                error = "Failed to expand drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Stakeholder Categorie"))))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.businessUnitexpandButton()))
                {
                    error = "Failed to wait for Stakeholder Categorie option :" + getData("Stakeholder Categorie");
                    return false;
                }
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Stakeholder Categorie"))))
            {
                error = "Failed to select Stakeholder Categorie option :" + getData("Stakeholder Categorie");
                return false;
            }
            pause(2000);

            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.StakeholdCategoriDropDown()))
            {
                error = "Failed to click Stakeholder Categorie .";
                return false;
            }

            narrator.stepPassedWithScreenShot("Stakeholder Categorie :" + getData("Stakeholder Categorie"));

        } else
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Stakeholder Categorie"))))
            {
                error = "Failed to wait for Stakeholder Categorie option :" + getData("Stakeholder Categorie");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Stakeholder Categorie"))))
            {
                error = "Failed to select Stakeholder Categorie option :" + getData("Stakeholder Categorie");
                return false;
            }
            pause(2000);

            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.StakeholdCategoriDropDown()))
            {
                error = "Failed to click Stakeholder Categorie .";
                return false;
            }

            narrator.stepPassedWithScreenShot("Stakeholder Categorie :" + getData("Stakeholder Categorie"));
        }

        //Applicable business units
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ApplicableBusinessUnitsDropDown()))
        {
            error = "Failed to wait for Applicable business units drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ApplicableBusinessUnitsDropDown()))
        {
            error = "Failed to click Applicable business units drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Applicable business units"))))
        {

            error = "Failed to wait for Applicable business units option :" + getData("Applicable business units");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Applicable business units"))))
        {
            error = "Failed to click Applicable business units option :" + getData("Applicable business units");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Applicable business units 1"))))
        {
            error = "Failed to wait for Applicable business units option :" + getData("Applicable business units 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Applicable business units 1"))))
        {
            error = "Failed to click Applicable business units option :" + getData("Applicable business units 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Applicable business units 2"))))
        {
            error = "Failed to wait for Applicable business units option :" + getData("Applicable business units 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Applicable business units 2"))))
        {
            error = "Failed to select Applicable business units option :" + getData("Applicable business units 2");
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Applicable business units 3"))))
        {
            error = "Failed to wait for Applicable business units option :" + getData("Applicable business units 3");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Applicable business units 3"))))
        {
            error = "Failed to select Applicable business units option :" + getData("Applicable business units 3");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ApplicableBusinessUnitsDropDown()))
        {
            error = "Failed to click Applicable business units drop down";
            return false;
        }

        //Responsible owner
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ResponsibleOwnersDropDown()))
//        {
//            error = "Failed to wait for Responsible person drop down";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ResponsibleOwnersDropDown()))
//        {
//            error = "Failed to click Responsible person drop down";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch()))
//        {
//            error = "Failed to wait for Responsible person text box.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch(), getData("Responsible owners")))
//        {
//            error = "Failed to enter Responsible owners option :" + getData("Responsible owners");
//            return false;
//        }
//        if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch()))
//        {
//            error = "Failed to press enter";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Responsible owners"))))
//        {
//            error = "Failed to wait for Responsible owners drop down option : " + getData("Responsible owners");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Responsible owners"))))
//        {
//            error = "Failed to click Responsible owners drop down option : " + getData("Responsible owners");
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Responsible owners  :" + getData("Responsible owners"));
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ResponsibleOwnersDropDown()))
//        {
//            error = "Failed to click Responsible person drop down";
//            return false;
//        }
//
//        //Accountable owner
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.AccountableOwnerDropDown()))
//        {
//            error = "Failed to wait for Accountable owner drop down";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.AccountableOwnerDropDown()))
//        {
//            error = "Failed to click Accountable owner drop down";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch()))
//        {
//            error = "Failed to wait for Accountable owner text box.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch(), getData("Accountable owner")))
//        {
//            error = "Failed to enter Accountable owner option :" + getData("Accountable owners");
//            return false;
//        }
//        if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch()))
//        {
//            error = "Failed to press enter";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Accountable owner"))))
//        {
//            error = "Failed to wait for Accountable owner drop down option : " + getData("Accountable owner");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Accountable owner"))))
//        {
//            error = "Failed to click Accountable owner drop down option : " + getData("Accountable owner");
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Accountable owner  :" + getData("Accountable owner"));

//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.IndustryOwnerDropDown()))
//        {
//            error = "Failed to wait for Industry drop down";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.IndustryOwnerDropDown()))
//        {
//            error = "Failed to click Industry drop down";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for Accountable owner text box.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Industry")))
//        {
//            error = "Failed to enter Industry option :" + getData("Industry");
//            return false;
//        }
//        if (!SeleniumDriverInstance.pressEnter())
//        {
//            error = "Failed to press enter";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Industry"))))
//        {
//            error = "Failed to wait for Industry drop down option : " + getData("Industry");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Industry"))))
//        {
//            error = "Failed to click Industry  drop down option : " + getData("Industry");
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Industry   :" + getData("Industry"));
        //Group description
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.GroupDescription()))
        {
            error = "Failed to wait for Group description  text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.GroupDescription(), getData("Group description")))
        {
            error = "Failed to enter Group description :" + getData("Group description");
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Applicable business units 1"))))
//        {
//            error = "Failed to wait for Applicable business units option :" + getData("Applicable business units 1");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Applicable business units 1"))))
//        {
//            error = "Failed to select Applicable business units option :" + getData("Applicable business units 1");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Applicable business units 2"))))
//        {
//            error = "Failed to wait for Applicable business units option :" + getData("Applicable business units 2");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Applicable business units 2"))))
//        {
//            error = "Failed to select Applicable business units option :" + getData("Applicable business units 2");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Applicable business units 3"))))
//        {
//            error = "Failed to wait for Applicable business units option :" + getData("Applicable business units 3");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.TypeSearchCheckBox(getData("Applicable business units3"))))
//        {
//            error = "Failed to select Applicable business units option :" + getData("Applicable business units 3");
//            return false;
//        }
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SaveButton()))
        {
            error = "Failed to wait for Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");
        return true;

    }

}
