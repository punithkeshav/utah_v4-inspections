/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR7 – Capture Contractor Documents Main Scenario",
        createNewBrowserInstance = false
)

public class FR7_CaptureContractorDocuments_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR7_CaptureContractorDocuments_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {

        if (!CaptureContractorDocuments())
        {
            return narrator.testFailed("Capture Contractor Documents Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Contractor Documents");
    }

    public boolean CaptureContractorDocuments()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.DocumentsDropDown()))
        {
            error = "Failed to wait for Documents Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.DocumentsDropDown()))
        {
            error = "Failed to click Documents Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.DocumentsAddButton()))
        {
            error = "Failed to wait for Documents add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.DocumentsAddButton()))
        {
            error = "Failed to click Documents add button";
            return false;
        }

        //processflow
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ProcessFlowQ()))
//        {
//            error = "Failed to locate process flow button";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ProcessFlowQ()))
//        {
//            error = "Failed to click on process flow button";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.DocumentUploaded()))
        {
            error = "Failed to wait Document uploaded";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.DocumentUploaded()))
        {
            error = "Failed to click Document uploaded Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Document uploaded")))
        {
            error = "Failed to enter Document uploaded :" + getData("Document uploaded");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Document uploaded"))))
        {
            error = "Failed to wait for Document uploaded drop down option : " + getData("Document uploaded");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Document uploaded"))))
        {
            error = "Failed to click Document uploaded drop down option : " + getData("Document uploaded");
            return false;
        }

        narrator.stepPassedWithScreenShot("Document uploaded :" + getData("Document uploaded"));

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Outcome()))
        {
            error = "Failed to click Outcome Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait Outcome";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Outcome")))
        {
            error = "Failed to enter Outcome :" + getData("Outcome");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Outcome"))))
        {
            error = "Failed to wait for Outcome drop down option : " + getData("Outcome");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Outcome"))))
        {
            error = "Failed to click Outcome drop down option : " + getData("Outcome");
            return false;
        }

        narrator.stepPassedWithScreenShot("Outcome :" + getData("Outcome"));

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.DateVerified()))
        {
            error = "Failed to wait Date verified";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.DateVerified(), startDate))
        {
            error = "Failed to enter Date verified :" + startDate;
            return false;
        }

        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SaveButton2()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SaveButton2()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");


        return true;
    }
}
