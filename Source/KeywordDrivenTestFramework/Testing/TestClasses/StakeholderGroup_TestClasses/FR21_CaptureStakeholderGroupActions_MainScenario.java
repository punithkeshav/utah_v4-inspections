/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR21 - Capture Stakeholder Group Actions",
        createNewBrowserInstance = false
)

public class FR21_CaptureStakeholderGroupActions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR21_CaptureStakeholderGroupActions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {

        if (!CaptureStakeholderGroupActions())
        {
            return narrator.testFailed("Capture Stakeholder Group Actions Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Stakeholder Group Actions");
    }

    public boolean CaptureStakeholderGroupActions()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Actions_Tab()))
        {
            error = "Failed to wait for Actions tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Actions_Tab()))
        {
            error = "Failed to click on Actions tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Actions tab.");

        
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ActionsAddButton()))
        {
            error = "Failed to locate Actions add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ActionsAddButton()))
        {
            error = "Failed to click on Actions add button";
            return false;
        }

        pause(5000);
        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ActionsProcessFlow()))
        {
            error = "Failed to locate process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ActionsProcessFlow()))
        {
            error = "Failed to click on process flow button";
            return false;
        }

        
         //Type of action
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to wait for Type of action.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to click Type of action";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Action description text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Type of action")))

        {
            error = "Failed to enter Type of action :" + getData("Type of action");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        //
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to click Project drop down option : " + getData("Type of action");
            return false;
        }
        narrator.stepPassedWithScreenShot("Type of action :" + getData("Type of action"));
        
        
        ///Action Description

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ActionDescription()))
        {
            error = "Failed to wait for Action description text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.ActionDescription(), getData("Action description")))

        {
            error = "Failed to enter Action description :" + getData("Action description");
            return false;
        }

        //Entity
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Entity_Dropdown()))
        {
            error = "Failed to wait for Entity dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Entity_Dropdown()))
        {
            error = "Failed to click Entity dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Business Unit"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to wait to expand Entity";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to expand Entity";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Entity:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity drop down :" + getData("Business unit 1");
            return false;
        }
        
          if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity:" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click Entity drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Entity option  :" + getData("Business unit option"));
        
        
        
        //Responsible person
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ResponsiblePerson_dropdown()))
        {
            error = "Failed to wait for Responsible person dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ResponsiblePerson_dropdown()))
        {
            error = "Failed to click Responsible person dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible person text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch(), getData("Responsible person")))
        {
            error = "Failed to enter Responsible person option :" + getData("Responsible person");
            return false;
        }

       if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to wait forResponsible person options.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person :" + getData("Responsible person");
            return false;
        }

        narrator.stepPassedWithScreenShot("Responsible person :" + getData("Responsible person"));
        
        
        
        
        
        //Action due date

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ActionDueDate()))
        {
            error = "Failed to wait for Action due date input field.";
            return false;

        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.ActionDueDate(), startDate))
        {
            error = "Failed to enter Action due date :" + startDate;
            return false;
        }

        narrator.stepPassedWithScreenShot("Action due date :" + startDate);
        
        

        
        //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Actions_save()))
            {
                error = "Failed to wait for Save button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Actions_save()))
            {
                error = "Failed to click Save button";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
            String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
            String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);

            narrator.stepPassedWithScreenShot("Successfully saved the record");

        return true;
    }
}
