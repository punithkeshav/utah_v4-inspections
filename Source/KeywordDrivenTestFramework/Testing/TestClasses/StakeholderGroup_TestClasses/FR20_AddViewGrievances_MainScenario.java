/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR20 - Add/View Grievances",
        createNewBrowserInstance = false
)

public class FR20_AddViewGrievances_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR20_AddViewGrievances_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {

        if (!AddViewGrievances())
        {
            return narrator.testFailed("Add/View Grievances Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Add/View Grievances");
    }

    public boolean AddViewGrievances()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.AddViewGrievances_Tab()))
        {
            error = "Failed to wait for Add/View Grievances tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.AddViewGrievances_Tab()))
        {
            error = "Failed to click on Add/View Grievances tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Add/View Grievances tab.");

        if (SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.RecordResults()))
        {
            String results = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.RecordResults());
            narrator.stepPassedWithScreenShot("Results :" + results);

        }

        return true;
    }
}
