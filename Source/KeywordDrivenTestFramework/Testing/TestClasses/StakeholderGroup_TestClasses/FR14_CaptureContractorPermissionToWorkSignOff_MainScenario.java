/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.StakeholderGroup_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects.Stakeholder_Group_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR14 – Capture Contractor Permission to Work Sign Off Main Scenario",
        createNewBrowserInstance = false
)

public class FR14_CaptureContractorPermissionToWorkSignOff_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR14_CaptureContractorPermissionToWorkSignOff_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CaptureContractor())
        {
            return narrator.testFailed("Capture Contractor Permission to Work Sign Off Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Contractor Permission to Work Sign Off");
    }

    public boolean CaptureContractor()
    {

        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.PermissionToWorkTab()))
        {
            error = "Failed to wait for Permission to Work Tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.PermissionToWorkTab()))
        {
            error = "Failed to click Permission to Work Tab";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.PermissionToWorkAdd()))
        {
            error = "Failed to wait for Permission to Work add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.PermissionToWorkAdd()))
        {
            error = "Failed to click Permission to Work add button";
            return false;
        }
        pause(6000);
        
        //Comments
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.PermissionComments()))
        {
            error = "Failed to wait for Comments";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.PermissionComments(), getData("Comments")))
        {
            error = "Failed to enter Comments";
            return false;
        }

        narrator.stepPassedWithScreenShot("Comments :"+ getData("Comments"));

      
        //Role
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.RoleDropDown()))
        {
            error = "Failed to wait for Roledropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.RoleDropDown()))
        {
            error = "Failed to click Role dropdown.";
            return false;
        }
        
          if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for type to search";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Role")))
        {
            error = "Failed to enter Role option :" + getData("Role");
            return false;
        }

         if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        //  pause(10000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Role"))))
        {
            error = "Failed to wait for Role drop down option : " + getData("Role");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Role"))))
        {
            error = "Failed to click Role drop down option : " + getData("Role");
            return false;
        }
        
        //Responsible person to sign off
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.ResponsiblePersonToSignOffDD()))
        {
            error = "Failed to wait for Responsible person to sign off dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.ResponsiblePersonToSignOffDD()))
        {
            error = "Failed to click Responsible person to sign off dropdown.";
            return false;
        }
        
           if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for type to search";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch(), getData("Responsible person to sign off")))
        {
            error = "Failed to enter Responsible person to sign off option :" + getData("Responsible person to sign off");
            return false;
        }

         if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        //  pause(10000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Responsible person to sign off"))))
        {
            error = "Failed to wait for Responsible person to sign off drop down option : " + getData("Responsible person to sign off");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Responsible person to sign off"))))
        {
            error = "Failed to click Responsible person to sign off drop down option : " + getData("Responsible person to sign off");
            return false;
        }
        
        //sign off
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SignOffDD()))
        {
            error = "Failed to wait for sign off dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.SignOffDD()))
        {
            error = "Failed to click sign off dropdown.";
            return false;
        }
        
           if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for type to search";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.TypeSearch2(), getData("Sign off")))
        {
            error = "Failed to enter Sign off  option :" + getData("Sign off");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Stakeholder_Group_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        //  pause(10000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Text2(getData("Sign off"))))
        {
            error = "Failed to wait for Sign off drop down option : " + getData("Sign off");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Text2(getData("Sign off"))))
        {
            error = "Failed to click Sign off drop down option : " + getData("Sign off");
            return false;
        }
        
           if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.SignOffDate()))
        {
            error = "Failed to wait for Sign off date";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Group_PageObjects.SignOffDate(), startDate))
        {
            error = "Failed to enter Sign off date :" +startDate ;
            return false;
        }
        
        //process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.PermissionToWork_processflow()))
        {
            error = "Failed to wait for Permission to Work process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.PermissionToWork_processflow()))
        {
            error = "Failed to click Permission to Work process flow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked process flow");

        
        
        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.Permission_save()))
        {
            error = "Failed to wait for Save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Group_PageObjects.Permission_save()))
        {
            error = "Failed to click Save";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Group_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Group_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Stakeholder_Group_PageObjects.setRecord_Number(record[2]);
        String record_ = Stakeholder_Group_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;
    }
}
