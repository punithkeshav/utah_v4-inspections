/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JobRiskAssessment_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects.JobRiskAssessment_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR7-Capture Controls - Main Scenario",
        createNewBrowserInstance = false
)
public class FR7_Capture_Controls_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR7_Capture_Controls_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Hazard())
        {
            return narrator.testFailed("Capture Controls Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Controls ");
    }

    public boolean Hazard()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ControlsAdd()))
        {
            error = "Failed to wait for add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ControlsAdd()))
        {
            error = "Failed to click on add button.";
            return false;
        }

        narrator.stepPassed("Successfully click  add button.");

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Controls_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Controls_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Hazard classification
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ControlSourceDropDown()))
        {
            error = "Failed to wait for Control Source drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ControlSourceDropDown()))
        {
            error = "Failed to click Control Source dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Control Source text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Control Source")))
        {
            error = "Failed to enter Control Source option :" + getData("Control Source");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text4(getData("Control Source"))))
        {
            error = "Failed to wait for Control Source drop down option : " + getData("Control Source");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text4(getData("Control Source"))))
        {
            error = "Failed to click Control Source drop down option : " + getData("Control Source");
            return false;
        }

        narrator.stepPassed("Control Source :" + getData("Control Source"));

        //Add new  control
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Control_Layer_3()))
        {
            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.Control_Layer_3(), getData("Control source 1")))
            {
                error = "Failed to enter Control source option :" + getData("Control source 1");
                return false;
            }
            narrator.stepPassedWithScreenShot("Control source :" + getData("Control source 1"));

        }

        //Select from layer 2 controls
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Bowtie_Control_Layer_DropDown()))
        {

            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Bowtie_Control_Layer_DropDown()))
            {
                error = "Failed to click Bowtie Control (Layer 2) drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Hazard classification  text box.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Bowtie Control (Layer 2)")))
            {
                error = "Failed to wait for Select from layer 2 controls option :" + getData("Bowtie Control (Layer 2)");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text(getData("Bowtie Control (Layer 2)"))))
            {
                error = "Failed to wait for Bowtie Control (Layer 2) drop down option : " + getData("Bowtie Control (Layer 2)");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text(getData("Bowtie Control (Layer 2)"))))
            {
                error = "Failed to click Bowtie Control (Layer 2)  drop down option : " + getData("Bowtie Control (Layer 2)");
                return false;
            }

            narrator.stepPassedWithScreenShot("Bowtie Control (Layer 2) :" + getData("Bowtie Control (Layer 2)"));

        }

        //Procedure followed/Controls used as specified (PTO)
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ProcedureSpecified_DropDown()))
        {
            error = "Failed to wait for Procedure followed/Controls used as specified (PTO) drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ProcedureSpecified_DropDown()))
        {
            error = "Failed to click Procedure followed/Controls used as specified (PTO) dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Procedure followed/Controls used as specified (PTO)  text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Procedure followed/Controls used as specified (PTO)")))
        {
            error = "Failed to wait for Procedure followed/Controls used as specified (PTO) option :" + getData("Procedure followed/Controls used as specified (PTO)");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Go_no_go_controlOption(getData("Procedure followed/Controls used as specified (PTO)"))))
        {
            error = "Failed to wait for Procedure followed/Controls used as specified (PTO) drop down option : " + getData("Procedure followed/Controls used as specified (PTO)");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Go_no_go_controlOption(getData("Procedure followed/Controls used as specified (PTO)"))))
        {
            error = "Failed to click  Procedure followed/Controls used as specified (PTO) drop down option : " + getData("Procedure followed/Controls used as specified (PTO)");
            return false;
        }

        narrator.stepPassedWithScreenShot("Procedure followed/Controls used as specified (PTO) :" + getData("Procedure followed/Controls used as specified (PTO)"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Controls_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Controls_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        //Select from layer 3 controls
        if (getData("Control Source 1").equalsIgnoreCase("Select from layer 3 controls"))
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ControlSourceDropDown()))
            {
                error = "Failed to wait for Control Source drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ControlSourceDropDown()))
            {
                error = "Failed to click Control Source dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Control Source text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Control Source 1")))
            {
                error = "Failed to enter Control Source option :" + getData("Control Source 1");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text4(getData("Control Source 1"))))
            {
                error = "Failed to wait for Hazard classification drop down option : " + getData("Control Source 1");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text4(getData("Control Source 1"))))
            {
                error = "Failed to click Hazard classification drop down option : " + getData("Control Source 1");
                return false;
            }

            narrator.stepPassed("Control Source :" + getData("Control Source 1"));
            pause(5000);

//            if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Select_from_layer_3_DropDown()))
//            {
//                error = "Failed to wait for Bowtie Control (Layer 3) drop down.";
//                return false;
//            }

            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Select_from_layer_3_DropDown()))
            {
                error = "Failed to click Bowtie Control (Layer 3) drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Hazard classification  text box.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Layer 3 control")))
            {
                error = "Failed to wait for Layer 3 control option :" + getData("Layer 3 control");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text4(getData("Layer 3 control"))))
            {
                error = "Failed to wait for Layer 3 control drop down option : " + getData("Layer 3 control");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text4(getData("Layer 3 control"))))
            {
                error = "Failed to click Layer 3 control drop down option : " + getData("Layer 3 control");
                return false;
            }

            narrator.stepPassedWithScreenShot("Layer 3 control :" + getData("Layer 3 control"));

            //Save
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Controls_Save()))
            {
                error = "Failed to wait for button save";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Controls_Save()))
            {
                error = "Failed to click button save";
                return false;
            }

            // SeleniumDriverInstance.pause(4000);
             saved = "";
            if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
            {
                saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup());
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
                {
                    error = "Failed to wait for 'Record Saved' popup.";
                    return false;
                }
            }

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

             acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
             record = acionRecord.split(" ");
            JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
             record_ = JobRiskAssessment_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        }
        return true;

    }
}
