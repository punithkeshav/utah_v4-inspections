/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JobRiskAssessment_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects.JobRiskAssessment_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR9-Capture Work Execution Specification - Main Scenario",
        createNewBrowserInstance = false
)
public class FR9_Capture_Work_Execution_Specification_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR9_Capture_Work_Execution_Specification_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureWorkExecutionSpecification())
        {
            return narrator.testFailed("Capture Work Execution Specification Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Work Execution Specification");
    }

    public boolean CaptureWorkExecutionSpecification()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.WorkExecutionSpecificationAdd()))
        {
            error = "Failed to wait for add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.WorkExecutionSpecificationAdd()))
        {
            error = "Failed to click on add button.";
            return false;
        }

        narrator.stepPassed("Successfully click  add button.");

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.WorkExecutionSpecification_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.WorkExecutionSpecification_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Order
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Order2()))
        {
            error = "Failed to wait for Order text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.Order2(), "" + 12))
        {
            error = "Failed to enter Order :";
            return false;
        }
        narrator.stepPassed("Order :" + 12);

        //Question
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Question()))
        {
            error = "Failed to wait for Question text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.Question(), getData("Question")))
        {
            error = "Failed to enter Question :" + getData("Question");
            return false;
        }
        narrator.stepPassed("Question :" + getData("Question"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.WorkExecutionSpecification_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.WorkExecutionSpecification_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;

    }

}
