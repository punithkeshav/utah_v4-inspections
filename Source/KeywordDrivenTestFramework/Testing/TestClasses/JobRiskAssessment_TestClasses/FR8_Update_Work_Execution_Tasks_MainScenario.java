/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JobRiskAssessment_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects.JobRiskAssessment_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR8-Update Work Execution Tasks - Main Scenario",
        createNewBrowserInstance = false
)
public class FR8_Update_Work_Execution_Tasks_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR8_Update_Work_Execution_Tasks_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!UpdateWorkExecutionTasks())
        {
            return narrator.testFailed("Update Work Execution Tasks Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Update Work Execution Tasks");
    }

    public boolean UpdateWorkExecutionTasks()
    {
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the current module close button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully closed current module");

        pause(7000);

        //SectionB_Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.SectionC_Tab()))
        {
            error = "Failed to wait for Section C - Work Execution Specification tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.SectionC_Tab()))
        {
            error = "Failed to click on Section C - Work Execution Specification tab.";
            return false;
        }
        // first record 
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.WorkExecutionTasksFirstRecord()))
        {
            error = "Failed to wait for first record";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.WorkExecutionTasksFirstRecord()))
        {
            error = "Failed to click first record";
            return false;
        }

        narrator.stepPassed("Successfully click on first record");

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.WorkExecutionTasks_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        pause(6000);
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.WorkExecutionTasks_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");
        
        
        
         //Are the person/s physically fit to conduct the work 
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.AreThePersonPhysicallyDropDown()))
        {
            error = "Failed to wait for Are the person/s physically fit to conduct the work  dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.AreThePersonPhysicallyDropDown()))
        {
            error = "Failed to click Are the person/s physically fit to conduct the work  dropdown.";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for Are the person/s physically fit to conduct the work  text box.";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Option_3(getData("Are the person/s physically fit to conduct the work"))))
        {
            error = "Failed to wait for Are the person/s physically fit to conduct the work  drop down option : " + getData("Are the person/s physically fit to conduct the work");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Option_3(getData("Are the person/s physically fit to conduct the work"))))
        {
            error = "Failed to click Are the person/s physically fit to conduct the work  drop down option : " + getData("Are the person/s physically fit to conduct the work");
            return false;
        }

        narrator.stepPassedWithScreenShot("Are the person/s physically fit to conduct the work   :" + getData("Are the person/s physically fit to conduct the work"));

        if (getData("Are the person/s physically fit to conduct the work").equalsIgnoreCase("No"))
        {
            //Why aren’t the person/s physically fit to conduct the work?
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.PhysicallyFitToConductTheWork()))
            {
                error = "Failed to wait for Why aren’t the person/s physically fit to conduct the work? text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.PhysicallyFitToConductTheWork(), getData("Why aren’t the person/s physically fit to conduct the work")))
            {
                error = "Failed to enter Why aren’t the person/s physically fit to conduct the work :" + getData("Why aren’t the person/s physically fit to conduct the work");
                return false;
            }

            //Describe any contextual and/ or environmental conditions at time of assessment that may be relevant
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.DescribeAnyContextual()))
            {
                error = "Failed to wait for Describe any contextual and/ or environmental conditions at time of assessment that may be relevant text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.DescribeAnyContextual(), getData("Describe any contextual and/ or environmental conditions at time of assessment that may be relevant")))
            {
                error = "Failed to enter Describe any contextual and/ or environmental conditions at time of assessment that may be relevant :" + getData("Describe any contextual and/ or environmental conditions at time of assessment that may be relevant");
                return false;
            }

        }
        
        

        //Is the procedure being observed the same as the listed above in the JRA
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ProcedureBeingObservedDropDown()))
        {
            error = "Failed to wait for Is the procedure being observed the same as the listed above in the JRA dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ProcedureBeingObservedDropDown()))
        {
            error = "Failed to click Is the procedure being observed the same as the listed above in the JRA dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Is the procedure being observed the same as the listed above in the JRA text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Option_1(getData("Is the procedure being observed the same as the listed above in the JRA"))))
        {
            error = "Failed to wait for Is the procedure being observed the same as the listed above in the JRA drop down option : " + getData("Is the procedure being observed the same as the listed above in the JRA");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Option_1(getData("Is the procedure being observed the same as the listed above in the JRA"))))
        {
            error = "Failed to click Is the procedure being observed the same as the listed above in the JRA drop down option : " + getData("Is the procedure being observed the same as the listed above in the JRA");
            return false;
        }

        narrator.stepPassedWithScreenShot("Is the procedure being observed the same as the listed above in the JRA option  :" + getData("Is the procedure being observed the same as the listed above in the JRA"));

        if (getData("Is the procedure being observed the same as the listed above in the JRA").equalsIgnoreCase("No"))
        {
            //Name of procedure
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.NameOfProcedure()))
            {
                error = "Failed to wait for Name of procedure text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.NameOfProcedure(), getData("Name of procedure")))
            {
                error = "Failed to enter Name of procedure :" + getData("Name of procedure");
                return false;
            }

            //Date of procedure
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.DateOfProcedure()))
            {
                error = "Failed to wait for Date of procedure text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.DateOfProcedure(), startDate))
            {
                error = "Failed to enter Date of procedure :" + startDate;
                return false;
            }

        }

       

        //Are the equipment/tools required for the task in place
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.EquipmentToolsRequiredDropDown()))
        {
            error = "Failed to wait for Are the equipment/tools required for the task in place dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.EquipmentToolsRequiredDropDown()))
        {
            error = "Failed to click Are the equipment/tools required for the task in place dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Active / inactive text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Option_2(getData("Are the equipment/tools required for the task in place"))))
        {
            error = "Failed to wait for Are the equipment/tools required for the task in place drop down option : " + getData("Are the equipment/tools required for the task in place");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Option_2(getData("Are the equipment/tools required for the task in place"))))
        {
            error = "Failed to click Are the equipment/tools required for the task in place drop down option : " + getData("Are the equipment/tools required for the task in place");
            return false;
        }

        narrator.stepPassedWithScreenShot("Are the equipment/tools required for the task in place  :" + getData("Are the equipment/tools required for the task in place"));

        if (getData("Are the equipment/tools required for the task in place").equalsIgnoreCase("No"))
        {
            //Why isn’t the Equipment/tools required for the task in place?
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.EquipmentToolsRequired()))
            {
                error = "Failed to wait for Why isn’t the Equipment/tools required for the task in place? text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.EquipmentToolsRequired(), getData("Why isn’t the Equipment/tools required for the task in place")))
            {
                error = "Failed to enter Why isn’t the Equipment/tools required for the task in place? :" + getData("Why isn’t the Equipment/tools required for the task in place");
                return false;
            }

        }

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.WorkExecutionTasks_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.WorkExecutionTasks_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup2()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup2());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup2()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;
    }

    public boolean UploadDocument()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Supporting_Documents_Tab()))
        {
            error = "Failed to wait for Supporting Documents.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Supporting_Documents_Tab()))
        {
            error = "Failed to click on Supporting Documents";
            return false;
        }

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.linkToADocument_2()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.linkToADocument_2()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.UrlInput_TextArea()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.UrlInput_TextArea(), getData("Document url")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.tile_TextArea()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.tile_TextArea(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(JobRiskAssessment_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }
}
