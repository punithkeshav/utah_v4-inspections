/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JobRiskAssessment_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects.JobRiskAssessment_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR4-Capture Unwanted Events - Main Scenario",
        createNewBrowserInstance = false
)
public class FR4_CaptureTaskInformation_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_CaptureTaskInformation_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!UnwantedEvents())
        {
            return narrator.testFailed("Capture Task Information Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Task Information");
    }

    public boolean UnwantedEvents()
    {
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the current module close button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully closed current module");

        pause(7000);

        //SectionB_Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.SectionB_Tab()))
        {
            error = "Failed to wait for Section B - JRA / PTO Task Information tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.SectionB_Tab()))
        {
            error = "Failed to click on Section B - JRA / PTO Task Information tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TaskInformationAdd()))
        {
            error = "Failed to wait for add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.TaskInformationAdd()))
        {
            error = "Failed to click on add button.";
            return false;
        }

        narrator.stepPassed("Successfully click  add button.");

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TaskInformation_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.TaskInformation_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Order
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Order()))
        {
            error = "Failed to wait for Order text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.Order(), "" + 12))
        {
            error = "Failed to enter Order :";
            return false;
        }
        narrator.stepPassed("Order :" + 12);

        //TaskActivityStepDescription
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TaskActivityStepDescription()))
        {
            error = "Failed to wait for Task Activity Step Description text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TaskActivityStepDescription(), getData("Task Activity Step Description")))
        {
            error = "Failed to enter Task Activity Step Description :" + getData("Task Activity Step Description");
            return false;
        }
        narrator.stepPassed("Task Activity Step Description :" + getData("Task Activity Step Description"));

        if (getData("Execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Supporting_Documents_Tab()))
            {
                error = "Failed to wait for Supporting Documents.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Supporting_Documents_Tab()))
            {
                error = "Failed to click on Supporting Documents";
                return false;
            }

            //Upload a link
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.linkToADocument_2()))
            {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.linkToADocument_2()))
            {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }

            narrator.stepPassed("Successfully click 'Link a document' button.");

            if (!SeleniumDriverInstance.switchToTabOrWindow())
            {
                error = "Failed to switch tab.";
                return false;
            }

            //URL Input
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.UrlInput_TextArea()))
            {
                error = "Failed to wait for url input textbox";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.UrlInput_TextArea(), getData("Document url")))
            {
                error = "Failed to enter url input";
                return false;
            }

            //Title Input
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.tile_TextArea()))
            {
                error = "Failed to wait for title text box.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.tile_TextArea(), getData("Title")))
            {
                error = "Failed to enter title text box";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.urlAddButton()))
            {
                error = "Failed to wait for 'Add' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.urlAddButton()))
            {
                error = "Failed to click on 'Add' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");

            if (!SeleniumDriverInstance.switchToDefaultContent())
            {
                error = "Failed to switch tab.";
                return false;
            }

            //iFrame
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.iframeXpath()))
            {
                error = "Failed to switch to frame.";
                return false;
            }
            if (!SeleniumDriverInstance.switchToFrameByXpath(JobRiskAssessment_PageObjects.iframeXpath()))
            {
                error = "Failed to switch to frame.";
                return false;
            }
                        narrator.stepPassed("Successfully uploaded a Supporting Document.");


        }

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TaskInformation_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.TaskInformation_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup2()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup2());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup2()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;
    }

    public boolean UploadDocument()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Supporting_Documents_Tab()))
        {
            error = "Failed to wait for Supporting Documents.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Supporting_Documents_Tab()))
        {
            error = "Failed to click on Supporting Documents";
            return false;
        }

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.linkToADocument_2()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.linkToADocument_2()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.UrlInput_TextArea()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.UrlInput_TextArea(), getData("Document url")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.tile_TextArea()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.tile_TextArea(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(JobRiskAssessment_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }
}
