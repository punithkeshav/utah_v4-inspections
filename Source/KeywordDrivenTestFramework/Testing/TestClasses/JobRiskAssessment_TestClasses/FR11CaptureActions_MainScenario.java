/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JobRiskAssessment_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects.JobRiskAssessment_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR11-Capture Actions - Main Scenario",
        createNewBrowserInstance = false
)
public class FR11CaptureActions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR11CaptureActions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureActions())
        {
            return narrator.testFailed("Capture Actions Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Actions");
    }

    public boolean CaptureActions()
    {
        pause(4000);

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ActionsTab()))
        {
            error = "Failed to wait for Actions tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ActionsTab()))
        {
            error = "Failed to click on Actions tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Actions tab.");

        pause(4000);

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Actions_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Actions_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Actions_processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Actions_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        //Type of action
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to wait for Type of action.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to click Type of action";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Action description text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Type of action")))

        {
            error = "Failed to enter Type of action :" + getData("Type of action");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        //
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(JobRiskAssessment_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to click Project drop down option : " + getData("Type of action");
            return false;
        }
        narrator.stepPassedWithScreenShot("Type of action :" + getData("Type of action"));

        ///Action Description
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ActionDescription()))
        {
            error = "Failed to wait for Action description text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.ActionDescription(), getData("Action description")))

        {
            error = "Failed to enter Action description :" + getData("Action description");
            return false;
        }

        //Entity
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ActionsEntityDropDown()))
        {
            error = "Failed to wait for Entity dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ActionsEntityDropDown()))
        {
            error = "Failed to click Entity dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Entity text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Business unit option")))

        {
            error = "Failed to enter Entity option :" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to wait to expand Entity";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to expand Entity";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Entity:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity drop down :" + getData("Business unit 1");
            return false;
        }

//          if (!SeleniumDriverInstance.waitForElementByXpath(EngagementPlan_PageObjects.businessUnitOption1(getData("Business unit 2"))))
//        {
//            error = "Failed to wait for Entity:" + getData("Business unit 2");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(EngagementPlan_PageObjects.businessUnitOption1(getData("Business unit 2"))))
//        {
//            error = "Failed to click Entity drop down :" + getData("Business unit 2");
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Entity option  :" + getData("Business unit option"));

        //Responsible person
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ResponsiblePersonDropDown()))
        {
            error = "Failed to wait for Responsible person dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ResponsiblePersonDropDown()))
        {
            error = "Failed to click Responsible person dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible person text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch(), getData("Responsible person")))
        {
            error = "Failed to enter Responsible person option :" + getData("Responsible person");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to wait forResponsible person options.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person :" + getData("Responsible person");
            return false;
        }

        narrator.stepPassedWithScreenShot("Responsible person :" + getData("Responsible person"));

        //Action due date
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Action_due_date()))
        {
            error = "Failed to wait for Action due date input field.";
            return false;

        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.Action_due_date(), startDate))
        {
            error = "Failed to enter Action due date :" + startDate;
            return false;
        }

        narrator.stepPassedWithScreenShot("Action due date :" + startDate);
        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Actions_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Actions_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup2()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup2());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup2()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;

    }

}
