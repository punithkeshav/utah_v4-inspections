/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JobRiskAssessment_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects.JobRiskAssessment_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR2 Capture Task Information - Main Scenario",
        createNewBrowserInstance = false
)
public class FR2_CaptureChange_Log_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_CaptureChange_Log_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!TaskInformation())
        {
            return narrator.testFailed("Capture Change Log Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Change Log");
    }

    public boolean TaskInformation()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ChangeLogAdd()))
        {
            error = "Failed to wait for Change Log add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ChangeLogAdd()))
        {
            error = "Failed to click on Change Log add button.";
            return false;
        }

        narrator.stepPassed("Successfully click Task Information add button.");

        pause(4000);

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ChangeLog_processflow()))
        {
            error = "Failed to wait for Change Log process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ChangeLog_processflow()))
        {
            error = "Failed to click Change Log process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Date of change
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.DateOfChange()))
        {
            error = "Failed to wait for Date of change";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.DateOfChange(), startDate))
        {
            error = "Failed to enter Date of change:" + startDate;
            return false;
        }
        narrator.stepPassedWithScreenShot("Date of change:" + startDate);

        //Reasons for review include
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.ReasonsForReviewIncludeDropDown()))
        {
            error = "Failed to wait for Reasons for review include dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ReasonsForReviewIncludeDropDown()))
        {
            error = "Failed to click 'Reasons for review include dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Person responsible for Sign off text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Reasons for review include")))
        {
            error = "Failed to enter Reasons for review include option :" + getData("Reasons for review include");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.PersonResponsibleForSignOffDropDownOption(getData("Reasons for review include"))))
        {
            error = "Failed to wait for Person responsible for Sign off dropdown :" + getData("Reasons for review include");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.PersonResponsibleForSignOffDropDownOption(getData("Reasons for review include"))))
        {
            error = "Failed to click 'Person responsible for Sign off dropdown option :" + getData("Reasons for review include");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.ReasonsForReviewIncludeDropDown()))
        {
            error = "Failed to click 'Reasons for review include dropdown.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Reasons for review include :" + getData("Reasons for review include"));

        if (getData("Reasons for review include").equalsIgnoreCase("Audit review"))
        {
            pause(3000);

            //Link to audit
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.LinkToAuditDropDown()))
            {
                error = "Failed to wait for Link to auditdropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.LinkToAuditDropDown()))
            {
                error = "Failed to clickLink to audit dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for PLink to audit text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Link to audit")))
            {
                error = "Failed to enter Link to audit option :" + getData("Link to audit");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text6(getData("Link to audit"))))
            {
                error = "Failed to wait for Link to audit option :" + getData("Link to audit");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text6(getData("Link to audit"))))
            {
                error = "Failed to click Link to audit option :" + getData("Link to audit");
                return false;
            }

            narrator.stepPassedWithScreenShot("Link to audit :" + getData("Link to audit"));

        }

        if (getData("Reasons for review include").equalsIgnoreCase("Event management"))
        {
            pause(3000);
//Event management
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.LinkToEventDropDown()))
            {
                error = "Failed to wait for Link to event dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.LinkToEventDropDown()))
            {
                error = "Failed to click Link to event dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Link to event text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Link to event")))
            {
                error = "Failed to enter Link to event option :" + getData("Link to event");
                return false;
            }
//            if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch2()))
//            {
//                error = "Failed to press enter";
//                return false;
//            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text6(getData("Link to event"))))
            {
                error = "Failed to enter Link to event option :" + getData("Link to event");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text6(getData("Link to event"))))
            {
                error = "Failed to enter Link to event option :" + getData("Link to audit");
                return false;
            }

            narrator.stepPassedWithScreenShot("Link to event :" + getData("Link to event"));

        }

        if (getData("Reasons for review include").equalsIgnoreCase("Stakeholder engagement"))
        {
            pause(3000);

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.LinkToStakeholderEngagementDropDown()))
            {
                error = "Failed to wait for Link to stakeholder engagement dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.LinkToStakeholderEngagementDropDown()))
            {
                error = "Failed to click Link to stakeholder engagement dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Link to stakeholder engagement text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch2(), getData("Link to stakeholder engagement")))
            {
                error = "Failed to enter Reasons for review include option :" + getData("Link to stakeholder engagement");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text6(getData("Link to stakeholder engagement"))))
            {
                error = "Failed to enter Link to stakeholder engagement option :" + getData("Link to stakeholder engagement");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text6(getData("Link to stakeholder engagement"))))
            {
                error = "Failed to enter Link to stakeholder engagement option :" + getData("Link to stakeholder engagement");
                return false;
            }

            narrator.stepPassedWithScreenShot("Link to stakeholder engagement :" + getData("Link to stakeholder engagement"));

        }

        //Person responsible for Sign off
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.PersonResponsibleForSignOffDropDown()))
        {
            error = "Failed to wait for Person responsible for Sign off dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.PersonResponsibleForSignOffDropDown()))
        {
            error = "Failed to click 'Person responsible for Sign off dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Person responsible for Sign off text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(JobRiskAssessment_PageObjects.TypeSearch(), getData("Person responsible for Sign off")))
        {
            error = "Failed to wait for Person responsible for Sign off option :" + getData("Person responsible for Sign off");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(JobRiskAssessment_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Text4(getData("Person responsible for Sign off"))))
        {
            error = "Failed to wait for Person responsible for Sign off drop down option : " + getData("Person responsible for Sign off");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Text4(getData("Person responsible for Sign off"))))
        {
            error = "Failed to click  Person responsible for Sign off drop down option : " + getData("Person responsible for Sign off");
            return false;
        }

        narrator.stepPassedWithScreenShot("Person responsible for Sign off :" + getData("Person responsible for Sign off"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.Change_Log_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(JobRiskAssessment_PageObjects.Change_Log_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        // SeleniumDriverInstance.pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(JobRiskAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(JobRiskAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        JobRiskAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = JobRiskAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;
    }
}
