/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR2_Capture_InspectionChecklist_MainScenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Inspection_Checklist_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Inspection_Checklist_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!addChecklist())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Inspections / Inspection Checklist record saved.");
    }

    public boolean addChecklist()
    {
        //Navigate to checklist Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklistTab()))
        {
            error = "Failed to wait for 'Checklist' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklistTab()))
        {
            error = "Failed to click on 'Checklist' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Checklist' tab.");

        //Add Checklist
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklist_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        pause(5000);

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklist_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button");

        //Select Checklist New Drodown
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_dropdown()))
        {
            error = "Failed to wait for Checklist dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklist_dropdown()))
        {
            error = "Failed to click Checklist dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_search()))
        {
            error = "Failed to wait for Measurement taken by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Checklist"))))
        {
            error = "Failed to wait for Checklist drop down option : " + getData("Checklist");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Checklist"))))
        {
            error = "Failed to click Checklist drop down option : " + getData("Checklist");
            return false;
        }

        //Save 
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.startChecklist()))
        {
            error = "Failed to wait for save  button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.startChecklist()))
        {
            error = "Failed to click save button.";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Inspection_PageObjects.setRecord_Number(record[2]);
        String record_ = Inspection_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully click save  button");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.CrticalControl_dropdown()))
        {
            error = "Failed to wait for crtical control dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.CrticalControl_dropdown()))
        {
            error = "Failed to click crtical control dropdown.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Critical control :" + getData("Critical control"));

        //Critical control
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Critical control"))))
        {
            error = "Failed to wait for Critical control drop down option : " + getData("Critical control");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Critical control"))))
        {
            error = "Failed to click Critical control drop down option : " + getData("Critical control");
            return false;
        }

        if (getData("Critical control").equalsIgnoreCase("Yes"))
        {

            
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Control_dropdown()))
            {
                error = "Failed to click control dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for type to search";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Critical"))))
            {
                error = "Failed to wait for Control drop down option : " + getData("Critical");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Critical"))))
            {
                error = "Failed to click Control drop down option : " + getData("Critical");
                return false;
            }

            narrator.stepPassedWithScreenShot("Control :" + getData("Critical"));
        }

        //Save 
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.saveButton()))
        {
            error = "Failed to wait for save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.saveButton()))
        {
            error = "Failed to click save button.";
            return false;
        }

        saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.getActionRecord());
        record = acionRecord.split(" ");
        Inspection_PageObjects.setRecord_Number(record[2]);
        record_ = Inspection_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully click save  button");

        return true;

    }
}
