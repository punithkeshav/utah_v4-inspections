/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "UC ISP 02-02 Capture Checklist - MainScenario",
        createNewBrowserInstance = false
)
public class UC_ISP_02_02_Capture_Checklist_MS extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_ISP_02_02_Capture_Checklist_MS()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CaptureDetails())
        {
            return narrator.testFailed("Capture Details Failed Due To :" + error);
        }

        return narrator.finalizeTest("The [Start] button automatically changes to [Continue] and tab details display the checklist progress.");

    }

    public boolean CaptureDetails()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.StartButton()))
        {
            error = "Failed to wait for start button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.StartButton()))
        {
            error = "Failed to click start button";
            return false;
        }

        // Questions 1
//       
//            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Question1_dropdown1()))
//            {
//                error = "Failed to wait for Question 1 dropdown";
//                return false;
//            }
//        
//        
//            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Question1_dropdown1()))
//            {
//                error = "Failed to click Question 1 dropdown";
//                return false;
//            }
//        
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to enter Question 1 option :" + getData("Question 1");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Question 1"))))
//        {
//            error = "Failed to wait for Question 1 drop down option : " + getData("Question 1");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Question 1"))))
//        {
//            error = "Failed to click Question 1 drop down option : " + getData("Question 1");
//            return false;
//        }
        //Answer
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Answer_dropdown1()))
        {
            error = "Failed to wait for Answer dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Answer_dropdown1()))
        {
            error = "Failed to click Answer dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch2()))
        {
            error = "Failed to enter Answer option :" + getData("Answer");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Answer"))))
        {
            error = "Failed to wait for Answer drop down option : " + getData("Question 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Answer"))))
        {
            error = "Failed to click Answer drop down option : " + getData("Answer");
            return false;
        }
        narrator.stepPassedWithScreenShot("Answer :" + getData("Answer"));

        //Save Current Finish
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.saveCurrentSection_Button()))
        {
            error = "Failed to wait for 'save current section button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.saveCurrentSection_Button()))
        {
            error = "Failed to save current section button.";
            return false;
        }

        pause(10000);

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Inspection_PageObjects.setRecord_Number(record[2]);
        String record_ = Inspection_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.exit_Button()))
        {
            error = "Failed to wait for cross button to close page";
            return false;
        }
        pause(20000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.exit_Button()))
        {
            error = "Failed to click cross button to close page";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Continue_Button()))
        {
            error = "Failed to wait for Continue button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Continue button");

        if (getData("Continue click").equalsIgnoreCase("True"))
        {
            pause(7000);
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Continue_Button()))
            {
                error = "Failed to click Continue button.";
                return false;
            }

            // Click Next
//            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.next_Button()))
//            {
//                error = "Failed to wait for 'next button.";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.next_Button()))
//            {
//                error = "Failed to click next button.";
//                return false;
//            }
//
//            narrator.stepPassedWithScreenShot("Successfully clicked next button.");
//
//            //Checklist 2
//            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.questionDropDown1()))
//            {
//                error = "Failed to wait for 'Question 1' dropdown..";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.questionDropDown1()))
//            {
//                error = "Failed to click Question 1 dropdown.";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.question_search1()))
//            {
//                error = "Failed to enter Question 2 option :" + getData("Question 2");
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Question 2"))))
//            {
//                error = "Failed to wait for Question 2 drop down option : " + getData("Question 2");
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Question 2"))))
//            {
//                error = "Failed to click Question 2 drop down option : " + getData("Question 2");
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Successfully completed question 2");
            //Finish
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Finish_Button()))
            {
                error = "Failed to wait for finish button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Finish_Button()))
            {
                error = "Failed to click finish button.";
                return false;
            }

            narrator.stepPassedWithScreenShot("Finish clicked");

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.View_Button()))
            {
                error = "Failed to wait view button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("View Button ");

        }

        if (getData("View click").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.View_Button()))
            {
                error = "Failed to click view button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("View Button Click");

            // Questions 1
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Question1_dropdown1()))
            {
                error = "Failed to wait for Question 1 dropdown";
                return false;
            }

            narrator.stepPassedWithScreenShot("Checklist");

        }

        return true;

    }
}
