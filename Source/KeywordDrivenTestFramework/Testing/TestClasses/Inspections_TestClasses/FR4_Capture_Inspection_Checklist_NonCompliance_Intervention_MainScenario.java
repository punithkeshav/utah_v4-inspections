/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR4_Capture_Inspection_Checklist_NonCompliance_Intervention_MainScenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Inspection_Checklist_NonCompliance_Intervention_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_Capture_Inspection_Checklist_NonCompliance_Intervention_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToInspections())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!AddInspectionNonCompliance())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Add Inspection Checklist Non-Compliance Intervention ");
    }

    public boolean NavigateToInspections()
    {
        //Navigate to Inspections
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspections()))
        {
            error = "Failed to wait for 'Inspections' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Inspections()))
        {
            error = "Failed to click on 'Inspections' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Checklist page opens but cannot be updated. ");

        pause(4000);
        //Search for inspections
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.inspections_Search(), 5000))
        {
            error = "Failed to wait for 'search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.inspections_Search()))
        {
            error = "Failed to click on 'search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'search' button.");

        //Click record
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.selectRecord()))
        {
            error = "Failed to wait for 'selecting a record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.selectRecord()))
        {
            error = "Failed to click record.";
            return false;
        }
//Add Checklist

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_Add(), 5000))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklist_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_processflow(), 10000))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklist_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        narrator.stepPassedWithScreenShot("Successfully clicked on record.");

        //Select checklist dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_dropdown()))
        {
            error = "Failed to wait for 'Checklist' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklist_dropdown()))
        {
            error = "Failed to click Checklist dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_search()))
        {
            error = "Failed to enter Checklist option :" + getData("Checklist");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Checklist"))))
        {
            error = "Failed to wait for Checklist drop down option : " + getData("Checklist");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Checklist"))))
        {
            error = "Failed to click Checklist drop down option : " + getData("Checklist");
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully completed 'question 1 dropdown.");
        return true;
    }

    public boolean AddInspectionNonCompliance()
    {

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.btnSave()))
        {
            error = "Failed to wait for 'save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.btnSave()))
        {
            error = "Failed to click save  button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'save' button.");

        //Non-Compliace Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.nonCompliancePanel()))
        {
            error = "Failed to wait for 'non compliance panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.nonCompliancePanel()))
        {
            error = "Failed to click non compliance panel.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'non compliance panel'.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.nonCompliance_Add()))
        {
            error = "Failed to wait for 'Non complaince inspections Add.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.nonCompliance_Add()))
        {
            error = "Failed to click Non complaince inspections Add.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'Non complaince inspections add button'.");

        //Date of non-compliance
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.dateOfNonCompliance()))
        {
            error = "Failed to wait for date of non-compliance.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.dateOfNonCompliance(), startDate))
        {
            error = "Failed to enter '" + startDate + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + startDate + "'.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.reportedDateOfNonCompliance()))
        {
            error = "Failed to wait for date of non-compliance.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.reportedDateOfNonCompliance(), startDate))
        {
            error = "Failed to enter '" + startDate + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + startDate + "'.");

        //Reported By
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.reportedBy_dropdown()))
        {
            error = "Failed to wait for 'Reported by' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.reportedBy_dropdown()))
        {
            error = "Failed to click Reported by dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.reportedBy_search()))
        {
            error = "Failed to enter Reported by option :" + getData("Reported by");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.reportedBy_search(), getData("Reported by")))
        {
            error = "Failed to enter  Reported by :" + getData("Reported by");
            return false;
        }
         if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.reportedBy_search()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Reported by1"))))
        {
            error = "Failed to wait for Reported by drop down option : " + getData("Reported by1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Reported by1"))))
        {
            error = "Failed to click Reported by drop down option : " + getData("Reported by");
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered Reported by'.");

        //Non-compliance intervention issued to
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.nonCompliance_DropDown()))
        {
            error = "Failed to wait for 'Non-compliance intervention issued to' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.nonCompliance_DropDown()))
        {
            error = "Failed to click Non-compliance intervention issued to dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.NonComplianceIntervention_search()))
        {
            error = "Failed to enter Non-compliance intervention issued to option :" + getData("Non-compliance intervention issued to");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.NonComplianceIntervention_search(), getData("Non-compliance intervention issued to")))
        {
            error = "Failed to enter  Non-compliance intervention issued to :" + getData("Non-compliance intervention issued to");
            return false;
        }
              if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.NonComplianceIntervention_search()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Non-compliance intervention issued to"))))
        {
            error = "Failed to wait for Non-compliance intervention issued to drop down option : " + getData("Non-compliance intervention issued to");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Non-compliance intervention issued to"))))
        {
            error = "Failed to click Non-compliance intervention issued to drop down option : " + getData("Non-compliance intervention issued to");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Reported by Non-Compliance intervention issued to.");

        //Person responsible for non-compliance intervention upliftment
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.personResponssibleDD()))
        {
            error = "Failed to wait for 'Person responsible for non-compliance intervention upliftment' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.personResponssibleDD()))
        {
            error = "Failed to click Person responsible for non-compliance intervention upliftment dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.personResponsibleForNonCompliance_search()))
        {
            error = "Failed to enter Person responsible for non-compliance intervention upliftment option :" + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.personResponsibleForNonCompliance_search(), getData("Person responsible for non-compliance intervention upliftment")))
        {
            error = "Failed to enter  Person responsible for non-compliance intervention upliftment :" + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }
              if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.personResponsibleForNonCompliance_search()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Person responsible for non-compliance intervention upliftment"))))
        {
            error = "Failed to wait for Person responsible for non-compliance intervention upliftment drop down option : " + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Person responsible for non-compliance intervention upliftment"))))
        {
            error = "Failed to click Person responsible for non-compliance intervention upliftment drop down option : " + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Person responsible for non-compliance intervention upliftment drop down option.");

        //Storage Referteence number
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.stoppageReferenceNumber(), getData("Stoppage reference number")))
        {
            error = "Failed to enter  Stoppage reference number :" + getData("Stoppage reference number");
            return false;
        }

        //Typr of Non-Compliance
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.typeOfNonComplianceDD()))
        {
            error = "Failed to wait for 'Type of non-compliance intervention' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.typeOfNonComplianceDD()))
        {
            error = "Failed to click Type of non-compliance intervention dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.typeNonCompliance_search()))
        {
            error = "Failed to enter Type of non-compliance intervention option :" + getData("Type of non-compliance intervention");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Type of non-compliance intervention"))))
        {
            error = "Failed to wait for Type of non-compliance intervention drop down option : " + getData("Type of non-compliance intervention");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Type of non-compliance intervention"))))
        {
            error = "Failed to click Type of non-compliance intervention drop down option : " + getData("Type of non-compliance intervention");
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered Type of non-compliance intervention drop down option drop down option.");
        //Clearance Classification

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.clearanceClassificationDD()))
        {
            error = "Failed to wait for 'Clearance classification' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.clearanceClassificationDD()))
        {
            error = "Failed to click Clearance classification dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.clearanceClassification_search()))
        {
            error = "Failed to enter Clearance classification option :" + getData("Clearance classification");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Clearance classification"))))
        {
            error = "Failed to wait for Clearance classification drop down option : " + getData("Clearance classification");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Clearance classification"))))
        {
            error = "Failed to click Clearance classification drop down option : " + getData("Clearance classification");
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered Clearance classification drop down option.");

        //Stoppage initiated by
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.stoppageInitiatedBynDD()))
        {
            error = "Failed to wait for 'Stoppage initiated by' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.stoppageInitiatedBynDD()))
        {
            error = "Failed to click Stoppage initiated by dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.stoppageInitiatedBy_search()))
        {
            error = "Failed to enter Stoppage initiated by option :" + getData("Stoppage initiated by");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Stoppage initiated by"))))
        {
            error = "Failed to wait for Stoppage initiated by drop down option : " + getData("Stoppage initiated by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Stoppage initiated by"))))
        {
            error = "Failed to click Stoppage initiated by drop down option : " + getData("Stoppage initiated by");
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered Stoppage initiated by drop down option.");

        //Work Stoppage
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.workStoppageDD()))
        {
            error = "Failed to wait for 'Work stoppage' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.workStoppageDD()))
        {
            error = "Failed to click Work stoppage dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.workStoppage_search()))
        {
            error = "Failed to enter Work stoppage option :" + getData("Work stoppage");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Work stoppage"))))
        {
            error = "Failed to wait for Work stoppage drop down option : " + getData("Work stoppage");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Work stoppage"))))
        {
            error = "Failed to click Work stoppage drop down option : " + getData("Work stoppage");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered work stoppage drop down option.");

        //Description of reason for work stoppage
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.descriptionOfReasonForWorkStoppage(), getData("Description of reason for work stoppage")))
        {
            error = "Failed to enter  Description of reason for work stoppage :" + getData("Description of reason for work stoppage");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Description of reason for work stoppage option.");

        //Stop note classification
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.stopNoteClassificationDD()))
        {
            error = "Failed to wait for 'Stop note classification' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.stopNoteClassificationDD()))
        {
            error = "Failed to click Stop note classification dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.stoppageNoteClassification_search()))
        {
            error = "Failed to enter Stop note classification option :" + getData("Stop note classification");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Stop note classification"))))
        {
            error = "Failed to wait for Stop note classification drop down option : " + getData("Stop note classification");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Stop note classification"))))
        {
            error = "Failed to click Stop note classification drop down option : " + getData("Stop note classification");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Stop note classification drop down option.");

        //Stoppage due to
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.stoppageDueToDD()))
        {
            error = "Failed to wait for 'Stop note classification' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.stoppageDueToDD()))
        {
            error = "Failed to click Stop note classification dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.stoppageDueSelectAll()))
        {
            error = "Failed to enter Stop note classification option :" + getData("Stop note classification");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Stop note classification drop down option.");

        //Stoppage affected other areas
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.stoppageAffectedOtherAreasDD()))
        {
            error = "Failed to wait for 'Stoppage affected other areas' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.stoppageAffectedOtherAreasDD()))
        {
            error = "Failed to click Stoppage affected other areas dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.stoppageAffectedOtherAreas_search()))
        {
            error = "Failed to enter Stoppage affected other areas option :" + getData("Stoppage affected other areas");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Stoppage affected other areas"))))
        {
            error = "Failed to wait for Stoppage affected other areas drop down option : " + getData("Stoppage affected other areas");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Stoppage affected other areas"))))
        {
            error = "Failed to click Stoppage affected other areas drop down option : " + getData("Stoppage affected other areas");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Stoppage affected other areas drop down option drop down option.");

        //Stoppage outcome
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.stoppageOutcomeDD()))
        {
            error = "Failed to wait for 'Stoppage outcome' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.stoppageOutcomeDD()))
        {
            error = "Failed to click Stoppage outcome dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.stoppageOutcome_search()))
        {
            error = "Failed to enter Stoppage outcome option :" + getData("Stoppage outcome");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Stoppage outcome"))))
        {
            error = "Failed to wait for Stoppage outcome drop down option : " + getData("Stoppage outcome");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Stoppage outcome"))))
        {
            error = "Failed to click Stoppage outcome drop down option : " + getData("Stoppage outcome");
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered Stoppage outcome drop down drop down option.");

        //Save to continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.saveToContinue()))
        {
            error = "Failed to wait for 'Save to continue..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.saveToContinue()))
        {
            error = "Failed to click save to continue.";
            return false;
        }
        
         pause(4000);

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Inspection_PageObjects.setRecord_Number(record[2]);
        String record_ = Inspection_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully save to continue.");

        return true;

    }

}
