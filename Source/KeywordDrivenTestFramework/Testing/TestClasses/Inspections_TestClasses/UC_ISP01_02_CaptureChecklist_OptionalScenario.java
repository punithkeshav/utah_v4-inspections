/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "UCISP01_02 CaptureChecklist - OptionalScenario",
        createNewBrowserInstance = false
)

public class UC_ISP01_02_CaptureChecklist_OptionalScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public UC_ISP01_02_CaptureChecklist_OptionalScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToInspections()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!CaptureChecklist()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Record is saved.  Uploaded or linked documents can be opened and viewed");
    }

    public boolean NavigateToInspections(){
        //Navigate to Inspections
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspections())){
            error = "Failed to wait for 'Inspections' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Inspections())){
            error = "Failed to click on 'Inspections' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Inspections' tab.");

        
        //Search for inspections
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.inspections_Search(),5000)){
            error = "Failed to wait for 'search' button.";
            return false;
        }
        pause(5000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.inspections_Search())){
            error = "Failed to click on 'search' button.";
            return false;
        }
        pause(5000);
        narrator.stepPassedWithScreenShot("Successfully clicked 'search' button.");

        return true;
    }
     
    public boolean CaptureChecklist(){
        
          pause(8000);
     
        
        //Click record
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.selectRecord())){
            error = "Failed to wait for 'selecting a record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.selectRecord())){
            error = "Failed to click record.";
            return false;
        }

       pause(12000);
        
    narrator.stepPassedWithScreenShot("Successfully clicked on record.");
    
      //Navigate to supporting documents panel

        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.supportingDocsTab(),5000)){
            error = "Failed to wait for 'Supporting Documents TAB'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.supportingDocsTab())){
            error = "Failed to click on 'Supporting Documents TAB.";
            return false;
        }
        pause(4000);
        narrator.stepPassedWithScreenShot("Successfully click 'Supporting Documents TAB.");
        
        //Link
        
         if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.LinkURL())){
            error = "Failed to wait for 'Supporting Documents Link URL'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkURL())){
            error = "Failed to click on 'Supporting Documents Link URL.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Supporting Documents TAB Link URL.");
        
        SeleniumDriverInstance.switchToTabOrWindow();
        
        //Link A document
       //URL
       
       if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.httpURL(),5000)){
            error = "Failed to wait for 'URL' field.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.httpURL())){
            error = "Failed to click on 'URL' field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.httpURL(), getData("URL"))){
            error = "Failed to click on 'URL.";
            return false;
        }
       pause(5500);
       narrator.stepPassedWithScreenShot("Successfully entered URL '.");
       
        // Title
        
       if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.titleURL(),5000)){
            error = "Failed to wait for 'Ttile' field.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.titleURL())){
            error = "Failed to click on 'title' field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.titleURL(), getData("Title"))){
            error = "Failed to click on 'linked' button.";
            return false;
        }
       pause(3500);
       narrator.stepPassedWithScreenShot("Successfully entered Title '.");
       pause(5500);
       //ADD
       
         if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.btnYesURL(),5000)){
            error = "Failed to wait for 'Ttile' field.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.btnYesURL())){
            error = "Failed to click on 'title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked ADD '.");


         pause(6000);
 
        return true;
    }
    
}
