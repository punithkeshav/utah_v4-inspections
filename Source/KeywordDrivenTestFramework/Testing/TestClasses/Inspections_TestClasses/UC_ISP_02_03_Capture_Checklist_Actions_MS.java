/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "UC ISP02 03 Capture Checklist Actions MS",
        createNewBrowserInstance = false
)
public class UC_ISP_02_03_Capture_Checklist_Actions_MS extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_ISP_02_03_Capture_Checklist_Actions_MS()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CaptureDetails())
        {
            return narrator.testFailed("Capture Details Failed Due To :" + error);
        }

        return narrator.finalizeTest("Captured Checklist Actions");

    }

    public boolean CaptureDetails()
    {

        pause(4000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.ActionsDropDown()))
        {
            error = "Failed to wait for actions tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.ActionsDropDown()))
        {
            error = "Failed to click on actions tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Actions Tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.ActionsAddButton()))
        {
            error = "Failed to wait for actions add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.ActionsAddButton()))
        {
            error = "Failed to click on actions add button";
            return false;
        }

        //Type Of Action
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.typeOfAction_dropdown()))
        {
            error = "Failed to wait for 'Type of action' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.typeOfAction_dropdown()))
        {
            error = "Failed to click Type of action dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.typeOfActionSearch()))
        {
            error = "Failed to enter Type of action option :" + getData("Type of action");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to click Type of action drop down option : " + getData("Type of action");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Type of action'.");
        //Action Description

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.actionDescription1(), getData("Action description")))
        {
            error = "Failed to enter  Action description :" + getData("Action description");
            return false;
        }

        //Entity
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.EntityDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.EntityDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.TypeSearch2(), getData("Entity option")))
        {
            error = "Failed to wait for Entity Unit :" + getData("Entity option");
            return false;
        }
          if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
          pause(3000);

       if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.EntityOption1(getData("Entity unit"))))
        {
            error = "Failed to wait for Entity Unit:" + getData("Entity unit");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.EntityOption1(getData("Entity unit"))))
        {
            error = "Failed to click Entity  Option drop down :" + getData("Business unit unit");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.EntityOption1(getData("Entity 1"))))
        {
            error = "Failed to wait for Entity Unit:" + getData("Entity 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.EntityOption1(getData("Entity 1"))))
        {
            error = "Failed to click Entity  Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.EntityOption1(getData("Entity 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Entity 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.EntityOption1(getData("Entity 2"))))
        {
            error = "Failed to click  Entity Option drop down :" + getData("Entity 2");
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.EntityOption1(getData("Entity 3"))))
//        {
//            error = "Failed to wait for Entity Option drop down :" + getData("Entity 2");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.EntityOption1(getData("Entity 3"))))
//        {
//            error = "Failed to click Entity Option drop down :" + getData("Entity unit 3");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.EntityOption1(getData("Entity 4"))))
//        {
//            error = "Failed to wait for Entity Option drop down :" + getData("Entity 4");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.EntityOption1(getData("Entity 4"))))
//        {
//            error = "Failed to click Entity Option drop down :" + getData("Entity 4");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.EntityOption1(getData("Entity 5"))))
//        {
//            error = "Failed to wait for Entity Option drop down :" + getData("Entity 5");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.EntityOption1(getData("Entity 5"))))
//        {
//            error = "Failed to wait for Entity Option drop down:" + getData("Entity 5");
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text3(getData("Entity option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Entity option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text3(getData("Entity option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Entity option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Entity option  :" + getData("Entity option"));

        //Responsible Person
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.responsibleDropDown()))
        {
            error = "Failed to wait for 'Responsible person' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.responsibleDropDown()))
        {
            error = "Failed to click Responsible person dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.responsible_Search()))
        {
            error = "Failed to enter Responsible person option :" + getData("Responsible person");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.responsible_Search(), getData("Responsible person")))
        {
            error = "Failed to enter  Responsible person :" + getData("Responsible person");
            return false;
        }
          if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.responsible_Search()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Responsible person1"))))
        {
            error = "Failed to wait for Responsible person drop down option : " + getData("Responsible person1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Responsible person1"))))
        {
            error = "Failed to click Responsible person drop down option : " + getData("Responsible person1");
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'Responsible person'.");

        //Agency
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.agencyDropDown()))
        {
            error = "Failed to wait for 'Agency' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.agencyDropDown()))
        {
            error = "Failed to click Agency dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.agencySearch1()))
        {
            error = "Failed to enter Agency option :" + getData("Agency");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Agency"))))
        {
            error = "Failed to wait for Agency drop down option : " + getData("Agency");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Agency"))))
        {
            error = "Failed to click Agency drop down option : " + getData("Agency");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Agency'.");
        //Action Due Date

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.actionDueDate()))
        {
            error = "Failed to wait for action due date.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.actionDueDate(), startDate))
        {
            error = "Failed to enter '" + startDate + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + startDate + "'.");

        //Multiple users (in addition to the user selected under the responsibility field)
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.multiUserTickBox1()))
        {
            error = "Failed to click Responsible person drop down option : " + getData("Responsible person1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.multiUserDropDown()))
        {
            error = "Failed to wait for 'Agency' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.multiUserDropDown()))
        {
            error = "Failed to click Agency dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.multiUserSearch1(), getData("Multiple users")))
        {
            error = "Failed to enter  Multiple users :" + getData("Multiple users");
            return false;
        }
           if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.multiUserSearch1()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.multiUserTickBox(getData("Multiple users"))))
        {
            error = "Failed to wait for multiple users drop down option : " + getData("Multiple users");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.multiUserTickBox(getData("Multiple users"))))
        {
            error = "Failed to click multiple users drop down option : " + getData("Multiple users");
            return false;
        }
        narrator.stepPassedWithScreenShot("Multiple users :" + getData("Multiple users"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.ActionSave()))
        {
            error = "Failed to wait for save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.ActionSave()))
        {
            error = "Failed to click save";
            return false;
        }
        pause(4000);

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Inspection_PageObjects.setRecord_Number(record[2]);
        String record_ = Inspection_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully saved actions.");

        return true;

    }
}
