/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR9_Delete_Inspection_MainScenario",
        createNewBrowserInstance = false
)

public class FR9_Delete_Inspection_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR9_Delete_Inspection_MainScenario()

    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!DeleteInspection())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Delete Inspection. ");
    }

    public boolean DeleteInspection()
    {//close window 
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.PresentExit_Button_1()))
        {
            error = "Failed to wait for cross button to close page";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.PresentExit_Button_1()))
        {
            error = "Failed to click cross button to close page";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Inspection_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to ifram";
            return false;
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.getActionRecord());
        String record_ = Inspection_PageObjects.getRecord_Number();
        String[] array = record_.split("#");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspections_Add()))
        {
            error = "Failed to wait for Add button.";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.DeleteButton()))
        {
            error = "Failed wait for delete button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.DeleteButton()))
        {
            error = "Failed click delete button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);

        return true;
    }

}
