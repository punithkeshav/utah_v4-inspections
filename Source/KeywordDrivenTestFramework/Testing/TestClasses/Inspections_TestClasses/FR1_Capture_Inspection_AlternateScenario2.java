/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */

@KeywordAnnotation(
        Keyword = "FR1 Capture Inspection - AlternateScenario2",
        createNewBrowserInstance = false
)

public class FR1_Capture_Inspection_AlternateScenario2 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Capture_Inspection_AlternateScenario2()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToInspections()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!CaptureInspection()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Captured Inspection:Inspections record is saved");
    }

   public boolean NavigateToInspections(){
        //Navigate to Inspections
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspections())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Inspections())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Inspections' tab.");

        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspections_Add(),5000)){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Inspections_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    public boolean CaptureInspection(){
        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspection_processflow(), 10000)){
            error = "Failed to wait for process flow button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Inspection_processflow())){
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");
        
        //Business unit
         if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.businessUnit_dropdown()))
        {
            error = "Failed to wait for 'Business Unit' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.businessUnit_dropdown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.businessUnit_search2()))
        {
            error = "Failed to enter Business Unit option :" + getData("Business Unit");
            return false;
        }
 pause(5000);
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.businessUnit_search2(), getData("Business Unit")))
        {
            error = "Failed to enter  Business Unit :" + getData("Business Unit");
            return false;
        }
         if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.businessUnit_search2()))
        {
            error = "Failed to press enter";
            return false;
        }
         pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Business Unit"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Business Unit"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business Unit");
            return false;
        }
  //New Functional Location
//  
//  
//   if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Functional_dropdown()))
//        {
//            error = "Failed to wait for 'Functional Location' dropdown..";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Functional_dropdown()))
//        {
//            error = "Failed to click Functional Location dropdown.";
//            return false;
//        }
//       
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Functional_search1()))
//        {
//            error = "Failed to enter Functional Location option :" + getData("Functional Location");
//            return false;
//        }
//		
//      pause(8000);
//        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Functional Location"))))
//        {
//            error = "Failed to wait for Functional Location drop down option : " + getData("Functional Location");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Functional Location"))))
//        {
//            error = "Failed to click Functional Location drop down option : " + getData("Functional Location");
//            return false;
//        }
        //New Type
        
          if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Type_dropdown()))
        {
            error = "Failed to wait for 'Type' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Type_dropdown()))
        {
            error = "Failed to click Type dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.type_Search1()))
        {
            error = "Failed to enter Type option :" + getData("Type");
            return false;
        }

      pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Type"))))
        {
            error = "Failed to wait for Type drop down option : " + getData("Type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Type"))))
        {
            error = "Failed to click Type drop down option : " + getData("Type");
            return false;
        }
        

        //Date conducted
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.dateConducted())){
            error = "Failed to wait for Date conducted field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.dateConducted(), startDate)){
            error = "Failed to enter '" + startDate + "' into Date conducted field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Date conducted: '" + startDate + "'.");
        
        
        
  
        
        //Project Link
        if(getData("Tick Project").equalsIgnoreCase("NO")){

            //Responsible Person
  //New ResponsibePerson

         if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.responsiblePersson_dd()))
        {
            error = "Failed to wait for 'Responsible Person' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.responsiblePersson_dd()))
        {
            error = "Failed to click Responsible Person dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.responsible_Search()))
        {
            error = "Failed to enter Responsible Person option :" + getData("Responsible Person");
            return false;
        }
		
    pause(3000);

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.responsible_Search(), getData("Responsible Person")))
        {
            error = "Failed to enter  Responsible Person :" + getData("Responsible Person");
            return false;
        }
         if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.responsible_Search()))
        {
            error = "Failed to press enter";
            return false;
        }
		
    pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Responsible Person"))))
        {
            error = "Failed to wait for Responsible Person drop down option : " + getData("Responsible Person");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Responsible Person"))))
        {
            error = "Failed to click Responsible Person drop down option : " + getData("Responsible Person");
            return false;
        }
           
 //Owner
 if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.owner_Dropdown()))
        {
            error = "Failed to wait for 'Owner' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.owner_Dropdown()))
        {
            error = "Failed to click Owner dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.owner_Search()))
        {
            error = "Failed to enter Owner option :" + getData("Owner");
            return false;
        }
		
    pause(3000);

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.owner_Search(), getData("Owner")))
        {
            error = "Failed to enter  Owner :" + getData("Owner");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.owner_Search()))
        {
            error = "Failed to press enter";
            return false;
        }
		
   pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Owner"))))
        {
            error = "Failed to wait for Owner drop down option : " + getData("Owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Owner"))))
        {
            error = "Failed to click Owner drop down option : " + getData("Owner");
            return false;
        }
    pause(3000);
    
          //New Team alternative
 
   if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.team_Dropdown()))
        {
            error = "Failed to wait for 'Team' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.team_Dropdown()))
        {
            error = "Failed to click Team dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.team_Search()))
        {
            error = "Failed to enter Team option :" + getData("Team");
            return false;
        }
		
 pause(3000);

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.team_Search(), getData("Team")))
        {
            error = "Failed to enter  Team :" + getData("Team");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.team_Search()))
        {
            error = "Failed to press enter";
            return false;
        }
		
      pause(5000);

 if (!SeleniumDriverInstance.clickElementByJavascript(Inspection_PageObjects.team_select()))
            {
                error = "Failed to enter Tem option :" + getData("Team");
                return false;
            }

            narrator.stepPassedWithScreenShot("Team: Populated on dropdown.");
        }
 pause(3000);
              //Equipment
if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.linkedEqupmentTickBox())){
            error = "Failed to wait for responsible owner tick box.";
            return false;
        }
         if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.linkedEqupmentTickBox())){
            error = "Failed to select responsible owner tick box.";
            return false;
        }
         
     
           if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.LinkedEquipment_dropdown()))
        {
            error = "Failed to wait for 'Linked Equipment' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkedEquipment_dropdown()))
        {
            error = "Failed to click Linked Equipment dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.text_Search()))
        {
            error = "Failed to enter Linked Equipment option :" + getData("Linked Equipment");
            return false;
        }
		
   pause(3000);

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.text_Search(), getData("Linked Equipment")))
        {
            error = "Failed to enter  Linked Equipment :" + getData("Linked Equipment");
            return false;
        }
         if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.text_Search()))
        {
            error = "Failed to press enter";
            return false;
        }
		
      pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Linked Equipment"))))
        {
            error = "Failed to wait for Linked Equipment drop down option : " + getData("Linked Equipment");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Linked Equipment"))))
        {
            error = "Failed to click Linked Equipment drop down option : " + getData("Linked Equipment");
            return false;
        }

        narrator.stepPassedWithScreenShot("Linked Equipment: '" + getData("Linked Equipment") + "'.");
  
        pause(5000);    
//        
//    
//     // Team Name
//    
//      if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.teamNameDD()))
//        {
//            error = "Failed to wait for 'Team Name' dropdown..";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.teamNameDD()))
//        {
//            error = "Failed to click Team Name dropdown.";
//            return false;
//        }
//        
//         pause(3000);
//        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Team Name"))))
//        {
//            error = "Failed to wait for Team Name drop down option : " + getData("Team Name");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Team Name"))))
//        {
//            error = "Failed to click Team Name drop down option : " + getData("Team Name");
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Team Name: '" + getData("Team Name") + "'.");

        
        if(getData("Save").equalsIgnoreCase("YES")){
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.SaveToContinue_Button())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.SaveToContinue_Button())){
            error = "Failed to click 'Save to continue' button.";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.saveWait(), 400)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Inspection_PageObjects.saveWait2(), 400)) {
                error = "Website too long to load wait reached the time out";
                return false;
            }
        }
        pause(8000);

        
        }else{
             if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.submitInspection_Button())){
            error = "Failed to wait for 'Submit Inspection' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.submitInspection_Button())){
            error = "Failed to click 'Submit Inspection' button.";
            return false;
        }
        pause(10000);
        }
        return true;
    }
    
}