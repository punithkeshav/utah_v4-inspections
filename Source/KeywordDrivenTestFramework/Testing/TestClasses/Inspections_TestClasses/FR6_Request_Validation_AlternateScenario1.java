/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
/**
 *
 * @author LDisemelo
 */


@KeywordAnnotation(
        Keyword = "FR6 Request_Validation_AlternateScenario",
        createNewBrowserInstance = false
)
public class FR6_Request_Validation_AlternateScenario1 extends BaseClass {
    
           String error = "";
           SikuliDriverUtility sikuliDriverUtility;
    
    
    public FR6_Request_Validation_AlternateScenario1()
    
       {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
          
          
      }
      
    public TestResult executeTest()
    {
        
       
        if(!RequestValidation()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully requested Validation");
    }
      
      
      public boolean RequestValidation()
              {
                  pause(5000);
        //Navigate to checklist Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklistTab())){
            error = "Failed to wait for 'Checklist' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklistTab())){
            error = "Failed to click on 'Checklist' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Checklist' tab.");
        
        pause(9000);
        //Add Checklist

        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_Add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklist_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(6000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_processflow(), 10000)){
            error = "Failed to wait for process flow button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklist_processflow())){
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");
        
        pause(4000);
        //Select Checklist New Drodown
         if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_dropdown()))
        {
            error = "Failed to wait for Checklist dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklist_dropdown()))
        {
            error = "Failed to click Checklist dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_search()))
        {
            error = "Failed to wait for Measurement taken by text box.";
            return false;
        }


        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Checklist"))))
        {
            error = "Failed to wait for Checklist drop down option : " + getData("Checklist");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Checklist"))))
        {
            error = "Failed to click Checklist drop down option : " + getData("Checklist");
            return false;
        }
       
 //Select Checklist
 
 //Save and start check list
        pause(9000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.startChecklist())){
            error = "Failed to wait for 'save and start checklist button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.startChecklist())){
            error = "Failed to click save and start checklist button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully click 'save and start checklist button.");
        pause(14000);
        
        //Startchecklist 
        
      if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.startChecklist1())){
            error = "Failed to wait for 'start button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.startChecklist1())){
            error = "Failed to click start button.";
            return false;
        }
         pause(10000);
 
         //Checklist Questions
           if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.questionDropDown()))
        {
            error = "Failed to wait for 'Question 1' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.questionDropDown()))
        {
            error = "Failed to click Question 1 dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.question_search()))
        {
            error = "Failed to enter Question 1 option :" + getData("Question 1");
            return false;
        }
		

      pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Question 1"))))
        {
            error = "Failed to wait for Question 1 drop down option : " + getData("Question 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Question 1"))))
        {
            error = "Failed to click Question 1 drop down option : " + getData("Question 1");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed 'question 1 dropdown.");
         //Checklist
  

         
        // Click Next
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.next_Button())){
            error = "Failed to wait for 'next button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.next_Button())){
            error = "Failed to click next button.";
            return false;
        }
     pause(5000);
        
   narrator.stepPassedWithScreenShot("Successfully clicked 'next button.");
   //Checklist 2
      if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.questionDropDown1()))
        {
            error = "Failed to wait for 'Question 1' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.questionDropDown1()))
        {
            error = "Failed to click Question 1 dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.question_search1()))
        {
            error = "Failed to enter Question 2 option :" + getData("Question 2");
            return false;
        }
		

      pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Question 2"))))
        {
            error = "Failed to wait for Question 2 drop down option : " + getData("Question 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Question 2"))))
        {
            error = "Failed to click Question 2 drop down option : " + getData("Question 2");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed 'question 2 dropdown.");
   
//   
         //Finish
         
             
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.finish_Button1())){
            error = "Failed to wait for 'save current section button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.finish_Button1())){
            error = "Failed to save current section button.";
            return false;
        }
     pause(4000);
     
     //Exit
     SeleniumDriverInstance.switchToFrameByXpath(Inspection_PageObjects.iframeCross());
     
      if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.exit_Button1())){
            error = "Failed to wait for 'save current section button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.exit_Button1())){
            error = "Failed to save current section button.";
            return false;
        }
     pause(8000);
     
     //Request validation
      if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.requestValidationDropDown()))
        {
            error = "Failed to wait for 'request Validation DropDown' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.requestValidationDropDown()))
        {
            error = "Failed to click request Validation DropDown dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.requestValidationSearh()))
        {
            error = "Failed to enter Request validation option :" + getData("Request validation");
            return false;
        }
		

      pause(5000);
      if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Request validation"))))
        {
            error = "Failed to wait for Request validation drop down option : " + getData("Request validation");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Request validation"))))
        {
            error = "Failed to click Request validation drop down option : " + getData("Request validation");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed 'Request validationdropdown.");
     pause(5000);
     
          //Process Flow
     
     if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.requestValiadation_processflow(), 10000)){
            error = "Failed to wait for process flow button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.requestValiadation_processflow())){
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");
        
        pause(3000);
     //Save
     pause(5000);
     
       if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.requestSaveButton()))
        {
            error = "Failed to wait for 'request Validation save button..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.requestSaveButton()))
        {
            error = "Failed to click request Validation save button.";
            return false;
        }
     
       narrator.stepPassedWithScreenShot("Successfully completed request Validation save button.");
      pause(8000);
     
     
     
     
     
                  return true;
              }
      
      
    
}

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

