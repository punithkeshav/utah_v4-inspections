/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "FR7 Capture_Validation_AlternateScenario",
        createNewBrowserInstance = false
)
public class FR7_Capture_Validation_AlternateScenario1 extends BaseClass {
    
      String error = "";
      SikuliDriverUtility sikuliDriverUtility;
    
    
public FR7_Capture_Validation_AlternateScenario1()
    {
    
     this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }
    public TestResult executeTest()
            
    {
        
       
        if(!CaptureValidation()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully requested Validation");
    }
      
      
      public boolean CaptureValidation()
      {
      //Navigate Validation TAB
          
          pause(12000);
          
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.validationTab())){
            error = "Failed to wait for 'validation' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.validationTab())){
            error = "Failed to click on 'validation' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'validation' tab.");
          
        pause(3000);
          //Comments
    
     if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.comment(), getData("Comments")))
        {
            error = "Failed to enter  Commentse :" + getData("Comments");
            return false;
        }
           narrator.stepPassedWithScreenShot("Successfully entered comments.");
           
     pause(5000);
     
        
          //Save

       if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.requestSaveButton())){
            error = "Failed to wait for 'save.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.requestSaveButton())){
            error = "Failed to click on save.";
            return false;
        }
         narrator.stepPassedWithScreenShot("Successfully saved.");

       
      
     
return true;

}
}
    
    
    
    
    

