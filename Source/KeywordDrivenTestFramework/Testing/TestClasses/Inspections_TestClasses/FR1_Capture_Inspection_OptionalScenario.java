/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR1 Capture Inspection - OptionalScenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Inspection_OptionalScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Capture_Inspection_OptionalScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
       
        if(!SaveDocs()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Captured Inspection:Inspections record is saved");
    }

    public boolean SaveDocs(){
        pause(3300);
        //Navigate to Supporting documents Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.supportingDocsTab())){
            error = "Failed to wait for 'Supporting documents' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.supportingDocsTab())){
            error = "Failed to click on 'Supporting documents' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Supporting documents' tab.");

        
       //Link A document
       
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.LinkURL(),5000)){
            error = "Failed to wait for 'linked' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkURL())){
            error = "Failed to click on 'linked' button.";
            return false;
        }
        pause(4000);
        narrator.stepPassedWithScreenShot("Successfully click 'linked' button.");
        
        SeleniumDriverInstance.switchToTabOrWindow();

        //Enter Url
         if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.httpURL(),5000)){
            error = "Failed to wait for 'linked' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.httpURL())){
            error = "Failed to click on 'linked' button.";
            return false;
        }
        
               if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.httpURL(), getData("URL")))
       
            {
                error = "Failed to enter Business Unit option :" + getData("URL");
                return false;
            }
               
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.httpTitle(), getData("Title")))
       
            {
                error = "Failed to enter Title  :" + getData("Title");
                return false;
            }
        
            if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.httpAdd())){
            error = "Failed to click on 'ADD' button.";
            return false;
        }
        
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully clicked the Add button.");
        
               pause(5000);
//
    
    return true;
}

} 

