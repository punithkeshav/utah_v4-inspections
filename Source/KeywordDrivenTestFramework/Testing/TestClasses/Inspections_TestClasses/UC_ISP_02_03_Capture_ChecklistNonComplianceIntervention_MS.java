/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "UC ISP02 03 Capture Checklist Non-Compliance Intervention MS",
        createNewBrowserInstance = false
)
public class UC_ISP_02_03_Capture_ChecklistNonComplianceIntervention_MS extends BaseClass
{
    
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public UC_ISP_02_03_Capture_ChecklistNonComplianceIntervention_MS()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }
    
    public TestResult executeTest()
    {
        
        if (!CaptureDetails())
        {
            return narrator.testFailed("Capture Details Failed Due To :" + error);
        }
        
        return narrator.finalizeTest("Capture Checklist Non-Compliance Intervention");
        
    }
    
    public boolean CaptureDetails()
    {
        
        pause(7000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.NonComplianceDropDown()))
        {
            error = "Failed to wait for Non Compliance tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.NonComplianceDropDown()))
        {
            error = "Failed to click on Non Compliance tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Actions Tab.");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.NonComplianceAddButton()))
        {
            error = "Failed to wait for Non Compliance add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.NonComplianceAddButton()))
        {
            error = "Failed to click on Non Compliance add button";
            return false;
        }

        //Date of non-compliance
        
          if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.DateofNonCompliance()))
        {
            error = "Failed to wait for Date of non-compliance";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.DateofNonCompliance(), startDate))
        {
            error = "Failed to Date of non-compliance :" + startDate;
            return false;
        }
        //Reported date of non-compliance
           if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.ReportedDateofNonCompliance()))
        {
            error = "Failed to wait for Date of non-compliance";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.ReportedDateofNonCompliance(), startDate))
        {
            error = "Failed to Date of non-compliance :" + startDate;
            return false;
        }
        
        
        //Reported by
        
           if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.ReportedByDropDown()))
        {
            error = "Failed to wait for Reported by dropdown";
            return false;
        }
         if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.ReportedByDropDown()))
        {
            error = "Failed to click Reported by drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.TypeSearch(), getData("Reported by")))
        {
            error = "Failed to enterReported by :" + getData("Reported by");
            return false;
        }
              if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text(getData("Reported by"))))
        {
            error = "Failed to wait for Reported by drop down option : " + getData("Reported by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text(getData("Reported by"))))
        {
            error = "Failed to click Reported by todrop down option : " + getData("Reported by");
            return false;
        }
        
         //Person responsible for non-compliance intervention upliftment
          if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.PersonResponsibleDropDown()))
        {
            error = "Failed to wait for Person responsible for non-compliance intervention upliftment dropdown";
            return false;
        }
         if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.PersonResponsibleDropDown()))
        {
            error = "Failed to click Person responsible for non-compliance intervention upliftment drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Person responsible for non-compliance intervention upliftment text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.TypeSearch(), getData("Person responsible for non-compliance intervention upliftment")))
        {
            error = "Failed to enter Person responsible for non-compliance intervention upliftment:" + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }
              if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Person responsible for non-compliance intervention upliftment"))))
        {
            error = "Failed to wait for Person responsible for non-compliance intervention upliftment drop down option : " + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Person responsible for non-compliance intervention upliftment"))))
        {
            error = "Failed to click Person responsible for non-compliance intervention upliftment drop down option : " + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }
        //Non-compliance intervention issued to
          if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.NonComplianceInterventionDropDown()))
        {
            error = "Failed to wait forNon-compliance intervention issued to dropdown";
            return false;
        }
          pause(3000);
         if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.NonComplianceInterventionDropDown()))
        {
            error = "Failed to click Non-compliance intervention issued to drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Non-compliance intervention issued to text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.TypeSearch(), getData("Non-compliance intervention issued to")))
        {
            error = "Failed to enter Non-compliance intervention issued to:" + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }
              if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Non-compliance intervention issued to"))))
        {
            error = "Failed to wait for Non-compliance intervention issued to drop down option : " + getData("Non-compliance intervention issued to");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Non-compliance intervention issued to"))))
        {
            error = "Failed to click Non-compliance intervention issued todrop down option : " + getData("Non-compliance intervention issued to");
            return false;
        }
       
        
        //Type of non-compliance intervention
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeofNonComplianceInterventionDropDown()))
        {
            error = "Failed to wait for Type of non-compliance intervention dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.TypeofNonComplianceInterventionDropDown()))
        {
            error = "Failed to click Type of non-compliance intervention dropdown.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for type search text box.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Type of non-compliance intervention"))))
        {
            error = "Failed to wait for Type of non-compliance intervention drop down option : " + getData("Type of non-compliance intervention");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Type of non-compliance intervention"))))
        {
            error = "Failed to click Type of non-compliance intervention drop down option : " + getData("Type of non-compliance intervention");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Type of non-compliance intervention :" + getData("Type of non-compliance intervention"));
        //Stoppage initiated by
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.StoppageInitiatedByDropDown()))
        {
            error = "Failed to wait for Stoppage initiated by dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.StoppageInitiatedByDropDown()))
        {
            error = "Failed to click Stoppage initiated by dropdown.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for type search text box.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Stoppage initiated by"))))
        {
            error = "Failed to wait for Stoppage initiated by drop down option : " + getData("Stoppage initiated by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Stoppage initiated by"))))
        {
            error = "Failed to click Stoppage initiated by drop down option : " + getData("Stoppage initiated by");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Stoppage initiated by :" + getData("Stoppage initiated by"));
        //Work stoppage
         if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.WorkStoppageDropDown()))
        {
            error = "Failed to wait for Work stoppage dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.WorkStoppageDropDown()))
        {
            error = "Failed to click Work stoppage dropdown.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for type search text box.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Work stoppage"))))
        {
            error = "Failed to wait for Work stoppage drop down option : " + getData("Work stoppage");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Work stoppage"))))
        {
            error = "Failed to click Work stoppage drop down option : " + getData("Work stoppage");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Work stoppage :" + getData("Work stoppage"));
        
        //Description of reason for work stoppage
         if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.DescriptionofReason()))
        {
            error = "Failed to wait for Description of reason for work stoppage text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.DescriptionofReason(), getData("Description of reason for work stoppage")))
        {
            error = "Failed to enter  Description of reason for work stoppage :" + getData("Description of reason for work stoppage");
            return false;
        }
                narrator.stepPassedWithScreenShot("Description of reason for work stoppage :" + getData("Description of reason for work stoppage"));

        
        //Stop note classification
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.StopNoteClassificationDropDown()))
        {
            error = "Failed to wait for Stop note classification dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.StopNoteClassificationDropDown()))
        {
            error = "Failed to click Stop note classification dropdown.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for type search text box.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Stop note classification"))))
        {
            error = "Failed to wait for Stop note classification drop down option : " + getData("Stop note classification");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Stop note classification"))))
        {
            error = "Failed to click Stop note classification drop down option : " + getData("Stop note classification");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Stop note classification :" + getData("Stop note classification"));

        //Stoppage due to
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.StoppageDueToDropDown()))
        {
            error = "Failed to wait for Stoppage due to dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.StoppageDueToDropDown()))
        {
            error = "Failed to click Stoppage due to dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.multiUserTickBox(getData("Stoppage due to"))))
        {
            error = "Failed to wait for stoppage due to drop down option : " + getData("Stoppage due to");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.multiUserTickBox(getData("Stoppage due to"))))
        {
            error = "Failed to click stoppage due to drop down option : " + getData("Stoppage due to");
            return false;
        }
        narrator.stepPassedWithScreenShot("Stoppage due to :" + getData("Stoppage due to"));
        
         if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.StoppageDueToDropDown()))
        {
            error = "Failed to click Stoppage due to dropdown.";
            return false;
        }
            
        //Stoppage outcome             
         if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.StoppageOutcomeDropDown()))
        {
            error = "Failed to wait for Stoppage outcome dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.StoppageOutcomeDropDown()))
        {
            error = "Failed to click Stoppage outcome dropdown.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for type search text box.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Stoppage outcome"))))
        {
            error = "Failed to wait for Stoppage outcome drop down option : " + getData("Stoppage outcome");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Stoppage outcome"))))
        {
            error = "Failed to click Stoppage outcome drop down option : " + getData("Stoppage outcome");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Stoppage outcome :" + getData("Stoppage outcome"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.NonComplianceSave()))
        {
            error = "Failed to wait for save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.NonComplianceSave()))
        {
            error = "Failed to click save";
            return false;
        }
        pause(4000);
        
        String saved = "";
        
        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }
        
        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }
            
            String failed = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.failed());
            
            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Inspection_PageObjects.setRecord_Number(record[2]);
        String record_ = Inspection_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully saved  Non-Compliance Intervention.");
        
        return true;
        
    }
}
