/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR1 Capture Inspection - MainScenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Inspection_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Inspection_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToInspections())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!CaptureInspection())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Inspection:Inspections record is saved");
    }

    public boolean NavigateToInspections()
    {
        //Navigate to Inspections
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspections()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspections_2()))
            {

                error = "Failed to wait for 'Inpections' tab.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Inspections()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Inspections_2()))
            {
                error = "Failed to click on 'Environmental, Health & Safety' tab.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Inspections' tab.");

        pause(5000);
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspections_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Inspections_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean CaptureInspection()
    {
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspection_processflow(), 10000))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Inspection_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //New business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.businessUnit_dropdown()))
        {
            error = "Failed to wait for Applicable business units.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.businessUnit_dropdown()))
        {
            error = "Failed to click Applicable business units.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for type search ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to enter Business unit option:" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter ";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait for Business unit:" + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to click Site Option drop down :" + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Site Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Site:" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click Site Option drop down :" + getData("Business unit 2");
            return false;
        }
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Site:" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Site Option drop down :" + getData("Business unit 3");
            return false;
        }
        
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Site:" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Site Option drop down :" + getData("Business unit 4");
            return false;
        }
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Site:" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to click Site Option drop down :" + getData("Business unit 5");
            return false;
        }
        

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to wait for Site:" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to click Site Option drop down :" + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Site option  :" + getData("Business unit option"));

        //Type
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Type_dropdown()))
        {
            error = "Failed to wait for 'Type' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Type_dropdown()))
        {
            error = "Failed to click Type dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.type_Search1()))
        {
            error = "Failed to enter Type option :" + getData("Type");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Type"))))
        {
            error = "Failed to wait for Type drop down option : " + getData("Type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Type"))))
        {
            error = "Failed to click Type drop down option : " + getData("Type");
            return false;
        }
        //Checklists
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Checklists_dropdown()))
        {
            error = "Failed to wait for Checklists dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Checklists_dropdown()))
        {
            error = "Failed to click Checklists dropdown.";
            return false;
        }
        pause(4000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Checklists_dropdown_Option(getData("Checklists"))))
        {
            error = "Failed to wait for Checklists dropdown option :" + getData("Checklists");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Checklists_dropdown_Option(getData("Checklists"))))
        {
            error = "Failed to click Checklists dropdown option :" + getData("");
            return false;
        }
        pause(4000);

        //Start Date
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.dateConducted()))
        {
            error = "Failed to wait for Date conducted field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.dateConducted(), startDate + 1))
        {
            error = "Failed to enter '" + startDate + "' into Date conducted field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Date conducted: '" + startDate + "'.");
        
        
          //Due date
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.DueDate()))
        {
            error = "Failed to wait for Due date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.DueDate(), endDate ))
        {
            error = "Failed to enter Due date :" + endDate;
            return false;
        }
        narrator.stepPassedWithScreenShot("Due date: '" + endDate + "'.");
        
        

        //Project Link
        if (getData("Project link checkbox").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Projectlink_checkbox()))
            {
                error = "Failed to wait for 'Project link' checkbox.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Projectlink_checkbox()))
            {
                error = "Failed to click 'Project link' checkbox.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Project_dropdown()))
            {
                error = "Failed to wait for 'Project' dropdown..";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Project_dropdown()))
            {
                error = "Failed to click Project dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.project_Search()))
            {
                error = "Failed to enter Project option :" + getData("Project");
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.project_Search(), getData("Project")))
            {
                error = "Failed to enter  Project :" + getData("Project");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.project_Search()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text4(getData("Project"))))
            {
                error = "Failed to wait for Project drop down option : " + getData("Project");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text4(getData("Project"))))
            {
                error = "Failed to click Project drop down option : " + getData("Project");
                return false;
            }

        }

        //Responsible Person
        if (getData("Responsible person checkbox").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.responPersonTickBox()))
            {
                error = "Failed to wait for responsible owner tick box.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.responPersonTickBox()))
            {
                error = "Failed to select responsible owner tick box.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Responsible person tickbox checked");

        }

        //Owner 
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.owner_Dropdown()))
        {
            error = "Failed to wait for 'Owner' dropdown..";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.owner_Dropdown()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.owner_Dropdown()))
            {
                error = "Failed to click Owner dropdown.";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.owner_Search()))
        {
            error = "Failed to enter Owner option :" + getData("Owner");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.owner_Search(), getData("Owner")))
        {
            error = "Failed to enter  Owner :" + getData("Owner");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.owner_Search()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Owner"))))
        {
            error = "Failed to wait for Owner drop down option : " + getData("Owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Owner"))))
        {
            error = "Failed to click Owner drop down option : " + getData("Owner");
            return false;
        }

        if (getData("Linked Equipment checkbox").equalsIgnoreCase("True"))
        {
            //Linked equipment
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.linkedEqupmentTickBox()))
            {
                error = "Failed to wait for Linked Equipment tick box.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.linkedEqupmentTickBox()))
            {
                error = "Failed to select Linked Equipmenttick box.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.LinkedEquipment_dropdown()))
            {
                error = "Failed to wait for 'Linked Equipment' dropdown..";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkedEquipment_dropdown()))
            {
                error = "Failed to click Linked Equipment dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Was Equipment Involved text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.TypeSearch(), getData("Linked Equipment")))
            {
                error = "Failed to enter   Linked Equipment :" + getData("Linked Equipment");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            pause(6000);

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.LinkedEquipmentexpandButton(getData("Linked Equipment"))))
            {
                error = "Failed to wait for Linked Equipment option :" + getData("Linked Equipment");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkedEquipmentexpandButton(getData("Linked Equipment"))))
            {
                error = "Failed to click Linked Linked Equipment option  drop down :" + getData("Linked Equipment");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text3(getData("Linked Equipment 1"))))
            {
                error = "Failed to wait for Linked Equipment option :" + getData("Linked Equipment 1");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text3(getData("Linked Equipment 1"))))
            {
                error = "Failed to click Linked Equipment option  drop down :" + getData("Linked Equipment 1");
                return false;
            }
            narrator.stepPassedWithScreenShot("Linked Equipment :" + getData("Linked Equipment 1"));

        }
        //Linked to social initiative
        if (getData("Linked to social initiative checkbox").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.LinkedsocialInitiativeTickBox()))
            {
                error = "Failed to wait for Linked to social initiative tick box.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkedsocialInitiativeTickBox()))
            {
                error = "Failed to select Linked to social initiative tick box.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.LinkedsocialInitiative_dropdown()))
            {
                error = "Failed to wait for Linked to social initiative dropdown..";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkedsocialInitiative_dropdown()))
            {
                error = "Failed to click Linked to social initiative dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Linked to social initiative text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.TypeSearch2(), getData("Social Initiatives")))
            {
                error = "Failed to enter Social Initiatives :" + getData("Social Initiatives");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            pause(6000);

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text3(getData("Social Initiatives"))))
            {
                error = "Failed to wait for Social Initiatives option :" + getData("Social Initiatives");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text3(getData("Social Initiatives"))))
            {
                error = "Failed to click Social Initiatives option  drop down :" + getData("Social Initiatives");
                return false;
            }
            narrator.stepPassedWithScreenShot("Social Initiatives :" + getData("Social Initiatives"));

        }

        //Linked stakeholder
        if (getData("Linked stakeholder checkbox").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.LinkedStakeholderTickBox()))
            {
                error = "Failed to wait for Linked stakeholder tick box.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkedStakeholderTickBox()))
            {
                error = "Failed to select Linked stakeholder tick box.";
                return false;
            }
            pause(3000);
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.LinkedStakeholder_dropdown()))
            {
                error = "Failed to wait for Linked stakeholderdropdown..";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkedStakeholder_dropdown()))
            {
                error = "Failed to click Linked stakeholder dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Linked stakeholder text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.TypeSearch(), getData("Linked stakeholder")))
            {
                error = "Failed to enter Linked stakeholder:" + getData("Linked stakeholder");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            pause(6000);
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.LinkedStakeholderExpandButton()))
            {
                error = "Failed to wait to expand Linked stakeholder";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkedStakeholderExpandButton()))
            {
                error = "Failed to expand Linked stakeholder";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checkbox(getData("Linked stakeholder"))))
            {
                error = "Failed to wait for Linked stakeholder option :" + getData("Linked stakeholder");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checkbox(getData("Linked stakeholder"))))
            {
                error = "Failed to click Social Initiatives option  drop down :" + getData("Linked stakeholder");
                return false;
            }
            narrator.stepPassedWithScreenShot("Linked stakeholder :" + getData("Linked stakeholder"));

            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkedStakeholder_dropdown()))
            {
                error = "Failed to click Linked stakeholder dropdown.";
                return false;
            }

        }

        //Linked permit
        if (getData("Linked permit checkbox").equalsIgnoreCase("True"))
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.LinkedPermitTickBox()))
            {
                error = "Failed to wait for Linked permit tick box.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkedPermitTickBox()))
            {
                error = "Failed to select Linked permit tick box.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.LinkedPermit_dropdown()))
            {
                error = "Failed to wait for Linked to social initiative dropdown..";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.LinkedPermit_dropdown()))
            {
                error = "Failed to click Linked permit dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Linked permit text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.TypeSearch(), getData("Permit")))
            {
                error = "Failed to enter Permit :" + getData("Permit");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(Inspection_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text4(getData("Permit"))))
            {
                error = "Failed to wait for Permit option :" + getData("Permit");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text4(getData("Permit"))))
            {
                error = "Failed to click Permit option  drop down :" + getData("Permit");
                return false;
            }
            narrator.stepPassedWithScreenShot("Permit :" + getData("Permit"));

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(5000);

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Inspection_PageObjects.setRecord_Number(record[2]);
        String record_ = Inspection_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;

    }
}
