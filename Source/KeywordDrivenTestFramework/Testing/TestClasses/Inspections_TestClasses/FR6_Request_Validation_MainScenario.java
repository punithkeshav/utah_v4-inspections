/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR6 Request_Validation_MainScenario",
        createNewBrowserInstance = false
)

public class FR6_Request_Validation_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_Request_Validation_MainScenario()

    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest()
    {

        if (!RequestValidation())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully requested Validation");
    }

    public boolean RequestValidation()
    {
        ///PresentExit_Button

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.PresentExit_Button()))
        {
            error = "Failed to wait for cross button to close page";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.PresentExit_Button()))
        {
            error = "Failed to click cross button to close page";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.PresentExit_Button()))
        {
            error = "Failed to wait for cross button to close page";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.PresentExit_Button()))
        {
            error = "Failed to click cross button to close page";
            return false;
        }
        narrator.stepPassedWithScreenShot("1");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.PresentExit_Button()))
        {
            error = "Failed to wait for cross button to close page";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.PresentExit_Button()))
        {
            error = "Failed to click cross button to close page";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully navigated to 'Inspections' tab.");

        //Navigate to Inspections
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspections()))
        {
            error = "Failed to wait for 'Inpections' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Inspections()))
        {
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("2");

        pause(5000);

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.getActionRecord());
        String record_ = Inspection_PageObjects.getRecord_Number();
        String[] array = record_.split("#");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspections_Add()))
        {
            error = "Failed to wait for Add button.";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.FirstRecordIn_checklist()))
        {
            error = "Failed to wait for first record in checklist";
            return false;
        }

        if (!SeleniumDriverInstance.RighclickElementbyXpath(Inspection_PageObjects.FirstRecordIn_checklist()))
        {
            error = "Failed to click first record in checklist";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click first record in checklist");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Record_Deletepopup()))
        {
            error = "Failed to wait for delete record pop up";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Record_Deletepopup()))
        {
            error = "Failed to click delete record pop up";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(Inspection_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to ifram";
            return false;
        }

        //close window 
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.PresentExit_Button_1()))
        {
            error = "Failed to wait for cross button to close page";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.PresentExit_Button_1()))
        {
            error = "Failed to click cross button to close page";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Inspection_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to ifram";
            return false;
        }

        pause(5000);
        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        //Request validation
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.requestValidationDropDown()))
        {
            error = "Failed to wait for 'request Validation DropDown' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.requestValidationDropDown()))
        {
            error = "Failed to click request Validation DropDown dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.requestValidationSearh()))
        {
            error = "Failed to enter Request validation option :" + getData("Request validation");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Request validation"))))
        {
            error = "Failed to wait for Request validation drop down option : " + getData("Request validation");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Request validation"))))
        {
            error = "Failed to click Request validation drop down option : " + getData("Request validation");
            return false;
        }
        narrator.stepPassedWithScreenShot("Request validation :"+getData("Request validation"));

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.requestValiadation_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.requestValiadation_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Save

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.requestSaveButton()))
        {
            error = "Failed to wait for 'request Validation save button..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.requestSaveButton()))
        {
            error = "Failed to click request Validation save button.";
            return false;
        }
        
         String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

         acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Inspection_PageObjects.setRecord_Number(record[2]);
         record_ = Inspection_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}
