/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "View Supporting Documents - Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_View_Supporting_Documents_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR5_View_Supporting_Documents_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {

        if (!ViewSupportingDocs())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean ViewSupportingDocs()
    {
        pause(5000);
        //Select Documents Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.supportDocPanel()))
        {
            error = "Failed to wait for supporting documents tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.supportDocPanel()))
        {
            error = "Failed to click on supporting documents tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully supporting documents Tab.");

        if (getData("Upload documents").equalsIgnoreCase("True"))
        {

            //Upload hyperlink to Supporting documents
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.linkToADocument()))
            {
                error = "Failed to wait for Supporting documents 'Link box' link.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.linkToADocument()))
            {
                error = "Failed to click on Supporting documents 'Link box' link.";
                return false;
            }
            pause(1000);
            narrator.stepPassedWithScreenShot("Successfully clicked Supporting documents 'Upload Hyperlink box'.");

            //switch to new window
            if (!SeleniumDriverInstance.switchToTabOrWindow())
            {
                error = "Failed to switch to new window or tab.";
                return false;
            }

            //URL https
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Link_URL()))
            {
                error = "Failed to wait for 'URL value' field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.Link_URL(), getData("Document Link")))
            {
                error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

            //Title
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.urlTitle()))
            {
                error = "Failed to wait for 'Url Title' field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.urlTitle(), getData("URL Title")))
            {
                error = "Failed to enter '" + getData("URL Title") + "' into 'Url Title' field.";
                return false;
            }
            narrator.stepPassedWithScreenShot("URL Title : '" + getData("URL Title") + "'.");

            //Add button
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.urlAddButton()))
            {
                error = "Failed to wait for 'Add' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.urlAddButton()))
            {
                error = "Failed to click on 'Add' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
            narrator.stepPassed("Successfully uploaded '" + getData("URL Title") + "' document using '" + getData("Document Link") + "' Link.");

            pause(4000);

        }

        return true;
    }

}
