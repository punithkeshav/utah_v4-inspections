/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * /**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR7 Capture_Validation_MainScenario",
        createNewBrowserInstance = false
)
public class FR7_Capture_Validation_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR7_Capture_Validation_MainScenario()

    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest()
    {

        if (!CaptureValidation())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully requested Validation");
    }

    public boolean CaptureValidation()
    {
        //Navigate Validation TAB

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.validationTab()))
        {
            error = "Failed to wait for 'validation' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.validationTab()))
        {
            error = "Failed to click on 'validation' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'validation' tab.");

        //Comments
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.comment()))
        {
            error = "Failed to click on 'Comments.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_PageObjects.comment(), getData("Comments")))
        {
            error = "Failed to enter  Commentse :" + getData("Comments");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered comments");
        //Declare tick box

        if (getData("Tick box").equalsIgnoreCase("True"))
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.declareTickBox()))
            {
                error = "Failed to wait for 'declare Tick Box.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.declareTickBox()))
            {
                error = "Failed to click on 'declare Tick Box.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully ticked declaration tick box.");
        }
        
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Inspection_PageObjects.setRecord_Number(record[2]);
        String record_ = Inspection_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}
