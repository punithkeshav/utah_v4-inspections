/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "FR11_View_Dashboards_MainScenario",
        createNewBrowserInstance = false
)



public class FR11_View_Dashboards_MainScenario extends BaseClass {
    
    
    
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    
    public FR11_View_Dashboards_MainScenario()
    {
    
    this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    
  
    }
    
    public TestResult executeTest()
    {
        if (!ViewDashboard()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("View Dashboard. ");
    }
    
    
    public boolean ViewDashboard(){
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Dashboard())){
            error = "Failed to wait for 'Dashboard' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Dashboard())){
            error = "Failed to click on 'Dashboard' tab.";
            return false;
        }
        
           pause(15000);
        narrator.stepPassedWithScreenShot("Successfully Navigated  to Dashboard. ");

         pause(5000);
         

        
        return true;
    }
    
}
