/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspections_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects.Inspection_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "UCISP01_02 CaptureChecklist - AlternativeScenario2",
        createNewBrowserInstance = false
)

public class UC_ISP01_02_CaptureChecklist_AlternativeScenario2 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public UC_ISP01_02_CaptureChecklist_AlternativeScenario2()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToInspections()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!CaptureChecklist()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Capture Inspection");
    }

    public boolean NavigateToInspections(){
        //Navigate to Inspections
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Inspections())){
            error = "Failed to wait for 'Inspections' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Inspections())){
            error = "Failed to click on 'Inspections' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Checklist page opens but cannot be updated. ");

         pause(4000);
        //Search for inspections
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.inspections_Search(),5000)){
            error = "Failed to wait for 'search' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.inspections_Search())){
            error = "Failed to click on 'search' button.";
            return false;
        }
        pause(8000);
        narrator.stepPassedWithScreenShot("Successfully clicked 'search' button.");

        return true;
    }
     
    public boolean CaptureChecklist(){
        
          pause(8000);
     
        
        //Click record
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.selectRecord())){
            error = "Failed to wait for 'selecting a record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.selectRecord())){
            error = "Failed to click record.";
            return false;
        }

       pause(6000);
        
    narrator.stepPassedWithScreenShot("Successfully clicked on record.");
    
      //Add Checklist

        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_Add(),5000)){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklist_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(3400);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_processflow(), 10000)){
            error = "Failed to wait for process flow button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklist_processflow())){
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");
        
        pause(3000);
        
      //Start
      //Select checklist dropdown
       
         if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_dropdown()))
        {
            error = "Failed to wait for 'Checklist' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.checklist_dropdown()))
        {
            error = "Failed to click Checklist dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.checklist_search()))
        {
            error = "Failed to enter Checklist option :" + getData("Checklist");
            return false;
        }
		

      pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Checklist"))))
        {
            error = "Failed to wait for Checklist drop down option : " + getData("Checklist");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Checklist"))))
        {
            error = "Failed to click Checklist drop down option : " + getData("Checklist");
            return false;
        }


      
 //Save and start check list
        pause(15000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.startChecklist())){
            error = "Failed to wait for 'save and start checklist button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.startChecklist())){
            error = "Failed to click save and start checklist button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully click 'save and start checklist button.");
        
   
         pause(5000);
         
       if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.startChecklist1())){
            error = "Failed to wait for 'save and start checklist button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.startChecklist1())){
            error = "Failed to click save and start checklist button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully click 'save and start checklist button.");
 pause(5000);
      
    //Checklist Questions
           if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.questionDropDown()))
        {
            error = "Failed to wait for 'Question 1' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.questionDropDown()))
        {
            error = "Failed to click Question 1 dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.question_search()))
        {
            error = "Failed to enter Question 1 option :" + getData("Question 1");
            return false;
        }
		

      pause(6000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Question 1"))))
        {
            error = "Failed to wait for Question 1 drop down option : " + getData("Question 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Question 1"))))
        {
            error = "Failed to click Question 1 drop down option : " + getData("Question 1");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed 'question 1 dropdown.");
         //Checklist
  

         
        // Click Next
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.next_Button())){
            error = "Failed to wait for 'next button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.next_Button())){
            error = "Failed to click next button.";
            return false;
        }
     pause(6000);
        
   narrator.stepPassedWithScreenShot("Successfully clicked 'next button.");
   //Checklist 2
      if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.questionDropDown1()))
        {
            error = "Failed to wait for 'Question 1' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.questionDropDown1()))
        {
            error = "Failed to click Question 1 dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.question_search1()))
        {
            error = "Failed to enter Question 2 option :" + getData("Question 2");
            return false;
        }
		

      pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.Text2(getData("Question 2"))))
        {
            error = "Failed to wait for Question 2 drop down option : " + getData("Question 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.Text2(getData("Question 2"))))
        {
            error = "Failed to click Question 2 drop down option : " + getData("Question 2");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed 'question 2 dropdown.");
      
         pause(9000);
         //Save Current Finish
         
             
        if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.saveCurrentSection_Button())){
            error = "Failed to wait for 'save current section button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.saveCurrentSection_Button())){
            error = "Failed to save current section button.";
            return false;
        }
     pause(5000);
     narrator.stepPassedWithScreenShot("Successfully saved current section.");
     pause(5000);
     //Exit
     SeleniumDriverInstance.switchToFrameByXpath(Inspection_PageObjects.iframeCross());
     
      if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.exit_Button())){
            error = "Failed to wait for 'save current section button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.exit_Button())){
            error = "Failed to save current section button.";
            return false;
        }
     pause(3000);
     narrator.stepPassedWithScreenShot("Successfully exited the pending checklist.");
     
     //Continue
      pause(3000);
      if(!SeleniumDriverInstance.waitForElementByXpath(Inspection_PageObjects.continue_Button())){
            error = "Failed to wait for 'save current section button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.continue_Button())){
            error = "Failed to save current section button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked the continue button.");
        
        pause(2000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.next_Button())){
            error = "Failed to click next button.";
            return false;
        }
        
        pause(2000);
        //Finish Button
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.finish_Button())){
            error = "Failed to click next button.";
            return false;
        }
        pause(7000);
        narrator.stepPassedWithScreenShot("Successfully clicked the Finish button.");
     
     pause(5000);
     //View
     if(!SeleniumDriverInstance.clickElementbyXpath(Inspection_PageObjects.viewFilter_button())){
            error = "Failed to click view button.";
            return false;
        }
     pause(7000);
     narrator.stepPassedWithScreenShot("Successfully clicked the View button.");
     
      
return true;
    }
}