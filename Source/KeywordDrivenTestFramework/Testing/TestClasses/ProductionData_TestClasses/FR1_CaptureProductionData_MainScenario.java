/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ProductionData_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.ProductionData_PageObjects.ProductionData_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR1 - Capture Production Data - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_CaptureProductionData_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR1_CaptureProductionData_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!CreateProductionData())
        {
            return narrator.testFailed("Capture Production Data Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Production Data");
    }

    public boolean CreateProductionData()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.ECO2ManTab()))
        {
            error = "Failed to wait for ECO2Man tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.ECO2ManTab()))
        {
            error = "Failed to click on ECO2Man tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to ECO2Man tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.ProductionDataTab()))
        {
            error = "Failed to wait for Production Data tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.ProductionDataTab()))
        {
            error = "Failed to click on Production Data tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Production Datatab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.ButtonAdd()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.ButtonAdd()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.Processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }

        //New Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.BusinessUnit_Dropdown()))
        {
            error = "Failed to wait for 'Business Unit' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.BusinessUnit_Dropdown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Monitoring type
//        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.MonitoringTypeDropDown()))
//        {
//            error = "Failed to wait for Monitoring type.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.MonitoringTypeDropDown()))
//        {
//            error = "Failed to click Monitoring type";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for search text box";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(ProductionData_PageObjects.TypeSearch2(), getData("Monitoring type")))
//
//        {
//            error = "Failed to enter Monitoring type :" + getData("Monitoring type");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.pressEnter_2(ProductionData_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to press enter";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Text2(getData("Monitoring type"))))
//        {
//            error = "Failed to wait for Monitoring type drop down option : " + getData("Monitoring type");
//            return false;
//        }
//        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ProductionData_PageObjects.Text2(getData("Monitoring type"))))
//        {
//            error = "Failed to click Monitoring type drop down option : " + getData("Monitoring type");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Monitoring type");
        //Month/Year
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.MonthYearDropDown()))
        {
            error = "Failed to wait for Month/Year month drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.MonthYearDropDown()))
        {
            error = "Failed to click Month/Year month date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ProductionData_PageObjects.TypeSearch2(), getData("Month/Year month")))

        {
            error = "Failed to enter Month/Year month :" + getData("Month/Year month");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(ProductionData_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Text2(getData("Month/Year month"))))
        {
            error = "Failed to wait for Month/Year month drop down option : " + getData("Month/Year month");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ProductionData_PageObjects.Text2(getData("Month/Year month"))))
        {
            error = "Failed to click Month/Year month drop down option : " + getData("Month/Year month");
            return false;
        }
        narrator.stepPassedWithScreenShot("Month/Year month");

        //Target start date year
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.MonthYearDropDown2()))
        {
            error = "Failed to wait for Month/Year year drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.MonthYearDropDown2()))
        {
            error = "Failed to click Target start year date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ProductionData_PageObjects.TypeSearch2(), getData("Month/Year year")))

        {
            error = "Failed to enter Month/Year date year :" + getData("Month/Year year");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(ProductionData_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Text2(getData("Month/Year year"))))
        {
            error = "Failed to wait for Month/Year year drop down option : " + getData("Month/Year year");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(ProductionData_PageObjects.Text2(getData("Month/Year year"))))
        {
            error = "Failed to click Month/Year year drop down option : " + getData("TMonth/Year year");
            return false;
        }
        narrator.stepPassedWithScreenShot("Month/Year year");

        //Save and Continue
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Button_Save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.Button_Save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(ProductionData_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(ProductionData_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ProductionData_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ProductionData_PageObjects.setRecord_Number(record[2]);
        String record_ = ProductionData_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Capture a Relationship Based Safety record for SLP Observation record saved");
        return true;
    }

}
