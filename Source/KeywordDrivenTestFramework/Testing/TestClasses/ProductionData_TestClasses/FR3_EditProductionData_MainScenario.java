/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ProductionData_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.ProductionData_PageObjects.ProductionData_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR3-Edit Production Data - Main scenario",
        createNewBrowserInstance = false
)

public class FR3_EditProductionData_MainScenario extends BaseClass
{
    
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR3_EditProductionData_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 2500);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }
    
    public TestResult executeTest()
    {
        if (!navigateToStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Stakeholder Individual updates saved");
    }
    
    public boolean navigateToStakeholderIndividual()
    {
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ProductionData_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.ContainsTextBox()))
        {
            error = "Failed to wait for contains";
            return false;
        }
        
        pause(3000);
      

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(ProductionData_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }
          if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.SearchButton()))
        {
            error = "Failed wait for Search Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);
        
        pause(5000);

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.Processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }
        
        
          //Month/Year
         if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.MonthYearDropDown()))
            {
                error = "Failed to wait for Month/Year month drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.MonthYearDropDown()))
            {
                error = "Failed to click Month/Year month date drop down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for search text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(ProductionData_PageObjects.TypeSearch2(), getData("Month/Year month")))

            {
                error = "Failed to enter Month/Year month :" + getData("Month/Year month");
                return false;
            }

             if (!SeleniumDriverInstance.pressEnter_2(ProductionData_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

           
            if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Text2(getData("Month/Year month"))))
            {
                error = "Failed to wait for Month/Year month drop down option : " + getData("Month/Year month");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ProductionData_PageObjects.Text2(getData("Month/Year month"))))
            {
                error = "Failed to click Month/Year month drop down option : " + getData("Month/Year month");
                return false;
            }
            narrator.stepPassedWithScreenShot("Month/Year month");
            
      
             //Target start date year
            if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.MonthYearDropDown2()))
            {
                error = "Failed to wait for Month/Year year drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.MonthYearDropDown2()))
            {
                error = "Failed to click Target start year date drop down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for search text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(ProductionData_PageObjects.TypeSearch2(), getData("Month/Year year")))

            {
                error = "Failed to enter Month/Year date year :" + getData("Month/Year year");
                return false;
            }

             if (!SeleniumDriverInstance.pressEnter_2(ProductionData_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

           
            if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Text2(getData("Month/Year year"))))
            {
                error = "Failed to wait for Month/Year year drop down option : " + getData("Month/Year year");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(ProductionData_PageObjects.Text2(getData("Month/Year year"))))
            {
                error = "Failed to click Month/Year year drop down option : " + getData("TMonth/Year year");
                return false;
            }
            narrator.stepPassedWithScreenShot("Month/Year year");
        

        //Save to continue
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Button_Save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.Button_Save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.recordSaved_popup1()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(ProductionData_PageObjects.recordSaved_popup1());
        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            
            error = "Failed to save record.";
            return false;            
        }
        
        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ProductionData_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ProductionData_PageObjects.setRecord_Number(record[2]);
        String record_ = ProductionData_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        
        return true;
    }
    
}
