/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ProductionData_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.ClimateChangeAndEnergyTargets_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.ProductionData_PageObjects.ProductionData_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR2- Capture Mineral Produced - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_CaptureMineralProduced_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR2_CaptureMineralProduced_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureMineralProduced())
        {
            return narrator.testFailed("Capture  Mineral Produced Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture  Mineral Produced");
    }

    public boolean CaptureMineralProduced()
    {

        SeleniumDriverInstance.pause(4000);

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.MineralProduced_Add()))
        {
            error = "Failed to wait for Mineral Production Targets 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.MineralProduced_Add()))
        {
            error = "Failed to click on Mineral Production Targets 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Mineral type
//        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.MineralType_Dropdown()))
//        {
//            error = "Failed to wait for Mineral type dropdown";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.MineralType_Dropdown()))
//        {
//            error = "Failed to click Mineral type dropdown";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for search text box";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(ProductionData_PageObjects.TypeSearch2(), getData("Mineral type")))
//
//        {
//            error = "Failed to enter Mineral type:" + getData("Mineral type");
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Text2(getData("Mineral type"))))
//        {
//            error = "Failed to wait for Mineral type drop down option : " + getData("Mineral type");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.Text2(getData("Mineral type"))))
//        {
//            error = "Failed to click Mineral type drop down option : " + getData("Mineral type");
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Mineral type option  :" + getData("Mineral type"));


 if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.ProductionIndicator_Dropdown()))
        {
            error = "Failed to wait for Mineral type dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.ProductionIndicator_Dropdown()))
        {
            error = "Failed to click Mineral type dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ProductionData_PageObjects.TypeSearch2(), getData("Production indicator")))

        {
            error = "Failed to enter Production indicator:" + getData("Production indicator");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Text2(getData("Production indicator"))))
        {
            error = "Failed to wait for Production indicator drop down option : " + getData("Production indicator");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.Text2(getData("Production indicator"))))
        {
            error = "Failed to click Production indicator drop down option : " + getData("Production indicator");
            return false;
        }

        narrator.stepPassedWithScreenShot("Production indicator :" + getData("Production indicator"));


        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.QuantityOfMineralProduced()))
        {
            error = "Failed to wait for Quantity of mineral produced text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ProductionData_PageObjects.QuantityOfMineralProduced(), getData("Quantity of mineral produced")))
        {
            error = "Failed to enter Quantity of mineral produced :" + getData("Quantity of mineral produced");
            return false;
        }

        //Quantity of mineral produced UOM
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.QuantityOfMineralProducedUOM_Dropdown()))
        {
            error = "Failed to wait for Quantity of mineral produced UOM dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.QuantityOfMineralProducedUOM_Dropdown()))
        {
            error = "Failed to click Quantity of mineral produced UOM dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Quantity of mineral produced text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ProductionData_PageObjects.TypeSearch2(), getData("Quantity of mineral produced UOM option")))
        {
            error = "Failed to enter Quantity of mineral produced UOM option :" + getData("Quantity of mineral produced UOM option");
            return false;
        }
          if (!SeleniumDriverInstance.pressEnter_2(ProductionData_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.businessUnitOption1(getData("Quantity of mineral produced UOM"))))
        {
            error = "Failed to wait for Quantity of mineral produced UOM:" + getData("Quantity of mineral produced UOM");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.businessUnitOption1(getData("Quantity of mineral produced UOM"))))
        {
            error = "Failed to click Quantity of mineral produced UOM Option drop down :" + getData("Quantity of mineral produced UOM");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Text3(getData("Quantity of mineral produced UOM option"))))
        {
            error = "Failed to wait for Quantity of mineral produced UOM drop down option : " + getData("Quantity of mineral produced UOM option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.Text3(getData("Quantity of mineral produced UOM option"))))
        {
            error = "Failed to click Quantity of mineral produced UOM drop down option : " + getData("Quantity of mineral produced UOM option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Quantity of mineral produced UOM option  :" + getData("Quantity of mineral produced UOM option"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.Button_Save()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ProductionData_PageObjects.Button_Save()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(ProductionData_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ProductionData_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(ProductionData_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ProductionData_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ProductionData_PageObjects.setRecord_Number(record[2]);
        String record_ = ProductionData_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Capture a Relationship Based Safety record for SLP Observation record saved");
        return true;
    }

}
