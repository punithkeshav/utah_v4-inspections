/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspection_Scheduler_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects.Inspection_Scheduler_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR5_Delete_Inspection_Scheduler_MainScenario",
        createNewBrowserInstance = false
)

public class FR5_Delete_Inspection_Scheduler_MainScenario extends BaseClass
{

    String error = "";

    public FR5_Delete_Inspection_Scheduler_MainScenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!delete_InspectionScheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Inspection Scheduler record deleted");
    }

    public boolean delete_InspectionScheduler()
    {

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Add(), 5000))
        {
            error = "Failed to wait for Add button";
            return false;
        }

        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.SearchOption()))
        {
            error = "Failed to wait for search options";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.SearchOption()))
        {
            error = "Failed to click search options";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter contains to search";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored :" + array[1]);

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecordInTable()))
        {
            error = "Failed to wait for record";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecordInTable()))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.DeleteButton()))
        {
            error = "Failed click delete button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.DeleteButton()))
        {
            error = "Failed click delete button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);

        return true;

    }

}
