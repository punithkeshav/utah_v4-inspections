/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspection_Scheduler_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects.Inspection_Scheduler_PageObjects;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "UC INS 06 02 - Main Scenario",
        createNewBrowserInstance = false
)

public class UC_INS_06_02_Main_Scenario extends BaseClass
{

    String error = "";

    public UC_INS_06_02_Main_Scenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_To_Inspection_Scheduler_Recurrence())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Viewed Inspections");
    }

    public boolean Navigate_To_Inspection_Scheduler_Recurrence()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

    
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");
 
        //Select record
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionSchedulerRecurrenceRecord()))
        {
            error = "Failed to wait on Inspection Scheduler Recurrence record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionSchedulerRecurrenceRecord()))
        {
            error = "Failed to click on Inspection Scheduler Recurrence record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click on the Inspection Scheduler Recurrence record");

        pause(5000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionSchedulerRecurrence_ProcessFlow()))
        {
            error = "Failed to wait for Process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionSchedulerRecurrence_ProcessFlow()))
        {
            error = "Failed to click on Process flow button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Active/Inactive 
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.ActiveInactive_Dropdown()))
        {
            error = "Failed to wait for Active/Inactive dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.ActiveInactive_Dropdown()))
        {
            error = "Failed to click the Active/Inactive dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.TypeSearch2()))
        {
            error = "Failed to enter Active/Inactive option :" + getData("Active/Inactive");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.TypeSearch2(), getData("Active/Inactive")))

        {
            error = "Failed to enter Active/Inactive option :" + getData("Active/Inactive");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

           if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Text2(getData("Active/Inactive"))))
        {
            error = "Failed to wait for Active/Inactive option :" + getData("Active/Inactive");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Text2(getData("Active/Inactive"))))
        {
            error = "Failed to click Active/Inactive option :" + getData("Active/Inactive");
            return false;
        }
        narrator.stepPassedWithScreenShot("Active/Inactive: " + getData("Active/Inactive") );

       //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Inspection_Scheduler_PageObjects.setRecord_Number(record[2]);
        String record_ = Inspection_Scheduler_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }
}
