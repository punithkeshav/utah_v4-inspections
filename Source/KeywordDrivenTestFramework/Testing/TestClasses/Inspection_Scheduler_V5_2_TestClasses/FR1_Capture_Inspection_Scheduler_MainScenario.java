
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspection_Scheduler_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects.Inspection_Scheduler_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Inspection Scheduler - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Inspection_Scheduler_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Inspection_Scheduler_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_To_Inspection_Scheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Inspection_Scheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Inspection Scheduler");
    }

    public boolean Navigate_To_Inspection_Scheduler()
    {

        //Inspection Scheduler
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Module()))
        {
            error = "Failed to wait for 'Inspection Scheduler' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Module()))
        {
            error = "Failed to click on 'Inspection Scheduler' module";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Inspection Scheduler' module search page.");

        pause(7000);
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Add(), 5000))
        {
            error = "Failed to wait for Add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Add()))
        {
            error = "Failed to click on Add button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Inspection_Scheduler()
    {
        pause(10000);

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //New Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Dropdown()))
        {
            error = "Failed to wait for 'Business Unit' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Dropdown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.businessUnitSearch()))
        {
            error = "Failed to enter Business Unit option :" + getData("Business Unit");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Text2(getData("Business Unit"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.businessUnitexpandButton()))
        {
            error = "Failed to wait to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.businessUnitexpandButton()))
        {
            error = "Failed to expand Business Unit";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }
        

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Functional Location
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.functional_Dropdown()))
        {
            error = "Failed to wait for 'Functional location' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.functional_Dropdown()))
        {
            error = "Failed to click Functional location dropdown.";
            return false;
        }
        
           if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.checklist_Search()))
        {
            error = "Failed to wait for search bar" ;
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.checklist_Search(), getData("Functional location")))
        {
            error = "Failed to enter  Functional location option: " + getData("Functional location");
            return false;
        }

      if (!SeleniumDriverInstance.pressEnter_2(Inspection_Scheduler_PageObjects.checklist_Search()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Text4(getData("Functional location"))))
        {
            error = "Failed to wait for Functional location drop down option : " + getData("Functional location");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Text4(getData("Functional location"))))
        {
            error = "Failed to click Functional location drop down option : " + getData("Functional location");
            return false;
        }
        narrator.stepPassedWithScreenShot("Functional location " + getData("Functional location"));

//Inspection Type
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.inspectionType_Dropdown()))
        {
            error = "Failed to wait for 'Inspection type' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.inspectionType_Dropdown()))
        {
            error = "Failed to click Inspection type dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.inspectionSearch1()))
        {
            error = "Failed to enter Inspection type option :" + getData("Inspection type");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Text4(getData("Inspection type"))))
        {
            error = "Failed to wait for Inspection type drop down option : " + getData("Inspection type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Text4(getData("Inspection type"))))
        {
            error = "Failed to click Inspection type drop down option : " + getData("Inspection type");
            return false;
        }
        narrator.stepPassedWithScreenShot("Inspection type " + getData("Inspection type"));

        //New Checklists to be done
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.checklist_Dropdown()))
        {
            error = "Failed to wait for 'Checklists to be done' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.checklist_Dropdown()))
        {
            error = "Failed to click Checklists to be done dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.checklist_Search1()))
        {
            error = "Failed to enter Checklists to be done option :" + getData("Checklists to be done");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.checklistSelectOption(getData("Checklists to be done"))))
        {
            error = "Failed to click the Impact type option: " + getData("Checklists to be done");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.checklistSelectOption(getData("Checklists to be done"))))
        {
            error = "Failed to click the Impact type option: " + getData("Checklists to be done");
            return false;
        }

        boolean flag = SeleniumDriverInstance.checkIfCheckboxIsChecked(Inspection_Scheduler_PageObjects.checklistSelectOption(getData("Checklists to be done")));
        while (!flag)
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.checklistSelectOption(getData("Checklists to be done"))))
            {
                error = "Failed to click the Impact type option: " + getData("Checklists to be done");
                return false;
            }

        }

        narrator.stepPassedWithScreenShot("Checklists to be done " + getData("Checklists to be done"));
        //
        //Name of inspection
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.nameOfInspectionField()))
        {
            error = "Failed to wait for name of Inspection Field.";
            return false;
        }

//        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.nameOfInspectionField()))
//        {
//            error = "Failed to click the name of Inspection Field.";
//            return false;
//        }
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.nameOfInspectionField(), getData("Name of inspection")))

        {
            error = "Failed to enter Name of inspection option :" + getData("Name of inspection");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Name of inspection.");

        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.SaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.SaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Inspection_Scheduler_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Inspection_Scheduler_PageObjects.setRecord_Number(record[2]);
        String record_ = Inspection_Scheduler_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        return true;

    }

}
