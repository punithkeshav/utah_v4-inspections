/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspection_Scheduler_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects.Inspection_Scheduler_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR3_Edit_Inspection_Scheduler_MainScenario",
        createNewBrowserInstance = false
)

public class FR3_Edit_Inspection_Scheduler_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Edit_Inspection_Scheduler_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {    
        if (!Edit_Inspection_Scheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Record is saved and updated with the changes made");
    }

   

    public boolean Edit_Inspection_Scheduler()
    {
        
        

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

    
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");
        
        
        

        //Select record
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionSchedulerRecurrenceRecord()))
        {
            error = "Failed to wait on Inspection Scheduler Recurrence record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionSchedulerRecurrenceRecord()))
        {
            error = "Failed to click on Inspection Scheduler Recurrence record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click on the Inspection Scheduler Recurrence record");

        pause(5000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionSchedulerRecurrence_ProcessFlow()))
        {
            error = "Failed to wait for Process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionSchedulerRecurrence_ProcessFlow()))
        {
            error = "Failed to click on Process flow button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Responsible 
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.responsible_Dropdown()))
        {
            error = "Failed to wait for Responsible Person dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.responsible_Dropdown()))
        {
            error = "Failed to click the Responsible Person dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.responsibleSearch()))
        {
            error = "Failed to enter Responsible Person option :" + getData("Responsible Person");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.responsibleSearch()))

        {
            error = "Failed to Responsible Person option :" + getData("Responsible Person");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.responsibleSearch(), getData("Responsible Person")))

        {
            error = "Failed to enter Responsible Person option :" + getData("Responsible Person");
            return false;
        }

       if (!SeleniumDriverInstance.pressEnter_2(Inspection_Scheduler_PageObjects.responsibleSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

           if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Text2(getData("Responsible Person"))))
        {
            error = "Failed to wait for Responsible Person option :" + getData("Responsible Person");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Text2(getData("Responsible Person"))))
        {
            error = "Failed to click Responsible Person option :" + getData("Responsible Person");
            return false;
        }
        narrator.stepPassedWithScreenShot("Responsible Person: " + getData("Responsible Person") );

       //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Inspection_Scheduler_PageObjects.setRecord_Number(record[2]);
        String record_ = Inspection_Scheduler_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
     
        return true;

    }

}
