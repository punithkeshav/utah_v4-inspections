package KeywordDrivenTestFramework.Testing.TestClasses.Inspection_Scheduler_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects.Inspection_Scheduler_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@KeywordAnnotation(
        Keyword = "Inspection Scheduler / Inspection Scheduler Recurrence updates saved.",
        createNewBrowserInstance = false
)

public class FR4_Edit_Inspection_Scheduler_Recurrence_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_Edit_Inspection_Scheduler_Recurrence_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_To_Inspection_Scheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        if (!Edit_Inspection_Scheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Inspection Scheduler / Inspection Scheduler Recurrence updates saved.");
    }

    public boolean Navigate_To_Inspection_Scheduler()
    {

        //Inspection Scheduler
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Module()))
        {
            error = "Failed to wait for 'Inspection Scheduler' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Module()))
        {
            error = "Failed to click on 'Inspection Scheduler' module";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Inspection Scheduler' module search page.");

        //Show All
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.showAllFilter()))
        {
            error = "Failed to wait for 'Show all' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.showAllFilter()))
        {
            error = "Failed to click on 'Show all' button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to filter page.");
        return true;
    }

    public boolean Edit_Inspection_Scheduler()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.activeInactiveDD()))
        {
            error = "Failed to wait for 'Inspection Scheduler' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.activeInactiveDD()))
        {
            error = "Failed to click on 'Inspection Scheduler' module";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully navigated to 'Active/Inaction dropdown.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.activeInactiveSearch()))
        {
            error = "Failed to wait for 'active/Inactive Search";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.activeInactiveSearch()))
        {
            error = "Failed to click on 'active/Inactive Search";
            return false;
        }

//        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.activeInactiveSearch(), getData("Status")))
//      
//            {
//                error = "Failed to select Status checkbox :" + getData("Status");
//                return false;
//            }
//      
//         if (!SeleniumDriverInstance.pressEnter())
//            {
//                error = "Failed to press enter";
//                return false;
//            }
        if (!SeleniumDriverInstance.clickElementByJavascript(Inspection_Scheduler_PageObjects.statusSelect()))
        {
            error = "Failed to enter Status option :" + getData("Status");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.statusDropClick()))
        {
            error = "Failed to click status drop close chevron ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Status: " + getData("Status") + " -> " + getData("Status") + " -> " + getData("Status"));

//        //Click on search
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.searchButton()))
        {
            error = "Failed to click on 'search' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.searchButton()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'search' button.");

        //Select record
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.clickRecord()))
        {
            error = "Failed to click on 'record'.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.clickRecord()))
        {
            error = "Failed to click on 'record' .";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'on the record' .");

        //Select record
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.clickRecord1()))
        {
            error = "Failed to click on 'record'.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.clickRecord1()))
        {
            error = "Failed to click on 'record' .";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'on the record' .");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_ProcessFlow1()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_ProcessFlow1()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        //Select only active records
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.activeInactiveDD2()))
        {
            error = "Failed to wait for 'Inspection Scheduler' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.activeInactiveDD2()))
        {
            error = "Failed to click on 'Inspection Scheduler' module";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully navigated to 'Active/Inactve dropdown.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.activeInactiveSearch3()))
        {
            error = "Failed to wait for 'active/Inactive Search";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.activeInactiveSearch3()))
        {
            error = "Failed to click on 'active/Inactive Search";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.activeInactiveSearch3(), getData("Active/Inactive")))

        {
            error = "Failed to select Active/Inactive :" + getData("Active/Inactive");
            return false;
        }

            if (!SeleniumDriverInstance.pressEnter_2(Inspection_Scheduler_PageObjects.activeInactiveSearch3()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementByJavascript(Inspection_Scheduler_PageObjects.statusSelect2()))
        {
            error = "Failed to select Status option :" + getData("Active/Inactive");
            return false;
        }

//    if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.statusDropClick()))
//        {
//            error = "Failed to click status drop close chevron ";
//            return false;
//        }
        narrator.stepPassedWithScreenShot("Active/Inactive: " + getData("Active/Inactive") + " -> " + getData("Active/Inactive") + " -> " + getData("Active/Inactive"));

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveDropButton1()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveDropButton1()))
        {
            error = "Failed to click on Save Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the save drop click");
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveCloseButton1()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveCloseButton1()))
        {
            error = "Failed to click on Save Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save and Close");

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Inspection_Scheduler_PageObjects.setRecord_Number(record[2]);
        String record_ = Inspection_Scheduler_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;

    }

}
