/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SikuliDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.TrainingSiteSignInPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "SignIn into TrainingSite",
    createNewBrowserInstance = true
)
public class TrainingSiteSignInTest extends BaseClass
{

    String error = "";

    public TrainingSiteSignInTest()
    {

    }

    public TestResult executeTest()
    {
        // This step will Launch the browser and navigate to the Training Site URL
        if (!NavigateToIsometrixUrl())
        {
            return narrator.testFailed("Failed to navigate to Training Site signin page" + error);
        }

        // This step will sign into the specified Training Site account with the provided credentials
        if (!signInIntoTrainingSite())
        {
            return narrator.testFailed("Failed to navigate to Training Site signin page");
        }

        return narrator.finalizeTest("Successfully Navigated through Training Site signin page");
    }

    public boolean NavigateToIsometrixUrl()
    {

        if (!SeleniumDriverInstance.navigateTo(TrainingSiteSignInPageObjects.url()))
        {
            error = "Failed to navigate to Training Site";
            return false;
        }

        return true;
    }

    public boolean signInIntoTrainingSite()
    {

        if (!SeleniumDriverInstance.enterTextByXpath(TrainingSiteSignInPageObjects.userNameTextXpath(), testData.getData("Username")))
        {
            error = "Failed to enter text into username text field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(TrainingSiteSignInPageObjects.passwordTextXpath(), testData.getData("Password")))
        {
            error = "Failed to enter text into password text field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteSignInPageObjects.signInButtontXpath()))
        {
            error = "Failed to click signin button.";
            return false;
        }
        return true;

    }

}
