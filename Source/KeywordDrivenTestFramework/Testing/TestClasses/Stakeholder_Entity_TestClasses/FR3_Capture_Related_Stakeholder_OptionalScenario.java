/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;

import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author Delwin.Horsthemke
 */
@KeywordAnnotation(
        Keyword = "Capture Related Stakeholder - Optional Scenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_Related_Stakeholder_OptionalScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR3_Capture_Related_Stakeholder_OptionalScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderEntityMembers())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterMemberDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToStakeholderEntityMembers(){    
        //Navigate to Members Tab
        pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.members_tab())){
            error = "Failed to wait for 'Members Tab' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.members_tab())){
            error = "Failed to click on 'Members Tab' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Members' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.member_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.member_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    //Enter data
    public boolean enterMemberDetails(){
        //Stakeholder name dropdown
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.member_name_dopdown())){
            error = "Failed to wait for 'Stakeholder name' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.member_name_dopdown())){
            error = "Failed to click 'Stakeholder name' dropdown.";
            return false;
        } 
        
        pause(3000);
        //Stakeholder name select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.member_name_select(testData.getData("Stakeholder name")))){
            error = "Failed to wait for 'Stakeholder name' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.member_name_select(testData.getData("Stakeholder name")))){
            error = "Failed to select '"+testData.getData("Stakeholder name")+"' from 'Stakeholder name' dropdown options.";
            return false;
        }
        
        //Position 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.member_position())){
            error = "Failed to wait for 'Position' dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.member_position())){
            error = "Failed to click 'Position' dropdown.";
            return false;
        }
        
        pause(3000);
        
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.member_Position_select(testData.getData("Position")))){
            error = "Failed to wait for 'Position' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.member_Position_select(testData.getData("Position")))){
            error = "Failed to select '"+testData.getData("Position")+"' from 'Position' dropdown options.";
            return false;
        }

        //Stakeholder Entity Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.member_save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.member_save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Entity_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        pause(5000);
        
        //IsoMetrix Logo
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.IsoMetrixlogo())){
            error = "Failed to wait for IsoMetrix Logo";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.IsoMetrixlogo())){
            error = "Failed to click on IsoMetrix Logo";
            return false;
        }
        
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Individual
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to wait for 'Stakeholders Individual' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to click on 'Stakeholders Individual' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(4000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Individuals' tab.");
        
        //Full Name Search
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.search(), testData.getData("FullNameSearch"))){
            error = "Failed to enter '"+testData.getData("FullNameSearch")+"' into 'FullNameSearch' input.";
            return false;
        }
        
        //Search Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to wait for Search Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to click on Search Button";
            return false;
        }
        
        SeleniumDriverInstance.pause(8000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Search Button");
        
        //Record select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SI_Record())){
            error = "Failed to wait for Stakeholder Individual Record select";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SI_Record())){
            error = "Failed to click on Stakeholder Individual Record select";
            return false;
        }
        
        SeleniumDriverInstance.pause(15000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Stakeholder Individual - Abel Mantoa (Percy)");
        
        //Entities Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SI_EntitiesTab())){
            error = "Failed to wait for Stakeholder Individual Entities Tab";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SI_EntitiesTab())){
            error = "Failed to click on Stakeholder Individual Entities Tab";
            return false;
        }
        
        SeleniumDriverInstance.pause(10000);
        
        if(!SeleniumDriverInstance.scrollToElement(Stakeholder_Entity_PageObjects.SI_EntitiesTab())){
            error = "Failed to click on Stakeholder Individual Entities Tab";
            return false;
        }
        
        SeleniumDriverInstance.pause(20000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Stakeholder Individual Entities Tab and Related Entity visible");
        
        //Stakeholder Entity Position Update
        //Position Dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SI_RecordPosition1())){
            error = "Failed to wait for Stakeholder Individual Position Dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SI_RecordPosition1())){
            error = "Failed to click on Stakeholder Individual Position Dropdown";
            return false;
        }
        
        pause(5000);
        //Update Position
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.member_Position_Update(testData.getData("PositionUpdate")))){
            error = "Failed to wait for 'PositionUpdate' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.member_Position_Update(testData.getData("PositionUpdate")))){
            error = "Failed to select '"+testData.getData("Position")+"' from 'PositionUpdate' dropdown options.";
            return false;
        }
        
        //Stakeholder Individual Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SI_RelatedEntitiesSave())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SI_RelatedEntitiesSave())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Entity_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        pause(15000);
        
        //IsoMetrix Logo
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.IsoMetrixlogo())){
            error = "Failed to wait for IsoMetrix Logo";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.IsoMetrixlogo())){
            error = "Failed to click on IsoMetrix Logo";
            return false;
        }
        
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to wait for 'Stakeholders Entity' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to click on 'Stakeholders Entity' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Entity' tab.");
        
        //Full Name Search
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.search(), testData.getData("Stakeholder name1"))){
            error = "Failed to enter '"+testData.getData("FullNameSearch")+"' into 'FullNameSearch' input.";
            return false;
        }
        
        //Search Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to wait for Search Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to click on Search Button";
            return false;
        }
        
        SeleniumDriverInstance.pause(8000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Search Button");
        
        //Entity Record select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_Record())){
            error = "Failed to wait for Stakeholder Entity Record select";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_Record())){
            error = "Failed to click on Stakeholder Entity Record select";
            return false;
        }
        
        SeleniumDriverInstance.pause(15000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Stakeholder Entity - Company_OS 13.3");
        
        //Members Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.members_tab())){
            error = "Failed to wait for Stakeholder Entity Members Tab";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.members_tab())){
            error = "Failed to click on Stakeholder Entity Members Tab";
            return false;
        }
        
        SeleniumDriverInstance.pause(10000);
        
        if(!SeleniumDriverInstance.scrollToElement(Stakeholder_Entity_PageObjects.members_tab())){
            error = "Failed to click on Stakeholder Entity Members Tab";
            return false;
        }
        
        SeleniumDriverInstance.pause(20000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Stakeholder Entity Members Tab and Related Individual visible");
        
        return true;
    }
 
}
