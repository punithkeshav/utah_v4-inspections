/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsometricsPOCPageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;
/**
 *
 * @author Delwin.Horsthemke
 */
@KeywordAnnotation(
        Keyword = "Capture Associated Entities - Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Associated_Entities_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR4_Capture_Associated_Entities_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToAssociatedEntities())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterAssociatedEntitiesDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToAssociatedEntities(){    
        //Navigate to Associated Entities Tab
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.associatedentities_tab())){
            error = "Failed to wait for 'Members Tab' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.associatedentities_tab())){
            error = "Failed to click on 'Members Tab' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Associated Entities' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.associatedentities_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.associatedentities_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    //Enter data
    public boolean enterAssociatedEntitiesDetails(){
        //Group name dropdown
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.group_name_dopdown())){
            error = "Failed to wait for 'Group name' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.group_name_dopdown())){
            error = "Failed to click 'Group name' dropdown.";
            return false;
        } 
        
        pause(3000);
        //Group name select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.entity_name_select(testData.getData("Entity name")))){
            error = "Failed to wait for 'Group name' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.entity_name_select(testData.getData("Entity name")))){
            error = "Failed to select '"+testData.getData("Group name")+"' from 'Group name' dropdown options.";
            return false;
        }
        
        //Position 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.group_position())){
            error = "Failed to wait for 'Position' dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.group_position())){
            error = "Failed to click 'Position' dropdown.";
            return false;
        }
        
        pause(3000);
        
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.group_Position_select(testData.getData("Position")))){
            error = "Failed to wait for 'Position' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.group_Position_select(testData.getData("Position")))){
            error = "Failed to select '"+testData.getData("Position")+"' from 'Position' dropdown options.";
            return false;
        }

        //Associated Entities Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.group_save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.group_save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        pause(15000);
        
        SeleniumDriverInstance.switchToDefaultContent();

        //IsoMetrix Logo
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.IsoMetrixlogo())){
            error = "Failed to wait for IsoMetrix Logo";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.IsoMetrixlogo())){
            error = "Failed to click on IsoMetrix Logo";
            return false;
        }
        
        if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
        }
        
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to wait for 'Stakeholders Entity' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to click on 'Stakeholders Entity' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Entity' tab.");
        
        pause(8000);
        
        //Full Name Search
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.entity_search(), testData.getData("FullNameSearch"))){
            error = "Failed to enter '"+testData.getData("FullNameSearch")+"' into 'FullNameSearch' input.";
            return false;
        }
        
        //Search Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to wait for Search Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to click on Search Button";
            return false;
        }
        
        SeleniumDriverInstance.pause(8000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Search Button");
        
        //Record Select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Entity_RecordSelect(testData.getData("RelatedEntitiesName")))){
            error = "Failed to wait for Record Select";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Entity_RecordSelect(testData.getData("RelatedEntitiesName")))){
            error = "Failed to select Record";
            return false;
        }
        
        SeleniumDriverInstance.pause(15000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Stakeholder Entity record");
        
        //Associated Entities Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.associatedentities_tab())){
            error = "Failed to wait for Stakeholder Associated Entities Tab";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.associatedentities_tab())){
            error = "Failed to click on Stakeholder Associated Entities Tab";
            return false;
        }
        
        SeleniumDriverInstance.pause(10000);
        
        if(!SeleniumDriverInstance.scrollToElement(Stakeholder_Entity_PageObjects.associatedentities_tab())){
            error = "Failed to click on Stakeholder Associated Entities Tab";
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        String SE_AssociatedEntitiesRecord;
        SE_AssociatedEntitiesRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.SE_AssociatedEntitiesRecord(testData.getData("NewRelatedEntitiesName")));

        narrator.stepPassedWithScreenShot("Successfully clicked on Stakeholder Associated Entities Tab and Related Entity visible");
        
        return true;
        
    }
}
