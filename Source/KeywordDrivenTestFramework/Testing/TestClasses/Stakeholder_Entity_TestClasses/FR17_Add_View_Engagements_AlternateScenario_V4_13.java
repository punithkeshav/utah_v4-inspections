/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_View_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Add Engagements - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR17_Add_View_Engagements_AlternateScenario_V4_13 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR17_Add_View_Engagements_AlternateScenario_V4_13()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
       
        if(!navigateToEngagements()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        

        return narrator.finalizeTest("Successfully Captured Engagement - record: #" + getRecordId());
    }

    public boolean navigateToEngagements(){
        
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to wait for 'Stakeholders Entity' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to click on 'Stakeholders Entity' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Entity' tab.");
        
        pause(10000);
        
        //Entity Name Search
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.SE_EntityNameSearchBox(), testData.getData("SE_EntityNameSearch"))){
            error = "Failed to enter '"+testData.getData("SE_EntityNameSearch")+"' into 'SE_EntityNameSearch' input.";
            return false;
        }
        
        pause(2000);
        
        //Search Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to wait for Search Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to click on Search Button";
            return false;
        }
        
        SeleniumDriverInstance.pause(20000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Search Button");
        
        //Stakeholder Selection
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.SE_RecordSelection())){
            error = "Failed to wait for Stakeholder Selection";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.SE_RecordSelection())){
            error = "Failed to click on Stakeholder Selection";
            return false;
        }
        
        pause(10000);
        
        //Right Arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.rightArrow())){
            error = "Failed to wait for 'Left' Arrow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.rightArrow())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.rightArrow())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.rightArrow())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(1000);

        narrator.stepPassedWithScreenShot("Successfully clicked 'Right' Arrow.");
        
        //Navigate to Engaments Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementsTab())){
            error = "Failed to wait for 'Engagements' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.engagementsTab())){
            error = "Failed to click on 'Engagements' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Engagements' tab.");
        
        return true;
    }
    
    public boolean enterDetails(){
        //Set the page as a parent page
        String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
         
        //Add an engagement button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.addEngagementsButton())){
            error = "Failed to wait for 'Add Engagements' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.addEngagementsButton())){
            error = "Failed to click on 'Add Engagements' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add Engagements' button.");
        
        //switch to the new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch on the 'Engagements' window.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched to the 'Engagements' window..");
        
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Entity_View_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.processFlow())){
            error = "Failed to wait for 'Engagement' Process Flow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.processFlow())){
            error = "Failed to click on 'Engagement' Process Flow.";
            return false;
        }        
        narrator.stepPassedWithScreenShot("Successfully click 'Engagement' Process Flow.");        
        
        //Engagement title
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementTitle())){
            error = "Failed to wait for 'Engagement Title' field";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.engagementTitle(), getData("Engagement Title"))){
            error = "Failed to '" + getData("Engagement Title") + "' into Engagement Title field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement Title : '" + getData("Engagement Title") + "'.");
        
          
        //Engagement date
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementDate())){
            error = "Failed to wait for 'Engagement date' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.engagementDate(), startDate)){
            error = "Failed to enter '" + startDate + "' into 'Engagement date' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement date : '" + startDate + "'.");
         

        
        //Business Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.businessUnitTab())){
            error = "Failed to wait for 'Business Unit' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.businessUnitTab())){
            error = "Failed to click on 'Business Unit' tab.";
            return false;
        }
        //Expand Global Company
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.expandGlobalCompany())){
            error = "Failed to wait for 'Global Company' expansion";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.expandGlobalCompany())){
            error = "Failed to wait for 'Global Company' expansion";
            return false;
        }
        //Expand South Africa
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.expandSouthAfrica())){
            error = "Failed to wait for 'South Africa' expansion";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.expandSouthAfrica())){
            error = "Failed to wait for 'South Africa' expansion";
            return false;
        }
        
        if (getData("Select All").equalsIgnoreCase("YES")){
            //Select all 
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.businessUnit_SelectAll())){
                error = "Failed to wait for 'Business Unit' tab.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.businessUnit_SelectAll())){
                error = "Failed to click on 'Select All' Business Unit.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'Select All' Business Unit."); 
        }else{
            //Victory Site
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.businessUnitSelect(getData("Business Unit")))){
                error = "Failed to wait for Business Unit option: '" + getData("Business Unit") + "'.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.businessUnitSelect(getData("Business Unit")))){
                error = "Failed to select Business Unit option: '" + getData("Business Unit") + "'.";
                return false;
            }
            //Close Businuss Tab //remember to change
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.closeBusinessUnit())){
                error = "Failed to wait for 'Business unit' close tab.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.closeBusinessUnit())){
                error = "Failed to click on 'Business unit' close tab.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Business unit : '" + getData("Business Unit") + "'.");
        }
         pause(2000);
        
        //Project Link checkbox
        if (getData("Project Link").equalsIgnoreCase("YES")) {
            //Check project link Checkbox
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.projectLink())) {
                error = "Failed to wait for 'Project link' check box.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.projectLink())) {
                error = "Failed to click on 'Project link' check box.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Project Link successfully checked.");

            //Project
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.projectTab())) {
                error = "Failed to wait for 'Project' tab.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.projectTab())) {
                error = "Failed to click on 'Project' tab.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.anyProject(getData("Project")))) {
                error = "Failed to wait for '" + getData("Project") + "' list.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.anyProject(getData("Project")))) {
                error = "Failed to click on '" + getData("Project") + "' list.";
                return false;
            }
            SeleniumDriverInstance.pause(2000);
            narrator.stepPassedWithScreenShot("Project : '" + getData("Project") + "'.");
        }
        
        
        //Engagement method
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementMethodTab())){
            error = "Failed to wait for 'Engagement method' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.engagementMethodTab())){
            error = "Failed to click on 'Engagement method' tab.";
            return false;
        }
        //Expand In-Person engagements
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.expandInPerson())){
            error = "Failed to wait for 'In-person engagements' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.expandInPerson())){
            error = "Failed to click on 'In-person engagements' option.";
            return false;
        }
        //Any anyEngagement
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.anyEngagementMethod(getData("Engagement Method")))){
            error = "Failed to wait for '" + getData("Engagement Method") + "' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.anyEngagementMethod(getData("Engagement Method")))){
            error = "Failed to click on '" + getData("Engagement Method") + "' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement Method : '" + getData("Engagement Method") + "'.");
        
        //Engagement purpose
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementPurposeTab())){
            error = "Failed to wait for 'Engagement Purpose' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.engagementPurposeTab())){
            error = "Failed to click on 'Engagement Purpose' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.anyEngagementPurpose(getData("Engagement Purpose")))){
            error = "Failed to wait for '" + getData("Engagement Purpose") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.anyEngagementPurpose(getData("Engagement Purpose")))){
            error = "Failed to click on '" + getData("Engagement Purpose") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement Purpose : '" + getData("Engagement Purpose") + "'.");
        
        //Responsible person
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.responsiblePersonTab())){
           error = "Failed to wait for 'Responsible Person' dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.responsiblePersonTab())){
           error = "Failed to click on 'Responsible Person' dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.anyResponsiblePerson(getData("Responsible Person")))){
           error = "Failed to wait for '" + getData("Responsible Person") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.anyResponsiblePerson(getData("Responsible Person")))){
           error = "Failed to click on '" + getData("Responsible Person") + "' option.";
           return false;
       }
        
       //Impact type
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.checklistSelect(getData("Impact type")))){
           error = "Failed to wait for '" + getData("Impact type") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.checklistSelect(getData("Impact type")))){
           error = "Failed to click on '" + getData("Impact type") + "' option.";
           return false;
       }
       
         //Audience Type
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.audienceType())){
           error = "Failed to wait for Audience Type dropdown";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.audienceType())){
           error = "Failed to click on Audience Type dropdown";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.anyResponsiblePerson(getData("Audience Type")))){
           error = "Failed to wait for '" + getData("Audience Type") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.anyResponsiblePerson(getData("Audience Type")))){
           error = "Failed to click on '" + getData("Audience Type") + "' option.";
           return false;
       }
        
        //Contact inquiry/topic
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.contactInquiryOrTopic())){
           error = "Failed to wait for Contact inquiry/topic dropdown";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.contactInquiryOrTopic())){
           error = "Failed to click on Contact inquiry/topic dropdown";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.anyResponsiblePerson(getData("Contact inquiry/topic")))){
           error = "Failed to wait for '" + getData("Contact inquiry/topic") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.anyResponsiblePerson(getData("Contact inquiry/topic")))){
           error = "Failed to click on '" + getData("Contact inquiry/topic") + "' option.";
           return false;
       }
        
         //Nature of contact inquiry/topic
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.natureOfContactInquiryTextbox())){
            error = "Failed to wait for 'Nature of contact inquiry/topic' field";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.natureOfContactInquiryTextbox(), getData("Nature of contact inquiry/topic"))){
            error = "Failed to '" + getData("Nature of contact inquiry/topic") + "' into Nature of contact inquiry/topic field";
            return false;
        }
        
        //Location
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.locationDropdown())){
           error = "Failed to wait for Location dropdown";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.locationDropdown())){
           error = "Failed to click on Location dropdown";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.Location(getData("Location")))){
           error = "Failed to wait for '" + getData("Location") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.Location(getData("Location")))){
           error = "Failed to click on '" + getData("Location") + "' option.";
           return false;
       }
       
        //Engagement description
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementDescription())){
            error = "Failed to wait for 'Engagement Description' field";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.engagementDescription(), getData("Engagement Description"))){
            error = "Failed to '" + getData("Engagement Description") + "' into Engagement Description field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement description : '" + getData("Engagement Description") + "'.");
        
        //Engagement Outcome
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementOutcome())){
            error = "Failed to wait for 'Engagement Outcome' field";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.engagementOutcome(), getData("Engagement Outcome"))){
            error = "Failed to '" + getData("Engagement Outcome") + "' into Engagement Outcome field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement Outcome : '" + getData("Engagement Outcome") + "'.");
        
         
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.Engagement_save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.Engagement_save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
       
        pause(25000);
        //Get the Record No
        String[] engagementRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_View_PageObjects.getEngamentRecord()).split("#");
        setRecordId(engagementRecord[1]);
        
        //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
        

        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Entity_View_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully switched to 'IsoMetrix' window.");
        
        //Engagement Viewing Grid
        if(!SeleniumDriverInstance.scrollToElement(Stakeholder_Entity_View_PageObjects.engagementsTab())){
            error = "Failed to click on 'Engagements' tab.";
            return false;
        }
        
        pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.EngamentVGRefresh())){
            error = "Failed to wait for Engagement Viewing Grid Refresh button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.EngamentVGRefresh())){
            error = "Failed to click on Engagement Viewing Grid Refresh button";
            return false;
        }
        
        pause(10000);
        
        return true;
    }
    
   

}

