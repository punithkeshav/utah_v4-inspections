/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;

import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsometricsPOCPageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author Delwin.Horsthemke
 */
@KeywordAnnotation(
        Keyword = "FR2_Capture_TopicIssue_Assessment_MainScenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_TopicIssue_Assessment_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR2_Capture_TopicIssue_Assessment_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!CaptureTopicIssueAssessment())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean CaptureTopicIssueAssessment(){
//        //Stakeholder Analysis Tab
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_AnalysisTab())){
//            error = "Failed to wait for Stakeholder Analysis Tab";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_AnalysisTab())){
//            error = "Failed to click on Stakeholder Analysis Tab";
//            return false;
//        }
//        SeleniumDriverInstance.pause(1000);
//        narrator.stepPassedWithScreenShot("Successfully navigated to Stakeholder Analysis Tab");
//        
//        if(!SeleniumDriverInstance.scrollToElement(Stakeholder_Entity_PageObjects.SE_AnalysisTab())){
//            error = "Failed to click on Stakeholder Analysis Tab";
//            return false;
//        }
//        
//        pause(1000);
        
        //Topic/Issue Assessment Panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_TopicIssueAssessmentPanel())){
            error = "Failed to wait for Topic/Issue Assessment Panel";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_TopicIssueAssessmentPanel())){
            error = "Failed to click on Topic/Issue Assessment Panel";
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to Topic/Issue Assessment Panel");
        
        pause(1000);
        
        //Topic/Issue Assessment Add
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_TopicIssueAssessmentAdd())){
            error = "Failed to wait for Topic/Issue Assessment Add button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_TopicIssueAssessmentAdd())){
            error = "Failed to click on Topic/Issue Assessment Add button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Topic/Issue Assessment Add button");
        
        pause(2000);
        
        //Topic/Issue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_TopicIssueDD())){
            error = "Failed to wait for Topic/Issue dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_TopicIssueDD())){
            error = "Failed to click on Topic/Issue dropdown";
            return false;
        }
        
        pause(5000);
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.expandChevronTree(testData.getData("TopicIssueDDExpand1"),"1"))){
            error = "Failed to wait for Topic/Issue expandChevronTree";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.expandChevronTree(testData.getData("TopicIssueDDExpand1"),"1"))){
            error = "Failed to click on Topic/Issue expandChevronTree";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.singleSelectDropDownByXpath1(testData.getData("TopicIssueSelect"),"1"))){
            error = "Failed to wait for Topic/Issue Select";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.singleSelectDropDownByXpath1(testData.getData("TopicIssueSelect"),"1"))){
            error = "Failed to click on Topic/Issue Select";
            return false;
        }
        
        //Influence
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_InfluenceDD())){
            error = "Failed to wait for Influence dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_InfluenceDD())){
            error = "Failed to click on Influence dropdown";
            return false;
        }
        
        pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.singleSelectDropDownByXpath2(testData.getData("InfluenceDD1"),"3"))){
            error = "Failed to wait for Influence Select";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.singleSelectDropDownByXpath2(testData.getData("InfluenceDD1"),"3"))){
            error = "Failed to click on Influence Select";
            return false;
        }
        
        //Interest
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_InterestDD())){
            error = "Failed to wait for Interest dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_InterestDD())){
            error = "Failed to click on Interest dropdown";
            return false;
        }
        
        pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.singleSelectDropDownByXpath2(testData.getData("InterestDD1"),"4"))){
            error = "Failed to wait for Interest Select";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.singleSelectDropDownByXpath2(testData.getData("InterestDD1"),"4"))){
            error = "Failed to click on Interest Select";
            return false;
        }
        
        pause(1000);
        
        narrator.stepPassedWithScreenShot("Successfully selected Influence and Interest and Assessment formula field updated with Actively Engage text");
        
        //Save All Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_TopicIssueAssessmentSaveAll())){
            error = "Failed to wait for Save All Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_TopicIssueAssessmentSaveAll())){
            error = "Failed to click on Save All Button";
            return false;
        }
        
        pause(10000);
        
        narrator.stepPassedWithScreenShot("Successfully clicked on Save All Button and Topic/Issue record saved");
        
        
        
        return true;
    }
}
