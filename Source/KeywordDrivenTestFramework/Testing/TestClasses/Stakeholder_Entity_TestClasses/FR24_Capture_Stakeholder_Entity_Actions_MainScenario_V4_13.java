/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_FR22toFR26_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Stakeholder Entity Actions - Main Scenario",
        createNewBrowserInstance = false
)

public class FR24_Capture_Stakeholder_Entity_Actions_MainScenario_V4_13 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR24_Capture_Stakeholder_Entity_Actions_MainScenario_V4_13()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderEntity())
        {
            return narrator.testFailed("Failed due - " + error);
        }
         return narrator.finalizeTest("Successfully navigated ");
    }

    
        public boolean navigateToStakeholderEntity(){   
        //Right Arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to wait for 'Left' Arrow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(500);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(500);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(500);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(500);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(500);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked 'Left' Arrow.");
            
        //Navigate to Actions Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.actions_tab())){
            error = "Failed to wait for 'Actions' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.actions_tab())){
            error = "Failed to click on 'Actions' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Actions' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.actions_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.actions_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

         //Enter Action description
         pause(5000);
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.action_description())){
            error = "Failed to wait for 'Action description' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.action_description(), testData.getData("Action description"))){
            error = "Failed to enter '"+testData.getData("Website")+"' into 'Action description' input field.";
            return false;
        }
         
        //Select Department responsible
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.departmentResponsibleDropDown())){
           error = "Failed to wait for 'Department responsible'";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.departmentResponsibleDropDown())){
           error = "Failed to click on 'Department responsible'";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.departmentResponsibleDropDownValue(getData("Department responsible")))){
           error = "Failed to wait for '" + getData("Department responsible") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.departmentResponsibleDropDownValue(getData("Department responsible")))){
           error = "Failed to click on '" + getData("Department responsible") + "' option.";
           return false;
       }
       
        //Select Responsible person
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.actionsresponsiblePersonTab())){
           error = "Failed to wait for 'Responsible Person' tab.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.actionsresponsiblePersonTab())){
           error = "Failed to click on 'Responsible Person' tab.";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.actionsanyResponsiblePerson(getData("Responsible Person")))){
           error = "Failed to wait for '" + getData("Responsible Person") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.actionsanyResponsiblePerson(getData("Responsible Person")))){
           error = "Failed to click on '" + getData("Responsible Person") + "' option.";
           return false;
       }
        
       //Action due date
        String ActionDueDate=SeleniumDriverInstance.getADate("", 0);
         
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.actionDueDate(), ActionDueDate))
        {
            error = "Failed to enter  Action due date";
            return false;
        }
        

        //Save Actions
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.actions_Save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.actions_Save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.saveWait2(), 400)) {
            error = "Website too long to load wait reached the time out";
            return false;
        }
        
        pause(20000);
        narrator.stepPassedWithScreenShot("Successfully saved actions");
      
        
        return true;
    }

}

