/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.TrainingSiteEHSHomePageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "Click on required module on EHS page",
    createNewBrowserInstance = false
)
public class TrainingSiteEHSHomePageTest extends BaseClass
{

    String error = "";

    public TrainingSiteEHSHomePageTest()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!navigateToAPageFromEHSHomePage())
        {
            return narrator.testFailed("Failed to navigate to a module from EHS home page - " + error);
        }
        return narrator.finalizeTest("Successfully Navigated to module from EHS home page");
    }

   
    public boolean navigateToAPageFromEHSHomePage() throws InterruptedException
    {
       
        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteEHSHomePageObjects.linkForAPageInHomePageXpath(testData.getData("EHSPageName")))) {
            error = "Failed to locate the module: "+testData.getData("EHSPageName");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteEHSHomePageObjects.linkForAPageInHomePageXpath(testData.getData("EHSPageName"))))
        {
            error = "Failed to navigate to the module: "+testData.getData("EHSPageName");
            return false;
        }

        
        return true;

    }

}
