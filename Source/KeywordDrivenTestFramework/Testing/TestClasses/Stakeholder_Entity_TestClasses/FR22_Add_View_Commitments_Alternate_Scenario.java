/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_FR22toFR26_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_View_PageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses.*;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Add Commitments - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR22_Add_View_Commitments_Alternate_Scenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR22_Add_View_Commitments_Alternate_Scenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderEntity())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToStakeholderEntity(){
         
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to wait for 'Stakeholders Entity' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to click on 'Stakeholders Entity' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Entity' tab.");
        
        pause(10000);
        
        //Entity Name Search
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.SE_EntityNameSearchBox(), testData.getData("SE_EntityNameSearch"))){
            error = "Failed to enter '"+testData.getData("SE_EntityNameSearch")+"' into 'SE_EntityNameSearch' input.";
            return false;
        }
        
        pause(2000);
        
        //Search Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to wait for Search Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to click on Search Button";
            return false;
        }
        
        SeleniumDriverInstance.pause(15000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Search Button");
        
        //Stakeholder Selection
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.SE_RecordSelection())){
            error = "Failed to wait for Stakeholder Selection";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.SE_RecordSelection())){
            error = "Failed to click on Stakeholder Selection";
            return false;
        }
        
        pause(15000);
        
        //Right Arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to wait for 'Left' Arrow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(500);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(500);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(500);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(500);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked 'Left' Arrow.");

        //Set the page as a parent page
        String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        
        //Commitments tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.commitmentsTab())){
            error = "Failed to wait for Commitments tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.commitmentsTab())){
            error = "Failed to click on Commitments tab";
            return false;
        }

         //Add a Commitments button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.addCommitmentsButton())){
            error = "Failed to wait for Add a Commitments button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.addCommitmentsButton())){
            error = "Failed to click on Add a Commitments button";
            return false;
        }
        
        pause(5000);
        
        //switch to the new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch on the 'Engagement Plan' window.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched to the 'Engagement Plan' window..");
        
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Entity_View_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //processflow
         pause(3000);
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.commitmentsProcessflow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.commitmentsProcessflow()))
        {
            error = "Failed to click on processflow";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.processFlowStatus("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }

         //Business Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }
    
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.expandChevron(testData.getData("Global Company expand")))){
                error = "Failed to wait to expand 'Global Company'.";
                return false;
            }
            
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.expandChevron(testData.getData("Global Company expand")))){
                error = "Failed to expand 'Global Company'.";
                return false;
            }
            
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.expandChevron(testData.getData("South Africa expand")))){
                error = "Failed to wait to expand 'South Africa'.";
                return false;
            }
            
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.expandChevron(testData.getData("South Africa expand")))){
                error = "Failed to expand 'South Africa'.";
                return false;
           }
               
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.businessUnitValue(getData("Business Unit")))) {
            error = "Failed to click on Business unit value";
            return false;
           }
           
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.businessUnitDpdn()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.businessUnitDpdn()))
        {
            error = "Failed to close Business Unit Dropdown";
            return false;
        }
        
        //Project Title   
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.projectTilte()))
        {
            error = "Failed to locate Project Title field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.projectTilte(),testData.getData("Project Title")))
        {
            error = "Failed to enter text in Project Title field";
            return false;
        }  
           
           
        //Commencement date   
        String commencementdate=SeleniumDriverInstance.getADate("", 0);

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.commencementDateXpath(), commencementdate))
        {
            error = "Failed to enter Commencement date";
            return false;
        }

        //Delivery date
        String deliverydate=SeleniumDriverInstance.getADate("", 1);

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.deliveryDateXpath(), deliverydate))
        {
            error = "Failed to enter Delivery date";
            return false;
        }
        
        //Approved budget
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.approvedBudget()))
        {
            error = "Failed to locate Approved budget field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.approvedBudget(),testData.getData("Approved budget")))
        {
            error = "Failed to enter text in Approved budget field";
            return false;
        }  
        
        //Location Dropdown
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.locationDropdown()))
        {
            error = "Failed to locate Location Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.locationDropdown()))
        {
            error = "Failed to click on Location Dropdown";
            return false;
        } 
        
         pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.singleSelect(testData.getData("Location"))))
        {
            error = "Failed to wait for Location value";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.singleSelect(testData.getData("Location"))))
        {
            error = "Failed to click on Location value";
            return false;
        }
        
        
        //Responsible Person Dropdown
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.responsiblePersonDropdown()))
        {
            error = "Failed to locate Responsible Person Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.responsiblePersonDropdown()))
        {
            error = "Failed to click on Responsible Person Dropdown";
            return false;
        } 
        
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.singleSelect(testData.getData("Responsible Person"))))
        {
            error = "Failed to wait for Responsible Person value";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.singleSelect(testData.getData("Responsible Person"))))
        {
            error = "Failed to click on Responsible Person value";
            return false;
        }
        
        //Sector Dropdown
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.sectorDropdown()))
        {
            error = "Failed to locate Sector Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.sectorDropdown()))
        {
            error = "Failed to click on Sector Dropdown";
            return false;
        } 
        
         
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.singleSelect(testData.getData("Sector"))))
        {
            error = "Failed to wait for Sector value";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.singleSelect(testData.getData("Sector"))))
        {
            error = "Failed to click on Sector value";
            return false;
        }
           
        //Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.description()))
        {
            error = "Failed to locate Description field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.description(),testData.getData("Description")))
        {
            error = "Failed to enter text in Description field";
            return false;
        }
        
        //Link to Meeting dropdown
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.linkToMeetingDropdown()))
        {
            error = "Failed to locate Link to Meeting Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.linkToMeetingDropdown()))
        {
            error = "Failed to click on Link to Meeting Dropdown";
            return false;
        } 
        
       
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.singleSelectIndex(testData.getData("Link to Meeting"),"1")))
        {
            error = "Failed to wait for Link to Meeting value";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.singleSelectIndex(testData.getData("Link to Meeting"),"1")))
        {
            error = "Failed to click on Link to Meeting value";
            return false;
        }
        
        
        //Save button
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }   
      
         if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_FR22toFR26_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        } 
        
        pause(15000);
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_FR22toFR26_PageObjects.commitmentsRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        
        //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
        

        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Entity_View_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }

        pause(2000);
        
        narrator.stepPassedWithScreenShot("Successfully added Commitments record");
        
        return true;
    }

}

