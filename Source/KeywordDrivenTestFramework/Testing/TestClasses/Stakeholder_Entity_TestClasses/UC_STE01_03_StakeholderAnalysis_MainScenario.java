/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;
/**
 *
 * @author Delwin.Horsthemke
 */
@KeywordAnnotation(
        Keyword = "Capture Stakeholder Analysis- Main Scenario",
        createNewBrowserInstance = false
)

public class UC_STE01_03_StakeholderAnalysis_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public UC_STE01_03_StakeholderAnalysis_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToAnalysis())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterAnalysis()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToAnalysis(){
        //Left Arrow
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.leftArrowTab())){
//            error = "Failed to wait for 'Left' Arrow.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Entity_PageObjects.leftArrowTab())){
//            error = "Failed to click on 'Left' Arrow.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked 'Left' Arrow.");
        
        //Navigate to Stakeholder Analysis Tab
        pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.analysisTab())){
            error = "Failed to wait for 'Stakeholder Analysis' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.analysisTab())){
            error = "Failed to click on 'Stakeholder Analysis' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Analysis' tab.");
        
        return true;
    }
    
    //Enter Analysis
    public boolean enterAnalysis(){
        //Stakeholder interest
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.StakeholderinterestDD())){
            error = "Failed to wait for 'Stakeholder interest' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.StakeholderinterestDD())){
            error = "Failed to click on 'Stakeholder interest' dropdown.";
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.StakeholderinterestSelect(testData.getData("Stakeholder interest")))){
            error = "Failed to wait for 'Stakeholder interest' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.StakeholderinterestSelect(testData.getData("Stakeholder interest")))){
            error = "Failed to select '"+testData.getData("Stakeholder interest")+"' from 'Stakeholder interest' dropdown options.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Stakeholder interest selected");

        //Stakeholder influence
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.StakeholderinfluenceDD())){
            error = "Failed to wait for 'Stakeholder influence' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.StakeholderinfluenceDD())){
            error = "Failed to click on 'Stakeholder influence' dropdown.";
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.StakeholderinfluenceSelect(testData.getData("Stakeholder influence")))){
            error = "Failed to wait for 'Stakeholder influence' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.StakeholderinfluenceSelect(testData.getData("Stakeholder influence")))){
            error = "Failed to select '"+testData.getData("Stakeholder influence")+"' from 'Stakeholder influence' dropdown options.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Stakeholder influence selected");
        
        String GuidelinesPanel;
        GuidelinesPanel = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.GuidelinesPanel());
        
        String Engageactivelywiththisstakeholder;
        Engageactivelywiththisstakeholder = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.Engageactivelywiththisstakeholder());
        
        narrator.stepPassedWithScreenShot("Guidelines panel displays and Stakeholder analysis image displays in the Social Status panel which is updated according to the matrix");

        //Stakeholder support
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.StakeholdersupportDD())){
            error = "Failed to wait for 'Stakeholder support' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.StakeholdersupportDD())){
            error = "Failed to click on 'Stakeholder support' dropdown.";
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.StakeholdersupportSelect(testData.getData("Stakeholder support")))){
            error = "Failed to wait for 'Stakeholder support' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.StakeholdersupportSelect(testData.getData("Stakeholder support")))){
            error = "Failed to select '"+testData.getData("Stakeholder support")+"' from 'Stakeholder support' dropdown options.";
            return false;
        }
        
        String Positivesupportisgivenfromthisstakeholder;
        Positivesupportisgivenfromthisstakeholder = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.Positivesupportisgivenfromthisstakeholder());
        
        narrator.stepPassedWithScreenShot("Stakeholder support image displays in the Social Status panel and updates");

        //Comments
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Comments())){
            error = "Failed to wait for 'Phone number' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Comments(), getData("Comments"))){
            error = "Failed to enter '" + getData("Comments") + "' into 'Comments' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Comments: '" + getData("Comments") + "' .");
        
        //Impact on stakeholder
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ImpactonstakeholderDD())){
            error = "Failed to wait for 'Impact on stakeholder' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ImpactonstakeholderDD())){
            error = "Failed to click on 'Impact on stakeholder' dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ImpactonstakeholderExpandChevron(testData.getData("Impact on stakeholder Expand"),"1"))){
            error = "Failed to wait for Impact on stakeholder expandChevronTree";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ImpactonstakeholderExpandChevron(testData.getData("Impact on stakeholder Expand"),"1"))){
            error = "Failed to click on Impact on stakeholder expandChevronTree";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.singleSelectDropDownByXpath(testData.getData("Impact on stakeholder Select"),"1"))){
            error = "Failed to wait for Impact on stakeholder Select";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.singleSelectDropDownByXpath(testData.getData("Impact on stakeholder Select"),"1"))){
            error = "Failed to click on Impact on stakeholder Select";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ImpactonstakeholderLabel())){
            error = "Failed to click on 'Impact on stakeholder' label.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Impact on stakeholder selected");
        
        //Save Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_SaveBtn())){
            error = "Failed to wait for 'Save Button'";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_SaveBtn())){
            error = "Failed to click on 'Save Button'";
            return false;
        }
        
        pause(15000);
        
        narrator.stepPassedWithScreenShot("Successfully added Stakeholder Analysis Details");
        
        return true;
        
    }
}
