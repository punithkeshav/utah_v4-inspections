/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsometricsPOCPageObjects;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Contractor Permission to Work Sign Off - Main Scenario",
        createNewBrowserInstance = false
)

public class FR14_Capture_Contractor_Permission_to_Work_Sign_Off_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR14_Capture_Contractor_Permission_to_Work_Sign_Off_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 90);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
 
    }

    public TestResult executeTest()
    {
        if (!navigateToPermissiontoWorkSignOff()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterPermissiontoWorkSignOffDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
       
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToPermissiontoWorkSignOff(){    
        //Navigate to Permission to Work tab
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ChildTabScrollRight())){
            error = "Failed to click on Tab Scroll Right button";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ChildTabScrollRight())){
            error = "Failed to click on Tab Scroll Right button";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ChildTabScrollRight())){
            error = "Failed to click on Tab Scroll Right button";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ChildTabScrollRight())){
            error = "Failed to click on Tab Scroll Right button";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_PermissiontoWorktab())){
            error = "Failed to wait for Permission to Work tab";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_PermissiontoWorktab())){
            error = "Failed to click on Permission to Work tab";
            return false;
        }
        SeleniumDriverInstance.pause(3000);
        narrator.stepPassedWithScreenShot("Successfully navigated to Permission to Work tab");
        
        //Add Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_PermissiontoWorkadd())){
            error = "Failed to wait for Permission to Work Add";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_PermissiontoWorkadd())){
            error = "Failed to click on Permission to Work Add";
            return false;
        }
        
        return true;
    }
    
    public boolean enterPermissiontoWorkSignOffDetails(){
        
        pause(5000);
        
        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.PermissiontoWorkPF())){
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.PermissiontoWorkPF())){
            error = "Failed to click on Process Flow button.";
            return false;
        }
        
        //Process Flow Add phase
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatusChild("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Role
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.PermissiontoWorkRoleDD())){
            error = "Failed to wait for Role dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.PermissiontoWorkRoleDD())){
            error = "Failed to click on Role dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Role_Dropdown_Option(getData("Role")))){
            error = "Failed to wait for Role option.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Role_Dropdown_Option(getData("Role")))){
            error = "Failed to click on Role option.";
            return false; 
        }
        
        //Responsible person to sign off
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.PermissiontoWorkResponsiblePersonDD())){
            error = "Failed to wait for Responsible person to sign off dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.PermissiontoWorkResponsiblePersonDD())){
            error = "Failed to click on Responsible person to sign off dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ResponsiblePerson_Dropdown_Option(getData("Responsible person to sign off")))){
            error = "Failed to wait for Responsible person to sign off option.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ResponsiblePerson_Dropdown_Option(getData("Responsible person to sign off")))){
            error = "Failed to click on Responsible person to sign off option.";
            return false; 
        }
        
        //Sign off
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SignoffDD())){
            error = "Failed to wait for Sign off dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SignoffDD())){
            error = "Failed to click on Sign off dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SignoffDD_Option())){
            error = "Failed to wait for Sign off - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SignoffDD_Option())){
            error = "Failed to click on Sign off - Yes";
            return false;
        }
        
        //Sign off date
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SignoffDate())) {
            error = "Failed to wait for Sign off date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.SignoffDate(), startDate)) {
            error = "Failed to enter '" + startDate + "' into Sign off date";
            return false;
        }
        
        //Comments
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.CommentsText())){
            error = "Failed to wait for Comments field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.CommentsText(), getData("Comments"))){
            error = "Failed to click on Comments field.";
            return false; 
        }
        
        //Save Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.PermissiontoWorkRoleSave())){
            error = "Failed to wait for Save Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.PermissiontoWorkRoleSave())){
            error = "Failed to click on Save Button";
            return false;
        }

        pause(15000);
        
        narrator.stepPassedWithScreenShot("Successfully clicked Save button and Permission to Work Sign Off record saved");
        
        return true;
    }
  
}
