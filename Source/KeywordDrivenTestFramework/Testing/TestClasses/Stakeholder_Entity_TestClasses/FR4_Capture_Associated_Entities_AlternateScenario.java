/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsometricsPOCPageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;
/**
 *
 * @author Delwin.Horsthemke
 */
@KeywordAnnotation(
        Keyword = "Capture Associated Entities - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Associated_Entities_AlternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR4_Capture_Associated_Entities_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToAssociatedEntities())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!createNewEntity()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToAssociatedEntities(){    
        //Navigate to Associated Entities Tab
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.associatedentities_tab())){
            error = "Failed to wait for 'Members Tab' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.associatedentities_tab())){
            error = "Failed to click on 'Members Tab' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Associated Entities' tab.");
        
        //Create new entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.se_createnewentity())){
            error = "Failed to wait for 'Create new entity' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.se_createnewentity())){
            error = "Failed to click on 'Create new entity' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        //Set the page as a parent page
        String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        
        
        return true;
    }
    
    //Enter data
    public boolean createNewEntity(){
        pause(5000);
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new tab.";
            return false;
        }
        //switch to the iframe
        if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
        }
        
        pause(20000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_processflow(), 5000)){
            error = "Failed to wait for Process flow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_processflow())){
            error = "Failed to click Process flow.";
            return false;
        }
        
        //Process Flow Add phase
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatus("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Entity Type dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_EntityType_dropdown(),15000)){
            error = "Failed to wait for 'Entity Type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_EntityType_dropdown())){
            error = "Failed to click 'Entity Type' dropdown.";
            return false;
        }
        
        pause(20000);
         
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_EntityType_dropdownValue(testData.getData("EntityType1"))))
        {
            error = "Failed to locate '"+testData.getData("Entity Type")+"' into 'Entity Type' input.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_EntityType_dropdownValue(testData.getData("EntityType1"))))
        {
            error = "Failed to enter '"+testData.getData("Entity Type")+"' into 'Entity Type' input.";
            return false;
        }

        //Entity name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_name())){
            error = "Failed to wait for 'Entity name' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.SE_name(), testData.getData("Entity name1"))){
            error = "Failed to enter '"+testData.getData("Entity name")+"' into 'Entity name' input.";
            return false;
        }
        
        //Industry dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_industry_dropdown())){
            error = "Failed to wait for 'Industry' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_industry_dropdown())){
            error = "Failed to click 'Industry' dropdown.";
            return false;
        }
        
        //Industry select 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_industry_select(testData.getData("Industry1")))){
            error = "Failed to wait for 'Industry' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_industry_select(testData.getData("Industry1")))){
            error = "Failed to select '"+testData.getData("Industry")+"' from 'Industry' dropdown options.";
            return false;
        }

        //Entity description
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_desc())){
            error = "Failed to wait for 'Entity name' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.SE_desc(), testData.getData("Entity description1"))){
            error = "Failed to enter '"+testData.getData("Entity description")+"' into 'Entity description' textarea.";
            return false;
        }
        
        //Relationship owner dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_owner_dropdown())){
            error = "Failed to wait for 'Relationship owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_owner_dropdown())){
            error = "Failed to click 'Relationship owner' dropdown.";
            return false;
        }
        
        //Relationship owner select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_owner_select(testData.getData("Relationship owner1")))){
            error = "Failed to wait for 'Relationship owner' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_owner_select(testData.getData("Relationship owner1")))){
            error = "Failed to select '"+testData.getData("Relationship owner")+"' from 'Relationship owner' dropdown options.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Entity details completed");
        
        //Stakeholder categories
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_categories(testData.getData("Categories1")))){
            error = "Failed to wait for '"+testData.getData("Categories")+"' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_categories(testData.getData("Categories1")))){
            error = "Failed to click '"+testData.getData("Categories")+"' option.";
            return false;
        }
        
        //Business Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_business_unit_selectall())){
            error = "Failed to wait for 'Business unit' select all.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_business_unit_selectall())){
            error = "Failed to click 'Business unit' select all.";
            return false;
        }
        
        //Impact types
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_impact_types_selectall())){
            error = "Failed to wait for 'Impact type' select all.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_impact_types_selectall())){
            error = "Failed to click 'Impact type' select all.";
            return false;
        }

        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_savetocontinue())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_savetocontinue())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Entity_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        pause(15000);
        
        //Process Flow Edit phase
        narrator.stepPassedWithScreenShot("Processflow in Edit phase");
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatus("Edit phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Edit phase";
        return false;
        }

        String SE_EntityStatusActive;
        SE_EntityStatusActive = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.SE_EntityStatusActive());
        
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

//        //Close the current window
//        SeleniumDriverInstance.Driver.close();
//        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
//        System.out.println(SeleniumDriverInstance.Driver.getTitle());

        //Switch to parent window iframe
        //switch to the iframe
          if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame "; 
        }
        
        pause(10000);
        
        narrator.stepPassedWithScreenShot("New Stakeholder Entity record saved and browser window closed and return to the Stakeholder Entity module");
        
        return true;
    }
}
