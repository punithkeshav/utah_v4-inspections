/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_View_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Add EngagementPlan - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR18_Add_View_EngagementPlan_AlternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR18_Add_View_EngagementPlan_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
       
        if(!navigateToEngagements()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Captured Engagement - record: #" + getRecordId());
    }

    public boolean navigateToEngagements(){
        
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to wait for 'Stakeholders Entity' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to click on 'Stakeholders Entity' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Entity' tab.");
        
        pause(10000);
        
        //Entity Name Search
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.SE_EntityNameSearchBox(), testData.getData("SE_EntityNameSearch"))){
            error = "Failed to enter '"+testData.getData("SE_EntityNameSearch")+"' into 'SE_EntityNameSearch' input.";
            return false;
        }
        
        pause(2000);
        
        //Search Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to wait for Search Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to click on Search Button";
            return false;
        }
        
        SeleniumDriverInstance.pause(15000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Search Button");
        
        //Stakeholder Selection
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.SE_RecordSelection())){
            error = "Failed to wait for Stakeholder Selection";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.SE_RecordSelection())){
            error = "Failed to click on Stakeholder Selection";
            return false;
        }
        
        pause(15000);
        
        //Right Arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.rightArrowTab())){
            error = "Failed to wait for 'Left' Arrow.";
            return false;
        }
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Entity_PageObjects.rightArrowTab())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.rightArrowTab())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.rightArrowTab())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(1000);

        narrator.stepPassedWithScreenShot("Successfully clicked 'Left' Arrow.");
        
        //Navigate to Engaments Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementsTab())){
            error = "Failed to wait for 'Engagements' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.engagementsTab())){
            error = "Failed to click on 'Engagements' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Engagements' tab.");
        
         //Engagement Plan tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementPlanTab())){
            error = "Failed to wait for Engagement Plan tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.engagementPlanTab())){
            error = "Failed to click on Engagement Plan tab";
            return false;
        }
        
        return true;
    }
    
    public boolean enterDetails(){
        //Set the page as a parent page
        String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
         
         //Add an Engagement plan button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.addEngagementPlanButton())){
            error = "Failed to wait for Add an Engagement plan button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.addEngagementPlanButton())){
            error = "Failed to click on Add an Engagement plan button";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully click 'Add Engagement Plan' button.");
        
        pause(10000);
        
        //switch to the new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch on the 'Engagement Plan' window.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched to the 'Engagement Plan' window..");
        
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Entity_View_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
       //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementPlanProcess_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.engagementPlanProcess_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        
        //Business unit dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.planBusinessUnitTab())) {
            error = "Failed to wait for 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.planBusinessUnitTab())) {
            error = "Failed to click on 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.businessUnit_Option(getData("Business unit 1")))) {
            error = "Failed to wait for Business Unit option: " + getData("Business unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.businessUnit_Option(getData("Business unit 1")))) {
            error = "Failed to click the Business Unit option: " + getData("Business unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.businessUnit_Option(getData("Business unit 2")))) {
            error = "Failed to click the Business Unit option: " + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.businessUnit_Option1(getData("Business unit 3")))) {
            error = "Failed to click the Business Unit option: " + getData("Business unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Business Unit: " + getData("Business unit 1") + " -> " + getData("Business unit 2") + " -> " + getData("Business unit 3"));

        //Engagement title field
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementPlanTitle())){
            error = "Failed to wait for 'Engagement plan title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.engagementPlanTitle(), getData("Engagement plan title"))){
            error = "Failed to enter  into engagement plan title field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered into engagement plan title field.");
        
        if(getData("Project link checkbox").equalsIgnoreCase("Yes")){
            //Project link checkbox
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.EP_projectLink())) {
                error = "Failed to wait for 'Project link' checkbox.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.EP_projectLink())) {
                error = "Failed to click on 'Project link' checkbox.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'Project link' checkbox.");
            
            //Project dropdown
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.EP_projectTab())) {
                error = "Failed to wait for 'Project' dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.EP_projectTab())) {
                error = "Failed to click on 'Project' dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.projectSelct(getData("Project")))) {
                error = "Failed to wait for '" + getData("Project") + "' option.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.projectSelct(getData("Project")))) {
                error = "Failed to click on '" + getData("Project") + "' option.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click '" + getData("Project") + "' option.");
        }
        
        //Engagement start date field
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementPlanStartDate())) {
            error = "Failed to wait for 'Engagement start date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.engagementPlanStartDate(), startDate)) {
            error = "Failed to enter '" + startDate + "' into engagement start date field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + startDate + "' into engagement start date field.");
        
        //Engagement end date field
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementPlanEndDate())) {
            error = "Failed to wait for 'Engagement end date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.engagementPlanEndDate(), endDate)) {
            error = "Failed to enter '" + endDate + "' into engagement end date field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + endDate + "' into engagement end date field.");

        //Frequency dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.frequency())) {
            error = "Failed to wait for 'Frequency' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.frequency())) {
            error = "Failed to click on 'Frequency' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.projectSelct(getData("Frequency")))) {
            error = "Failed to wait for '" + getData("Frequency") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.projectSelct(getData("Frequency")))) {
            error = "Failed to click on '" + getData("Frequency") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Frequency") + "' option.");
        
         //Purpose of engagement dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.EP_engagementPurposeTab())) {
            error = "Failed to wait for 'Purpose of engagement' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.EP_engagementPurposeTab())) {
            error = "Failed to click on 'Purpose of engagement' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Engagement purpose' dropdown.");
        
        //Engagement purpose  select
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.purposeOfEngagement(getData("Engagement purpose")))) {
            error = "Failed to wait for '" + getData("Engagement purpose") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.purposeOfEngagement(getData("Engagement purpose")))) {
            error = "Failed to click on '" + getData("Engagement purpose") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Engagement purpose") + "' option.");
        
        //Engagement method dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.methodOfEngagement())) {
            error = "Failed to wait for 'Engagement method' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.methodOfEngagement())) {
            error = "Failed to click on 'Engagement method' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.engagementMethodArrow())) {
            error = "Failed to wait for 'Engagement method' arrow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.engagementMethodArrow())) {
            error = "Failed to click on 'Engagement method' arrow.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.methodOfEngagementSelect(getData("Engagement method")))) {
            error = "Failed to wait for '" + getData("Engagement method") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.methodOfEngagementSelect(getData("Engagement method")))) {
            error = "Failed to click on '" + getData("Engagement method") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Engagement method") + "' option.");

         //Responsible person dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.personResponsibleTab())) {
            error = "Failed to wait for 'Responsible person' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.personResponsibleTab())) {
            error = "Failed to click on 'Responsible person' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.responsiblePersonSelect(getData("Responsible person")))) {
            error = "Failed to wait for '" + getData("Responsible person") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.responsiblePersonSelect(getData("Responsible person")))) {
            error = "Failed to click on '" + getData("Responsible person") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Responsible person") + "' option.");
       
        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.EP_SaveBtn())) {
              error = "Failed to wait for 'Save' button.";
              return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.EP_SaveBtn())) {
              error = "Failed to click on 'Save' button.";
              return false;
        }
        
        pause(25000);
        
        //Get the Record No
        String[] engagementRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_View_PageObjects.getEngamentRecord()).split("#");
        setRecordId(engagementRecord[1]);
        
        //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();

        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Entity_View_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully switched to 'IsoMetrix' window.");
        
        //Engagement Plan Viewing Grid
        if(!SeleniumDriverInstance.scrollToElement(Stakeholder_Entity_View_PageObjects.engagementsTab())){
            error = "Failed to click on 'Engagements' tab.";
            return false;
        }
        
        pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.EngamentPlanVGRefresh())){
            error = "Failed to wait for Engagement Viewing Grid Refresh button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.EngamentPlanVGRefresh())){
            error = "Failed to click on Engagement Viewing Grid Refresh button";
            return false;
        }
        
        pause(10000);

        return true;
    }
    
   

}

