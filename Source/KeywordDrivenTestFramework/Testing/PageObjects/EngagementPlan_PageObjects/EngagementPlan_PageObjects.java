/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.EngagementPlan_PageObjects;

/**
 *
 * @author smabe
 */
public class EngagementPlan_PageObjects
{

    public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String ContainsTextBox()
    {
        return "(//input[@class='txt border'])[1]";
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String SupportingDocumentsTab()
    {
        return "(//div[text()='Supporting Documents'])[1]";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String recordSaved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordSaved_popup1()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordNoChanges_Saved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record has no changes to save')])[1]";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String DeleteButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Delete']";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String uploadLinkbox()
    {
        return "//div[@class='linkbox-options']//b[@class='linkbox-link']";
    }

    public static String businessUnitexpand(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule3()
    {
        return "//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[ contains(text(),'" + text + "')]";

    }
    
    
     public static String Text_2(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "'])[2]";

    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String SearchOption()
    {
        return "//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String FinishButton()
    {
        return "//div[text()='Finish']";
    }

    public static String SaveCurrentSectionButton()
    {
        return "//div[text()='Save current section']";
    }

    public static String ContinueButton()
    {
        return "//div[text()='CONTINUE']";
    }

    public static String PreviousButton()
    {
        return "//div[text()='Previous']";
    }

    public static String StartButton()
    {
        return "//div[text()='START']";
    }

    public static String NextButton()
    {
        return "//div[text()='NEXT']";
    }

    public static String EngagementPlanTab()
    {
        return "//label[text()='Engagement Plan']";
    }

    public static String EngagementPlan_Add()
    {
        return "//div[contains(@class,'active form transition visible')]//div[@id='btnActAddNew']";
    }

    public static String EngagementPlan_processflow()
    {
        return "//div[@id='btnProcessFlow_form_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F']";
    }

    public static String BusinessUnit_Dropdown()
    {
        return "//div[@id='control_8BE367EF-E449-4165-BC05-74385ECBF771']//ul";
    }

    public static String EngagementPlanTitle()
    {
        return "//div[@id='control_185410E8-D077-4DE6-8958-5772CA36E091']//input";
    }

    public static String EngagementStartDate()
    {
        return "//div[@id='control_90276DFA-A2DD-4A38-8D96-E84491597886']//input";
    }

    public static String ShowInAdvance()
    {
        return "(//div[@id='control_7929B98B-21B8-41E1-B4CF-873D96EC5263']//input)[1]";
    }

    public static String PurposeOfEngagementDropDown()
    {
        return "//div[@id='control_36951962-3063-4DA2-9846-ED7137AFC783']//ul";
    }

    public static String ResponsiblePersonDropDown()
    {
        return "//div[@id='control_C2B7C6FA-10FC-4593-BD27-6869D1790758']//ul";
    }

    public static String FrequencyDropDown()
    {
        return "//div[@id='control_E457C6F1-F44C-4B21-8089-E7153700AFB4']//ul";
    }

    public static String LinkToProject()
    {
        return "//div[@id='control_DE6A7B48-B355-48A7-AA3E-8475171708AF']//div[@class='c-chk']//div";
    }

    public static String ProjectDropDown()
    {
        return "//div[@id='control_301410A3-9118-4FCA-93B7-4C2A90320266']//ul";
    }

    public static String OnWhichDayOfTheWeekDropDown()
    {
        return "//div[@id='control_D24F5B79-62C5-46E8-B38C-32AF14CCFD8C']//ul";
    }

    public static String EngagementEndDate()
    {
        return "//div[@id='control_56848732-729E-4C20-A0FC-1034ACF3D6F4']//input";
    }

    public static String EngagementPlan_save()
    {
        return "//div[@id='btnSave_form_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F']";
    }

    public static String FrequencyFromDropDown()
    {
        return "//div[@id='control_7DA20857-9EFF-4817-A80B-ABD07F22AEED']//ul";
    }

    public static String WeekOfMonthDropDown()
    {
        return "//div[@id='control_86F620B2-495B-46CC-B2D5-1A8A78A91096']//ul";
    }

    public static String GroupsDropDown()
    {
        return "//div[@id='control_7524420C-1E4E-49C7-AAF6-C87B97ECEFCF']//ul";
    }

    public static String StakeholdersDropDown()
    {
        return "//div[@id='control_86E5207D-ECEA-4F6D-9680-376A0EB4C2FE']//ul";
    }

    public static String AttendeesTab()
    {
        return "//div[text()='Attendees']";
    }

    public static String ActiveInactiveDropDown()
    {
        return "//div[@id='control_EFAD65B7-67FD-4BE2-BD27-B29F780D7AAB']//ul";
    }

    public static String ActionsTab()
    {
       return "//div[text()='Actions']";
    }

    public static String Actions_Add()
    {
        return"//div[@id='control_52657B39-E580-49F5-88CB-D30DCB545A7B']//div[@id='btnAddNew']";
    }

    public static String Actions_processflow()
    {
        return"//div[@id='btnProcessFlow_form_C7B74A9B-D0CC-4925-832E-F08CD2272A95']";
    }
    
      public static String TypeOfActionDropDown()
    {
       return "//div[@id='control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE']//ul";
    }

    public static String ActionDueDate()
    {
       return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
    
      public static String Entity_Dropdown()
    {
       return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String ActionDescription()
    {
       return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String ResponsiblePerson_dropdown()
    {
       return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }

    public static String Actions_save()
    {
       return "//div[@id='btnSave_form_C7B74A9B-D0CC-4925-832E-F08CD2272A95']";
    }

}
