/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.RelationshipBasedSafety_PageObjects;

/**
 *
 * @author smabe
 */
public class RelationshipBasedSafety_PageObjects
{

    public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String ContainsTextBox()
    {
        return "(//input[@class='txt border'])[1]";
    }

  

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String recordSaved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordSaved_popup1()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordNoChanges_Saved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record has no changes to save')])[1]";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String DeleteButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Delete']";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String uploadLinkbox()
    {
        return "//div[@class='linkbox-options']//b[@class='linkbox-link']";
    }

    public static String businessUnitexpand(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }
    
    public static String Text5(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])[2]";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }
   public static String TypeOfObservation(String text)
    {
       return "(//li[@title='"+text+"']//a[text()='"+text+"'])[3]";
    }
    
     public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String Text4(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String RelationshipBasedSafetyTab()
    {
        return "//label[text()='Relationship Based Safety']";
    }

    public static String RelationshipBased_Add()
    {
        return "//div[contains(@class,'active form transition visible')]//div[@id='btnActAddNew']";
    }

    public static String BusinessUnit_Dropdown()
    {
       return "//div[@id='control_7F840C09-6302-464F-B274-355C33FE2232']//ul";
    }

    public static String DateObserved()
    {
        return"//div[@id='control_97C36EE8-316C-42F2-8569-ADD0C4FD823D']//input";
    }

    public static String TimeObserved()
    {
       return "//div[@id='control_573100B6-D270-4B06-AB9E-F8421E30DD70']//input";
    }

    public static String Observation()
    {
       return "//div[@id='control_1A740402-4144-4BF5-82E0-73179A5C894E']//textarea";
    }
     public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }
    

    public static String PersonObserving_dropdown()
    {
        return"//div[@id='control_4CFEEB48-2002-4677-BA4E-27EAE8C4105F']//ul";
    }

    public static String ObservedGroupIndividual_DropDown()
    {
       return "//div[@id='control_C6497CCE-9BB5-4693-A00D-CC92870A8DF2']//ul";
    }

    public static String RelationshipBased_processflow()
    {
       return "//div[@id='btnProcessFlow_form_6E7B288D-285E-4D74-976E-B6CC1B96292E']";
    }

    public static String RelationshipBasedSafety_save()
    {
       return "//div[@id='btnSave_form_6E7B288D-285E-4D74-976E-B6CC1B96292E']";
    }
    
       public static String SearchOption()
    {
        return"//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String LinkToProject()
    {
       return "//div[@id='control_EFCD8FEC-4BC8-4C30-B069-EF11E5C24984']//div[@class='c-chk']";
    }

    public static String ProjectDropDown()
    {
      return  "//div[@id='control_F4A6B712-F04F-4551-8B46-7492F68A0446']//ul";
    }

    public static String deleteRecordButton()
    {
        return"//div[@id='btnDelete_form_6E7B288D-285E-4D74-976E-B6CC1B96292E']";
    }

    public static String TypeOfObservation_dropdown()
    {
        return"//div[@id='control_762AE2DF-FEC7-4CBC-BB8E-01E662521C2E']//ul";
    }

    public static String WhoWasObserved()
    {
       return "(//div[@id='control_B961592B-E588-4505-96F6-09477AD93003']//input)[1]";
    }

    public static String WhoWasObserved_DropDown()
    {
       return "//div[@id='control_6FDD9BD1-C551-4CB4-85FE-17D553C16AD7']//ul";
    }

    public static String StatusDropDown()
    {
       return "//div[@id='control_6436D4D2-F339-4C3B-B864-8C7739055404']//ul";
    }

    public static String SafetyLeadershipActions_Add()
    {
       return "//div[@id='control_8E84CB39-DEEE-4819-832F-E4A10339C343']//div[@id='btnAddNew']";
    }

    public static String Actions_processflow()
    {
       return "//div[@id='btnProcessFlow_form_4477F482-07A2-4919-82A8-1D3F8EE36CE2']";
    }

    public static String Entity_Dropdown()
    {
       return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String ActionDescription()
    {
       return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String ResponsiblePerson_dropdown()
    {
        return"//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }

    public static String TypeOfActionDropDown()
    {
       return "//div[@id='control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE']//ul";
    }

    public static String ActionDueDate()
    {
       return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String Actions_save()
    {
       return "//div[@id='btnSave_form_4477F482-07A2-4919-82A8-1D3F8EE36CE2']";
    }

    public static String StartButton()
    {
       return "//div[text()='Start']";
    }
    
      public static String StartButton2()
    {
       return "//div[text()='START']";
    }

    public static String AnswerDropDown()
    {
       return "(//div[@name='control_DDL0'])[1]";
    }

    public static String NextButton()
    {
       return "//div[text()='NEXT']";
    }

    public static String AnswerDropDown2()
    {
       return "(//div[@name='control_DDL0'])[5]";
    }

    public static String FinishButton()
    {
       return "//div[text()='Finish']";
    }

    public static String BaselineDataCollectionTab()
    {
        return"//label[contains(text(),'Baseline Data Collection')] ";
    }

    public static String BaselineDataCollection_Add()
    {
       return "//div[@class='active form transition visible']//div[@id='btnActAddNew']";
    }

    public static String BaselineDataCollection_processflow()
    {
       return "//div[@id='btnProcessFlow_form_9D8FF3BF-C0FD-423E-A196-8C74C8A84CF6']";
    }

    public static String DataCollectionStartDate()
    {
        return"//div[@id='control_2954B417-114C-4033-9227-162DA49A13D8']//input";
    }

    public static String Comments()
    {
       return "(//div[@id='control_D6901D59-0E99-4F88-B3A8-31A1A14B4700']//input)[1]";
    }

    
}
