/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.JobRiskAssessment_PageObjects;

/**
 *
 * @author smabe
 */
public class JobRiskAssessment_PageObjects
{

    public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String supporting_tab()
    {
        return "(//div[text()='Supporting Documents'])[1]";
    }

    public static String linkADocument()
    {
        return "(//b[@class='linkbox-link'])[2]";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_button()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "')]";

    }

    public static String Text5(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String Text4(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String Text6(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "')])[1]";
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])[2]";
    }

    public static String Text1(String data)
    {
        return "(//li[@title='" + " " + "''" + data + "']//a[text()='" + " " + "''" + data + "'])";
    }

    public static String Go_no_go_controlOption(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])[3]";
    }

    public static String ExperienceRoleDropDown()
    {
        return "//div[@id='control_A1BF9E59-A685-4589-A3B6-9519333C11D1']//ul";
    }

    public static String saveWait()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String recordSaved_popup_2()
    {
        return "//div[@class='ui floating icon message transition visible']//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String linkToADocument_2()
    {
        return "(//b[@original-title='Link to a document'])[3]";
    }

    public static String UrlInput_TextArea()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextArea()
    {
        return "//input[@id='urlTitle']";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String ContainsTextBox()
    {
        return "(//input[@class='txt border'])[1]";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String recordSaved_popup2()
    {
        return "(//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')])[2]";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String MaskBlock()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone()
    {
        return "//div[@class='ui inverted dimmer']";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String DeleteButton()
    {
        return "//div[@id='btnDelete_form_D5CD6F34-1C5A-488F-94B0-27E8C73678E9']";
    }

    public static String Button_Save()
    {
        return "//div[@id='btnSave_form_D5CD6F34-1C5A-488F-94B0-27E8C73678E9']";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String ActiveInactiveDropDown()
    {
        return "//div[@id='control_5CD5E51E-6759-4CE9-A919-9BC43714814E']";
    }

    public static String EntityDropDown()
    {
        return "//div[@id='control_C3B55F9A-9273-4AB3-ABDF-7EE499DFFE78']//ul";
    }

    public static String EntityexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";

    }

    public static String FunctionalLocationDropDown()
    {
        return "//div[@id='control_0097245D-FE23-481C-B019-C792FDE94D92']//ul";
    }

    public static String FunctionalLocationexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String RelatedSAPTaskDropDown()
    {
        return "//div[@id='control_FE20AA32-6F87-40EF-BB06-7719C576654D']//ul";
    }

    public static String ProcessActivityDropDown()
    {
        return "//div[@id='control_6CBA62CE-8B77-4FD1-8367-4F8D54A6B595']//ul";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String JobRiskAssessment_processflow()
    {
        return "//div[@id='btnProcessFlow_form_D5CD6F34-1C5A-488F-94B0-27E8C73678E9']";
    }

    public static String JobTaskTitle()
    {
        return "//div[@id='control_E17B8C26-0CA1-4962-8964-5B1F1D29CB32']//input";
    }

    public static String JRAReferenceNumber()
    {
        return "//div[@id='control_ED2A8896-644E-4527-8410-76510DF4DFC0']//input";
    }

    public static String JobTaskScopeAndObjectives()
    {
        return "//div[@id='control_0E0F1D4C-A02B-438C-B418-44B64963EE2C']//textarea";
    }

    public static String JRATeamLeaderDropDown()
    {
        return "//div[@id='control_2021F26E-E560-4F21-A61A-5DA257E522B8']//ul";
    }

    public static String JRARecordedByDropDown()
    {
        return "//div[@id='control_344A7183-6494-4CEC-8619-DCFF6077B2BA']//ul";
    }

    public static String DateJRAConducted()
    {
        return "//div[@id='control_EE909313-ED9B-4F29-B7C6-33AEE3BE3852']//input";
    }

    public static String ListEquipment()
    {
        return "//div[@id='control_806FF953-F9E7-4530-882B-B312EB42B9E9']//textarea";
    }

    public static String JobRiskAssessmentTab()
    {
        return "//label[text()='Layer 3 - Job Risk Assessment']";
    }

    public static String JobRiskAssessment_Add()
    {
        return "//div[@id='btnActAddNew']";
    }

    public static String ExistingProcedureDropDown()
    {
        return "//div[@id='control_E9C73876-7B34-49E5-A553-F1EE53111F12']//ul";
    }

    public static String ActivitiesImpactDropDown()
    {
        return "//div[@id='control_475F1454-92EF-40B0-A677-7F27180E6E47']//ul";
    }

    public static String TeamNameDropDown()
    {
        return "//div[@id='control_FF105D49-9099-4128-9CBC-E75E784EE816']//ul";
    }

    public static String SectionB_Tab()
    {
        return "//div[text()='Section B - JRA / PTO Task Information']";
    }

    public static String TaskInformationAdd()
    {
        return "//div[@id='control_9B0A538C-E33D-41C0-A45B-141022E8817D']//div[@id='btnAddNew']";
    }

    //TaskActivityStepDescription,,Order
    public static String Order()
    {
        return "//div[@id='control_CA05A1B3-8881-42B0-A9FE-5B7916344219']//input";
    }

    public static String TaskActivityStepDescription()
    {
        return "//div[@id='control_C26A96D7-B59D-4DC8-88B0-F23C14D911C4']//textarea";
    }

    public static String Button_Save2()
    {
        return "//div[@id='btnSave_form_827C0B31-EAAF-47A0-A63F-E20932247E3E']";
    }

    public static String TaskInformation_processflow()
    {
        return "//div[@id='btnProcessFlow_form_827C0B31-EAAF-47A0-A63F-E20932247E3E']";
    }

    public static String HazardAdd()
    {
        return "//div[@id='control_C8D5D798-BD5A-469A-9A79-1146D9048E64']//div[@id='btnAddNew']";
    }

    public static String HazardclassificationDropDown()
    {
        return "//div[@id='control_6EA1618F-7BFF-4DFC-BEFE-C12B9464AD65']//ul";
    }

    public static String Hazard()
    {
        return "//div[@id='control_C634DE9F-2A57-4240-9B65-DB4EC277E42D']//textarea";
    }

    public static String Hazard_processflow()
    {
        return "//div[@id='btnProcessFlow_form_C867FFD2-3DD3-4EC9-AFCA-EA60A92CFBC0']";
    }

    public static String Hazard_Save()
    {
        return "//div[@id='btnSave_form_C867FFD2-3DD3-4EC9-AFCA-EA60A92CFBC0']";
    }

    public static String UnwantedEventsAdd()
    {
        return "//div[@id='control_EE91AAC3-552B-41C5-B35D-EACF6E858BAC']//div[@id='btnAddNew']";
    }

    public static String UnwantedEvents_Save()
    {
        return "//div[@id='btnSave_form_7BAB7F70-3D5B-4EF3-9313-3D38641CB0B0']";
    }

    public static String UnwantedEventsText()
    {
        return "//div[@id='control_C7832CD1-2111-4C53-83CA-7F37656E30A9']//textarea";
    }

    public static String UnwantedEvents_processflow()
    {
        return "//div[@id='btnProcessFlow_form_7BAB7F70-3D5B-4EF3-9313-3D38641CB0B0']";
    }

    public static String ControlsAdd()
    {
        return "//div[@id='control_912D730B-1B30-4780-B6A3-F4D555467A1F']//div[@id='btnAddNew']";
    }

    public static String Controls_processflow()
    {
        return "//div[@id='btnProcessFlow_form_369E2CA8-0B19-4C6F-9C37-F95EB3514685']";
    }

    public static String ControlSourceDropDown()
    {
        return "//div[@id='control_B1C236AB-C11C-44A3-9917-0C846C28D38D']//ul";
    }

    public static String Control_Layer_3()
    {
        return "(//div[@id='control_1B054821-A523-4DF4-98CE-206D4BB60B46']//input)[1]";
    }

    public static String Bowtie_Control_Layer_DropDown()
    {
        return "//div[@id='control_4209F41B-8CE6-40C1-A0F8-D8F5DA045F33']//ul";
    }

    public static String Select_from_layer_3_DropDown()
    {
        return "//div[@id='control_B94629FA-67A5-4E24-9E5E-D44F75F86CE0']//ul";
    }

    public static String Controls_Save()
    {
        return "//div[@id='btnSave_form_369E2CA8-0B19-4C6F-9C37-F95EB3514685']";
    }

    public static String ProcedureSpecified_DropDown()
    {
        return "//div[@id='control_D77F35CE-E1B3-4620-AB60-3DB9950AA7AE']//ul";
    }

    public static String Actions_Tab()
    {
        return "//div[text()='Actions']";
    }

    public static String ActionsAdd()
    {
        return "//div[@id='control_60D20F13-9F33-41C6-AF3A-F1F233813825']//div[@id='btnAddNew']";
    }

    public static String Actions_processflow()
    {
        return "//div[@id='btnProcessFlow_form_743EA779-D1BA-4E0B-8A3B-057CE3D673D3']";
    }

    public static String TypeOfActionDropDown()
    {
        return "//div[@id='control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE']//ul";
    }

    public static String ActionDescription()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String ActionsEntityDropDown()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String Action_due_date()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String ResponsiblePersonDropDown()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }

    public static String Actions_Save()
    {
        return "//div[@id='btnSave_form_743EA779-D1BA-4E0B-8A3B-057CE3D673D3']";
    }

    public static String ChangeLogAdd()
    {
        return "//div[text()='Change Log']/..//div[@id='btnAddNew']";
    }
     public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String ChangeLog_processflow()
    {
        return "//div[@id='btnProcessFlow_form_807CE9BB-2CF2-4856-9C2C-EBE516FBEA4E']";
    }

    public static String DateOfChange()
    {
        return "//div[@id='control_2F345D95-2937-4E24-9266-A0E63D87FF43']//input";
    }

    public static String ReasonsForReviewIncludeDropDown()
    {
        return "//div[@id='control_94BF8E2B-025E-4533-BFE0-EC7E765A6FF9']//ul";
    }

    public static String PersonResponsibleForSignOffDropDown()
    {
        return "//div[@id='control_654B1273-9E8C-4E6E-834D-2B589E3311E0']//ul";
    }

    public static String PersonResponsibleForSignOffDropDownOption(String data)
    {
        return "//a[contains(text(),'" + data + "')]/..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String Change_Log_Save()
    {
        return "//div[@id='btnSave_form_807CE9BB-2CF2-4856-9C2C-EBE516FBEA4E']";
    }

    public static String LinkToAuditDropDown()
    {
        return "//div[@id='control_A3A2D058-C269-4274-8808-EF35ABD57BD9']//ul";
    }

    public static String LinkToEventDropDown()
    {
        return "//div[@id='control_8D1B7234-2CFC-43E7-B1BE-E770A1D237DD']//ul";
    }

    public static String LinkToStakeholderEngagementDropDown()
    {
        return "//div[@id='control_D3DD4627-9AF6-4275-BAA3-49A9E1FAAC91']//ul";
    }

    public static String FullNameDropDown()
    {
        return "//div[@id='control_E0535A21-FBC8-4F90-B767-B9C02D121842']//ul";
    }

    public static String Assessment_Team_Save()
    {
        return "//div[@id='btnSave_form_807CE9BB-2CF2-4856-9C2C-EBE516FBEA4E']";
    }

    public static String Supporting_Documents_Tab()
    {
        return "//span[text()='Supporting Documents']";
    }

    public static String TaskInformation_Save()
    {
        return "//div[@id='btnSave_form_827C0B31-EAAF-47A0-A63F-E20932247E3E']";
    }

    public static String HazardClassificationDropDown()
    {
        return "//div[@id='control_6EA1618F-7BFF-4DFC-BEFE-C12B9464AD65']//ul";
    }

    public static String SectionC_Tab()
    {
        return "//div[text()='Section C - Work Execution Specification']";
    }

    public static String WorkExecutionTasksFirstRecord()
    {
        return "(//div[@id='control_2852FBF3-86AF-419B-B999-F73B2C56684A']//table//tbody[@role='rowgroup']//tr)[1]";
    }

    public static String WorkExecutionTasks_processflow()
    {
        return "//div[@id='btnProcessFlow_form_7B661A42-60C5-4558-B396-9A9222077FC4']";
    }

    public static String ProcedureBeingObservedDropDown()
    {
        return "//div[@id='control_6B6B5909-B741-4C1C-995F-5A9C4941C504']";
    }

    public static String NameOfProcedure()
    {
        return "(//div[@id='control_654AE183-5CF0-4C90-8E5C-C16D0E82CE2A']//input)[1]";
    }

    public static String DateOfProcedure()
    {
        return "(//div[@id='control_A34D9284-C141-4098-836B-27AA53FD3DFB']//input)[1]";
    }

    public static String PhysicallyFitToConductTheWork()
    {
        return "(//div[@id='control_4D3A5171-58A9-4B56-8893-B18178EB3861']//textarea)[1]";
    }

    public static String DescribeAnyContextual()
    {
        return "(//div[@id='control_3197C17A-CC40-4C22-A8D9-9BAB36581F51']//textarea)[1]";
    }

    public static String EquipmentToolsRequired()
    {
        return "(//div[@id='control_96F2C6C7-6F4A-48F0-9271-382E9C2C7161']//textarea)[1]";
    }

    public static String AreThePersonPhysicallyDropDown()
    {
        return "//div[@id='control_13FCB6E7-FA76-4C4E-B2E4-9E9EBCB0B706']//ul";
    }

    public static String Option_3(String data)
    {
        return "//ul[@class='select3-results jstree jstree-11 jstree-default jstree-loading']//a[contains(text(),'"+data+"')]";
    }

    public static String Option_2(String data)
    {
        return "//ul[@class='select3-results jstree jstree-13 jstree-default jstree-loading']//a[text()='" + data + "']";
    }

    public static String Option_1(String data)
    {
        return "//ul[@class='select3-results jstree jstree-12 jstree-default jstree-loading']//a[text()='" + data + "']";
    }

    public static String EquipmentToolsRequiredDropDown()
    {
        return "//div[@id='control_DC912698-8C10-4193-8180-8BDFBE152B88']";
    }

    public static String WorkExecutionTasks_Save()
    {
       return "//div[@id='btnSave_form_7B661A42-60C5-4558-B396-9A9222077FC4']";
    }

    public static String WorkExecutionSpecificationAdd()
    {
        return"//div[@id='control_ADC684BF-9AE9-43D2-A5CF-F1C8D079D9DA']//div[@id='btnAddNew']";
    }

    public static String WorkExecutionSpecification_processflow()
    {
        return"//div[@id='btnProcessFlow_form_98D3A3A0-41A1-4A95-A409-47E9C53F44C6']";
    }

    public static String Order2()
    {
        return"//div[@id='control_2352EB55-6C8A-4429-940D-8DD259716BFB']//input";
    }

    public static String Question()
    {
        return"//div[@id='control_A3C2F0F7-BB57-459E-8130-4DA1E8D1BAD5']//textarea";
    }

    public static String WorkExecutionSpecification_Save()
    {
        return"//div[@id='btnSave_form_98D3A3A0-41A1-4A95-A409-47E9C53F44C6']";
    }

    public static String CompletedDropDown()
    {
        return"//div[@id='control_434D611E-A016-4089-9481-276A22F57435']//ul";
    }

    public static String ActionsTab()
    {
       return "//div[text()='Actions']";
    }

    public static String Actions_Add()
    {
       return "//div[@id='control_60D20F13-9F33-41C6-AF3A-F1F233813825']//div[@id='btnAddNew']";
    }

    public static String JobRiskAssessmentTeamAdd()
    {
        return"//div[@id='control_97D1292B-D58E-45AA-9DBE-97593E367493']//div[@id='btnAddNew']";
    }

    public static String TopEventsDropDown()
    {
        return"//div[@id='control_85F4A08C-3270-492B-BA96-5172DEFFDFA1']//ul";
    }
    

}
