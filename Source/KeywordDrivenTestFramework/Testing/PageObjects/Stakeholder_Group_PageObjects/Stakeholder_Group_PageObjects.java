/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Group_PageObjects;

/**
 *
 * @author smabe
 */
public class Stakeholder_Group_PageObjects
{

    public static String Record_Number;
    public static String Window_1;

    public static String getWindow()
    {
        return Window_1;
    }

    public static void setWindow(String Window)
    {
        Window_1 = Window;
    }

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

        public static String businessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }
    
    
     public static String GeographiclocationDropdown()
    {
        return "//div[@id='control_F703A144-D0B6-4D4D-B5E2-D4E186427A43']//ul";
    }

    public static String Engagements_Tab()
    {
        return "(//div[contains(text(),'Engagements')])[1]";
    }

    public static String ColapsArrow()
    {
        return"//div[@class='select3 select3-ddl select3_1b24a4f1 c-ddl select3-drop-above']//b[@class='select3-down drop_click']";
    }

    public static String ColapsArrow1()
    {
       return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//b[@class='select3-down drop_click']";
    }

    public static String ContactInquiryTopicDropDown()
    {
        return"//div[@id='control_2D1B5E8D-BBF2-448A-9765-F03FA8C31019']//ul";
    }
     public static String FunctionOfEngagementDropDown()
    {
        return "//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']//ul";
    }
     public static String EngagementDescription()
    {
        return "//div[@id='control_1C19AE65-23A1-4ADC-A631-D9273FC0CE9F']//textarea";
    }

    public static String ResponsiblePersonDropDown2()
    {
        return "//div[@id='control_213251A2-010A-4BBF-A65A-A1FC8C6F7033']//ul";
    }

    public static String MethodOfEngagementDropDown()
    {
        return "//div[@id='control_4A471537-8229-4E54-A86C-DCEB99BA24D0']//ul";
    }

    public static String FunctionColapsArrow()
    {
       return "//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']//b[@class='select3-down drop_click']";
    }
      public static String EngagementsSaveButton()
    {
        return "//div[@id='btnSave_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String uploadLinkbox()
    {
        return "//div[@class='linkbox-options']//b[@class='linkbox-link']";
    }

    public static String businessUnitexpand(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String ContainsTextBox()
    {
        return "//input[@class='txt border']";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String checkBoxTick1()
    {
        return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']/div/a/span[2]/b";
    }

    public static String si_save()
    {
        return "//div[@id='btnSave_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']";
    }

    public static String checkBoxTick2()
    {

        return "//div[@id='control_4CFB2165-708B-4D55-9988-9CDCB5487291']/div/a/span[2]/b";
    }

    public static String applicableBusinessUnitDD()
    {
        return "//*[@id=\"control_4CFB2165-708B-4D55-9988-9CDCB5487291\"]/div[1]/a/span[2]/b[1]";
    }

    public static String checkBoxTick()
    {
        return "//div[@id='control_4BC61A3A-EC52-4BEA-807E-B70C75D5B421']/div/a/span[2]/b";
    }
    
     public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }
     
      public static String EngagementDate()
    {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }
      public static String EngagementTitle()
    {
        return "//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input";
    }
     
      public static String EngagementsprocessFlow()
    {
        return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }
    
     public static String BusinessUnitDropdown()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//ul";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String si_stakeholdercatDD()
    {
        return "//*[@id=\"control_F1357856-7A84-4716-9B1D-077C87CC8591\"]/div[1]/a/span[2]/b[1]";
    }

    public static String accountableOwner()
    {
        return "//div[@id='control_2A0EA9D1-04B4-4092-AD71-0CA6ED2D9C4B']/div/a/span[2]/b";
    }

    public static String StakeholderGroupTab()
    {
        return "//label[text()='Stakeholder Group']";
    }

    public static String AddButton()
    {
        return "//div[contains(@class,'active form transition visible')]//div[text()='Add']";
    }
      public static String ResponsiblePerson_dropdown()
    {
        return"//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }
      
      
       public static String ActionDueDate()
    {
       return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
         public static String TypeOfActionDropDown()
    {
       return "//div[@id='control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE']//ul";
    }
      
        public static String Entity_Dropdown()
    {
       return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String ActionDescription()
    {
       return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }

    public static String GroupName()
    {
        return "(//div[@id='control_6B36E56B-4BD2-4A16-AD58-94FE1883EFE2']//input)[1]";
    }

    public static String StakeholdCategoriDropDown()
    {
        return "//div[@id='control_BDB3E74D-818E-4A51-8443-3F30BA7A472A']//ul";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String ResponsibleOwnersDropDown()
    {
        return "//div[@id='control_A01F1D1A-45BF-4A6B-B2C2-88046BDAFDA1']//ul";
    }

    public static String AccountableOwnerDropDown()
    {
        return "//div[@id='control_A1C07241-404B-4E18-90D1-E7F20AB4E625']//ul";
    }

    public static String IndustryOwnerDropDown()
    {
        return "//div[@id='control_425BD26A-5DDB-485A-BA7F-E8D2E51C4BEA']//ul";
    }

    public static String GroupDescription()
    {
        return "//div[@id='control_4AEFCBE1-7C06-4528-BB6B-CFD298C47AA1']//textarea";
    }

    public static String ApplicableBusinessUnitsDropDown()
    {
        return "//div[@id='control_931D1181-0EA6-4EBF-BAA9-B497F5793EC0']//ul";
    }

    public static String recordSaved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordSaved_popup1()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordNoChanges_Saved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record has no changes to save')])[1]";
    }

    public static String SaveButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Save']";
    }

    public static String SaveButton2()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[1]";
    }

    public static String RiskAssSave()
    {
        return "//div[@id='btnSave_form_7F5090E0-7D49-4B93-962C-FA8DF119B3D6']";
    }

    ////div[@id='btnSave_form_7F5090E0-7D49-4B93-962C-FA8DF119B3D6']
    public static String SaveButton3()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[2]";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String DeleteButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Delete']";
    }

    public static String iframeName()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String linkToADocument()
    {
        return "//div[@id='control_C440202B-AFE6-4EB9-B180-57BBF25C57D6']//b[@original-title='Link to a document']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String uploadDocument()
    {
        return "//div[@id='control_C440202B-AFE6-4EB9-B180-57BBF25C57D6']//b[@original-title='Upload a document']";
    }

    public static String SupportingDocumentsTab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String PhoneNumber()
    {
        return "(//div[@id='control_F58CA39A-8E24-4A56-987E-5B84412F0A9C']//input)[1]";
    }

    public static String EmailAddress()
    {
        return "(//div[@id='control_E95B004E-1283-4AD5-85C0-2AA11196F34D']//input)[1]";
    }

    public static String StreetAddress()
    {
        return "(//div[@id='control_C30F3943-2351-490A-A865-7D2337A1F7C5']//textarea)";
    }

    public static String ZipPostalCode()
    {
        return "(//div[@id='control_99DCFFAC-34D1-4291-9154-5B3B7DC59D1B']//input)[1]";
    }

    public static String LocationDropDown()
    {
        return "//div[@id='control_B64B2E96-473B-4207-8FCE-C7DAB53F8834']//ul";
    }

    public static String StakeholderDetailsTab()
    {
        return "//div[text()='Stakeholder Details']";
    }

    public static String CorrespondenceAddressDropDown()
    {
        return "//span[text()='Correspondence Address']/..//i";
    }

    public static String CorrespondenceAddress()
    {
        return "//div[@id='control_CFC6CEFA-5974-4A77-956A-315292071111']//textarea";
    }

    public static String ZipPostalCode2()
    {
        return "//div[@id='control_9D91E3DA-6FA0-4E43-B74C-6806CF76EFD7']//input";
    }

    public static String StakeholderAnalysisComments()
    {
        return "//div[@id='control_31C00096-86E4-448A-8925-DEC2C7CD3995']//textarea";
    }

    public static String StakeholderSupportDD()
    {
        return "//div[@id='control_6307BAB8-A857-4489-8F4E-B90B68B812B2']//ul";
    }

    public static String InfluenceDropdown()
    {
        return "//div[@id='control_F72B39E0-9AEB-436C-8732-F06C20862FF0']//ul";
    }

    public static String InfluenceDropdown2()
    {
        return "//div[@id='control_46284CB6-8E0D-4AF6-BD22-0AF4AA6E7759']//ul";
    }

    public static String InterestDropdown()
    {
        return "//div[@id='control_4F51BB5C-34D0-42B8-9324-66AC1198CF45']//ul";
    }

    public static String InterestDropdown2()
    {
        return "//div[@id='control_63C66781-07ED-4EB2-8A2F-CAED92ED118C']//ul";
    }

    public static String StakeholerAnalysis_Tab()
    {
        return "//div[text()='Stakeholder Analysis']";
    }

    public static String GuidelinesDropDown()
    {
        return "//span[text()='Guidelines']/..//i";
    }

    public static String Members_Tab()
    {
        return "//div[text()='Members']";
    }

    public static String TopicIssueAssessmentDropdown()
    {
        return "//span[text()='Topic/Issue Assessment']/..//i";
    }

    public static String si_title(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String si_relationshipOwner_dropdown()
    {
        return "//div[@id='control_4BC61A3A-EC52-4BEA-807E-B70C75D5B421']//li";
    }

    public static String TopicIssueAssessmentAdd()
    {
        return "//div[@id='control_791664E9-F4D5-4CBB-9BAA-B187310C3D6F']//div[@id='btnAddNew']";
    }

    public static String si_knownas()
    {
        return "//div[@id='control_BED0557B-BBC2-46C1-B571-BE60A267F0EA']/div/div/input";
    }

    public static String si_lname()
    {
        return "//div[@id='control_A9D1A3E8-C561-452A-A1F4-7BCB496B365F']/div/div/input";
    }

    public static String si_title_dropdown()
    {
        return "//div[@id='control_28C03054-D663-431B-9F65-38BE54617019']//li";
    }

    public static String si_fname()
    {
        return "//div[@id='control_E186B9E5-4102-409D-8F57-7355938C09D9']/div/div/input";
    }

    public static String si_processflow()
    {
        return "//div[@id='btnProcessFlow_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']";
    }

    public static String Topic_IssueAssessmentDropdown()
    {
        return "//div[@id='control_DECCD753-8245-48AE-9160-4E5F237C119A']//ul";
    }

    public static String MembersAdd()
    {
        return "//div[@id='control_7016173C-BF8B-419B-8C46-D81EAA9AEC45']//div[@id='btnAddNew']";
    }

    public static String IndividualNameDropDown()
    {
        return "//div[@id='control_623A6228-2A56-4954-91D6-D2E07B56E612']//ul";
    }

    public static String CreateaNewIndividual()
    {
        return "//div[text()='Create a new individual']";
    }

    public static String AssociatedStakeholderGroups_Tab()
    {
        return "//div[text()='Associated Stakeholder Groups']";
    }

    public static String AssociatedStakeholderGroupsAdd()
    {
        return "//div[@id='control_90C85E93-A691-4C58-982F-39EEE9A0BC53']//div[@id='btnAddNew']";
    }

    public static String GroupNameDropDown()
    {
        return "//div[@id='control_F2EBECAD-5135-4844-B579-0A02F8CE3738']//ul";
    }

    public static String RelationshipDropDown()
    {
        return "//div[@id='control_D50B12E5-3EBB-4095-AA89-FFC23B7AC247']//ul";
    }

    public static String ContractororSupplierManager_Tab()
    {
        return "//div[text()='Contractor or Supplier Manager']";
    }

    public static String StatusDropDown()
    {
        return "//div[@id='control_AEDC76EE-6EA0-48C6-91D0-A2CE27CEF60D']//ul";
    }

    public static String MainContactIndividualDropDown()
    {
        return "//div[@id='control_1FE052A1-221D-426B-B9E1-46569A2BA8EF']//ul";
    }

    public static String MainContact()
    {
        return "//div[@id='control_DB611D04-5A6A-485B-B7FD-3E7468577C4D']//input";
    }

    public static String MainContactPhone()
    {
        return "//div[@id='control_1588E4E5-AAB4-4254-8D7D-0ED15A0CC591']//input";
    }

    public static String MainContactEmail()
    {
        return "(//div[@id='control_D7FD3FAD-217D-47F0-B28F-C2B1AAFF57B3']//input)[1]";
    }

    public static String QuestionnaireDropDown()
    {
        return "//span[text()='Questionnaire']/..//i";
    }

    public static String QuestionnaireAddButton()
    {
        return "//div[@id='control_7517949D-9F94-4E11-8C20-9CE0EF740490']//div[@id='btnAddNew']";
    }

    public static String ProcessFlowQ()
    {
        return "//div[@id='btnProcessFlow_form_7B9DCBC8-678E-4ED9-8CF5-C5AA78BED2D8']";
    }

    public static String RelevantStakeholder()
    {
        return "(//div[@id='control_D6E2582F-135F-475C-8180-054CA159CFCC']//input)[1]";
    }

    public static String DocumentsDropDown()
    {
        return "//span[text()='Documents']/..//i";
    }

    public static String DocumentsAddButton()
    {
        return "//div[@id='control_1D721996-A61A-40FD-91E8-7FAFDF3B5039']//div[@id='btnAddNew']";
    }

    public static String DocumentUploaded()
    {
        return "//div[@id='control_F3FF8316-9847-4F33-ACCE-CEDA5D02BE01']//ul";
    }

    public static String Outcome()
    {
        return "//div[@id='control_6BF2BC67-11B6-45D8-9A90-E7A8FB55DDAE']//ul";
    }

    public static String DateVerified()
    {
        return "//div[@id='control_1FD49B28-3FF0-4931-9C73-2E3D2D7AA68E']//input";
    }

    public static String OrdersDropDown()
    {
        return "//span[text()='Orders']/..//i";
    }

    public static String OrdersAddButton()
    {
        return "//div[@id='control_D0274203-59E0-4BD0-8D74-E760964E515E']//div[@id='btnAddNew']";
    }

    public static String ProcessFlow1()
    {
        return "//div[@id='btnProcessFlow_form_AC3617FE-8383-45BB-8611-EA8CA705B2C1']";
    }

    public static String OrderStatusDropDown()
    {
        return "//div[@id='control_F50C9133-D3BE-45C0-88F6-4FCCCE4BEE4D']//ul";
    }

    public static String ApprovedByDropDown()
    {
        return "//div[@id='control_060F866E-28F3-4F44-817C-76780546B4ED']//ul";
    }

    public static String DateApproved()
    {
        return "//div[@id='control_010013A1-86AE-4EB6-AC9F-847A74A5055F']//input";
    }

    public static String ScopeOfWork_Tab()
    {
        return "//div[text()='Scope of Work']";
    }

    public static String StartDate()
    {
        return "//div[@id='control_90D93B64-68B5-4748-897A-498F7C8DE4BA']//input";
    }

    public static String EndDate()
    {
        return "//div[@id='control_3A59A41B-41D0-498E-BD56-1D56B52C07B9']//input";
    }

    public static String Project()
    {
        return "//div[@id='control_202D2497-A69E-4769-8980-4367637F0BB8']//input";
    }

    public static String AreaWhereWorkWillBeConductedDropDown()
    {
        return "//div[@id='control_58BBDC9C-54FC-4770-9212-5E9BB7957725']//ul";
    }

    public static String ContractorRiskAssessmentTab()
    {
        return "//div[text()='Risk Assessment']";
    }

    public static String ContractorRiskAssessmentAddButton()
    {
        return "//div[@id='control_2466C37B-B25C-4DF6-B32D-6F623F2AC781']//div[@id='btnAddNew']";
    }

    public static String ContractorRiskAssessmentProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_7F5090E0-7D49-4B93-962C-FA8DF119B3D6']";
    }

    public static String CapturerDropDown()
    {
        return "//div[@id='control_8EAF69D1-B750-4545-BD49-99E898FA1971']//ul";
    }

    public static String RiskRankingDate()
    {
        return "//div[@id='control_90DA037B-D4F2-4862-BF60-34485B5C0DB6']//input";
    }

    public static String DescriptionOfScope()
    {
        return "//div[@id='control_85DB80BB-F2BD-41B8-901C-338EBCF53047']//textarea";
    }

    public static String BlastingActivitiesIncludingExplosivesDropDown()
    {
        return "//div[@id='control_E81E2C37-3D9A-46A5-932D-30B82C0F97D5']//ul";
    }

    public static String CommunityImpactDropDown()
    {
        return "//div[@id='control_0D4428C1-5B20-4EF6-B7C1-E139F54B03F1']//ul";
    }

    public static String RelatedDocumentsTab()
    {
        return "(//div[text()='Related Documents'])[1]";
    }

    public static String EmployeesTab()
    {
        return "//div[text()='Employees']";
    }

    public static String AddContractorButton()
    {
        return "//div[@id='control_07AB87B2-5D6A-42C2-8216-C78C28FD54DE']";
    }

    public static String FleetManagementTab()
    {
        return "//div[text()='Fleet Management']";
    }

    public static String AddFleetManagement()
    {
        return "//div[@id='control_5ECBE260-6D53-4E0C-A206-BAAA150BAC85']//div[@id='btnAddNew']";
    }

    public static String fleet_processflow()
    {
        return "//div[@id='btnProcessFlow_form_1F7D4C38-EE03-4F83-815A-94FD64BFC9C4']";
    }

    public static String VehicleType()
    {
        return "//div[@id='control_C662CD0F-B707-4A9B-AB16-476005E74C1F']//input";
    }

    public static String RegNo()
    {
        return "(//div[@id='control_25C0418F-B6DF-4845-B713-E1CD10250FC6']//input)[1]";
    }

    public static String LicenseExpiryDate()
    {
        return "(//div[@id='control_9826F1CB-B2D2-4460-84BD-E9F986CD8173']//input)[1]";
    }

    public static String fleet_save()
    {
        return "//div[@id='btnSave_form_1F7D4C38-EE03-4F83-815A-94FD64BFC9C4']";
    }

    public static String AddToolsEquipmentTab()
    {
        return "//div[text()='Tools & Equipment']";
    }

    public static String AddToolsEquipment()
    {
        return "//div[@id='control_3849FD5B-7DEC-44B2-BA0F-6774192E3E51']//div[@id='btnAddNew']";
    }

    public static String Tools_processflow()
    {
        return "//div[@id='btnProcessFlow_form_4A401000-E42A-4AFD-B9C8-8A1A21187C73']";
    }

    public static String DescriptionOfEquipment()
    {
        return "(//div[@id='control_5F36A2D2-2304-48BB-BB1A-574EFBF50B10']//input)[1]";
    }

    public static String Tools_save()
    {
        return "//div[@id='btnSave_form_4A401000-E42A-4AFD-B9C8-8A1A21187C73']";
    }

    public static String ChemicalRegisterTab()
    {
        return "//div[text()='Chemical Register']";
    }

    public static String ChemicalRegisterRecord()
    {
        return "(//div[@id='control_F0A5A555-5D4A-4594-9F61-831090EBCB2A']//table//tbody//tr)[6]";
    }

    public static String Hazardous_processflow()
    {
        return "//div[@id='btnProcessFlow_form_67D12C4F-9715-42CB-8583-738A005007A8']";
    }

    public static String Hazardous_save()
    {
        return "//div[@id='btnSave_form_67D12C4F-9715-42CB-8583-738A005007A8']";
    }

    public static String PermissionToWorkTab()
    {
        return "//div[text()='Permission to Work']";
    }

    public static String PermissionToWorkAdd()
    {
        return "//div[@id='control_E46D1FE3-3FAA-4DB0-A3E1-06CE9A9CEFC8']//div[@id='btnAddNew']";
    }

    public static String PermissionToWork_processflow()
    {
        return "//div[@id='btnProcessFlow_form_446929D1-893C-47E0-BCB9-30C0C1D7A70B']//span";
    }

    public static String Permission_save()
    {
        return "//div[@id='btnSave_form_446929D1-893C-47E0-BCB9-30C0C1D7A70B']";
    }

    public static String PermissionComments()
    {
        return "//div[@id='control_E9196EB7-D01F-479C-9C61-9C60F7ACD48D']//textarea";
    }

    public static String ResponsiblePersonToSignOffDD()
    {
        return "//div[@id='control_6AFD8447-DD45-4E0B-9885-558782D0DB69']//ul";
    }

    public static String RoleDropDown()
    {
        return "//div[@id='control_928D5A28-956F-4C01-B43A-EB08F0157406']//ul";
    }

    public static String SignOffDD()
    {
        return "//div[@id='control_DAE7D09B-B8AE-41D8-B834-F2E347322FD9']//ul";
    }

    public static String SignOffDate()
    {
        return "//div[@id='control_FA67DC5D-68BF-4A12-86E2-0CC7B93C9F41']//input";
    }

    public static String VulnerabilityTab()
    {
        return "//div[text()='Vulnerability']";
    }

    public static String CloseDropDown()
    {
        return "//div[@id='control_BDB3E74D-818E-4A51-8443-3F30BA7A472A']//b[@class='select3-down drop_click']";
    }

    public static String AddAnEngagement()
    {
        return"//div[text()='Add an engagement']";
    }
      public static String NextButton()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String ContractorOrSupplierManager_Tab()
    {
       return "//div[text()='Contractor or Supplier Manager']";
   }

    public static String AddViewGrievances_Tab()
    {
       return "(//div[text()='Related Grievances'])[1]";
    }

    public static String RecordResults()
    {
       return "//div[@id='control_AE8EC219-4756-40BA-8773-68B10FE425BA']//div[text()='No results returned']";
    }

    public static String Actions_Tab()
    {
       return "//div[text()='Actions']";
    }

    public static String ActionsAddButton()
    {
       return "//div[text()='Stakeholder Group Actions']/..//div[@id='btnAddNew']";
    }

    public static String Actions_save()
    {
       return "//div[@id='btnSave_form_F72A20BD-FF84-43A1-8729-31F4F84F23C5']";
    }

    public static String ActionsProcessFlow()
    {
        return"//div[@id='btnProcessFlow_form_F72A20BD-FF84-43A1-8729-31F4F84F23C5']";
    }

    public static String StakeholderPositionDropDown()
    {
       return "//div[@id='control_65D64BB9-F6EC-4C8C-9E9D-C3E22C4B36FA']//ul";
    }

    public static String MemberSaveButton()
    {
        return"//div[@id='btnSave_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }

}
