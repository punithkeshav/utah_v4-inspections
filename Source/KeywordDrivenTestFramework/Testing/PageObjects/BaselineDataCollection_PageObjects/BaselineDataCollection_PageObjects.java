/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.BaselineDataCollection_PageObjects;

/**
 *
 * @author smabe
 */
public class BaselineDataCollection_PageObjects
{
    
     public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String ContainsTextBox()
    {
        return "(//input[@class='txt border'])[1]";
    }
 public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }
  
  public static String SupportingDocumentsTab()
    {
        return "(//div[text()='Supporting Documents'])[1]";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }
    
     public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

      public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }
   
    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String recordSaved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordSaved_popup1()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordNoChanges_Saved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record has no changes to save')])[1]";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String DeleteButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Delete']";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String uploadLinkbox()
    {
        return "//div[@class='linkbox-options']//b[@class='linkbox-link']";
    }

    public static String businessUnitexpand(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }
    
     public static String CloseCurrentModule3()
    {
        return "//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }
    
     public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }
    
       public static String SearchOption()
    {
        return"//span[@class='advanced-options icon more_horiz transition visible']";
    }
    
     public static String DataCollectionStartDate()
    {
        return"//div[@id='control_2954B417-114C-4033-9227-162DA49A13D8']//input";
    }
     
         public static String BaselineDataCollection_Add()
    {
       return "//div[@class='active form transition visible']//div[@id='btnActAddNew']";
    }

    public static String BaselineDataCollection_processflow()
    {
       return "//div[@id='btnProcessFlow_form_9D8FF3BF-C0FD-423E-A196-8C74C8A84CF6']";
    }

    
     public static String BaselineDataCollectionTab()
    {
        return"//label[contains(text(),'Baseline Data Collection')] ";
    }

    public static String Site_Dropdown()
    {
      return "//div[@id='control_B26D3884-A0B9-4030-A4F4-3EAE76CDC15C']//ul";
    }

    public static String DataCollectionEndDate()
    {
        return"//div[@id='control_8AF8FC7B-05E5-4160-8EBE-DE6B651D2795']//input";
    }

    public static String OwnerDropDown()
    {
        return"//div[@id='control_A103012C-EC58-4CC4-9313-68107194A3BF']//ul";
    }

    public static String BaselineDataCollection_save()
    {
       return "//div[@id='btnSave_form_9D8FF3BF-C0FD-423E-A196-8C74C8A84CF6']";
    }

    public static String ApplicableSurveyToBeUsedDropDown()
    {
       return "//div[@id='control_F89EC2D1-2879-4379-AEF3-68BDEF474038']//ul";
    }

    public static String LinkedStakeholderGroupsCommunitiesDropDown()
    {
        return"//div[@id='control_9C64D6E7-E150-43BA-9C57-B06F581F4BEA']//ul";
    }

    public static String TeamDropDown()
    {
        return"//div[@id='control_70488276-E0F2-4D0B-89C5-1E1478A8F1EA']//ul";
    }

    public static String StatusDropDown()
    {
       return "//div[@id='control_38D3D931-EBDD-4732-A573-E5F13D55D1F5']//ul";
    }

    public static String Title()
    {
        return"(//div[@id='control_81FA8B5D-6B8F-4F6A-8B82-2A6831B40179']//input)[1]";
    }

    public static String Actions_Add()
    {
        return"//div[@id='control_B047BC07-3B9E-4577-A34F-E313BB83CFF6']//div[@id='btnAddNew']";
    }

    public static String ActionsTab()
    {
       return "//div[text()='Actions']";
    }
    
    
     public static String Entity_Dropdown()
    {
       return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String ActionDescription()
    {
       return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String ResponsiblePerson_dropdown()
    {
        return"//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }

    public static String TypeOfActionDropDown()
    {
       return "//div[@id='control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE']//ul";
    }

    public static String ActionDueDate()
    {
       return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String Actions_save()
    {
       return "//div[@id='btnSave_form_D64A5CCF-80FF-48BE-BA89-3C2BCCED1152']";
    }

    public static String Actions_processflow()
    {
       return "//div[@id='btnProcessFlow_form_D64A5CCF-80FF-48BE-BA89-3C2BCCED1152']";
    }

    public static String SurveyTab()
    {
       return "//div[text()='Survey']";
    }

    public static String Survey_Add()
    {
        return"//div[@id='control_52772A2D-30BB-4D97-9AD0-B292B55AB401']//div[@id='btnAddNew']";
    }

    public static String Survey_processflow()
    {
        return"//div[@id='btnProcessFlow_form_84689229-E012-4864-81DA-849A34367DC1']";
    }

    public static String Survey_save()
    {
       return "//div[@id='btnSave_form_84689229-E012-4864-81DA-849A34367DC1']";
    }

    public static String GeographicLocationDropDown()
    {
       return "//div[@id='control_669136F0-EBF1-412A-8B60-4EF0433E19DE']//ul";
    }

    public static String SurveyConducteByDropDown()
    {
       return "//div[@id='control_C8533C3C-6B41-4D9E-A7BA-D69CED135AC7']//ul";
    }

    public static String SurveyNameDropDown()
    {
       return "//div[@id='control_509E21D4-6B42-4154-8DDD-B99F421F45EE']//ul";
    }

    public static String ExcludeFromResults()
    {
       return "//div[@id='control_101740D6-A260-4B2D-9DB5-B4FEC3BAE230']//div[@class='c-chk']//div";
    }

    public static String ReasonForExcludingFromResults()
    {
        return"//div[@id='control_CA3B8F8B-CC41-4DE8-A7BC-7E3127CDBBA3']//textarea";
    }
    
     public static String StartButton()
    {
       return "(//div[text()='START'])[1]";
    }
     
      public static String StartButton_2()
    {
       return "(//div[text()='Start'])[1]";
    }
     
     public static String NextButton()
    {
       return "//div[text()='NEXT']";
    }
       public static String NextButton_2()
    {
       return "//div[text()='Next']";
    }

   public static String AnswerDropDown()
    {
       return "(//div[@name='control_DDL0'])[1]";
    }
   
   public static String AnswerDropDown2()
    {
       return "(//div[@name='control_DDL0'])[2]";
    }
    public static String FinishButton()
    {
       return "//div[text()='Finish']";
    }

    public static String SaveCurrentSectionButton()
    {
        return"//div[text()='Save current section']";
    }

    public static String ContinueButton()
    {
       return "//div[text()='CONTINUE']";
    }
    
    public static String ContinueButton_2()
    {
       return "//div[text()='Continue']";
    }

    public static String PreviousButton()
    {
        return"//div[text()='Previous']";
    }

  
}
