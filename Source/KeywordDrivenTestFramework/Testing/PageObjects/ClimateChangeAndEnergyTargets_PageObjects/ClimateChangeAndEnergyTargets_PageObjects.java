/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyTargets_PageObjects;

/**
 *
 * @author smabe
 */
public class ClimateChangeAndEnergyTargets_PageObjects
{
    
    public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String ContainsTextBox()
    {
        return "(//input[@class='txt border'])[1]";
    }

  

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String recordSaved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordSaved_popup1()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordNoChanges_Saved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record has no changes to save')])[1]";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String DeleteButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Delete']";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String uploadLinkbox()
    {
        return "//div[@class='linkbox-options']//b[@class='linkbox-link']";
    }

    public static String businessUnitexpand(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String RelationshipBasedSafetyTab()
    {
        return "//label[text()='Relationship Based Safety']";
    }

    public static String RelationshipBased_Add()
    {
        return "//div[contains(@class,'active form transition visible')]//div[@id='btnActAddNew']";
    }

    public static String BusinessUnit_Dropdown()
    {
       return "//div[@id='control_2E885063-50E0-44CA-91FA-B1E3FACA2370']//ul";
    }
    
      public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String ClimateChangeAndEnergyTargetsTab()
    {
       return "//label[text()='Climate Change and Energy Targets']";
    }

    public static String ECO2ManTab()
    {
       return "//label[text()='ECO2Man']";
    }

    public static String ClimateChangeAndEnergyTargets_Add()
    {
        return"//div[@class='active form transition visible']//div[@id='btnActAddNew']";
    }

    public static String Processflow()
    {
       return "//div[@id='btnProcessFlow_form_13D75BAF-64AD-4BAA-970F-F1F37E6AA401']";
    }

    public static String TargetStartDateMonthDropDown()
    {
        return"//div[@id='control_AEF932A8-1CB8-406A-89A3-6805268F03A5']//ul";
    }

    public static String TargetStartDateYearDropDown()
    {
       return "//div[@id='control_131E42B2-EC37-4FA1-B857-DF0782F05F97']//ul";
    }

    public static String TargetStartDateYearDropDown2()
    {
        return"//div[@id='control_D73BED43-1858-4244-9F5D-39FD697AB950']//ul";
    }

    public static String TargetStartDateMonthDropDown2()
    {
        return"//div[@id='control_799AEDE6-3D4C-4FD1-921D-FE725A55B335']//ul";
    }

    public static String Button_Save()
    {
       return "//div[@id='btnSave_form_13D75BAF-64AD-4BAA-970F-F1F37E6AA401']";
    }

    public static String deleteRecordButton()
    {
        return"//div[@id='btnDelete_form_13D75BAF-64AD-4BAA-970F-F1F37E6AA401']";
    }

    public static String MineralProductionTargets_Add()
    {
      return  "//div[@id='control_14C536D9-DFCB-4E41-BC3E-B56B0745AB7C']//div[@id='btnAddNew']";
    }

    public static String MineralType_Dropdown()
    {
       return "//div[@id='control_B1AC2FDC-5848-4A8D-9AF0-0F2C6487FE50']//ul";
    }

    public static String CO2e_MineralProduced()
    {
        return"(//div[@id='control_3225595B-0423-4ED3-B29C-FA25CDFF4D20']//input)[1]";
    }
    
}
