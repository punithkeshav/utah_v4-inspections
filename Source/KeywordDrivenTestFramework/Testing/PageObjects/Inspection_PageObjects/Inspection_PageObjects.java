/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Inspection_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Testing.TestMarshall;

/**
 *
 * @author Skhumalo
 */
public class Inspection_PageObjects extends BaseClass
{

    public static String IsometrixURL()
    {
        // Use ENUM
        return TestMarshall.currentEnvironment.isoMetrixEnvironment;
//        return "https://usazu-pr01.isometrix.net/IsoMetrix.UK.V4.Automation.1/default.aspx";
    }

    public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String isoSideMenu()
    {
        return "//i[@class='large sidebar link icon menu']";
    }

    public static String iframeName()
    {
        return "ifrMain";
    }

    public static String version()
    {
        return "//footer";
    }

    public static String homepage_SolutionBranchCode_Btn()
    {
//        return"//div[@id='events_321F20B4-3A9E-4120-8C5F-3967AD338B4A_form']//..//..//td[4]//div";
        return "(//div[text()='Search'])[1]";
    }

    public static String saveButton()
    {
        return "//div[@id='btnSave_form_E2BEE86E-F593-4213-A0D7-556C53A310AC']";
    }

    public static String sb_text()
    {
        return "//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//div[@class='align-left']";
    }

    public static String backtohomepage_Btn()
    {
        return "//div[@class='active form transition visible']//i[@class='back icon arrow-left']";
    }

    public static String userProjectXpath()
    {
        return "//span[@class='user']";
    }

    public static String navigate_EHS()
    {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }

    public static String Password()
    {
        return "//input[@id='txtPassword']";
    }

    public static String Username()
    {
        return "//input[@id='txtUsername']";
    }

    public static String LoginBtn()
    {
        return "//div[@id='btnLoginSubmit']";
    }

    public static String homepage_SolutionBranch_Btn()
    {
        return "//label[text()='Solution Branch']";
    }

    public static String reportedDateOfNonCompliance()
    {
        return "//*[@id=\"control_817B1336-983B-4C72-BAD5-304A992D37A1\"]/div[1]/span/span/input";
    }

    public static String typeOfNonComplianceDD()
    {
        return "//*[@id=\"control_C92CA60D-7027-4F48-B9B4-60ABFDA93A62\"]/div[1]/a/span[2]/b[1]";
    }

    public static String clearanceClassificationDD()
    {
        return "//*[@id=\"control_C6C29054-080D-4972-A794-811062B59AF2\"]/div[1]/a/span[2]/b[1]";
    }

    public static String stoppageInitiatedBynDD()
    {
        return "//*[@id=\"control_FDBF77F4-AEAF-4F79-B2E2-7E4DB11A1DE5\"]/div[1]/a/span[2]/b[1]";
    }

    public static String stopNoteClassificationDD()
    {
        return "//*[@id=\"control_0870BCBF-A0B7-40B2-A18F-D088477B890C\"]/div[1]/a/span[2]/b[1]";
    }

    public static String stoppageDueToDD()
    {
        return "//*[@id=\"control_F3E89F54-2B95-4D1F-9AB4-25A3A4B0BEBC\"]/div[1]/a/span[2]/b[1]";
    }

    public static String stoppageAffectedOtherAreasDD()
    {
        return "//*[@id=\"control_5E1C5AC3-208F-453B-B154-0F6CF3EECD25\"]/div[1]/a/span[2]/b[1]";
    }

    public static String stoppageOutcomeDD()
    {
        return "//*[@id=\"control_AB93D552-65AF-4AB1-AB2B-612D4C2F78DF\"]/div[1]/a/span[2]/b[1]";
    }

    public static String saveToContinue()
    {
        return "//*[@id=\"control_4DF8BD1F-EC15-455C-AB14-13170AB2449B\"]/div[1]/div";
    }

    public static String actionSave()
    {
        return "//*[@id=\"btnSave_form_03C1E47F-14A5-43B6-BA2F-C5F17A3A47E0\"]/div[3]";
    }
    
    
     public static String ActionSave()
    {
        return "//div[@id='btnSave_form_03C1E47F-14A5-43B6-BA2F-C5F17A3A47E0']";
    }

    public static String workStoppageDD()
    {
        return "//*[@id=\"control_89EDFE37-2C85-45CD-9C73-338374164EF0\"]/div[1]/a/span[2]/b[1]";
    }

    public static String dateOfNonCompliance()
    {
        return "//*[@id=\"control_E81D5D30-D0CF-4B4D-A45B-9D6A2759166A\"]/div[1]/span/span/input";
    }

    public static String actionDueDate()
    {
        return "//*[@id=\"control_A1A7A250-4916-472D-A6A5-CDA980F5DA52\"]/div[1]/span/span/input";
    }

    public static String homepage_SolutionsBranch_Btn()
    {
        return "//label[text()='Solutions Branch']";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String search()
    {
        return "//div[text()='Search']";
    }

    public static String MaskBlock()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String iframeCross()
    {
        return "//*[@id=\"ifrMain\"]";
    }

    public static String MaskNone()
    {
        return "//div[@class='ui inverted dimmer']";
    }

    public static String Audits_Inspections()
    {
        return "//label[text()='Audits & Inspections']";
    }

    public static String Inspections()
    {
        return "//label[text()='Inspection Management']";
    }
      public static String Inspections_2()
    {
        return "//label[text()='Inspections']";
    }

    public static String Dashboard()
    {
        return "//label[text()='Dashboard']";
    }

    public static String Text(String text)
    {
        return  "//a[text()='" + text + "']";

    }
     public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }
     
       public static String checkbox(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }
    
     public static String Text4(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "')]";

    }

    public static String supportingDocsTab()
    {
        return "//*[@id=\"tab_45F559C4-7888-48E4-A5BF-E2919D06B9E2\"]/div[1]";
    }

    public static String checklistTab()
    {
        return "//li[@id='tab_3EA164B6-A3C1-47D3-86B1-F7501E619D2A']";
    }

    public static String validationTab()
    {
        return "//div[text()='Validation']";
    }

    public static String declareTickBox()
    {
        return "//div[@id='control_95E9508A-1915-4A26-9BA3-20C3B4909006']";
    }

    public static String declareSave()
    {
        return "//*[@id=\"control_95E9508A-1915-4A26-9BA3-20C3B4909006\"]/div[1]/div";
    }

    //FR1 Capture Inspection
    public static String Inspections_Add()
    {
        return "/html/body/div[1]/div[3]/div/div[1]/div[2]/div[2]/div[1]/div";
    }

    public static String nonCompliance_Add()
    {
        return "(//div[@id='btnAddNew']/div)[3]";
    }

    public static String inspections_Search()
    {
        return "//div[@id='btnActApplyFilter']/div";
    }

    public static String checklist_Add()
    {
        return "//div[@id='btnAddNew']/div";
    }

    public static String supportDocTab()
    {
        return "//li[@id='tab_45F559C4-7888-48E4-A5BF-E2919D06B9E2']/div[1]";
    }

    public static String supportDocPanel()
    {
        return "//div[@id='control_1DEDA02E-9D98-4F88-9E04-A2F840E14376']//i";
    }

    public static String closeInspection()
    {
        return "//*[@id=\"form_D9A8DB93-E3EF-4C40-B3B9-5FE8F6D5AFE2\"]/div[1]/i[2]";
    }

    public static String Inspection_processflow()
    {
        return "//div[@id='btnProcessFlow_form_D9A8DB93-E3EF-4C40-B3B9-5FE8F6D5AFE2']";
    }

    public static String checklist_processflow()
    {
        return "//div[@id='btnProcessFlow_form_E2BEE86E-F593-4213-A0D7-556C53A310AC']/span";
    }

    public static String actions_processflow()
    {
        return "//*[@id=\"btnProcessFlow_form_E2BEE86E-F593-4213-A0D7-556C53A310AC\"]";
    }

    public static String inspectionCheckLst_processflow()
    {
        return "//div[@id='btnProcessFlow_form_BD480451-4A6C-411C-999A-CDF1914C013B']/span";
    }

    public static String requestValiadation_processflow()
    {
        return "//*[@id=\"btnProcessFlow_form_D9A8DB93-E3EF-4C40-B3B9-5FE8F6D5AFE2\"]/span";
    }

    public static String dateConducted()
    {
        return "//div[@id='control_5D0FDFD9-8448-44C2-98C3-9C4DD80F78EB']//input";
    }

    public static String responPersonTickBox()
    {
        return " //div[@id='control_DA9D84B9-03B0-493E-9C54-DA8366F03B4B']";
    }

    public static String multiUserTickBox()
    {
        return "//*[@id=\"control_B85CC3EF-7F60-4CE6-8F64-E978C40DD033\"]/div[1]/a/span[2]/b[2]";
    }

    public static String multiUserTickBox1()
    {
        return "//*[@id=\"control_9FB64F38-240A-4D57-8EBF-202D8124CDEE\"]/div[1]/div";
    }

    public static String multiUserTickBoxSelectAll()
    {
        return "//*[@id=\"control_B85CC3EF-7F60-4CE6-8F64-E978C40DD033\"]/div[1]/a/span[2]/b[2]";
    }

    public static String linkedEqupmentTickBox()
    {

        return "//div[@id='control_7980BCBC-217D-4DCB-B3E7-3DFC33710E63']/div/div";
    }

    public static String stoppageDueSelectAll()
    {
        return "//*[@id=\"control_F3E89F54-2B95-4D1F-9AB4-25A3A4B0BEBC\"]/div[1]/a/span[2]/b[2]";
    }

    public static String inspectionCompletionDate()
    {
        return "//div[@id='control_36DCD52E-E248-424A-82C1-97F091931E63']//input";
    }

    public static String businessUnit_dropdown()
    {
        return "//div[@id='control_1245AEF0-33C6-4C52-8EBE-9EBC5B91BF81']";
    }

    public static String reportedBy_dropdown()
    {
        return "//div[@id='control_7CC7802D-F601-479A-A368-EA7E27A2B502']/div/a/span/ul";
    }

    public static String nonCompliance_DropDown()
    {
        return "//div[@id='control_8A50209C-3EDB-40C6-A225-6EB5DA2AE0DF']/div/a/span[2]/b";
    }

    public static String nonMonitoredDD()
    {
        return "//*[@id=\"control_4F62E358-3A49-4EE5-980F-6B8904670686\"]/div[1]/a/span[2]/b[1]";
    }

    public static String crticalControlDD()
    {
        return "//div[@id='control_4F62E358-3A49-4EE5-980F-6B8904670686']//ul";
    }

    public static String CrticalControl_dropdown()
    {
        return "//div[@id='control_4F62E358-3A49-4EE5-980F-6B8904670686']//ul";
    }

    public static String personResponssibleDD()
    {
        return "//div[@id='control_F36DD606-5017-4C7B-9E0E-B4F354F7E45B']";
    }

    public static String personCompliance_DropDown()
    {
        return "//div[@id='control_F36DD606-5017-4C7B-9E0E-B4F354F7E45B']/div/a/span/ul/li";
    }

    public static String storageRedNo()
    {
        return "//div[@id='control_F36DD606-5017-4C7B-9E0E-B4F354F7E45B']/div/a/span/ul/li";
    }

    public static String typeOfAction_dropdown()
    {
        return "//div[@id='control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE']/div/a/span/ul";
    }
    
      public static String EntityOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String actionDescription()
    {
        return "//*[@id=\"control_1255F613-A69C-476A-8B05-4B87E5CA009F\"]/div[1]/div/textarea";
    }

    public static String entityDropDown()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']/div/a/span/ul/li";
    }

    public static String responsibleDropDown()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']/div/a/span/ul/li";
    }

    public static String agencyDropDown()
    {
        return "//div[@id='control_5B580F56-394D-4695-8AB2-C2CB9AAE9EB9']/div/a/span/ul/li";
    }

    public static String multiUserDropDown()
    {
        return "//div[@id='control_B85CC3EF-7F60-4CE6-8F64-E978C40DD033']/div/a/span/ul/li";
    }

    public static String dropClick()
    {
        return "//div[@id='control_B85CC3EF-7F60-4CE6-8F64-E978C40DD033']/div/a/span[2]/b";
    }

    public static String selectRecord()
    {
        return "//div[@id='grid']/div[3]/table/tbody/tr/td[5]/div";
    }

    public static String selectRecord1()
    {
        return "(//div[@id='grid']/div[3]/table/tbody/tr/td[7]/div)[31]";
    }

    public static String startChecklist()
    {
        return "//div[@id='control_C71A1D8F-4D61-4447-A5C0-1D5798777A72']/div/div";
    }

    public static String btnSave()
    {
        return "//div[@id='btnSave_form_E2BEE86E-F593-4213-A0D7-556C53A310AC']/div[3]";
    }

    public static String startChecklist1()
    {
        return "//div[@id='btnChecklist_form_E2BEE86E-F593-4213-A0D7-556C53A310AC']";
    }

    public static String saveSupportDocButton()
    {
        return "//div[@id='control_79204685-0F9E-4000-B21C-8E0EFE5784C1']/div/div";
    }

    public static String questionDropDown()
    {
        return "//div[@id='control_877D2D5E-BC06-4CD0-B778-FAA988465644']/div/a/span/ul/li";
    }

    public static String inspectionDelete()
    {
        return "//*[@id=\"btnDelete_form_E2BEE86E-F593-4213-A0D7-556C53A310AC\"]/div";
    }

    public static String confirmDelete()
    {
        return "//*[@id=\"btnConfirmYes\"]";
    }

    public static String questionDropDown2()
    {
        return "//*[@id=\"control_D572CDF7-9F5B-4ABF-9693-05CA66F87323\"]/div[1]/a/span[2]/b[1]";
    }

    public static String questionDropDown1()
    {
        return "//div[@id='control_591E6292-DB9B-40B6-B57A-22AEA7297E62']/div/a/span/ul/li";
    }

    public static String requestValidationDropDown()
    {
        return "//div[@id='control_AFB07843-CF25-4B08-839B-24F3AB0EEE09']";
    }

    public static String requestSaveButton()
    {
        return "//div[@id='btnSave_form_D9A8DB93-E3EF-4C40-B3B9-5FE8F6D5AFE2']/div[3]";
    }

    public static String requestValidationSearh()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String checklist_dropdown()
    {
        return "//div[@id='control_4B9CCBAC-E8BC-49FE-9D85-35373C31F0DF']/div/a/span/ul/li";
    }

    public static String businessUnit_select(String data)
    {
        //return "//div[@id='select3_9dfa89cf']/div/input";
        return "////div[@id='select3_6a74062b']/div/input";
    }

    public static String businessUnit_search()
    {
        return "//div[@id='control_1245AEF0-33C6-4C52-8EBE-9EBC5B91BF81']/div/a/span[2]/b";
    }

    public static String type_Search()
    {
        return "/html/body/div[9]/div[3]/div/div[2]/div[9]/div[1]/input";

    }

    public static String Functional_search()
    {
        return "//*[@id=\"control_F1B1B256-75A5-4C10-8352-65498186C4DF\"]/div[1]/a/span[1]/ul/li";
        // return "//*[@id=\"select3_27e93f5a\"]/div[1]/input";
        //return"//div[@id='select3_21244ca1']/div/input";
    }

    public static String businessUnit_RO()
    {
        return "//div[@id='control_1B7F9E95-D550-4113-9B52-580290EBFD0F']/div/a/span/ul/li";
    }

    public static String businessUnit_select()
    {

        return "//li[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c']/a";
    }

    public static String checklist_select()
    {

        return "//*[@id=\"e9ddb316-5034-4eef-ae5c-efc2ef3cc035_anchor\"]";
    }

    public static String nonMonitored_select()
    {

        return "//*[@id=\"14f2fba0-4b57-42aa-9cba-f1d9c372d3e6_anchor\"]";
    }

    public static String criticalControl_select()
    {

        return "//li[@id='16a0433e-b203-415e-a2bd-a5c90d1204b7']/i";
    }

    public static String criticalControl_select1()
    {

        return "//*[@id=\"cccc09a1-2e55-4c17-8ace-8ce4687190a9_anchor\"]";
    }

    public static String inspectionSave()
    {

        return "//*[@id=\"btnSave_form_E2BEE86E-F593-4213-A0D7-556C53A310AC\"]/div[3]";
    }

    public static String question_select1()
    {

        return "//li[@id='14f2fba0-4b57-42aa-9cba-f1d9c372d3e6']/a";
    }

    public static String question_select2()
    {

        return "(//li[@id='14f2fba0-4b57-42aa-9cba-f1d9c372d3e6']/a)[2]";
    }

    public static String functional_select()
    {

        return "//li[@id='65f0ce43-a3e9-42e6-b5c0-c841f08e8670']/a";
    }

    public static String type_select()
    {

        return "//li[@id='50e07298-4754-4321-95be-3ab02c94b8e8']/a";
    }

    public static String typeOfAction_select()
    {

        return "//li[@id='2006cc14-2025-4073-97e4-fca83aa8a2c4']/a";
    }

    public static String nonCompliance_select()
    {

        return "//li[@id='b8ad5f90-d582-46c4-b186-d99649824acd']/a";
    }

    public static String personCompliance_select()
    {

        return "//*[@id=\"b8ad5f90-d582-46c4-b186-d99649824acd_anchor\"]";
    }

    public static String reportedBy_select()
    {

        return "//li[@id='30223b19-5e24-47d1-bc8e-5c1c018deeec']/a";
    }

    public static String entity_select()
    {
        return "//*[@id=\"4cee9a75-7667-44e9-a0c1-77ad5092e86c\"]/i";
    }

    public static String responsiblePerson_select()
    {
        return "//li[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c']/i";
    }

    public static String entity_select1()
    {
        return "//*[@id=\"4cee9a75-7667-44e9-a0c1-77ad5092e86c\"]/i";
    }

    public static String agency_select()
    {
        return "//li[@id='554262db-646e-40bf-a696-74475cfbb7d1']/a";
    }

    public static String multiUser_TickBox()
    {
        return "//a[@id='b8ad5f90-d582-46c4-b186-d99649824acd_anchor']/i";
    }

    public static String owner_select()
    {

        return "//*[@id=\"b8ad5f90-d582-46c4-b186-d99649824acd_anchor\"]";
    }

    public static String owner_select1()
    {

        return "(//li[@id='b8ad5f90-d582-46c4-b186-d99649824acd']/a)[2]";
    }

    public static String team_select()
    {

        return "//a[@id='12fcd1c4-f2ed-4245-b81d-3108e553e96f_anchor']/i";
    }

    public static String equipment_select()
    {

        return "//li[@id='056ccddd-5de3-40d7-8397-66a42b853eb1']/a";
    }

    public static String text_Search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String equipment_search()
    {

        return "(//input[contains(@placeholder,'Type and enter to search')])[6]";
    }

    public static String social_select()
    {

        return "//*[@id=\"a513c29e-150c-4e08-89eb-17bab5965d43_anchor\"]";
    }

    public static String businessUnit_search2()
    {

        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String businessUnit_select2()
    {
        return "//*[@id=\"4cee9a75-7667-44e9-a0c1-77ad5092e86c\"]/i";
    }

    public static String businessUnit_select3()
    {
        return "//*[@id=\"8ba02925-abbb-4dff-a1d1-a33ba5414986\"]/i";
    }

    public static String text_Search1()
    {

        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String checklist_search()
    {

        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String reportedBy_search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String NonComplianceIntervention_search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String typeNonCompliance_search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String clearanceClassification_search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String stoppageInitiatedBy_search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String workStoppage_search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String stoppageNoteClassification_search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String stoppageAffectedOtherAreas_search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String stoppageOutcome_search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String personResponsibleForNonCompliance_search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String stoppageReferenceNumber()
    {
        return "//*[@id=\"control_CB772C7C-ABD8-4B0D-8618-F20ECC6A3F58\"]/div[1]/div/input";
    }

    public static String actionDescription1()
    {
        return "//*[@id=\"control_1255F613-A69C-476A-8B05-4B87E5CA009F\"]/div[1]/div/textarea";
    }

    public static String entitySelect()
    {
        return "//*[@id=\"4cee9a75-7667-44e9-a0c1-77ad5092e86c\"]/i";
    }

    public static String entitySelect1()
    {
        return "//*[@id=\"4cee9a75-7667-44e9-a0c1-77ad5092e86c\"]/i";
    }

    public static String comment()
    {
        return "//div[@id='control_BE62E110-7219-48F2-B4DD-4BAADE1D6E9D']//textarea";
    }

    public static String descriptionOfReasonForWorkStoppage()
    {
        return "//*[@id=\"control_BFA70286-6BD9-4063-ABA8-82FE45B96A6A\"]/div[1]/div/textarea";
    }

    public static String nonMonitored_search()
    {

        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String criticalControl_search()
    {

        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String supportingDocumentsDD()
    {

        return "//div[@id='control_1DEDA02E-9D98-4F88-9E04-A2F840E14376']/div[9]/div";

    }

    public static String question_search()
    {

        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String question_search1()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String type_Search1()
    {

        return "/html/body/div[1]/div[3]/div/div[2]/div[10]/div[1]/input";
    }

    public static String linked_Search()
    {

        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String saveStartChecklist()
    {

        return "(//input[contains(@placeholder,'Type to search')])[6]";
    }

    public static String owner_Search()
    {

        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";

    }

    public static String responsible_Search()
    {

        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";

    }

    public static String team_Search()
    {

        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String Functional_search1()
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[9]/div[1]/input";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String Projectlink_checkbox()
    {
        return "//div[@id='control_866CF8C6-BE75-4BAD-879C-798F4D218DA5']";
    }

    public static String Project_dropdown()
    {
        return "//div[@id='control_5F8F8D23-1D41-4FEE-ABD8-12D1C129E708']";
    }

    public static String Project_select()
    {
        // return "//li[@id='4eb45752-8d4a-47ab-a21a-af0e237eff32']/a";
        return "//*[@id=\"299f5fc3-7622-4362-bcef-e803ee84c4e2_anchor\"]";
    }

    public static String responsible_select()
    {
        return "//li[@id='b8ad5f90-d582-46c4-b186-d99649824acd']/a";
    }

    public static String project_Search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String SocialInitiativeslink_checkbox()
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[35]/div[1]/div";
    }

    public static String SocialInitiatives_dropdown()
    {
        return "//div[@id='control_A75D5577-D661-458F-9027-1EE8DC616FCB']/div/a/span/ul/li";
    }

    public static String individualEntity_dropdown()
    {
        return "//div[@id='control_E804F73A-E8FE-4596-9CFE-4C9A4194DE2C']//li";
    }

    public static String Functional_dropdown()
    {

        return "//div[@id='control_F1B1B256-75A5-4C10-8352-65498186C4DF']";
    }

    public static String nextButton()
    {
        // return "/html/body/div[12]/div[3]/div/div[2]/div[40]/div[2]/div[2]/div[3]/div";
        return "(//div[@id='act_form_right']/div[3])[3]";
    }

    public static String RelatedStakeholder_dropdown()
    {
        return "//div[@id='control_BE43AACA-D143-4D92-B0B9-3AE9A436B1C5']";
    }

    public static String responsiblePersson_dd()
    {

        return "//*[@id=\"control_1B7F9E95-D550-4113-9B52-580290EBFD0F\"]/div[1]/a/span[2]/b[1]";
    }

    public static String owner_Dropdown()
    {

        return "//div[@id='control_CB6C0FC6-FC03-463C-B4A4-4C45A69EB966']//ul";
    }

    public static String teamNameDD()
    {

        return "//*[@id=\"control_B1BD502D-2B40-4247-8B04-FE224983D1BE\"]/div[1]/a/span[2]/b[1]";
    }

    public static String team_Dropdown()
    {

        //return "//div[@id='control_04902FEC-C700-4373-9B26-44C4C1C2F760']";
        return "//*[@id=\"control_04902FEC-C700-4373-9B26-44C4C1C2F760\"]/div[1]/a/span[2]/b[1]";
    }

    public static String RelatedStakeholder_select(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String ImpactType_dropdown()
    {
        return "//div[@id='control_34C99D1C-9A7D-4BDB-81C7-109EEC9B5926']";
    }

    public static String Type_dropdown()
    {
        return "//div[@id='control_41D966BC-3A7D-4366-80CB-DBB6C8545295']/div/a/span/ul/li";
    }

    public static String BusinessRisk_hardwait()
    {
        return "//a[contains(text(),'Business Risk')]";
    }

    public static String ImpactType_selectall()
    {
        return "(//div[@id='control_34C99D1C-9A7D-4BDB-81C7-109EEC9B5926']//b)[2]";
    }

    public static String actionsPanel()
    {
        return "//div[@id='control_411385A6-315A-49F6-8CBD-85E89AE481F2']/div[9]/div";
    }

    public static String nonCompliancePanel()
    {
        return "//div[@id='control_052A4B93-7EEC-4717-B796-2606634CA9DD']/div[9]/div/span";
    }

    public static String actionsAddButton()
    {
        return "(//div[@id='btnAddNew']/div)[2]";
    }

    public static String inspectionsChecklistAddButton()
    {
        return "(//div[@id='btnAddNew']/div)[3]";
    }

    public static String InspectionConductedBy()
    {
        return "//div[@id='control_DA9D84B9-03B0-493E-9C54-DA8366F03B4B']";
    }

    public static String InspectionConductedBy_Dropdown()
    {
        return "//div[@id='control_1B7F9E95-D550-4113-9B52-580290EBFD0F']";
    }

    public static String InspectionTeam()
    {
        return "//div[@id='control_58E56654-402C-45DE-8794-3A814AB320A4']//textarea";
    }

    public static String InspectionType_dropdown()
    {
        return "//div[@id='control_77D51D6F-A09A-4CC0-ADEB-8536320AA0BB']";
    }

    public static String InspectionType_select(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String RelatedEquipment_dropdown()
    {
        return "//div[@id='control_A809B695-DA5B-4EE4-8635-9C75BB5D0541']";
    }

    public static String LinkedEquipment_dropdown()
    {
        return "//*[@id=\"control_A809B695-DA5B-4EE4-8635-9C75BB5D0541\"]";
    }

    public static String RelatedEquipment_select(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String SaveToContinue_Button()
    {
        return "//div[@id='control_66D8EC74-A15E-4260-8D5E-8A28442E7C23']";
    }

    public static String continue_Button()
    {
        return "//*[@id=\"btnChecklist_form_E2BEE86E-F593-4213-A0D7-556C53A310AC\"]/div";
    }

    public static String saveSupportDocs()
    {
        return "//*[@id=\"control_79204685-0F9E-4000-B21C-8E0EFE5784C1\"]/div[1]/div";
    }

    public static String checklistSave_Button()
    {
        return "//div[@id='btnSave_form_E2BEE86E-F593-4213-A0D7-556C53A310AC']/div[3]";
    }

    public static String relatedCriticalControlDD()
    {
        return "//div[@id='control_4F62E358-3A49-4EE5-980F-6B8904670686']/div/a/span/ul";
    }

    public static String submitInspection_Button()
    {
        return "//div[@id='control_C6C8CE6E-A001-4ED5-9525-1E6842BB1500']";
    }

    public static String ErrorOk_Button()
    {
        return "//div[@id='control_C6C8CE6E-A001-4ED5-9525-1E6842BB1500']";
    }

    public static String saveWait()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String Submit_Button()
    {
        return "//div[@id='control_C6C8CE6E-A001-4ED5-9525-1E6842BB1500']";
    }

    public static String viewFilter_button()
    {
        return "//div[@id='btnChecklist_form_E2BEE86E-F593-4213-A0D7-556C53A310AC']";
    }

    public static String search_button()
    {
        return "(//div[@id='btnActApplyFilter']//div[contains(text(),'Search')])[1]";
    }

    public static String inspectionRecord()
    {
        return "(//div[text()='Global Company'])[1]";
    }

    public static String status()
    {
        return "//div[@id='control_5C41234A-AB85-4FDB-A4D2-765944EFD718']//li[contains(text(),'Not Started')]";
    }

    //FR5 View Reports
    public static String report()
    {
        return "//div[@id='btnReports']";
    }

    public static String view_reports()
    {
        return "//span[@title='View report ']";
    }

    public static String full_Reports()
    {
        return "//span[@title='Full report ']";
    }

    public static String inspection_wait()
    {
        return "//div[text()='Inspections']";
    }

    public static String view_wait()
    {
        return "//div[text()='Entity report']";
    }

    public static String status_wait()
    {
        return "//div[text()='Status']";
    }

    public static String full_wait()
    {
        return "//div[text()='Entity status']";
    }

    public static String popup_conf()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String Inspection_start()
    {
        return "//div[text()='Start']";
    }

    public static String UseOfEquipment_select(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String text_Search2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String UseOfEquipment_dropdown()
    {
        return "//div[@id='control_D675843C-5B7D-4646-82C7-DB5D3B424FB4']";
    }

    public static String stackingLifting_checklist()
    {
        return "//div[@id='control_A5852972-280F-43B9-AD16-44A21B1AA3AB']//div//div";
    }

    public static String Ergonomics_checklist()
    {
        return "//div[@id='control_B7AE568E-7027-48B5-8D61-490EF546F7AA']//div//div";
    }

    public static String Emergency_checklist()
    {
        return "//div[@id='control_4ACBA0C7-476C-4C38-8FBC-7C09DCB338E9']//div//div";
    }

    public static String EyeProtection_checklist()
    {
        return "//div[@id='control_25FB6698-EC1E-4032-9485-D6B73C1E153A']//div//div";
    }

    public static String EarProtection_checklist()
    {
        return "//div[@id='control_0EA9EAFB-B4D7-406C-999F-58A717B732A5']//div//div";
    }

    public static String next_Button()
    {
        return "(//div[@id='btnSave_checklist_E2BEE86E-F593-4213-A0D7-556C53A310AC']/div)[3]";
    }

    public static String saveCurrentSection_Button()
    {
        return "//div[contains(text(),'Save current section')]";
    }

    public static String exit_Button()
    {
        return "//div[@id='form_checklist_E2BEE86E-F593-4213-A0D7-556C53A310AC']/div/i[2]";
    }

    public static String exit_Button1()
    {
        return "//div[@id='form_E2BEE86E-F593-4213-A0D7-556C53A310AC']/div/i[2]";
    }

    public static String confirmationYes_Button()
    {
        return "//div[@id='divConfirm']/div/div/div[2]/div[2]";

    }

    public static String InspectionRecord()
    {
        return "(//div[@id='btnSave_checklist_E2BEE86E-F593-4213-A0D7-556C53A310AC']/div)[2]";
    }

    public static String InspectionClose_button()
    {
        return "//div[@id='form_checklist_D9A8DB93-E3EF-4C40-B3B9-5FE8F6D5AFE2']//div//i[@class='close icon cross']";
    }

    public static String finish_Button()
    {
        //return "//div[contains(text(),'Finish')]";
        return "//div[@id='btnSave_checklist_E2BEE86E-F593-4213-A0D7-556C53A310AC']/div";
    }

    public static String view_Button()
    {

        return "//*[@id=\"btnChecklist_form_E2BEE86E-F593-4213-A0D7-556C53A310AC\"]/div";
    }

    public static String finish_Button1()
    {
        return "//div[contains(text(),'Finish')]";
    }

    public static String reasonForWalk_Dropdown()
    {
        return "//div[@id='control_4323B747-9D86-4826-8A15-C1088961E8B6']";
    }

    public static String reasonForWalk_select(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String reasonForWalk_select2(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }

    public static String compliment_textarea()
    {
        return "//div[@id='control_1BBAD98B-E412-4D98-A213-874B9D834FB5']//textarea";
    }

    public static String unsafeBehaviour_textarea()
    {
        return "//div[@id='control_99741985-A10D-4EE7-BD7F-0656C18F5BC1']//textarea";
    }

    public static String intervene_Dropdown()
    {
        return "//div[@id='control_F421D074-415B-4374-8B46-681AB126C243']";
    }

    public static String intervene_select(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[3]";
    }

    public static String rectifyUnsafeBehaviour_textarea()
    {
        return "//div[@id='control_DCB80445-B0C9-4DB6-ABCA-3E445EE778AE']//textarea";
    }

    public static String agreement_Dropdown()
    {
        return "//div[@id='control_88D85A6A-4321-4C94-AC65-00E653D872E0']";
    }

    public static String agreement_Select(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[4]";
    }

    public static String aspects_textarea()
    {
        return "//div[@id='control_7BC87A80-5916-4773-80FF-DFC3B825E287']//textarea";
    }

    public static String feedback_Dropdown()
    {
        return "//div[@id='control_7F854952-DADE-4AE6-93DE-C190C3A54CE3']";
    }

    public static String feedback_Select(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String completedStatus()
    {
        return "//div[@id='control_5C41234A-AB85-4FDB-A4D2-765944EFD718']//li[contains(text(),'Complete')]";
    }

    //FR2 Findings
    public static String Findings_Add()
    {
        return "//div[@id='control_EEBC2DAB-77E3-4AF5-9238-C0293741561B']//div[text()='Add']";
    }

    public static String Findings_pf()
    {
        return "//div[@id='btnProcessFlow_form_9DE682E7-18A4-4632-80DA-F0261923C0B9']";
    }

    public static String Findings_desc()
    {
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']/div/div/textarea";
    }

    public static String Findings_owner_dropdown()
    {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']";
    }

    public static String Findings_owner_select(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[1]";
    }

    public static String Findings_risksource_dropdown()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']";
    }

    public static String Findings_risksource_select(String text)
    {
//        return "(//a[contains(text(),'"+text+"')]/../i)[1]";
        return "(//a[contains(text(),'" + text + "')]/i)[1]";
    }

    public static String Findings_risksource_select2(String text)
    {
        return "(//a[contains(text(),'" + text + "')]/i[1])[1]";
    }

    public static String Findings_risksource_arrow()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@class='select3-down drop_click']";
    }

    public static String Findings_class_dropdown()
    {
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']";
    }

    public static String Findings_class_select(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[1]";
    }

    public static String Findings_risk_dropdown()
    {
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']";
    }

    public static String Findings_risk_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String Findings_stc()
    {
        return "(//div[text()='Save to continue'])[2]";
    }

    //FR3 ACtions
    public static String Actions_tab()
    {
        return "//div[text()='Actions']";
    }

    public static String Actions_add()
    {
        return "//div[@id='control_731174EE-AD50-4449-AF3F-253162F405CB']//div[text()='Add']";
    }

    public static String Actions_pf()
    {
        return "//div[@id='btnProcessFlow_form_293FCFDD-A8E8-4DE8-AD1F-AA5C82E219C5']";
    }

    public static String Actions_desc()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String Actions_deptresp_dropdown()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }

    public static String Actions_deptresp_select(String text)
    {
        return "(//a[text()='" + text + "'])[2]";
    }

    public static String Actions_respper_dropdown()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }

    public static String Actions_respper_select(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";
    }

    public static String Actions_ddate()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String typeOfActionSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String reportedBySearch()
    {
        return "/html/body/div[3]/div[3]/div/div[2]/div[31]/div[1]/input";
    }

    public static String nonComplianceSearch()
    {
        return "/html/body/div[5]/div[3]/div/div[2]/div[32]/div[1]/input";
    }

    public static String personComplianceSearch()
    {
        return "/html/body/div[10]/div[3]/div/div[2]/div[33]/div[1]/input";
    }

    public static String entitySearch()
    {
        return "(//input[contains(@placeholder,'Type to search')])[16]";
    }

    public static String entitySearch1()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String Actions_save()
    {
        return "//div[@id='btnSave_form_03C1E47F-14A5-43B6-BA2F-C5F17A3A47E0']/div[3]";
    }

    public static String responsiblePerSearch()
    {
        return "//div[2]/div[33]/div/input";
    }

    public static String agencySearch()
    {
        return "(//input[contains(@placeholder,'Type to search')])[17]";
    }

    public static String agencySearch1()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String multiUserSearch()
    {
        return "(//input[contains(@placeholder,'Type and enter to search')])[9]";
    }

    public static String multiUserSearch1()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String Actions_savewait()
    {
        return "(//div[text()='Action Feedback'])[1]";
    }

    public static String inspection_RecordSaved_popup()
    {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }

    public static String recordSaved_popup()
    {
        return "//div[@class='ui floating icon message transition visible']/..//div[contains(text(),'Record saved')]";
    }

    public static String failed()
    {
        return "(//div[@id='txtAlert'])[2]";
    }

    public static String supportingDocuments_Tab()
    {
        return "//li[@id='tab_DA2BB07F-D6D6-4090-907A-D966829DF4E6']";
    }

    public static String LinkURL()
    {
        return "//*[@id=\"control_FF4A779C-2097-43B0-918E-C0FA23C390C8\"]/div[1]/div[1]/div[2]/b[2]";
    }

    public static String uploadDocument()
    {
        return "//div[@id='control_C440202B-AFE6-4EB9-B180-57BBF25C57D6']//b[@original-title='Upload a document']";
    }

    public static String LinkURL1()
    {
        return "\"//div[@class='confirm-popup popup']//input[@id='urlValue']\"";
    }

    public static String linkToADocument()
    {
        return "//div[@id='control_2557656E-2BF9-468C-B4E7-4A0591B5C226']//b[@original-title='Link to a document']";
    }

    public static String btnYesURL()
    {
        return "//*[@id=\"btnConfirmYes\"]/div";
    }

    public static String titleURL()
    {
        return "//input[@id='urlTitle']";
    }

    public static String httpURL()
    {
        return "//*[@id=\"urlValue\"]";
    }

    public static String httpAdd()
    {
        return "//*[@id=\"btnConfirmYes\"]/div";
    }

    public static String httpTitle()
    {
        return "//*[@id=\"urlTitle\"]";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String linkbox()
    {
        return "//div[@id='control_FF4A779C-2097-43B0-918E-C0FA23C390C8']//b[@original-title='Link to a document']";
    }

    public static String Save_Button()
    {
        return "(//div[@id='btnSave_form_D9A8DB93-E3EF-4C40-B3B9-5FE8F6D5AFE2']//div[contains(text(),'Save')])[3]";
    }

    public static String IndividualEntity_dropdown()
    {
        return "//div[@id='control_E804F73A-E8FE-4596-9CFE-4C9A4194DE2C']";
    }

    public static String IndividualEntity_select(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    //GMAIL
    public static String first_name()
    {
        return "//input[@name='firstName']";
    }

    public static String last_Name()
    {
        return "//input[@name='lastName']";
    }

    public static String Gmail()
    {
        return TestMarshall.currentEnvironment.isoMetrixEnvironment;
    }

    public static String record_No()
    {
        return "//input[@class='txt border']";
    }

    public static String recordNo()
    {
        return "(//input[@class='txt border'])[1]";
    }

    public static String serachBtn()
    {
        return "//div[@class='actionBar filter']//div[text()='Search']";
    }

    public static String selectRecord(String data)
    {
        return "//span[text()='" + data + "']";
    }

    public static String selectActionRecord()
    {
        return "//div[@id='control_731174EE-AD50-4449-AF3F-253162F405CB']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr[1]";
    }

    public static String reportsBtn()
    {
        return "//div[@id='btnReports_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }

    public static String viewRecord_reports()
    {
        return "(//span[@class='panelbarReportIcons icon report'])[2]";
    }

    public static String Status_tab()
    {
        return "//div[@id='control_5C41234A-AB85-4FDB-A4D2-765944EFD718']";
    }

    public static String inspectionAction_processflow()
    {
        return "//div[@id='btnProcessFlow_form_293FCFDD-A8E8-4DE8-AD1F-AA5C82E219C5']";
    }

    public static String LinkedEquipmentexpandButton(String data)
    {
        return "(//div[contains(@class,'transition visible')]//a[contains(text(),'" + data + "')]/..//i)[1]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }
    
     public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }
    
     public static String EntityexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String Button_Save()
    {
        return "//div[@id='btnSave_form_D9A8DB93-E3EF-4C40-B3B9-5FE8F6D5AFE2']";
    }

    public static String Checklists_dropdown()
    {
        return "//div[@id='control_B153DF4E-EF6F-41E8-9282-3AE420985061']//ul";
    }

    public static String Checklists_select_all_dropdown()
    {
        return "//div[@id='control_B153DF4E-EF6F-41E8-9282-3AE420985061']//b[@class='select3-all']";
    }

    public static String LinkedsocialInitiativeTickBox()
    {
        return "//div[@id='control_A1224879-94F2-4C2B-AE43-CA2F62C53E06']//div[@class='c-chk']";
    }

    public static String LinkedsocialInitiative_dropdown()
    {
        return "//div[@id='control_A75D5577-D661-458F-9027-1EE8DC616FCB']//ul";
    }

    public static String LinkedPermitTickBox()
    {
        return "//div[@id='control_015DB271-FC71-4C41-87E7-55C528EB3A99']//div[@class='c-chk']";
    }

    public static String LinkedPermit_dropdown()
    {
        return "//div[@id='control_35A5B479-37C3-4F23-B85D-936FF1C2A969']//li";
    }

    public static String LinkedStakeholderTickBox()
    {
        return "//div[@id='control_396ED63F-1C76-4396-97E7-FE0E2101C3C1']//div[@class='c-chk']";
    }

    public static String LinkedStakeholder_dropdown()
    {
        return "//div[@id='control_0724DA84-2161-4D42-8604-3580C5CD5232']//li";
    }

    public static String LinkedStakeholderExpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String multiUserTickBox(String data)
    {
        return "(//a[contains(text(),'" + data + "')]/..//i[@class='jstree-icon jstree-checkbox'])";
    }

    public static String StartButton()
    {
        return "//div[text()='Start']";
    }

    public static String Question1_dropdown()
    {
        return "//div[text()='Group 1']/..//div[@name='control_DDL0']//ul";
    }

    public static String Continue_Button()
    {
        return "//div[text()='Continue']";
    }

    public static String Finish_Button()
    {
        return "//div[text()='Finish']";
    }

    public static String View_Button()
    {
        return "//div[text()='VIEW']";
    }

    public static String Link_URL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String FirstRecordIn_checklist()
    {
        return "//div[@id='control_B63AC603-846C-421F-80B2-99924856445E']//tr//div[text()=' SOI - Pit']";
    }
    
     public static String ContainsTextBox()
    {
        return"(//input[@class='txt border'])[1]";
    }
    
    
    public static String SearchButton()
    {
       return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return"//span[text()='"+string+"']";
    }

    public static String PresentExit_Button()
    {
        return "(//div[@class='form transition visible active']//i[@class='close icon cross'])[1]";
    }
    
    public static String PresentExit_Button_1()
    {
        return "(//div[@class='form active transition visible']//i[@class='close icon cross'])[1]";
    }

    public static String Checklists_dropdown_Option(String  text)
    {
        return "//a[text()='"+text+"']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String Control_dropdown()
    {
        return "//div[@id='control_010DD5A8-B76E-4D4D-A135-F106CFF9B729']";
    }

    public static String Question1_dropdown1()
    {
       return "//div[@id='control_877D2D5E-BC06-4CD0-B778-FAA988465644']";
    }

    public static String Record_Deletepopup()
    {
        return"(//a[text()='Delete'])[2]";
    }
    
     public static String ButtonOK()
    {
       return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }
     
     public static String ButtonConfirm()
    {
        return"//div[@id='btnConfirmYes']";
    }

    public static String DeleteButton()
    {
        return"(//div[text()='Delete'])";
    }

    public static String ActionsDropDown()
    {
       return "(//div[@id='control_411385A6-315A-49F6-8CBD-85E89AE481F2']//i)[1]";
    }

    public static String ActionsAddButton()
    {
        return"//div[@id='control_1937AC8A-8D79-4086-9BCA-CFEEE2C0559F']//div[text()='Add']";
    }

    public static String EntityDropdown()
    {
       return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']";
    }

    public static String NonComplianceDropDown()
    {
        return"(//div[@id='control_052A4B93-7EEC-4717-B796-2606634CA9DD']//i)[1]";
    }

    public static String NonComplianceAddButton()
    {
       return "//div[@id='control_B3B39111-8079-4030-9F9C-3B4B8D7E21FC']//div[text()='Add']";
    }

    public static String NonComplianceSave()
    {
       return "//div[@id='btnSave_form_BD480451-4A6C-411C-999A-CDF1914C013B']";
    }

    public static String StoppageDueToDropDown()
    {
        return"//div[@id='control_F3E89F54-2B95-4D1F-9AB4-25A3A4B0BEBC']";
    }

    public static String StopNoteClassificationDropDown()
    {
       return "//div[@id='control_0870BCBF-A0B7-40B2-A18F-D088477B890C']//ul";
    }

    public static String DescriptionofReason()
    {
       return "//div[@id='control_BFA70286-6BD9-4063-ABA8-82FE45B96A6A']//textarea";
    }

    public static String WorkStoppageDropDown()
    {
       return "//div[@id='control_89EDFE37-2C85-45CD-9C73-338374164EF0']//ul";
    }

    public static String StoppageInitiatedByDropDown()
    {
        return"//div[@id='control_FDBF77F4-AEAF-4F79-B2E2-7E4DB11A1DE5']//ul";
    }

    public static String TypeofNonComplianceInterventionDropDown()
    {
        return"//div[@id='control_C92CA60D-7027-4F48-B9B4-60ABFDA93A62']//ul";
    }

    public static String PersonResponsibleDropDown()
    {
        return"//div[@id='control_F36DD606-5017-4C7B-9E0E-B4F354F7E45B']//ul";
    }

    public static String NonComplianceInterventionDropDown()
    {
       return "//div[@id='control_8A50209C-3EDB-40C6-A225-6EB5DA2AE0DF']//ul";
    }

    public static String ReportedByDropDown()
    {
        return"//div[@id='control_7CC7802D-F601-479A-A368-EA7E27A2B502']//ul";
    }

    public static String ReportedDateofNonCompliance()
    {
        return"//div[@id='control_817B1336-983B-4C72-BAD5-304A992D37A1']//input";
    }

    public static String DateofNonCompliance()
    {
       return "//div[@id='control_E81D5D30-D0CF-4B4D-A45B-9D6A2759166A']//input";
    }

    public static String StoppageOutcomeDropDown()
    {
        return"//div[@id='control_AB93D552-65AF-4AB1-AB2B-612D4C2F78DF']//ul";
    }

    public static String Answer_dropdown1()
    {
       return "//div[@id='control_0ED89FF9-7694-4A68-8E98-6CDD0092AD62']//ul";
    }

    public static String DueDate()
    {
       return "//div[@id='control_36DCD52E-E248-424A-82C1-97F091931E63']//input";
    }
}
