/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.ProductionData_PageObjects;

/**
 *
 * @author smabe
 */
public class ProductionData_PageObjects
{
    
    public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String ContainsTextBox()
    {
        return "(//input[@class='txt border'])[1]";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String recordSaved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordSaved_popup1()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordNoChanges_Saved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record has no changes to save')])[1]";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String DeleteButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Delete']";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String uploadLinkbox()
    {
        return "//div[@class='linkbox-options']//b[@class='linkbox-link']";
    }

    public static String businessUnitexpand(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String RelationshipBasedSafetyTab()
    {
        return "//label[text()='Relationship Based Safety']";
    }

    public static String RelationshipBased_Add()
    {
        return "//div[contains(@class,'active form transition visible')]//div[@id='btnActAddNew']";
    }

    public static String BusinessUnit_Dropdown()
    {
       return "//div[@id='control_2EFE3990-9EB5-4B41-BEB7-8FCF2D4CAAAE']//ul";
    }
    
       public static String ECO2ManTab()
    {
       return "//label[text()='ECO2Man']";
    }

    public static String ProductionDataTab()
    {
        return"//label[text()='Production Data']";
    }

    public static String ButtonAdd()
    {
       return "//div[@class='active form transition visible']//div[@id='btnActAddNew']";
    }

    public static String Processflow()
    {
        return"//div[@id='btnProcessFlow_form_DFE45300-55BA-4BCD-BBE8-245BC1ED23FF']";
    }

    public static String MonitoringTypeDropDown()
    {
       return "//div[@id='control_C3595362-5A92-4312-97A3-8154C671C1C2']//ul";
    }

    public static String deleteRecordButton()
    {
        return"//div[@id='btnDelete_form_DFE45300-55BA-4BCD-BBE8-245BC1ED23FF']";
    }

    public static String Button_Save()
    {
        return"//div[@id='btnSave_form_DFE45300-55BA-4BCD-BBE8-245BC1ED23FF']";
    }

    public static String MonthYearDropDown()
    {
      return  "//div[@id='control_7D8DFC19-F194-4E69-8A12-192151A5ABB8']//ul";
    }

    public static String MonthYearDropDown2()
    {
        return"//div[@id='control_64D7DA8B-FB24-4E72-8F4C-7732C413FC6E']//ul";
    }

    public static String MineralType_Dropdown()
    {
       return "//div[@id='control_338FA6A1-F2A8-4D9F-8B21-158F7D4C03A0']//ul";
    }

    public static String QuantityOfMineralProduced()
    {
       return "(//div[@id='control_43F621A6-855C-4C64-BD66-A1DBB3422172']//input)[1]";
    }

    public static String MineralProduced_Add()
    {
        return"//div[@id='control_986036D6-3258-4C36-91AF-F83183E1AFF6']//div[@id='btnAddNew']";
    }

    public static String QuantityOfMineralProducedUOM_Dropdown()
    {
       return "//div[@id='control_DCC0F180-B135-4038-9A5B-0C3BA5AEB9B1']//ul";
    }

    public static String ProductionIndicator_Dropdown()
    {
        return"//div[@id='control_7DF5F45A-592F-49AA-BC19-FB5BEEEDBD50']//ul";
    }
    
}
