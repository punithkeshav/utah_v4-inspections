/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.ClimateChangeAndEnergyMonitoring_PageObjects;

/**
 *
 * @author smabe
 */
public class ClimateChangeAndEnergyMonitoring_PageObjects
{
    
    public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String ContainsTextBox()
    {
        return "(//input[@class='txt border'])[1]";
    }

   public static String ECO2ManTab()
    {
       return "//label[text()='ECO2Man']";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String recordSaved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordSaved_popup1()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordNoChanges_Saved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record has no changes to save')])[1]";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String DeleteButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Delete']";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String uploadLinkbox()
    {
        return "//div[@class='linkbox-options']//b[@class='linkbox-link']";
    }

    public static String businessUnitexpand(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }
      public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String RelationshipBasedSafetyTab()
    {
        return "//label[text()='Relationship Based Safety']";
    }

    public static String RelationshipBased_Add()
    {
        return "//div[contains(@class,'active form transition visible')]//div[@id='btnActAddNew']";
    }

   

    public static String ClimateChangeAndEnergyMonitoringTab()
    {
       return "//label[text()='Climate Change and Energy Monitoring']";
    }
    
     public static String Button_Add()
    {
        return"//div[@class='active form transition visible']//div[@id='btnActAddNew']";
    }

    public static String Processflow()
    {
       return "//div[@id='btnProcessFlow_form_9DC31173-8C5B-4ACF-AF93-B0F59005FBC5']";
    }
    
     public static String BusinessUnit_Dropdown()
    {
       return "//div[@id='control_C57FA867-0410-4DFD-A737-68E1D6309394']//ul";
    }

    public static String MonthDropDown()
    {
        return"//div[@id='control_AF87ED2E-44C9-4AC7-AAE0-5125CE96AD8F']//ul";
    }

    public static String YearDropDown()
    {
        return"//div[@id='control_F0DDC82D-46D3-427A-933F-267393C3CC0A']//ul";
    }

    public static String DateCapturedByDropDown()
    {
       return "//div[@id='control_4F7D9430-6F00-4905-BEF1-D702BCA035BB']//ul";
    }

    public static String Button_Save()
    {
      return  "//div[@id='btnSave_form_9DC31173-8C5B-4ACF-AF93-B0F59005FBC5']";
    }

    public static String deleteRecordButton()
    {
        return"//div[@id='btnDelete_form_9DC31173-8C5B-4ACF-AF93-B0F59005FBC5']";
    }

    public static String LinkToEnvironmental()
    {
       return "//div[@id='control_F2D8BCB8-1924-4F05-8EB6-AD957B37823C']//div[@class='c-chk']";
    }

    public static String LinkToEnvironmentalDropDown()
    {
       return "//div[@id='control_BDA4C696-A3A2-4206-845A-DE48EED2197F']//ul";
    }

    public static String LinkToProjects()
    {
       return "//div[@id='control_5CE16D3A-F5EB-42CD-B0F7-2701D43893AC']//div[@class='c-chk']";
    }

    public static String LinkToProjectsDropDown()
    {
       return "//div[@id='control_B6CA027C-6F9F-4724-A51F-40AB0BB24216']//ul";
    }

    public static String linkToADocument()
    {
        return"//div[@id='control_8C88EB5C-69F5-41EB-9DD0-4332E6C3A956']//b[@class='linkbox-link']";
        
    }

    public static String Link_URL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String MeasurementsTab()
    {
       return "//span[text()='Measurements']";
    }

    public static String Measurements_Add()
    {
        return"//div[@id='control_60C4A7BD-C45E-44C2-A73B-43B47347EEE4']//div[@id='btnAddNew']";
    }

    public static String EmissionSource_Dropdown()
    {
        return"//div[@id='control_FE707531-9D1D-4591-86A9-6E667856415B']//ul";
    }

    public static String UnitDropDown()
    {
       return "//div[@id='control_2F9EA8A6-E0B0-46C7-ABDE-030F8A2692CC']//ul";
    }

    public static String Measurement()
    {
        return"(//div[@id='control_18C58CFC-2E75-44F3-A490-E6833BA28142']//input)[1]";
    }

    public static String ManagementFindingsPanel()
    {
       return "//span[text()='Energy Monitoring Findings']";
    }

    public static String findingsAddButton()
    {
        return"//div[@id='control_C720B63A-3CE3-42E1-AF58-AC414614A925']//div[@id='btnAddNew']";
    }

    public static String findingsProcessflow()
    {
       return "//div[@id='btnProcessFlow_form_ABA81AE7-7747-4DDB-89BE-3702E9BDD96C']";
    }

    public static String Findings_desc()
    {
       return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }

    public static String Findings_owner_dropdown()
    {
       return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']//ul";
    }

    public static String Findings_class_dropdown()
    {
      return  "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']//ul";
    }
    
     public static String Findings_class_select(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[1]";
    }

    public static String SaveButtonFindings()
    {
       return "//div[@id='btnSave_form_ABA81AE7-7747-4DDB-89BE-3702E9BDD96C']";
    }
}
