/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects;

/**
 *
 * @author skhumalo
 */
public class Inspection_Scheduler_PageObjects
{

    public static String InspectionScheduler_Module;
    public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String navigate_EHS()
    {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
    
      public static String SearchOption()
    {
        return"//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String EvaluatePerformanceMaintenance()
    {
        return "//div[@original-title='Evaluate Performance Maintenance']//i[@class='icon settings size-five cool grey module iso-icon row']";
    }

    public static String InspectionScheduler_Module()
    {
        return "//*[@id=\"section_eb301df5-f6b5-46a1-98f7-735365480e0f\"]/label";
    }

             public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }
    public static String showAllFilter()
    {
        return "//*[@id=\"searchOptions\"]/div[1]/span[2]";
    }
       public static String SearchButton()
    {
       return "//div[@id='btnActApplyFilter']";
    }

          public static String Search_Button()
    {
       return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return"//span[text()='"+string+"']";
    }
 
      public static String ContainsTextBox()
    {
        return"(//input[@class='txt border'])[1]";
    }
    
   
     public static String CloseCurrentModule()
    {
        return"(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }
     public static String CloseCurrentModule2()
    {
      return  "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String activeInactiveDD()
    {
        return "//div[@id='searchOptions']/div[3]/table/tbody/tr[3]/td[5]/div/a/span/ul/li";
    }

     public static String getActionRecord()
    {
        return "//div[@class='record']";
    }
    public static String activeInactiveDD1()
    {
        return "//div[@id='control_9FA45347-6BF9-4F66-8E94-C107EDDC834A']/div/a/span/ul/li";
    }
    
      public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    
     public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String activeInactiveDD2()
    {
        return "//div[@id='control_085C9EBC-20A8-464F-A128-1D13C086EE6D']/div/a/span/ul/li";
    }

    public static String deleteButton()
    {
        return "//*[@id=\"btnDelete_form_10C56277-BF3D-4C75-BE8A-F6F46BB33480\"]";
    }
    
    
    public static String ButtonOK()
    {
       return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }
      public static String ButtonConfirm()
    {
        return"//div[@id='btnConfirmYes']";
    }
    
      public static String DeleteButton()
    {
        return "//div[@id='btnDelete_form_10C56277-BF3D-4C75-BE8A-F6F46BB33480']";
    }

    public static String deletePopUpButton()
    {
        return "//*[@id=\"btnConfirmYes\"]";
    }

    public static String activeInactiveSearch()
    {
        return " (//input[contains(@class,'select3-input')])[1]";
    }

    public static String activeInactiveSearch1()
    {
        return " (//input[contains(@class,'select3-input')])[1]";
    }

    public static String activeInactiveSearch2()
    {
        return " //div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String activeInactiveSearch3()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }
    
     public static String businessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String InspectionScheduler_Add()
    {
        return "//div[@id='btnActAddNew']//div[text()='Add']";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String searchButton()
    {
        return "//div[@id='btnActApplyFilter']/div";
    }

    public static String cancelFilterButton()
    {
        return "//div[@id='btnActCancel']/div";
    }

    public static String refreshResults()
    {
        return "//*[@id=\"btnActSearch\"]/div[2]";
    }

    public static String filterChevron()
    {
        return "//*[@id=\"btnActFilter\"]/div[1]/div";
    }

    public static String clickRecordAgain()
    {
        return "//*[@id=\"grid\"]/div[3]/table/tbody/tr[1]/td[5]";
    }

    public static String clickRecord()
    {
        return "//div[@id='grid']/div[3]/table/tbody/tr/td[10]";
    }

    public static String clickRecord1()
    {
        return "(//div[@id='grid']/div[3]/table/tbody/tr/td[6])[31]";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }
    
     public static String Text4(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"')]";

    }

    public static String InspectionScheduler_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_10C56277-BF3D-4C75-BE8A-F6F46BB33480']";
    }

    public static String InspectionScheduler_ProcessFlow1()
    {
        return "//*[@id=\"btnProcessFlow_form_C8563EE3-091D-4C7B-B946-1C0B2C0B86EB\"]/span";
    }

    public static String businessUnitSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String functionalSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
        // return "(//input[contains(@placeholder,'Type to search')])[3]";
    }

    public static String supportingDocsTab()
    {
        return "//*[@id=\"control_9057C2FA-65BC-47D0-80AC-A780487E2170\"]/div[9]/div[1]";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String titleURL()
    {
        return "//input[@id='urlTitle']";
    }

    public static String btnYesURL()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String LinkURL1()
    {
        return "//div[@class='linkbox-choice']//div/b[2]";
    }

    public static String responsibleSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    
        public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }
    public static String ownerSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String teamSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String dropClick1()
    {
        return "//div[9]/div/a/span[2]/b[1]";
    }

    public static String statusDropClick()
    {
        return "//*[@id=\"searchOptions\"]/div[3]/table/tbody/tr[3]/td[5]/div[1]/a/span[2]/b[1]";
    }

    public static String inspectionSearch()
    {

        return "(//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String inspectionSearch1()
    {

        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String inspectionSelect()
    {
        //return " //*[@class='jstree-anchor']/text()";
        return "//li[@id='50e07298-4754-4321-95be-3ab02c94b8e8']/a";
    }

    public static String inspectionSelect1()
    {
        return "//*[@id=\"52802b57-3138-4285-91b3-0baa2ccefe92_anchor\"]";
    }

    public static String checklistSelect()
    {
        return "//a[@id='e9ddb316-5034-4eef-ae5c-efc2ef3cc035_anchor']/i";
    }
    
        public static String checklistSelectOption(String Data)
    {
        return "//a[text()='"+Data+"']/..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String checklistSelect1()
    {
        return "//*[@id=\"9afd7109-c6c8-40b1-ad8e-5f3ee6129432_anchor\"]/i[1]";
    }

    public static String controlSelect()
    {
        return "//*[@id=\"39929ec0-ba8b-4dad-85d9-664e890110f1\"]/i";
    }

    public static String controlSelect1()
    {
        return "//*[@id=\"39929ec0-ba8b-4dad-85d9-664e890110f1_anchor\"]";
    }

    public static String dropClick()
    {
        return "//*[@id=\"control_7A04BA0A-2785-41C7-95C8-C83E902BF783\"]/div[1]/a/span[2]/b[1]";
    }

    public static String businessUnitSelect()
    {
        // return "/li[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c']/a";
        //return"//*[@id="4cee9a75-7667-44e9-a0c1-77ad5092e86c_anchor"]/i";
        return "//li/a/i";
    }

    public static String functionalSelect()
    {
        return "//*[@id=\"65f0ce43-a3e9-42e6-b5c0-c841f08e8670_anchor\"]/i";
    }

    public static String responsibleSelect()
    {
        return "//*[@id=\"b8ad5f90-d582-46c4-b186-d99649824acd_anchor\"]/i";

    }

    public static String statusSelect()
    {
        return "(//li[@id='5868055a-5368-4f65-b9df-c6220408fb8c']/a)/i[1]";

    }

    public static String statusSelect1()
    {
        return "(//li[@id='396a9071-2420-4f72-a986-171091d0c3ba']/a)[1]";

    }

    public static String statusSelect2()
    {
        return "//li[@id='396a9071-2420-4f72-a986-171091d0c3ba']/a";

    }

    public static String ownerSelect()
    {
        return "(//li[@id='b8ad5f90-d582-46c4-b186-d99649824acd']/a)[2]";
    }

    public static String teamSelect()
    {
        return "(//a[@id='b8ad5f90-d582-46c4-b186-d99649824acd_anchor']/i)[3]";
        // return"//*[@id=\"b8ad5f90-d582-46c4-b186-d99649824acd_anchor\"]/i[1]";
        // return "(//a[@id='b8ad5f90-d582-46c4-b186-d99649824acd_anchor']//i[@class='jstree-icon jstree-checkbox']";

    }

    public static String teamFrame()
    {

        return "//*[@id=\"select3_23a6b0b6\"]/ul[1]";

    }

    public static String popUp()
    {

        return "//div[@id='divConfirm']/div/div";

    }

    public static String InspectionSchedulerRecurrence_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_C8563EE3-091D-4C7B-B946-1C0B2C0B86EB']";
    }

    public static String InspectionScheduler_Dropdown()
    {
        return "//div[@id='control_BFE2139A-F39D-4F4E-BD6D-572B68546C05']";
    }

    public static String BusinessUnit_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String functional_Dropdown()
    {
        return "//div[@id='control_0325B29B-8410-4B30-80B5-783BA7D28499']/div/a/span/ul/li";
    }

    public static String responsible_Dropdown()
    {
        return "//*[@id=\"control_01B1845E-0081-4304-B0C6-239D8B2B144A\"]/div[1]/a/span[1]/ul/li";
    }

    public static String responsible_Dropdown1()
    {
        return "//li[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c']";
    }

    public static String team_Dropdown()
    {
        return "//div[@id='control_8F14A7C7-310A-4836-8190-B9DCD966C7F2']/div/a/span/ul";
    }

    public static String team_Dropdown1()
    {
        return "//div[@id='control_C7BC891B-0559-4895-B1D4-FFF3B498BE59']/div/a/span/ul/li";
    }

    public static String owner_Dropdown()
    {
        return "//*[@id=\"control_23E99F78-777B-430C-91F5-F30AA1DA3F27\"]/div[1]/a/span[1]/ul/li";

    }
      public static String Owner_Dropdown()
    {
        return "//div[@id='control_23E99F78-777B-430C-91F5-F30AA1DA3F27']//ul";

    }

    public static String owner_Dropdown1()
    {
        return "//div[@id='control_0325B29B-8410-4B30-80B5-783BA7D28499']/div/a/span/ul/li";

    }

    public static String control_Dropdown()
    {
        return "//div[@id='control_A02469B0-042B-407A-A2CD-E0FE300EC1DF']//ul";
    }

    public static String inspectionType_Dropdown()
    {
        return "//div[@id='control_C7BC891B-0559-4895-B1D4-FFF3B498BE59']/div/a/span/ul/li";
    }

    public static String checklist_Dropdown()
    {
        return "//*[@id=\"control_7A04BA0A-2785-41C7-95C8-C83E902BF783\"]";
    }

    public static String checklist_Search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String checklist_Search1()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String control_Search()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String BusinessUnit_Option1()
    {

        return "(//input[contains(@placeholder,'Type and enter to search')])[1]";
    }

    public static String ImpactType_Dropdown()
    {
        return "//div[@id='control_8876D62C-BC85-4F02-A389-D78DC27E54BB']//li";
    }

    public static String nameOfInspectionField()
    {

        return "//div[@id='control_F1F3CF3D-7BFE-44CA-A915-A6281CB0466C']/div/div/input";
    }

    public static String ImpactType_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String CloseImpactType_Dropdown()
    {
        return "//div[@id='control_8876D62C-BC85-4F02-A389-D78DC27E54BB']//b[@class='select3-down drop_click']";
    }

    public static String InspectionType_Dropdown()
    {
        return "//div[@id='control_C7BC891B-0559-4895-B1D4-FFF3B498BE59']";
    }

    public static String Project_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String ReplicateInspectionTo_Dropdown()
    {
        return "//div[@id='control_D9058671-DFF1-4B10-AC86-D18E77DC95B2']";
    }

    public static String ApplicableUsers_Option(String data)
    {
        return "//div[@id='control_8777C05F-0A7B-4735-B937-19DC86E69BFF']//a[text()='" + data + "']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String SaveButton()
    {
        return "//div[@id='btnSave_form_10C56277-BF3D-4C75-BE8A-F6F46BB33480']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String recordSavedNumber()
    {
        return "//*[@id=\"divPager\"]/div[1]";
    }

    public static String validateSave()
    {
        //return "//div[@class='ui floating icon message transition visible']";

        return "//div[@id='divMainNotif']/div";
    }

    public static String InspectionType_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String ApplicableGroups_Option(String data)
    {
        return "//div[@id='control_B912FBD3-878C-45BD-B9A9-ACA0D45A9383']//a[text()='" + data + "']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String InspectionSchedulerRecurrence_Panel()
    {
        //return "//span[text()='Inspection Scheduler Recurrence']";
        return "/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[17]/div[9]/div[1]/i";

    }

    public static String Recurrence_Add()
    {
        return "//div[@id='btnAddNew']";
    }

    public static String Recurrence_ProcessFlow()
    {
        //return "//div[@id='btnProcessFlow_form_C8563EE3-091D-4C7B-B946-1C0B2C0B86EB']";
        return "//*[@id=\"btnProcessFlow_form_10C56277-BF3D-4C75-BE8A-F6F46BB33480\"]/span";
    }

    public static String StartDate()
    {
        return "//div[@id='control_0F04A24A-49B8-4229-84E1-3816AF826DD7']//input[@type='text']";
    }

    public static String RecurrenceFrequency_Dropdown()
    {
        return "//div[@id='control_1FD95F7C-2974-4DFD-BAE6-8693FA639CFA']";
    }

    public static String RecurrenceFrequency_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String Frequency_CheckBox()
    {
        return "//div[@id='control_368F387B-E638-4592-A429-7397542FE787']//div[@class='c-chk']";
    }

    public static String RecurrenceFrequency_SaveButton()
    {
        return "//div[@id='btnSave_form_C8563EE3-091D-4C7B-B946-1C0B2C0B86EB']";
    }

    public static String RecurrenceFrequency_SaveCloseButton()
    {
        return "//div[@id='btnSaveClose_form_C8563EE3-091D-4C7B-B946-1C0B2C0B86EB']";
    }

    public static String RecurrenceFrequency_SaveCloseButton1()
    {
        return "//*[@id=\"btnSaveClose_form_10C56277-BF3D-4C75-BE8A-F6F46BB33480\"]";
    }

    public static String deletConButton()
    {
        return "(//div[@id='btnHideAlert']/div)[2]";
    }

    public static String RecurrenceFrequency_SaveDropButton()
    {
        return "//*[@id=\"btnSave_form_C8563EE3-091D-4C7B-B946-1C0B2C0B86EB\"]/div[1]";
    }

    public static String RecurrenceFrequency_SaveDropButton1()
    {
        return "//*[@id=\"btnSave_form_10C56277-BF3D-4C75-BE8A-F6F46BB33480\"]/div[1]/div";
    }

    public static String RecurrenceFrequency_tickBox()
    {
        // return "//div[@id='control_368F387B-E638-4592-A429-7397542FE787']/div/div";
        return "//div[@id=\"control_368F387B-E638-4592-A429-7397542FE787\"]/div[1]/div";
    }

    public static String NumberOfDays()
    {
        return "//div[@id='control_0F94299B-F336-4626-9839-554CD71BF769']//input[@type='number']";
    }

    public static String EndDate()
    {
        return "//div[@id='control_DCC55A98-9EAA-452B-9823-783816390399']//input[@type='text']";
    }

    public static String DayOfTheWeek_Dropdown()
    {
        return "//div[@id='control_38888542-B6ED-4900-9C0F-E6BB11DFB754']";
    }

    public static String WeekOfMonth_Dropdown()
    {
        return "//div[@id='control_69B70CD4-110F-4DFB-ADA9-C3E974F7728B']";
    }

    public static String AnnuallyFrom_Dropdown()
    {
        return "//div[@id='control_0AD5C3DC-29C4-4F21-B8C6-E008B836B316']";
    }

    public static String Inspection_Scheduler_Recurrence_Heading()
    {
        return "//div[text()='Inspection Scheduler Recurrence']";

    }

    public static String Inspection_Scheduler_Recurrence_Label()
    {
        return "//div[@class='c-pnl']//i";

    }

    public static String Inspection_Scheduler_Recurrence_Add_Button()
    {
        return "//div[@id='btnAddNew']//div[text()='Add']";
    }

    public static String RecurrenceFrequency_Dropdown_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";

    }

    public static String InspectionSchedulerRecurrenceRecord()
    {
        return"(//div[text()='Inspection Scheduler Recurrence']/../..//div[@class='bottom']//table[@role='grid'])//tbody//tr[1]";
    }

    public static String ActiveInactive_Dropdown()
    {
        return"//div[@id='control_085C9EBC-20A8-464F-A128-1D13C086EE6D']//ul";
    }

    public static String RecordInTable()
    {
        return"(//div[contains(@class,'form transition visible active')]//div[@id='divSearchWrapper']//table//tbody)[2]//tr[1]";
    }

    public static String DeleteButtonPopUp()
    {
        return"//div[@id='grid_C8563EE3-091D-4C7B-B946-1C0B2C0B86EB_context']//a[text()='Delete']";
    }

}
