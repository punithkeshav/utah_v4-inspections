/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_PageObject;

/**
 *
 * @author SKhumalo
 */
public class Project_Management_PageObjects
{

    public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String ContainsTextBox()
    {
        return "(//input[@class='txt border'])[1]";
    }

    public static String SearchOption()
    {
        return "//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String SupportingDocumentsTab()
    {
        return "(//div[text()='Supporting Documents'])[1]";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String linkADoc_button()
    {
        return "(//b[@original-title='Link to a document'])[2]";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String recordSaved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordSaved_popup1()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }

    public static String recordSaved_popup2()
    {
        return "(//div[@class='ui floating icon message transition visible']//div[contains(text(),'Record saved')])";
    }

    public static String recordNoChanges_Saved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record has no changes to save')])[1]";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String DeleteButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Delete']";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String uploadLinkbox()
    {
        return "//div[@class='linkbox-options']//b[@class='linkbox-link']";
    }

    public static String businessUnitexpand(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule3()
    {
        return "//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[ contains(text(),'" + text + "')]";

    }

    public static String Text_2(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "'])[2]";

    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String Actions_pf()
    {
        return "//div[@id='btnProcessFlow_form_F54AE783-1378-4974-A7E8-4B5D1C4A79F3']";
    }

    public static String BudgetExtension_Checkbox()
    {
        return "//div[@id='control_1D35A9A4-3615-4BE5-B4F9-1238EC675264']//div//div";
    }

    public static String projectClose()
    {
        return "//div[@id='form_D1FDADB7-365B-4404-86D2-CA8FC2DE7FE8']/div/i[2]";
    }

    public static String projectClose1()
    {
        return "//div[@id='form_D1FDADB7-365B-4404-86D2-CA8FC2DE7FE8']/div/i[2]";
    }

    public static String BusinesssUnit_Checkbox()
    {
        return "//div[@id='control_3EF346F7-9D0F-45AB-A19F-3E674291E795']//div[@class='c-chk']";
    }

    public static String projectApprovalTab()
    {
        return "//div[text()='Project Approval']";
    }

    public static String groupApporoval_Checkbox()
    {
        return "//*[@id=\"control_1A10F073-629F-402F-ABA6-5EDB4613C198\"]/div[1]/div";
        //  return"//*[@id=\"control_1A10F073-629F-402F-ABA6-5EDB4613C198\"]/div[1]/div";
    }

    public static String budgetExtension_Checkbox()
    {
        return "//*[@id=\"control_1D35A9A4-3615-4BE5-B4F9-1238EC675264\"]/div[1]/div";
    }

    public static String Expenditure(int i)
    {
        return "(//div[@id='control_EB59C8C5-1747-4893-9EF3-37AD3077A53D']//input)[" + i + "]";
    }

    public static String PA_Quarter_Dropdown(int y)
    {
        return "(//div[@id='control_09A9662E-8EDC-4BEA-87C3-0C62227BCFF5'])[" + y + "]";
    }

    public static String PA_Year_Dropdown(int y)
    {
        return "(//div[@id='control_8B94E076-D52E-415B-BC9F-AE2A6E65EA76'])[" + y + "]";
    }

    public static String reportingFrequencyDD()
    {
        return "//*[@id=\"control_6B3D7F66-D515-47C0-8751-98B399656D8E\"]/div[1]/a/span[2]/b[1]";
    }

    public static String PercentageOfTotalBudget()
    {
        return "(//div[@id='control_39298B05-8ABA-4918-83CD-572F5B96390F']//input)[1]";
    }

    public static String ProjectActions_CloseButton()
    {
        return "(//div[@id='form_F54AE783-1378-4974-A7E8-4B5D1C4A79F3']//i[@class='close icon cross'])[1]";
    }

    public static String ProjectActions_Record(String data)
    {
        return "((//div[@id='control_4F298569-07E5-4373-83F4-300C9C350ACF']//div//table)[3]//div[text()='" + data + "'])[1]";
    }

    public static String ProjectActuals_Panel()
    {
        return "//span[text()='Project Actuals']";
    }

    public static String teamNameDD()
    {
        return "//*[@id=\"control_12699924-3386-4657-B242-21404757E151\"]/div[1]/a/span[2]/b[1]";
    }

    public static String totalCosts_Panel()
    {
        return "//span[text()='Total costs']";
    }

    public static String paybackPeriodInYears()
    {
        return "//div[@id='control_C981D6F2-A95F-4B83-87A6-5864488FBD93']/div/div/input";
    }

    public static String simplePayback()
    {
        return "//div[@id='control_FBD8966F-CB4D-4B5A-B182-AC5E901270B0']/div/div/input";
    }

    public static String netPresentValue()
    {
        return "//div[@id='control_39FE0AE0-BA7F-4D9F-BCF8-A4D0E33CE97D']/div/div/input";
    }

    public static String costBeforeIncentives()
    {
        return "//div[@id='control_60594A68-0C5A-4409-8E25-810B54511EB0']/div/div/input";
    }

    public static String totalCost()
    {
        return "//div[@id='control_FE4F54B8-5E2C-47E7-825E-D0DC8FB785EA']/div/div/input";
    }

    public static String rOIPercentage()
    {
        return "//div[@id='control_6CE31FE7-FEB4-436E-BEA3-F1F980F8E58A']/div/div/input";
    }

    public static String internalRateOfReturn()
    {
        return "//div[@id='control_939012E2-E17E-4161-9AE1-FBF832FC4208']/div/div/input";
    }

    public static String loanCost()
    {
        return "//div[@id='control_24C24F48-A617-4B22-BD64-A1AA6AAE2A82']/div/div/input";
    }

    public static String expectedCosts()
    {
        return "//div[@id='control_8AFCEDFA-1DFB-499D-84F0-66B208796C88']/div/div/input";
    }

    public static String budgetedForThisPeriod()
    {
        return "//div[@id='control_FCD35FA1-9033-4961-A302-8AA9451E838E']/div/div/input";
    }

    public static String expenditure()
    {
        return "//div[@id='control_EB59C8C5-1747-4893-9EF3-37AD3077A53D']/div/div/input";
    }

    public static String actualCosts()
    {
        return "//div[@id='control_0CAE4516-3D66-4B45-9B23-1C7A82CA3413']/div/div/input";
    }

    public static String comments()
    {
        return "//div[@id='control_A548DCD9-D4BE-49AE-B3E0-BA5DD9F1DFE1']/div/div/textarea";
    }

    public static String comments1()
    {
        return "//div[@id='control_6AA1FBC9-1B91-48A3-8BD3-6C7897CDB165']/div/div/textarea";
    }

    public static String comments2()
    {
        return "//div[@id='control_57B502E1-6C11-4074-8ADA-9241754F6C6F']/div/div/textarea";
    }

    public static String financialIncentives()
    {
        return "//div[@id='control_7D8C168E-E149-471C-A72F-C96C05A09BAE']/div/div/input";
    }

    public static String projectRunningCostsPanel()
    {
        return "//span[text()='Project Running Cost']";
    }

    public static String ProjectFinancials_Tab()
    {
        return "//div[text()='Project Financials']";
    }

    public static String eco2Man_Tab()
    {
        return "//div[text()='ECO2Man']";
    }

    public static String ForecastedBudget_Panel()
    {
        return "//span[text()='Forecasted Budget']";
    }

    public static String Quarter_Dropdown()
    {
        return "//div[@id='control_E0BA8E50-4E2F-4EFB-A7B8-278823A61B83']";
    }

    public static String RemainingBudget()
    {
        return "(//div[@id='control_6DDE36C9-5245-4A3F-A867-824B4DAF7BEE']//input)[1]";
    }

    public static String Supporting_Doocuments_tab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String TotalBudget()
    {
        return "(//div[@id='control_A56EC1D2-826D-4F38-AE2E-8EEA7D8B72F4']//input)[1]";
    }

    public static String budgetExtension()
    {
        return "(//div[@id='control_A56EC1D2-826D-4F38-AE2E-8EEA7D8B72F4']//input)[1]";
    }

    public static String TotalBudgetWithExtension()
    {
        return "(//div[@id='control_A43D2E22-8796-41E7-A189-280A5AAB4551']//input)[1]";
    }

//    public static String TotalExtension()
//    {
//        return "//div[@id='control_B3EB88DA-F550-4AF0-AE8D-0167244448C8']/div/div/input";
//    }
    public static String TotalExtension_Label()
    {
        return "//div[text()='Total extension']";
    }

    public static String Total_Of_Budget()
    {
        return "//div[@id='control_D5AE2A39-9DF9-4BB3-95EA-8FC3E718A312']//input";
    }

    public static String Year_Dropdown()
    {
        return "//div[@id='control_8B94E076-D52E-415B-BC9F-AE2A6E65EA76']/div/a/span[2]/b";
    }

    public static String ecoYear_Dropdown()
    {
        return "//*[@id=\"control_45F50355-91F4-453E-8BA5-27960EA63317\"]/div[1]/a/span[2]/b[1]";
    }

    public static String activeInactive_Dropdown()
    {
        return "//div[@id='control_2B65C98F-21C8-4F18-A9B1-53955C3E053C']/div/a/span[2]/b";
    }

    public static String actualForcast_Dropdown()
    {
        return "//div[@id='control_784368E3-FC35-4E6E-B282-A3C57BF9749A']/div/a/span[2]/b";
    }

    public static String forcastedYear_Dropdown()
    {
        return "//div[@id='control_150EC693-165C-4E73-BEBB-504FBD502D82']/div/a/span[2]/b";
    }

    public static String Month_Dropdown()
    {
        return "//div[@id='control_D7960673-32FB-45E2-AE62-A23E9DD56AB5']/div/a/span[2]/b";
    }

    public static String ecoMonth_Dropdown()
    {
        return "//div[@id='control_40F5E1DE-02A0-4792-91BB-CCD5B87E86FC']/div/a/span[2]/b";
    }

    public static String forcastedMonth_Dropdown()
    {
        return "//div[@id='control_08E434A9-DE22-411F-BBF3-2C3571D40DDD']/div/a/span[2]/b";
    }

    public static String approvePark_Dropdown()
    {
        return "//div[@id='control_2C9ACBE2-77AD-412D-B47B-BDD8CC3E879A']/div/a/span[2]/b";
    }

    public static String projectComplete_Dropdown()
    {
        return "//div[@id='control_29349D39-CD39-4D5C-9540-5A484828AEB8']/div/a/span[2]/b";
    }

    public static String Quater_Dropdown()
    {
        return "//div[@id='control_09A9662E-8EDC-4BEA-87C3-0C62227BCFF5']/div/a/span[2]/b";
    }

    public static String emmisionSource_Dropdown()
    {
        return "//*[@id=\"control_FE707531-9D1D-4591-86A9-6E667856415B\"]/div[1]/a/span[2]/b[1]";
    }

    public static String unit_Dropdown()
    {
        return "//*[@id=\"control_2F9EA8A6-E0B0-46C7-ABDE-030F8A2692CC\"]/div[1]/a/span[2]/b[1]";
    }

    public static String approvedEco2ManIndicatorDD()
    {
        return "//div[@id='control_C0B14A30-1FAD-4AA3-9660-571C8BEA7D30']/div/a/span[2]/b";
    }

    public static String target()
    {
        return "//div[@id='control_4BF33F0A-9B7D-44F4-ADFD-75B9915C3B98']/div/div/input";
    }

    public static String unit1_Dropdown()
    {
        return "//*[@id=\"control_2ED61E8F-494F-49AC-AC2F-9ED47E59AD05\"]/div[1]/a/span[2]/b[1]";
    }

    public static String measurement()
    {
        return "//div[@id='control_18C58CFC-2E75-44F3-A490-E6833BA28142']/div/div/input";
    }

    public static String ecoQuater_Dropdown()
    {
        return "//div[@id='control_B9379CD8-9321-422E-A41F-234FC59BB47F']/div/a/span[2]/b";
    }

    public static String forcastedQuater_Dropdown()
    {
        return "//div[@id='control_E0BA8E50-4E2F-4EFB-A7B8-278823A61B83']/div/a/span[2]/b";
    }

    public static String SaveSupportingDocuments_Button()
    {
        return "//div[text()='Save supporting documents']";
    }

    public static String Budget_tab()
    {
        return "//div[text()='Budget']";
    }

    public static String StrategicObjectives_tab()
    {
        return "//div[text()='Strategic Objectives']";
    }

    public static String LinkedEngagements_tab()
    {
        return "//div[text()='Linked Engagements']";
    }

    public static String LinkedEngagementsSearch()
    {
        return "//div[@id='btnFilter']/div";
    }

    public static String LinkedEngagementsSearch1()
    {
        return "//div[@id='btnActApplyFilter_C5D7993E-A223-4AE0-A15D-119FE22E21DC_smc13']/div";
    }

    public static String ProjectActions_tab()
    {
        return "//div[text()='Project Actions']";
    }

    public static String Actions_desc()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String Actions_deptresp_dropdown()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }

    public static String Actions_deptresp_select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String Actions_deptresp_select1(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }

    public static String Actions_respper_dropdown()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }

    public static String Actions_respper_select(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }

    public static String Actions_ddate()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String Actions_save()
    {
        return "//div[@id='btnSave_form_F54AE783-1378-4974-A7E8-4B5D1C4A79F3']";
    }

    public static String Actions_savewait()
    {
        return "(//div[text()='Action Feedback'])[1]";
    }

    public static String DueDiligenceEndDate()
    {
        return "//div[@id='control_DC137F55-ABC4-4DDD-93A1-0EA9C0070A86']//input";
    }

    public static String DueDiligenceNotes()
    {
        return "(//div[@id='control_3D5045CD-51F0-4805-93B1-9FAE68008EB5']//textarea)[1]";
    }

    public static String DueDiligenceStartDate()
    {
        return "//div[@id='control_AD0BB29D-A2C2-45FB-9CFC-1EB133D86E62']//input";
    }

    public static String ObjectivesAndProposedActivities()
    {
        return "//div[@id='control_6860687C-65AE-45F7-BDDA-8CDEA75D220F']//textarea";
    }

    public static String PlannedCompletionDate()
    {
        return "//div[@id='control_2696F1DC-0178-43DA-8B70-91507F331D0E']//input";
    }

    public static String PlannedStartDate()
    {
        return "//div[@id='control_5074F575-966B-494C-B1ED-67E9183DA29B']//input";
    }

    public static String actionDueDate()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']/div/span/span/input";
    }

    public static String ProjectOriginator_dropdown()
    {
        return "//div[@id='control_9980D6DE-BCBF-457A-BCAC-A88401FC774F']//ul";
    }

    public static String groupVerification_dropdown()
    {
        //return "//*[@id=\"control_65874E3C-E890-439D-9391-CB2C5C097F2D\"]";
        return "//*[@id=\"control_65874E3C-E890-439D-9391-CB2C5C097F2D\"]/div[1]/a/span[2]/b[1]";
    }

    public static String technicalFeasibilityDD()
    {
        return "//*[@id=\"control_83F381E5-DD3B-4008-9CCC-0F0E1E0F035C\"]/div[1]/a/span[2]/b[1]";
    }

    public static String budgetDD()
    {
        return "//*[@id=\"control_3F6F4219-6F71-485F-A6C7-753117E42AEE\"]/div[1]/a/span[2]/b[1]";
    }

    public static String humanResourcesDD()
    {
        return "//*[@id=\"control_1ECF82CA-EA5E-42FC-89FF-112BA6C9FDF5\"]/div[1]/a/span[2]/b[1]";
    }

    public static String businessUnitVerificationDD()
    {
        return "//*[@id=\"control_9980D6DE-BCBF-457A-BCAC-A88401FC774F\"]/div[1]/a/span[2]/b[1]";
    }

    public static String Function_dropdown()
    {
        return "//div[@id='control_41A605ED-98FD-4802-BC9D-2FCA94269092']//ul";
    }

    public static String projectCategoryDD()
    {
        return "//*[@id=\"control_47DF413F-B814-46A5-902C-E084306C7D3F\"]/div[1]";
    }

    public static String projectTypeDD()
    {
        return "//*[@id=\"control_F1E07791-01A9-4375-9B8C-96AF11CAEDC7\"]/div[1]";
    }

    public static String ProjectStatus_Dropdown()
    {
        return "//div[@id='control_A6891E66-8FCB-44C6-A96B-5E1B8291B66B']";
    }

    public static String Project_Description()
    {
        return "(//div[@id='control_A85BCF71-D318-48A6-8BFD-97C66773E72A']//textarea)[1]";
    }

    public static String technicalFeasibilityComments()
    {
        return "//div[@id='control_8B96ED08-C19E-42D9-9B8C-B77CFAAEDD2C']//textarea";
    }

    public static String budgetComments()
    {
        return "//*[@id=\"control_A5D45285-68EB-4A87-B9B8-A7B91D98FC41\"]/div[1]/div/textarea";
    }

    public static String humanResourceComment()
    {
        return "//*[@id=\"control_9127E5E2-5433-4386-8544-1EBA5CA00ADA\"]/div[1]/div/textarea";
    }

    public static String Project_Input()
    {
        return "(//div[@id='control_25EB4FB1-8A46-4B5C-9419-D850BAC7D5C0']//input)[1]";
    }

    public static String SaveToContinue_saveBtn()
    {
        return "//div[@id='control_EE0FBD60-BD61-43F4-8795-CB976E20E55F']//div[text()='Save to continue']";
    }

    public static String SpecifyOriginator()
    {
        return "(//div[@id='control_15207237-9464-4AC8-B349-9C29A4A3D384']//input)[1]";
    }

    public static String SubThemeType_SelectAll()
    {
        return "//div[@id='control_0BC3832E-6D79-42DF-9F26-657403886FD3']//b[@class='select3-all']";
    }

    public static String KeyWords_SelectAll()
    {
        return "//div[@id='control_AAC6688B-16F5-4BBD-B954-0DC276F467EA']//b[@class='select3-all']";
    }

    public static String TotalNumberOfBeneficiaries()
    {
        return "(//div[@id='control_CAF62539-DE9D-432F-B307-09DC6B7C7EB0']//input)[1]";
    }

    public static String impactType_SelectAll()
    {
        return "//div[@id='control_9A3C6D10-271D-4BAE-86DE-B2FA885D3F00']//b[@class='select3-all']";
    }

    public static String themeType_SelectAll()
    {
        return "//div[@id='control_715A0D37-4AC8-4BE7-95B2-71553DB78C68']//b[@class='select3-all']";
    }

    public static String navigate_EHS()
    {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }

    public static String project_Management()
    {
        return "//label[contains(text(),'Project Management')]";
    }

    public static String viewDasboard()
    {
        return "//label[contains(text(),'Dashboard')]";
    }

    public static String navigateButton()
    {
        return "//div[@id='#common-navigation']/vzl-button/div";
    }

    public static String appOverView()
    {
        return "//div[@id='rlui-popover-1']/div/ul/div/li/div[2]";
    }

    public static String searchFilter()
    {
        return "//div[@id='btnActApplyFilter']/div";
    }

    public static String deleteRecord()
    {
        return "//div[@id='btnDelete_form_D1FDADB7-365B-4404-86D2-CA8FC2DE7FE8']";
    }

    public static String yesButton()
    {
        return "//div[@id='btnConfirmYes']/div";
    }

    public static String okButton()
    {
        return "(//div[@id='btnHideAlert']/div)[2]";
    }

    public static String selectRecord()
    {
        return "//div[@id='grid']/div[3]/table/tbody/tr/td[5]";
    }

    public static String Add_Button()
    {
        return "//div[text()='Add']";
    }

    public static String ForecastedBudget_Add_Button()
    {
        return "(//div[@id='btnAddNew']/div)[2]";
    }

    public static String ProjectActuals_Add_Button()
    {
        return "//div[@id='control_7603A440-AC0A-4EE4-82F2-33FF78507A66']//div[text()='Add']";
    }

    public static String ProjectActions_Add_Button()
    {
        return "//div[@id='btnAddNew']/div";
    }

    public static String eco2Man_Add_Button()
    {
        return "//div[@id='btnAddNew']";
    }

    public static String energySavings_Add_Button()
    {
        return "//div[@id='control_8093F136-0AFB-4B29-9842-5AB063DF577C']/div/div/div[2]/div";
    }

    public static String businessUnit_Option1(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String text_Search1(String text)
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String text_Search2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String businessUnit_Option(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }

    public static String businessUnit_dropdown()
    {
        return "//div[@id='control_1520F92C-A849-4812-8D9D-48C3B8A475C1']";
    }

    public static String typeOfAction_dropdown()
    {
        return "//*[@id=\"control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE\"]/div[1]/a/span[2]/b[1]";
    }

    public static String entity_dropdown()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']/div/a/span[2]/b";
    }

    public static String responsiblePerson_dropdown()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']/div/a/span[2]/b";
    }

    public static String agency_dropdown()
    {
        return "//div[@id='control_5B580F56-394D-4695-8AB2-C2CB9AAE9EB9']/div/a/span[2]/b";
    }

    public static String actionDescription()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']/div/div/textarea";
    }

    public static String fullName_Dropdown()
    {
        return "//div[@id='control_6EE2438D-2B28-4A11-8E8E-B81E57A86B27']";
    }

    public static String impactType_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String impactType_Dropdown()
    {
        return "//div[@id='control_9A3C6D10-271D-4BAE-86DE-B2FA885D3F00']";
    }

    public static String Theme_Dropdown()
    {
        return "//div[@id='control_715A0D37-4AC8-4BE7-95B2-71553DB78C68']//ul";
    }

    public static String forcastedTheme_Dropdown()
    {
        return "//div[@id='control_D18498F9-9022-47BF-A73C-BF3E30A58FBA']/div/a/span[2]/b";
    }

    public static String Theme_Option(String data)
    {
        return "//a[contains(text(),'" + data + "')]/..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String theme_Select1()
    {
        return "//*[@id=\"64b0960b-eda4-434b-ba39-275d09f5571a_anchor\"]/i[1]";
    }

    public static String Charge_Code()
    {
        return "//label[text()='Charge Code']";
    }

    public static String select_Entity()
    {
        return "//*[@id=\"afbdce18-71a6-4b96-8e0c-1ce6c85254f2_anchor\"]/i[1]";
    }

    public static String Entitydrop_click()
    {
        return "//*[@id=\"control_1520F92C-A849-4812-8D9D-48C3B8A475C1\"]/div[1]/a/span[2]/b[1]";
    }

    public static String themedrop_click()
    {
        return "//*[@id=\"control_715A0D37-4AC8-4BE7-95B2-71553DB78C68\"]/div[1]/a/span[2]/b[1]";
    }

    public static String LabTestDetails()
    {
        return "(//div[@id='control_A899D7B2-06C7-437E-BD53-7A8E8CD0F8DA']//input)[1]";
    }

    public static String LabTestDetails_TestType()
    {
        return "(//div[@id='control_A899D7B2-06C7-437E-BD53-7A8E8CD0F8DA']//input)[3]";
    }

    public static String Laboratory_Panel()
    {
        return "//span[text()='Laboratory']";
    }

    public static String MaintenanceLab_Add_Button()
    {
        return "//div[text()='Maintenance - Lab']//..//div[text()='Add']";
    }

    public static String MaintenanceRadiology_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_456DDB8E-4CF6-4324-A828-0D73B314E53D']";
    }

    public static String MaintenanceResultSet_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_6346CDCD-C59E-4331-A2D2-D52B1E5F8C5A']";
    }

    public static String MaintenanceResultSet_SaveButton()
    {
        return "//div[@id='btnSave_form_6346CDCD-C59E-4331-A2D2-D52B1E5F8C5A']//div[text()='Save']";
    }

    public static String MaintenanceTestType_AddButton()
    {
        return "//div[text()='Maintenance - Test type']//..//div[text()='Add']";
    }

    public static String ProjectActions_AddButton()
    {
        return "//div[text()='Project Actions']//..//div[text()='Add']";
    }

    public static String MaintenanceResultSet_AddButton()
    {
        return "//div[text()='Maintenance - Result set']//..//div[text()='Add']";
    }

    public static String MaintenanceLab_CloseButton()
    {
        return "(//div[@id='form_D0029D75-2491-4D6F-BBFD-E6D9D0D0D475']//i[@class='close icon cross'])[1]";
    }

    public static String MaintenanceResultSet_CloseButton()
    {
        return "(//div[@id='form_6346CDCD-C59E-4331-A2D2-D52B1E5F8C5A']//i[@class='close icon cross'])[1]";
    }

    public static String MaintenanceTestType_CloseButton()
    {
        return "(//div[@id='form_CBF09758-4352-418C-B417-14212578DBDB']//i[@class='close icon cross'])[1]";
    }

    public static String MaintenanceRadiology_CloseButton()
    {
        return "(//div[@id='form_456DDB8E-4CF6-4324-A828-0D73B314E53D']//i[@class='close icon cross'])[1]";
    }

    public static String MaintenanceLab_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_D0029D75-2491-4D6F-BBFD-E6D9D0D0D475']";
    }

    public static String MaintenanceTestType_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_CBF09758-4352-418C-B417-14212578DBDB']";
    }

    public static String MaintenanceLab_Record()
    {
        return "(//div[@id='control_7D9B22B2-19FC-4C18-8ADF-F0BEBDB897FD']//div//table)[3]";
    }

    public static String MaintenanceResultSet_Record()
    {
        return "(//div[@id='control_FDA44C35-02E9-4530-B53F-637C6B5C3F51']//div//table)[3]";
    }

    public static String MaintenanceRadiology_Record(String data)
    {
        return "(//div[@id='control_51764267-4ECB-4223-B334-66C7B2A9B8D6']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String MaintenanceRadiology_Add_Button()
    {
        return "//div[text()='Maintenance - Radiology']//..//div[text()='Add']";
    }

    public static String Maintenance_TestType()
    {
        return "//div[text()='Maintenance - Test type']";
    }

    public static String Maintenance_ResultSet()
    {
        return "//div[text()='Maintenance - Result set']";
    }

    public static String RadiologyDetails()
    {
        return "(//div[@id='control_958D0095-92F1-45C7-8912-9087DC89CB12']//input)[1]";
    }

    public static String Radiology_Panel()
    {
        return "//span[text()='Radiology']";
    }

    public static String Occupational_Health()
    {
        return "//label[text()='Occupational Health']";
    }

    public static String Projects()
    {
        return "//label[text()='Projects']";
    }

    public static String ProcessFlow_Button()
    {
        return "//div[@id='btnProcessFlow_form_D1FDADB7-365B-4404-86D2-CA8FC2DE7FE8']";
    }

    public static String actionsProcessFlow_Button()
    {
        return "//div[@id='btnProcessFlow_form_F54AE783-1378-4974-A7E8-4B5D1C4A79F3']/span";
    }

    public static String ResultSet()
    {
        return "(//div[@id='control_599EA408-079F-4003-BCF0-2F185C01E96F']//input)[1]";
    }

    public static String Type_Of_Test_Dropdown()
    {
        return "//div[@id='control_10275A0F-6E1A-4B14-A828-0CCA46FA7F46']";
    }

    public static String ProjectStatus_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String SaveBtn()
    {
        return "//div[@id='btnSave_form_D1FDADB7-365B-4404-86D2-CA8FC2DE7FE8']//div[text()='Save']";
    }

    public static String saveBtn1()
    {
        return "//div[@id='btnSave_form_2EF3D326-2BF6-4A97-87DF-3A7EC95ADDD6']/div[3]";
    }

    public static String saveBtn2()
    {
        return "//div[@id='btnSave_form_F54AE783-1378-4974-A7E8-4B5D1C4A79F3']/div[3]";
    }

    public static String editEco2man()
    {
        return "//i[@onclick=\"var recid = $(this).closest('tr').find('#recordID').val().toUpperCase(); var isMdgEdit = $(this).parents('div[sourceid=2EF3D326-2BF6-4A97-87DF-3A7EC95ADDD6]').attr('smctype'); var MDGControl = $(this).parents('div[sourceid=2EF3D326-2BF6-4A97-87DF-3A7EC95ADDD6]');var parentFormGrid = $(this).parents('div[sourceid=2EF3D326-2BF6-4A97-87DF-3A7EC95ADDD6]').parents('.form'); saveDataV2(function(valid) { if(valid === false){return;} showForm('2EF3D326-2BF6-4A97-87DF-3A7EC95ADDD6', '9A33000D-4B81-4A35-8AE1-24BB7C892E81', recid, isMdgEdit ,parentFormGrid, metrix.core.formBrowser.clearFormType.children, MDGControl, true, false,false) },parentFormGrid, undefined, parentFormGrid,null, false)\"]";
    }

    public static String MaintenanceLab_SaveButton()
    {
        return "//div[@id='btnSave_form_D0029D75-2491-4D6F-BBFD-E6D9D0D0D475']//div[text()='Save']";
    }

    public static String MaintenanceRadiology_SaveButton()
    {
        return "//div[@id='btnSave_form_456DDB8E-4CF6-4324-A828-0D73B314E53D']//div[text()='Save']";
    }

    public static String MaintenanceTestType_SaveButton()
    {
        return "//div[@id='btnSave_form_CBF09758-4352-418C-B417-14212578DBDB']//div[text()='Save']";
    }

    public static String validateSave()
    {
        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String Entity_dropdown()
    {
        return "//div[@id='control_1520F92C-A849-4812-8D9D-48C3B8A475C1']//ul";
    }

    public static String Select_EntityOption(String data)
    {
        return "//a[contains(text(),'" + data + "')]/..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String BusinessUnitVerificationAndApproval_dropdown()
    {
        return "//div[@id='control_2FF574BD-30CF-4FF5-91FF-BA377D274257']//ul";
    }

    public static String GroupVerificationAndApproval_dropdown()
    {
        return "//div[@id='control_65874E3C-E890-439D-9391-CB2C5C097F2D']//ul";
    }

    public static String Approve_ParkProject_dropdown()
    {
        return "//div[@id='control_2C9ACBE2-77AD-412D-B47B-BDD8CC3E879A']//ul";
    }

    public static String Group_Approval_Checkbox()
    {
        return "//div[@id='control_1A10F073-629F-402F-ABA6-5EDB4613C198']//div[@class='c-chk']";
    }

    public static String Project_Complete_dropdown()
    {
        return "//div[@id='control_29349D39-CD39-4D5C-9540-5A484828AEB8']//ul";
    }

    public static String ProjectFinancialsTab()
    {
        return "//div[text()='Project Financials']";
    }

    public static String BudgetExtension()
    {
        return "//div[@id='control_1D35A9A4-3615-4BE5-B4F9-1238EC675264']//div[@class='c-chk']";
    }

    public static String TotalExtension()
    {
        return "(//div[@id='control_B3EB88DA-F550-4AF0-AE8D-0167244448C8']//input)[1]";
    }

    public static String PaybackPeriodInYears()
    {
        return "(//div[@id='control_C981D6F2-A95F-4B83-87A6-5864488FBD93']//input)[1]";
    }

    public static String ProjectRunningCostTab()
    {
        return "//span[text()='Project Running Cost']";
    }

    public static String ExpectedCost()
    {
        return "//div[@id='control_8AFCEDFA-1DFB-499D-84F0-66B208796C88']//input";
    }

    public static String ActualCost()
    {
        return "(//div[@id='control_0CAE4516-3D66-4B45-9B23-1C7A82CA3413']//input)[1]";
    }

    public static String ProjectRunningCostTabAdd()
    {
        return "(//div[@id='control_1BA1AE31-5065-4E58-8BD1-708AF6A42E76']//div[@id='btnAddNew'])";
    }

    public static String Quarter_dropdown()
    {
        return "(//div[@id='control_E0BA8E50-4E2F-4EFB-A7B8-278823A61B83']//ul)";
    }

    public static String Month_dropdown()
    {
        return "(//div[@id='control_08E434A9-DE22-411F-BBF3-2C3571D40DDD']//ul)";
    }

    public static String Year_dropdown()
    {
        return "(//div[@id='control_150EC693-165C-4E73-BEBB-504FBD502D82']//ul)";
    }

    public static String ProjectActualsTab()
    {
        return "//span[text()='Project Actuals']";
    }

    public static String ProjectActualsAdd()
    {
        return "//div[@id='control_7603A440-AC0A-4EE4-82F2-33FF78507A66']//div[@id='btnAddNew']";
    }

    public static String ProjectActuals_Expenditure()
    {
        return "(//div[@id='control_EB59C8C5-1747-4893-9EF3-37AD3077A53D']//input)[1]";
    }

    public static String ProjectActuals_Quarter_dropdown()
    {
        return "(//div[@id='control_09A9662E-8EDC-4BEA-87C3-0C62227BCFF5']//ul)[1]";
    }

    public static String ProjectActuals_Month_dropdown()
    {
        return "(//div[@id='control_D7960673-32FB-45E2-AE62-A23E9DD56AB5']//ul)[1]";
    }

    public static String ProjectActuals_Year_dropdown()
    {
        return "(//div[@id='control_8B94E076-D52E-415B-BC9F-AE2A6E65EA76']//ul)[1]";
    }

    public static String ProjectActuals_BudgetForThisPeriod()
    {
        return "(//div[@id='control_FCD35FA1-9033-4961-A302-8AA9451E838E']//input)[1]";
    }

    public static String ECO2ManTab()
    {
        return "//div[text()='ECO2Man']";
    }

    public static String ECO2ManAdd()
    {
        return "//div[@id='control_99DA189A-C5BA-4F3F-9C6B-6CA2C58E63BF']//div[@id='btnAddNew']";
    }

    public static String ECO2Man_Quarter_dropdown()
    {
        return "//div[@id='control_B9379CD8-9321-422E-A41F-234FC59BB47F']//ul";
    }

    public static String ECO2Man_Month_dropdown()
    {
        return "//div[@id='control_40F5E1DE-02A0-4792-91BB-CCD5B87E86FC']//ul";
    }

    public static String ECO2Man_Year_dropdown()
    {
        return "//div[@id='control_45F50355-91F4-453E-8BA5-27960EA63317']//ul";
    }

    public static String ActionsTab()
    {
        return "//div[text()='Project Actions']";
    }

    public static String Actions_processflow()
    {
        return "//div[@id='btnProcessFlow_form_F54AE783-1378-4974-A7E8-4B5D1C4A79F3']";
    }

    public static String TypeOfActionDropDown()
    {
        return "//div[@id='control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE']//ul";
    }

    public static String ActionDueDate()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String ActionDescription()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String ResponsiblePerson_dropdown()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }

    public static String Entity_Dropdown()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String Actions_Add()
    {
        return "//div[@id='control_4F298569-07E5-4373-83F4-300C9C350ACF']//div[@id='btnAddNew']";
    }

    public static String Entity_dropdown_2()
    {
        return"//div[@id='control_1520F92C-A849-4812-8D9D-48C3B8A475C1']//span//b[@class='select3-down drop_click']";
    }

}
