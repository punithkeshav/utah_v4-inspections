/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_V5_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class Stakeholder_Entity_PageObjects extends BaseClass
{

    public static String navigate_EHS()
    {
        return "//label[contains(text(),'Environmental, Health & Safety')]";
    }

    public static String navigate_Stakeholders()
    {
        return "//label[text()='Stakeholders']";
    }

    public static String navigate_StakeholderEntity()
    {
        return "//label[contains(text(),'Stakeholder Entity')]";
    }
    
    public static String navigate_StakeholderEntity2()
    {
        return "(//label[contains(text(),'Stakeholder Entity')])[2]";
    }

    public static String SE_add()
    {
        return "//div[text()='Add']";
    }

    public static String SE_processflow()
    {
        return "//div[@id='btnProcessFlow_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }

    public static String SE_name()
    {
        return "//div[@id='control_6B36E56B-4BD2-4A16-AD58-94FE1883EFE2']/div/div//input";
    }

    public static String SE_industry_dropdown()
    {
        return "//div[@id='control_425BD26A-5DDB-485A-BA7F-E8D2E51C4BEA']";
    }

    public static String SE_industry_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String SE_desc()
    {
        return "//div[@id='control_4AEFCBE1-7C06-4528-BB6B-CFD298C47AA1']/div/div//textarea";
    }

    public static String SE_owner_dropdown()
    {
        return "//div[@id='control_A01F1D1A-45BF-4A6B-B2C2-88046BDAFDA1']";
    }

    public static String SE_owner_select(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String SE_categories(String text)
    {
        return "//div[@id='control_BDB3E74D-818E-4A51-8443-3F30BA7A472A']//a[contains(text(),'" + text + "')]/i[1]";
    }

    public static String SE_business_unit_selectall()
    {
        return "//div[@id='control_931D1181-0EA6-4EBF-BAA9-B497F5793EC0']//b[@original-title='Select all']";
    }

    public static String SE_impact_types_selectall()
    {
        return "//div[@id='control_962A59FE-78C1-4FCE-B40E-CAC2A8124522']//b[@original-title='Select all']";
    }

    public static String SE_savetocontinue()
    {
        return "//div[@id='control_9E597588-6E9E-4EB7-A607-CC0E20503899']//div[text()='Save to continue']";
    }

    public static String saveWait()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    //Entinty Information
    public static String entityInformationTab()
    {
        return "//li[@id='tab_337B1D56-1774-4C9D-8515-CC886C55C1FB']";
    }

    public static String primaryContactNo()
    {
        return "(//div[@id='control_B37A5BE9-D18E-4D38-97C8-A79B0143F45A']//input)[1]";
    }

    public static String secondaryContactNo()
    {
        return "(//div[@id='control_5FB377EB-8769-48EA-9981-11E3CBF7BEE4']//input)[1]";
    }

    public static String address()
    {
        return "//div[@id='control_7A4E044D-6ACD-4B15-A059-EBF7A1947AAF']//textarea";
    }

    public static String correspondenceAddress()
    {
        return "//div[@id='control_FA9DF216-979C-4C30-98B6-3E6E7961F8BC']//textarea";
    }

    public static String website()
    {
        return "(//div[@id='control_D2683D07-2857-4D9C-8EF6-588B6625B2EE']//input)[1]";
    }

    public static String SE_save()
    {
        return "(//div[@id='btnSave_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']//div[contains(text(),'Save')])[3]";
    }

    public static String leftArrowTab()
    {
        return "//div[@class='tabpanel_left_scroll icon chevron left tabpanel_left_scroll_disabled']";
    }

    public static String rightArrowTab()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    //Upload Hyperlink Documents
    public static String supportDocumentsTab()
    {
        return "//li[@id='tab_144D92B6-733C-4F37-8572-6F268883A0EC']//div[contains(text(),'Supporting Documents')]";
    }

    public static String SE_uploadLinkbox()
    {
        return "//div[@id='control_BFA08DA7-DF6E-4B10-A435-4EBD13654902']//b[@class='linkbox-link']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    //Engagements
    public static String engagementsTab()
    {
        return "//li[@id='tab_3942DBC8-B12A-4A79-97F6-4ED66D494909']";
    }

    public static String addEngagementsButton()
    {
        return "//div[@id='control_A50A638E-99DE-44B8-A7B5-2BF8A948862A']";
    }

    public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

    public static String businessUnitTab()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']";
    }

    public static String businessUnit_GlobalCompany()
    {
        return "//a[contains(text(),'Global Company')]";
    }

    public static String businessUnit_SelectAll()
    {
        return "(//span[@class='select3-arrow']//b[@class='select3-all'])[3]";
    }

    public static String expandGlobalCompany()
    {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'Global Company')]//..//i)[1]";
    }

    public static String expandSouthAfrica()
    {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'South Africa')]//..//i)[1]";
    }

    public static String businessUnitSelect(String text)
    {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'" + text + "')]/i)[1]";
    }

    public static String specificLocation()
    {
        return "(//div[@id='control_0501A692-460C-4DB5-9EAC-F15EE8934113']//input)[1]";
    }

    public static String projectLink()
    {
        return "//div[@id='control_29AB36D5-E83F-43EF-AFF5-F7353A5353E9']/div[1]";
    }

    public static String projectTab()
    {
        return "//div[@id='control_963F5190-1317-42C1-AD7A-B277FCBA7101']";
    }

    public static String anyProject(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String ShowMapLink()
    {
        return "//div[@id='control_6723362D-F9E2-457C-B125-13246382C3F7']/div[1]";
    }

    public static String engagementDate()
    {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }
    
//    public static String actionDueDate()
//    {
//        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
//    }

    public static String communicationWithTab()
    {
        return "//div[@id='control_45EA783F-D6C7-452F-B628-C3C91C7AB4B5']";
    }

    public static String communicationWith_SelectAll()
    {
        return "//div[@id='control_45EA783F-D6C7-452F-B628-C3C91C7AB4B5']//b[@class='select3-all']";
    }

    public static String communicationUsers()
    {
        return "//a[contains(text(),'IsoMetrix Users')]";
    }

    public static String usersTab()
    {
        return "//div[@id='control_CD33B817-1DF7-4499-A055-2A70F29398FC']";
    }

    public static String userOption(String text)
    {
        return "(//a[contains(text(),'" + text + "')]/i)[1]";
    }

    public static String stakeholdersTab()
    {
        return "//div[@id='control_79D83FF1-6A75-4E3C-9555-ADEF3FFBFA07']";
    }

    public static String anyStakeholders(String text)
    {
        return "(//a[contains(text(),'" + text + "')]/i)[1]";
    }

    public static String Stakeholders_SelectAll()
    {
        return "//div[@id='control_79D83FF1-6A75-4E3C-9555-ADEF3FFBFA07']//b[@class='select3-all']";
    }

    public static String entitiesTab()
    {
        return "//div[@id='control_4C008128-D6F8-4D73-9AF1-FBED1B1C1029']";
    }

    public static String entities(String data)
    {
        return "(//a[contains(text(),'" + data + "')]/i)[1]";
    }

    public static String expandInPerson()
    {
        return "(//a[contains(text(),'In-person engagements')]//..//i)[1]";
    }

    public static String expandOtherForms()
    {
        return "(//a[contains(text(),'Other forms of engagement')]//..//i)[1]";
    }

    public static String anyEngagementMethod(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String engagementMethodTab()
    {
        return "//div[@id='control_4A471537-8229-4E54-A86C-DCEB99BA24D0']";
    }

    public static String engagementPurposeTab()
    {
        return "//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']";
    }

    public static String anyEngagementPurpose(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String engagementTitle()
    {
        return "(//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input)[1]";
    }

    public static String engagementDescription()
    {
        return "(//div[@id='control_1C19AE65-23A1-4ADC-A631-D9273FC0CE9F']//textarea)[1]";
    }

    public static String engagementPlan()
    {
        return "(//div[@id='control_F082EFDC-2D10-478A-AB94-BD4A3FB4FE4B']//textarea)[1]";
    }

    public static String managementApprovalActionCheckBox()
    {
        return "(//div[@id='control_523CF0A9-803B-44F2-8572-BED13C022A03']/div)[1]";
    }

    public static String stakeholderApprovalAction()
    {
        return "(//div[@id='control_D0FFF631-1DA5-4BD3-A1C8-75A7527E6A34']/div)[1]";
    }

    public static String responsiblePersonTab()
    {
        return "//div[@id='control_213251A2-010A-4BBF-A65A-A1FC8C6F7033']";
    }

    public static String departmentResponsibleDropDown()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }
    
    public static String departmentResponsibleDropDownValue(String value)
    {
        return "(//a[text()='"+value+"'])[2]";
    }
    
    public static String numberOfAttendees()
    {
        return "(//div[@id='control_3D6CBB44-8CCF-4C7F-9150-D5DDD630995E']//input)[1]";
    }

    public static String anyResponsiblePerson(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }

    public static String Engagement_save()
    {
        return "(//div[@id='btnSave_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']//div[contains(text(),'Save')])[3]";
    }

    public static String entitiesCloseDropdown()
    {
        return "//div[@id='control_4C008128-D6F8-4D73-9AF1-FBED1B1C1029']//b[@class='select3-down drop_click']";
    }

    public static String projectTitle()
    {
        return "IsoMetrix";
    }

    public static String searchTab()
    {
        return "(//div[contains(text(),'Search')])[5]";
    }

    public static String searchButton()
    {
        return "(//div[contains(text(),'Search')])[6]";
    }

    public static String engagementsGridView()
    {
        return "(//div[@id='control_89186F28-56D9-423C-9FA4-8D688243B982']//div//table)[2]";
    }

    //FR2
    public static String members_tab()
    {
        return "//div[text()='Members']";
    }

    public static String member_add()
    {
        return "(//div[text()='Add'])[2]";
    }

    public static String member_name_dopdown()
    {
        return "//div[@id='control_623A6228-2A56-4954-91D6-D2E07B56E612']";
    }

    public static String member_name_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String member_position()
    {
        return "//div[@id='control_4CADD8F0-604A-4E1F-9A9A-1D1932001902']/div/div/input";
    }

    public static String member_save()
    {
        return "(//div[text()='Save'])[3]";
    }

    public static String si_fname()
    {
        return "//div[@id='control_E186B9E5-4102-409D-8F57-7355938C09D9']/div/div/input";
    }

    public static String si_lname()
    {
        return "//div[@id='control_A9D1A3E8-C561-452A-A1F4-7BCB496B365F']/div/div/input";
    }

    public static String si_knownas()
    {
        return "//div[@id='control_BED0557B-BBC2-46C1-B571-BE60A267F0EA']/div/div/input";
    }

    public static String si_title_dropdown()
    {
        return "//div[@id='control_28C03054-D663-431B-9F65-38BE54617019']";
    }

    public static String si_title(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String actions_tab()
    {
        return "//div[text()='Actions']";
    }

    public static String actions_add()
    {
        return "//div[@id='control_6AECC604-74CD-4CA2-8014-252301E5A6E8']//div[text()='Add']";
    }
    
    
    public static String entity_InformationTab()
    {
        return "//div[text()='Entity Information']";
    }
    
    
    public static String actions_pf()
    {
        return "//div[@id='btnProcessFlow_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }

    public static String actions_pcn()
    {
        return "//div[@id='control_B37A5BE9-D18E-4D38-97C8-A79B0143F45A']/div/div/input";
    }

    public static String actions_scn()
    {
        return "//div[@id='control_5FB377EB-8769-48EA-9981-11E3CBF7BEE4']/div/div/input";
    }

    public static String actions_address()
    {
        return "//div[@id='control_7A4E044D-6ACD-4B15-A059-EBF7A1947AAF']/div/div/textarea";
    }

    public static String actions_caddress()
    {
        return "//div[@id='control_FA9DF216-979C-4C30-98B6-3E6E7961F8BC']/div/div/textarea";
    }
 
    public static String action_description()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']/div/div/textarea";
    }
     
    
    
    public static String actions_website()
    {
        return "//div[@id='control_D2683D07-2857-4D9C-8EF6-588B6625B2EE']/div/div/input";
    }

    public static String actions_Save()
    {
        return "(//div[text()='Save'])[6]";
    }
    
    public static String StakeholderEntity_Save()
    {
        return "(//div[text()='Save'])[2]";
    }
    
     public static String right_ChevronArrow()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }
    

    public static String si_processflow()
    {
        return "//div[@id='btnProcessFlow_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']";
    }

    public static String si_designation()
    {
        return "//div[@id='control_DE0171C4-DDEA-47BD-A5D5-F4DF639EC9E2']/div/div/input";
    }

    public static String si_dob()
    {
        return "//div[@id='control_501DB50D-5488-43AF-91D3-B74D77CC09A3']//input";
    }

    public static String si_age()
    {
        return "//div[@id='control_1BC13F9E-47D7-436E-A5BB-97E88B8D86F5']/div/input";
    }

    public static String si_idnum()
    {
        return "//div[@id='control_5A680F16-7C29-49C2-9698-68C906C1F35F']/div/div/input";
    }

    public static String si_gender_dropdown()
    {
        return "//div[@id='control_BC0B3729-5DA7-493E-BA90-653F5C3E6151']";
    }

    public static String si_gender_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String si_nationality_dropdown()
    {
        return "//div[@id='control_447AC363-4DFF-4821-AB92-6695191EC822']";
    }

    public static String si_nationality_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String profile_tab()
    {
        return "//div[@id='control_38AB48FE-5C8D-401B-948C-9985EF810CED']//div[text()='Profile']";
    }

    public static String si_stakeholdercat_selectall()
    {
        return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']//b[@original-title='Select all']";
    }

    public static String si_stakeholdercat(String text)
    {
        return "//a[text()='" + text + "']/i[1]";
    }

    public static String si_appbu_selectall()
    {
        return "//div[@id='control_4CFB2165-708B-4D55-9988-9CDCB5487291']//b[@original-title='Select all']";
    }

    public static String si_appbu_expand(String text)
    {
        return "//a[text()='" + text + "']/../i";
    }

    public static String si_appbu(String text)
    {
        return "//a[text()='" + text + "']/i[1]";
    }

    public static String si_impacttypes_selectall()
    {
        return "//div[@id='control_65F1B5F4-17B9-48FE-817D-B27F54AB360E']//b[@original-title='Select all']";
    }

    public static String si_impacttypes(String text)
    {
        return "//a[text()='" + text + "']/i[1]";
    }

    public static String si_locationmarker()
    {
        return "//div[@id='control_6A7B2112-2526-41F1-893D-2702E81144D7']//div[@class='icon location mark mapbox-icons']";
    }

    public static String location_tab()
    {
        return "//div[text()='Location']";
    }

    public static String location_dropdown()
    {
        return "//div[@id='control_B64B2E96-473B-4207-8FCE-C7DAB53F8834']//span[@class='select3-chosen']";
    }

    public static String si_location()
    {
        return "jozi3.PNG";
    }

    public static String si_save()
    {
        return "(//div[text()='Save'])[2]";
    }

    public static String contractorSupplierManager_Tab()
    {
        return "//li[@id='tab_5649976E-D3E2-4B5D-8E83-DECD2901F3A0']//div[contains(text(),'Contractor or Supplier Manager')]";
    }

    public static String questionnaires_Panel()
    {
        return "//span[contains(text(),'Questionnaire')]";
    }

    public static String questionnaires_add()
    {
        return "//div[@id='control_7517949D-9F94-4E11-8C20-9CE0EF740490']//div[contains(text(),'Add')]";
    }

    public static String questionnaireProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_7B9DCBC8-678E-4ED9-8CF5-C5AA78BED2D8']";
    }

    public static String documents_Panel()
    {
        return "//span[contains(text(),'Documents')]";
    }

    public static String documents_add()
    {
        return "//div[@id='control_1D721996-A61A-40FD-91E8-7FAFDF3B5039']//div[contains(text(),'Add')]";
    }

    public static String documents_Input()
    {
        return "(//div[@id='control_4EC7578A-B88B-4442-AA32-754B27F4FDF3']//input)[1]";
    }

    public static String documentsUploaded_Dropdown()
    {
        return "//div[@id='control_F3FF8316-9847-4F33-ACCE-CEDA5D02BE01']";
    }

    public static String documentsUploaded_Dropdown_Option(String data)
    {
        return "(//a[text()='" + data + "'])[1]";
    }

    public static String outcome_Dropdown_Option(String data)
    {
        return "(//a[text()='" + data + "'])[1]";
    }

    public static String outcome_Dropdown()
    {
        return "//div[@id='control_6BF2BC67-11B6-45D8-9A90-E7A8FB55DDAE']";
    }

    public static String dateVerifiedTab()
    {
        return "(//div[@id='control_1FD49B28-3FF0-4931-9C73-2E3D2D7AA68E']//span//input)[1]";
    }

    public static String commentsTab()
    {
        return "(//div[@id='control_1FF7D5F1-8209-4446-ABE0-D0D89B11FF22']//textarea)[1]";
    }

    public static String documents_Save()
    {
        return "(//div[@id='btnSave_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']//div[contains(text(),'Save')])[3]";
    }

    public static String supporting_tab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String linkbox()
    {
        return "//div[@id='control_1F0A25D4-9905-4913-BC61-B1EED48CCCC6']//b[@original-title='Link to a document']";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String si_savetocontinue()
    {
        return "//div[text()='Save supporting documents']";
    }

    public static String se_createnewindividual()
    {
        return "//div[text()='Create a new individual']";
    }

    public static String si_rightarrow()
    {
        return "//div[@id='control_38AB48FE-5C8D-401B-948C-9985EF810CED']//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String sik_rightarrow()
    {
        return "rightarrow.PNG";
    }

    public static String ri_ename()
    {
        return "//div[@class='optionsPanel firstPanel primary']//td[text()='Entity name']/../td[5]/input";
    }

    public static String ri_esearch()
    {
        return "//div[text()='Search']";
    }

    public static String ri_eselect(String text)
    {
        return "(//div[text()='" + text + "'])[1]";
    }

    public static String ri_related()
    {
        return "(//div[text()='Related Incidents'])[1]";
    }

    public static String ri_record()
    {
        return "//div[@id='control_AE8EC219-4756-40BA-8773-68B10FE425BA']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr[1]";
    }

    public static String ri_wait()
    {
        return "(//div[text()='Incident Management '])[1]";
    }

    public static String supportDocumentsTab_CQ()
    {
        return "//li[@id='tab_88BC6C8F-E230-4853-BE1A-9E90200423E0']//div[contains(text(),'Supporting Documents')]";
    }

    public static String uploadLinkbox()
    {
        return "(//div[contains(text(),'Supporting Documents')]//..//div[@class='linkbox-options']//b[@class='linkbox-link'])[2]";
    }

    public static String saveToContinueButton()
    {
        return "//div[@id='control_D4D50C09-26C5-410D-9549-2CC4BE86A134']//div[contains(text(),'Save to continue')]";
    }

    public static String saveSupportingDocuments()
    {
        return "//div[@id='btnSave_form_7B9DCBC8-678E-4ED9-8CF5-C5AA78BED2D8']";
    }

    public static String companyType_Dropdown()
    {
        return "//div[@id='control_D8B07754-94EC-4FB2-859C-B738B6EAFD91']";
    }

    public static String companyType_Option(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String relevantEntity_RegNo()
    {
        return "(//div[@id='control_D6E2582F-135F-475C-8180-054CA159CFCC']//input)[1]";
    }

    //Tabs
    public static String companyInformationTab()
    {
        return "//li[@id='tab_2AED1AEA-A885-4D65-8C46-A4E0FDD8A034']";
    }

    public static String businessPartnerTab()
    {
        return "//li[@id='tab_ECC116DB-F2F9-49D9-82C8-122332F6CCC8']";
    }

    public static String companyCertificatesTab()
    {
        return "//li[@id='tab_73A0738E-B7E2-4F10-9CE7-EF3C31E48A89']";
    }

    public static String safetyHealthIssuesTab()
    {
        return "//li[@id='tab_44F21890-1F73-40D6-BCEF-0696F8566A3A']";
    }

    public static String healthSafeyEnvironmentTab()
    {
        return "//li[@id='tab_B01371BE-B0A1-4F0F-A195-7DD5D7BC264E']";
    }

    public static String additionalCompanyInformationTab()
    {
        return "//li[@id='tab_25BDF161-DD62-438F-B244-F3BF5FD82B72']";
    }

    public static String businessPartnerRiskRankingTab()
    {
        return "//li[@id='tab_CA730638-F9B1-4552-AE71-1892F034122D']";
    }

    //Data
    public static String mainArea()
    {
        return "(//div[@id='control_349C41BC-5C52-4311-B0AD-5203F0596F74']//input)[1]";
    }

    public static String operatingUnit()
    {
        return "(//div[@id='control_7236A83F-F382-487E-A660-2213CFC22BB0']//input)[1]";
    }

    public static String qualityManagement_Dropdown()
    {
        return "//div[@id='control_1575F13D-D625-43F4-AAF6-3462C227AD3A']";
    }

    public static String qualityManagement_Option(String data)
    {
        return "(//div[@id='control_3AA7ED92-601B-477C-9BF2-A248733FD534']/../../../../../../../..//a[text()='" + data + "'])[1]";
    }

    public static String environmentalManagement_Dropdown()
    {
        return "//div[@id='control_749B58E4-1696-44A2-A2B5-6544169861E1']";
    }

    public static String environmentalManagement_Option(String data)
    {
        return "(//div[@id='control_0D0FD72F-3263-4222-82C9-5964A0D96B95']/../../../../../../../..//a[text()='" + data + "'])[2]";
    }

    public static String COID_Act_Dropdown()
    {
        return "//div[@id='control_2BF947B0-C0CE-4F29-BEB7-EEAD413BCABF']";
    }

    public static String COID_Act_Option(String data)
    {
        return "(//div[@id='control_D8A26477-3F38-4CA0-A72D-19E94C7B0F3A']/../../../../../../../..//a[text()='" + data + "'])[3]";
    }

    public static String COID_RegNo()
    {
        return "(//div[@id='control_D70BA03D-C157-42B0-9C6E-4F1A62106056']//input)[1]";
    }

    public static String rightArrowButton()
    {
        return "//div[@id='control_5554718D-1C5B-401C-AE1E-6ABF12EDB801']//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String legalRegister_Dropdown()
    {
        return "//div[@id='control_AC471A15-AE17-44CA-9330-9110BEAA186E']";
    }

    public static String legalRegister_Option(String data)
    {
        return "(//div[@id='control_3C1ADF6C-2A91-4289-B91B-FBA0B3C43FE4']/../../../../../../../..//a[text()='" + data + "'])[4]";
    }

    public static String healthSafetyPlan_Dropdown()
    {
        return "//div[@id='control_3C62267D-11AA-4E60-AEB8-7F89010D81AE']";
    }

    public static String healthSafetyPlan_Option(String data)
    {
        return "(//div[@id='control_204F364D-A254-4AA9-BE1C-0089EE76B00F']/../../../../../../../..//a[text()='" + data + "'])[5]";
    }

    public static String rentedOwnedLeased_Dropdown()
    {
        return "//div[@id='control_CC9B150A-E4AF-47F4-82A2-A191DBC23DDC']";
    }

    public static String rentedOwnedLeased_Option(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String furnishAnnualNettProfit()
    {
        return "(//div[@id='control_86D35ED9-FA92-4695-8B43-9CFBE975017E']//input)[1]";
    }

    public static String questionnaire_Save()
    {
        return "//div[@id='btnSave_form_7B9DCBC8-678E-4ED9-8CF5-C5AA78BED2D8']//div[text()='Save']";
    }

    public static String scopeOfWork()
    {
        return "(//div[@id='control_32D49E9F-7654-44D8-89AB-9A24D0E343A4']//textarea)[1]";
    }

    public static String report()
    {
        return "//div[@id='btnReports']";
    }

    public static String view_reports()
    {
        return "//span[@title='View report ']";
    }

    public static String full_Reports()
    {
        return "//span[@title='Full report ']";
    }

    public static String view_wait()
    {
        return "//div[text()='Entity report']";
    }

    public static String full_wait()
    {
        return "//div[text()='Entity status']";
    }

    public static String popup_conf()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String stakeholder_Array(String data)
    {
        return "(//a[text()='" + data + "']/i[1])";
    }

    public static String anyStakeholders2(String text, int counter)
    {
        return "(//a[contains(text(),'" + text + "')]/i[1])[ " + counter + " ]";
    }

    public static String SE_entityTypeDropdown()
    {
        return "//div[@id='control_121C3080-4A13-4AC2-8640-F2380C230CDC']//span[@class='select3-chosen']";
    }

    public static String SE_industryType_select(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String se_locationmarker()
    {
        return "//div[@id='control_D451A141-6811-4844-9A2C-501A338A05C7']//div[@class='icon location mark mapbox-icons']";
    }

    public static String inspection_RecordSaved_popup()
    {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }

    public static String recordSaved_popup()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])[2]";
    }

    public static String failed()
    {
        return "(//div[@id='txtAlert'])[2]";
    }

    public static String stateholderDetails_tab()
    {
        return "//div[contains(text(),'Stakeholder Details')]";
    }

    public static String SE_PhoneNo()
    {
        return "(//div[@id='control_F58CA39A-8E24-4A56-987E-5B84412F0A9C']//input)[1]";
    }

    public static String SE_EmailAddress()
    {
        return "(//div[@id='control_E95B004E-1283-4AD5-85C0-2AA11196F34D']//input)[1]";
    }

    public static String SE_Street1()
    {
        return "(//div[@id='control_C30F3943-2351-490A-A865-7D2337A1F7C5']//input)[1]";
    }

    public static String SE_Street2()
    {
        return "(//div[@id='control_95D0CC4B-878A-48CE-8BF1-5149BF5D5FD6']//input)[1]";
    }

    public static String SE_ZipCode()
    {
        return "(//div[@id='control_99DCFFAC-34D1-4291-9154-5B3B7DC59D1B']//input)[1]";
    }

    public static String SE_Latitude()
    {
        return "(//div[@id='control_6DE16A17-A6F0-4F52-9DB9-220BC3F5048E']//input)[1]";
    }

    public static String SE_Longitude()
    {
        return "(//div[@id='control_F48DA652-2EC8-496D-AF2A-BF08D33B73EB']//input)[1]";
    }

    public static String SE_location_dropdown()
    {
        return "//div[@id='control_B64B2E96-473B-4207-8FCE-C7DAB53F8834']";
    }

    public static String SE_location_select(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }

    public static String stateholderAnalysis_tab()
    {
        return "//div[contains(text(),'Stakeholder Analysis')]";
    }

    public static String SE_StakeholderInterest()
    {
        return "//div[@id='control_4F51BB5C-34D0-42B8-9324-66AC1198CF45']";
    }

    public static String SE_StakeholderInterest_select(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String SE_StakeholderInfluence_select(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }

    public static String SE_StakeholderInfluence()
    {
        return "//div[@id='control_F72B39E0-9AEB-436C-8732-F06C20862FF0']";
    }

    public static String SE_StakeholderSupport()
    {
        return "//div[@id='control_6307BAB8-A857-4489-8F4E-B90B68B812B2']";
    }

    public static String SE_StakeholderSupport_select(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String SE_TopicIsssuesAssessment()
    {
        return "//div[@id='control_43968C1B-65B3-43EC-8AF7-5F14D2382B42']//span[contains(text(),'Topic / Issue Assessment')]";
    }

    public static String SE_Assessment_AddButton()
    {
        return "//div[@id='control_791664E9-F4D5-4CBB-9BAA-B187310C3D6F']//div[text()='Add']";
    }

    public static String SE_TopicIssue()
    {
        return "//div[@id='control_359049FA-9B56-4D59-A670-93070530E0E4']";
    }

    public static String SE_TopicIssue_AccessControlHazards()
    {
        return "//a[contains(text(),'Access control hazards')]";
    }

    public static String SE_TopicIssue_select(String data)
    {
        return "//a[contains(text(),'Entering the workplace when not fit to work')]";
    }

    public static String SE_TopicInfluence()
    {
        return "//div[@id='control_46284CB6-8E0D-4AF6-BD22-0AF4AA6E7759']";
    }

    public static String SE_TopicInfluence_select(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[3]";
    }

    public static String SE_TopicInterest()
    {
        return "//div[@id='control_63C66781-07ED-4EB2-8A2F-CAED92ED118C']";
    }

    public static String SE_TopicInterest_select(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[4]";
    }

    public static String SE_Members_AddButton()
    {
        return "//div[@id='control_7016173C-BF8B-419B-8C46-D81EAA9AEC45']//div[text()='Add']";
    }

    public static String SE_IndividualName()
    {
        return "//div[@id='control_623A6228-2A56-4954-91D6-D2E07B56E612']";
    }

    public static String SE_IndividualName_select(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String SE_Position()
    {
        return "//div[@id='control_8C0CA237-7663-407A-A383-71A76D340D97']";
    }

    public static String SE_Position_select(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String SE_NewIndividual_Button()
    {
        return "//div[@id='control_1F609E28-601C-41B0-A02A-C99C19AB23B1']";
    }

    public static String Contractor_SupplierManager_tab()
    {
        return "//div[contains(text(),'Contractor or Supplier Manager')]";
    }

    public static String categoryLabel()
    {
        return "//div[text()='Stakeholder categories']";
    }

    public static String rightArrow()
    {
        return "(//div[@class='tabpanel_right_scroll icon chevron right'])[2]";
    }

    public static String stakeholderDeatilsTab()
    {
        return "//div[text()='Stakeholder Details ']";
    }

    public static String phoneNumber()
    {
        return "//div[@id='control_F58CA39A-8E24-4A56-987E-5B84412F0A9C']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String emailAddress()
    {
        return "//div[@id='control_E95B004E-1283-4AD5-85C0-2AA11196F34D']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String street1()
    {
        return "//div[@id='control_C30F3943-2351-490A-A865-7D2337A1F7C5']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String street2()
    {
        return "//div[@id='control_95D0CC4B-878A-48CE-8BF1-5149BF5D5FD6']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String southAfricaArrow()
    {
        return "(//a[text()='South Africa']/../i[@class='jstree-icon jstree-ocl'])[2]";
    }

    public static String gautengArrow()
    {
        return "//a[text()='Gauteng']/../i[@class='jstree-icon jstree-ocl']";
    }

    public static String jhbArrow()
    {
        return "//a[text()='Johannesburg']/../i[@class='jstree-icon jstree-ocl']";
    }

    public static String selectLocation(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String SE_Status()
    {
        return "//div[@id='control_AEDC76EE-6EA0-48C6-91D0-A2CE27CEF60D']";
    }

    public static String SE_Status_Select(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String SE_MainContact()
    {
        return "(//div[@id='control_DB611D04-5A6A-485B-B7FD-3E7468577C4D']//input)[1]";
    }

    public static String SE_MainContactPhone()
    {
        return "(//div[@id='control_1588E4E5-AAB4-4254-8D7D-0ED15A0CC591']//input)[1]";
    }

    public static String SE_MainContactEmail()
    {
        return "(//div[@id='control_D7FD3FAD-217D-47F0-B28F-C2B1AAFF57B3']//input)[1]";
    }

    public static String SE_MainContactIndividual_dropdown()
    {
        return "//div[@id='control_1FE052A1-221D-426B-B9E1-46569A2BA8EF']";
    }

    public static String SE_MainContactIndividual_select(String data)
    {
        return "(//a[text()='" + data + "'])[1]";
    }

    public static String questionnairePanel_tab()
    {
        return "//div[@id='control_AAA7B9B3-B923-4EF6-841E-21D723778FDF']//span[text()='Questionnaire']";
    }

    public static String right_Arrow()
    {
        return "(//div[@class='tabpanel_tab_content']/div)[10]";
    }

    public static String x_close()
    {
        return "(//div[@id='form_7B9DCBC8-678E-4ED9-8CF5-C5AA78BED2D8']//i[@class='close icon cross'])[1]";
    }

    public static String SE_questionnaire_Record()
    {
        return "(//div[@id='control_7517949D-9F94-4E11-8C20-9CE0EF740490']//../div[3]//tr[1])[2]";
    }

    public static String documentsPanel_tab()
    {
        return "//div[@id='control_A65C4553-85A2-476A-B22C-52A0AAE691F6']//span[text()='Documents']";
    }

    public static String ordersPanel_tab()
    {
        return "//div[@id='control_04DEEC6F-6187-494E-8E64-CABB6EC134C4']//span[text()='Orders']";
    }

    public static String orders_add()
    {
        return "//div[@id='control_D0274203-59E0-4BD0-8D74-E760964E515E']//div[contains(text(),'Add')]";
    }

    public static String ordersProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_AC3617FE-8383-45BB-8611-EA8CA705B2C1']";
    }

    public static String ordersNo_Input()
    {
        return "(//div[@id='control_BE92E94B-74CE-4BF6-B70E-31CE21A1422B']//input)[1]";
    }

    public static String orderStatus_Option(String data)
    {
        return "(//a[text()='" + data + "'])[1]";
    }

    public static String orderStatus_Dropdown()
    {
        return "//div[@id='control_F50C9133-D3BE-45C0-88F6-4FCCCE4BEE4D']";
    }

    public static String approvedBy_Dropdown()
    {
        return "//div[@id='control_060F866E-28F3-4F44-817C-76780546B4ED']";
    }

    public static String approvedBy_Dropdown_Option(String data)
    {
        return "(//a[text()='" + data + "'])[2]";
    }

    public static String ScopeOfWork_Tab()
    {
        return "//li[@id='tab_B0CCF67B-84FF-4F61-8964-163DE258781C']";
    }

    public static String areaConducted()
    {
        return "(//a[contains(text(),'Global Company')]//i)[3]";
    }

    public static String startDate()
    {
        return "//div[@id='control_90D93B64-68B5-4748-897A-498F7C8DE4BA']//input";
    }

    public static String endDate()
    {
        return "//div[@id='control_3A59A41B-41D0-498E-BD56-1D56B52C07B9']//input";
    }

    public static String contractorOrders_Save()
    {
        return "(//div[@id='btnSave_form_AC3617FE-8383-45BB-8611-EA8CA705B2C1']//div[contains(text(),'Save')])[3]";
    }

    public static String contractorRiskAssess_Save()
    {
        return "(//div[@id='btnSave_form_7F5090E0-7D49-4B93-962C-FA8DF119B3D6']//div[contains(text(),'Save')])[3]";
    }

    public static String dateApproved()
    {
        return "//div[@id='control_010013A1-86AE-4EB6-AC9F-847A74A5055F']//input";
    }

    public static String project()
    {
        return "(//div[@id='control_202D2497-A69E-4769-8980-4367637F0BB8']//input)[1]";
    }

    public static String Close_button()
    {
        return "(//div[@id='form_AC3617FE-8383-45BB-8611-EA8CA705B2C1']//i[@class='close icon cross'])[1]";
    }

    public static String RiskAssessment_tab()
    {
        return "//div[text()='Risk Assessment']";
    }

    public static String contractorOrder_add()
    {
        return "//div[@id='control_2466C37B-B25C-4DF6-B32D-6F623F2AC781']//div[contains(text(),'Add')]";
    }

    public static String rightarrow() {
        return "(//div[@class='tabpanel_tab_content']/div)[10]";
    }
    
    public static String addBtn()
    {
        return "//div[@id='control_90C85E93-A691-4C58-982F-39EEE9A0BC53']//div[@id='btnAddNew']";
    }

    public static String groupNameDropdown()
    {
        return "//div[@id='control_F2EBECAD-5135-4844-B579-0A02F8CE3738']//span[@class='select3-chosen']";
    }

    public static String selectGroupName(String data)
    {
        return "(//a[text()='" + data + "'])[1]";
    }

    public static String relationshipDropdown()
    {
        return "//div[@id='control_D50B12E5-3EBB-4095-AA89-FFC23B7AC247']//span[@class='select3-chosen']";
    }

    public static String selectRelationship(String data)
    {
        return "(//ul[@class='jstree-container-ul jstree-children'])[9]//a[text()='" + data + "']";
    }

    public static String AE_save()
    {
        return "//div[@id='control_90C85E93-A691-4C58-982F-39EEE9A0BC53']//div[text()='Save']";
    }

    public static String associateEntitiesTab()
    {
        return "//div[text()='Associated Entities']";
    }

    public static String addNewEntityBtn()
    {
        return "//div[@id='control_9CBA1F32-EAA3-41AD-BFBD-C9D500471B1C']//div[@class='c-btn imgButton']";
    }

    public static String riskAsseementProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_7F5090E0-7D49-4B93-962C-FA8DF119B3D6']";
    }

    public static String riskRankingDate()
    {
        return "//div[@id='control_90DA037B-D4F2-4862-BF60-34485B5C0DB6']//input[@type='text']";
    }

    public static String descriptionOfScope()
    {
        return "//div[@id='control_85DB80BB-F2BD-41B8-901C-338EBCF53047']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String relatedDocumentsTab()
    {
        return "(//div[text()='Related Documents'])[1]";
    }

    public static String linkBtn()
    {
        return "//div[@id='control_315F609A-FA2C-44FE-A010-149E750E725E']//b[@class='linkbox-link']";
    }

    public static String fleetManagementTab()
    {
        return "//div[text()='Fleet Management']";
    }

    public static String fleetMngmAddBtn()
    {
        return "(//div[@class='gridMenu']//div[@id='btnAddNew'])[4]";
    }

    public static String vehicleMake()
    {
        return "//div[@id='control_C662CD0F-B707-4A9B-AB16-476005E74C1F']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String fleetManagementProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_1F7D4C38-EE03-4F83-815A-94FD64BFC9C4']";
    }

    public static String fleetManagement_Save()
    {
        return "(//div[@id='btnSave_form_1F7D4C38-EE03-4F83-815A-94FD64BFC9C4']//div[contains(text(),'Save')])[3]";
    }

    public static String licenseExpiryDate()
    {
        return "//div[@id='control_9826F1CB-B2D2-4460-84BD-E9F986CD8173']//input[@type='text']";
    }

    public static String toolsAndEquipmentTab()
    {
        return "//div[text()='Tools & Equipment']";
    }

    public static String toolsAndEquipmentProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_4A401000-E42A-4AFD-B9C8-8A1A21187C73']";
    }

    public static String descriptionOfEquipment()
    {
        return "//div[@id='control_5F36A2D2-2304-48BB-BB1A-574EFBF50B10']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String toolsAndEquipment_Save()
    {
        return "(//div[@id='btnSave_form_4A401000-E42A-4AFD-B9C8-8A1A21187C73']//div[contains(text(),'Save')])[3]";
    }

    public static String linkBox()
    {
        return "//div[@id='control_68F3E7FD-324B-4813-A5C0-EC1B0F255D96']//b[@class='linkbox-link']";
    }

    public static String SE_search()
    {
        return "//div[text()='Search']";
    }

    public static String SE_selectRecord(String data)
    {
        return "//span[text()='"+data+"']";
    }

    public static String entityNameField()
    {
        return "//tr[@module_definition_name='txbEntityName']//input[@class='txt border']";
    }

    public static String recordNumberField()
    {
        return "//tr[@module_definition_name='record_number']//input[@class='txt border']";
    }

    public static String BlastingActivitiesDD()
    {
        return "//div[@id='control_E81E2C37-3D9A-46A5-932D-30B82C0F97D5']//span[@class='select3-chosen']";
    }
    
    public static String selectBlastingActivities(String data)
    {
        return "(//a[text()='"+data+"'])[1]";
    }
    
    public static String communityImpactDD()
    {
        return "//div[@id='control_0D4428C1-5B20-4EF6-B7C1-E139F54B03F1']//span[@class='select3-chosen']";
    }
    
    public static String selectCommunityImpact(String data)
    {
        return "(//a[text()='"+data+"'])[2]";
    }
    
    public static String confinedSpacesDD()
    {
        return "//div[@id='control_03562577-C7CA-4F51-A506-03D9C5CCC74B']//span[@class='select3-chosen']";
    }
    
    public static String selectconfinedSpaces(String data)
    {
        return "(//a[text()='"+data+"'])[3]";
    }

    public static String chemicalRegisterTab()
    {
        return "//div[text()='Chemical Register']";
    }

    public static String selectChemicalTheyBring(String data)
    {
        return "(//div[@id='control_7C39C09C-183D-4074-9A22-D0DB4318E7D6']//a[text()='"+data+"']//i)[1]";
    }

    public static String selectChemicalsWillBeExposeTo(String data)
    {
        return "//div[text()='"+data+"']";
    }

    public static String casNumber()
    {
        return "//div[@id='control_3718882C-CF41-4499-A620-18A495CABDD5']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String methodOfUse()
    {
        return "//div[@id='control_3C53D123-6E58-4247-83D4-8C53A8ABAA14']//span[@class='select3-chosen']";
    }

    public static String methodOfUseSelect(String data)
    {
        return "(//a[text()='"+data+"']//i)[1]";
    }

    public static String directLink()
    {
        return "//div[@id='control_D538FDF0-1C5A-44D9-A074-F7EBB092FBFE']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String htmlLink()
    {
        return "//div[@id='control_52ED9BDD-1B58-4DF9-9CB0-B5F9F05BA405']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String chemicalRegisterProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_67D12C4F-9715-42CB-8583-738A005007A8']";
    }

    public static String chemicalRegisterAndEquipment_Save()
    {
        return "(//div[@id='btnSave_form_67D12C4F-9715-42CB-8583-738A005007A8']//div[contains(text(),'Save')])[3]";
    }

    public static String chemicalRegister_RecordSaved_popup()
    {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }

    public static String permissionToWorkSignOff_tab()
    {
       return "//div[text()='Permission to Work']";
    }

    public static String contractorPermission_add()
    {
       return "//div[@id='control_E46D1FE3-3FAA-4DB0-A3E1-06CE9A9CEFC8']//div[contains(text(),'Add')]";
    }

    public static String permissionProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_446929D1-893C-47E0-BCB9-30C0C1D7A70B']";
    }

    public static String roleDD()
    {
        return "//div[@id='control_928D5A28-956F-4C01-B43A-EB08F0157406']//span[@class='select3-chosen']";
    }

    public static String roleSelect(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String responsiblePersonDD()
    {
        return "//div[@id='control_6AFD8447-DD45-4E0B-9885-558782D0DB69']//span[@class='select3-chosen']";
    }

    public static String selectresponsiblePerson(String data)
    {
        return "(//a[contains(text(),'"+data+"')])[3]";
    }

    public static String contractorPermission_Save()
    {
        return "(//div[@id='btnSave_form_446929D1-893C-47E0-BCB9-30C0C1D7A70B']//div[contains(text(),'Save')])[3]";
    }

    public static String signOffDD()
    {
        return "//div[@id='control_DAE7D09B-B8AE-41D8-B834-F2E347322FD9']//span[@class='select3-chosen']";
    }

    public static String selectSignOff(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String signOffDate()
    {
        return "//div[@id='control_FA67DC5D-68BF-4A12-86E2-0CC7B93C9F41']//input[@type='text']";
    }

    public static String Evaluations_tab()
    {
        return "//div[text()='Evaluations']";
    }

    public static String checklistPermission_add()
    {
        return "//div[@id='control_75AE308C-4204-44CC-8A14-A2167C9827C9']//div[contains(text(),'Add')]";
    }

    public static String checklistProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_0AE7D7E4-12B9-4159-83E3-5DF8D4F49CD4']";
    }

    public static String checkListStartDate()
    {
        return "//div[@id='control_BB124A7C-02A5-47A5-A918-BBDF0C1AA35B']//input[@type='text']";
    }

    public static String checklistEndDate()
    {
        return "//div[@id='control_C23CB71D-B301-4EB4-84E1-65BF4642B711']//input[@type='text']";
    }

    public static String ckeckListDD()
    {
        return "//div[@id='control_2A842B0C-8258-421F-997F-388DE02D9335']//span[@class='select3-chosen']    ";
    }

    public static String selectChecklist(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String personConductingDD()
    {
        return "//div[@id='control_D3BD3B30-B5B7-49DE-BD8D-CF9BE72E62C0']//span[@class='select3-chosen']";
    }

    public static String selectPersonConducting(String data)
    {
       return "(//a[contains(text(),'"+data+"')])[3]";
    }

    public static String contractorChecklist_Save()
    {
        return "(//div[@id='btnSave_form_0AE7D7E4-12B9-4159-83E3-5DF8D4F49CD4']//div[contains(text(),'Save')])[3]";
    }

    public static String areaLabel()
    {
        return "//div[text()='Area where work will be conducted']";
    }

    public static String totalScore()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static String totalLabel()
    {
        return "//div[text()='Total Score']";
    }

    public static String actionsTab()
    {
        return "//div[text()='Actions']";
    }

    public static String actionsAdd()
    {
        return "//div[@id='control_6AECC604-74CD-4CA2-8014-252301E5A6E8']//div[contains(text(),'Add')]";
    }

    public static String actions_processflow()
    {
       return "//div[@id='btnProcessFlow_form_F72A20BD-FF84-43A1-8729-31F4F84F23C5']";
    }

    public static String actionDescription()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String departmentResponsible()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//span[@class='select3-chosen']";
    }

    public static String responsiblePerson()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//span[@class='select3-chosen']";
    }

    public static String departmentResponsibleSelect(String data)
    {
       return "(//a[text()='"+data+"'])[2]";
       
    }

    public static String responsiblePersonSelect(String data)
    {
        return "//ul[@class='jstree-container-ul jstree-children']//a[contains(text(),'"+data+"')]";
    }

    public static String actionDueDate()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input[@type='text']";
    }

    public static String replicateCheckBox()
    {
        return "//div[@id='control_9FB64F38-240A-4D57-8EBF-202D8124CDEE']//div[@class='c-chk']";
    }

    public static String multipleUsersSelectAll()
    {
        return "//div[@id='control_B85CC3EF-7F60-4CE6-8F64-E978C40DD033']//b[@class='select3-all']";
    }

    public static String actionsSave()
    {
        return "//div[@id='btnSave_form_F72A20BD-FF84-43A1-8729-31F4F84F23C5']//div[text()='Save']";
    }

    public static String actionsRightArrow()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String contactInfoLabel()
    {
        return "//div[text()='Contact Information']";
    }

    public static String entityProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_0530E237-3D65-4F8D-8F0C-95A9B4E42AB0']";
    }

    public static String relatedAssessment_tab()
    {
        return "//div[contains(text(),'Related Assessment')]";
    }

    public static String selectRecord()
    {
        return "//div[@id='control_65FF0BD7-91A2-46FB-9FA0-E0A4C7A0CE4D']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr[1]";
    }

    public static String relatedProcessFlow()
    {
       return "//div[@id='btnProcessFlow_form_D9A8DB93-E3EF-4C40-B3B9-5FE8F6D5AFE2']";
    }
    
    public static String relatedInspectionsProcessFlow()
    {
       return "//div[@id='btnProcessFlow_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }

    public static String SE_rightArrow()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String contaminatedWater()
    {
       return "//div[@id='control_7170CA96-6A82-4F56-B465-414D4ECAFF43']//span[@class='select3-chosen']";
    }

    public static String selectContaminatedWater(String data)
    {
        return "(//a[text()='"+data+"'])[4]";
    }

    public static String likelihood()
    {
        return "//div[@id='control_B5C6710C-8125-483D-B2C2-EE45900D2ADD']//span[@class='select3-chosen']";
    }

    public static String selectLikelihood(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String numberOfEmployees()
    {
       return "//div[@id='control_0626E3E2-444B-4E50-986E-02A7A110F5B9']//span[@class='select3-chosen']";
    }

    public static String selectNumberOfEmployees(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String durationOfWork()
    {
        return "//div[@id='control_66E09ED3-2A7F-4816-BE7F-8ADD9F6B4E1E']";
    }

    public static String selectDurationOfWork(String data)
    {
       return "//a[text()='"+data+"']";
    }

    public static String navigate_Engagements()
    {
        return "//label[text()='Engagements']";
    }

    public static String businessUnit_SelectAllBtn()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//b[@class='select3-all']";
    }

    public static String projectSelct(String data)
    {
       return "//a[text()='"+data+"']";
    }

    public static String confidentialCheckBox()
    {
        return "//div[@id='control_C108E1A8-9B60-4E8B-B85A-08E47E5C6A7D']/div[1]//div[1]";
    }

    public static String impactTypeSelectAll()
    {
        return "//div[@id='control_18DD1597-B800-4D1B-B063-A2E539BB3B8A']//b[@class='select3-all']";
    }

    public static String contactInquiry()
    {
        return "//div[@id='control_2D1B5E8D-BBF2-448A-9765-F03FA8C31019']";
    }

    public static String contactInquirySelect(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String locationDD()
    {
        return "//div[@id='control_F703A144-D0B6-4D4D-B5E2-D4E186427A43']";
    }

    public static String locationSelect(String data)
    {
        return "(//a[text()='"+data+"'])[2]";
    }

    public static String relatedSaveBtn()
    {
        return "//div[@id='btnSave_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']//div[text()='Save']";
    }

    public static String searchField()
    {
       return "//input[@class='txt border dirty-filter']";
    }

    public static String searchBtn()
    {
        return "//div[@id='act_filter_right']//div[text()='Search']";
    }

    public static String selectEntityRecord(String data)
    {
        return "//div[text()='"+data+"']";
    }

    public static String engagements_tab()
    {
        return "//div[text()='Engagements']";
    }

    public static String attendeesTab()
    {
       return "//div[text()='Attendees']";
    }

    public static String individualsTab()
    {
        return "//div[text()='Individuals']";
    }

    public static String engagements_add()
    {
        return "//div[@id='control_2E2CA02B-9570-4DC8-89A7-8EE214D2F06E']//div[text()='Add']";
    }

    public static String attendeeDD()
    {
        return "//div[@id='control_DA9B0184-991D-4267-B01B-36EF4792AF67']";
    }

    public static String save_individuals()
    {
        return "//div[@id='control_2E2CA02B-9570-4DC8-89A7-8EE214D2F06E']//div[@id='btnSaveAll']";
    }

    public static String entities_Tab()
    {
        return "//div[text()='Entities']";
    }

    public static String entities_add()
    {
       return "//div[@id='control_FBBF4EE8-0B0B-43B6-9B13-642704FDA74B']//div[text()='Add']";
    }

    public static String entity_attendeeDD()
    {
        return "//div[@id='control_BCCAA4BD-BD02-428C-A1E3-3BFE31AF66BF']";
    }

    public static String save_entities()
    {
        return "//div[@id='control_FBBF4EE8-0B0B-43B6-9B13-642704FDA74B']//div[@id='btnSaveAll']";
    }

    public static String engagementMethodArrow()
    {
        return "//a[text()='In-person engagements']/../i";
    }

    public static String selectEngagementRecord()
    {
        return "//div[@id='control_89186F28-56D9-423C-9FA4-8D688243B982']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr";
    }

    public static String process_flow()
    {
       return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

    public static String process_Flow()
    {
        return "//div[@id='btnProcessFlow_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }

    public static String attendeeNameSelect(String data)
    {
       return "//a[text()='DVT JHB (ITAC)']";
    }

    public static String entityAttendeeSelect(String data)
    {
       return "(//a[text()='"+data+"'])[3]";
    }

    public static String attendeeNameArrow()
    {
        return "(//a[text()='DVT JHB (ITAC)']/../i[@class='jstree-icon jstree-ocl'])[2]";
    }

    public static String entitySearchField()
    {
        return "(//input[@class='txt border'])[2]";
    }

    public static String engagementsProcessFlow()
    {
       return "//div[@id='divForms']//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

    public static String addAndEngagement()
    {
        return "//div[@id='control_A50A638E-99DE-44B8-A7B5-2BF8A948862A']//div[text()='Add an engagement']";
    }

    public static String navigate_EngagementPlan()
    {
        return"//label[text()='Engagement Plan']";
    }

    public static String engagementPlanProcess_flow()
    {
        return "//div[@id='btnProcessFlow_form_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F']";
    }

    public static String engagementPlanTitle()
    {
        return "(//div[@id='control_185410E8-D077-4DE6-8958-5772CA36E091']//input)[1]";
    }

    public static String engagementPlanStartDate()
    {
        return "//div[@id='control_90276DFA-A2DD-4A38-8D96-E84491597886']//input[@type='text']";
    }

    public static String planBusinessUnitTab()
    {
        return "//div[@id='control_8BE367EF-E449-4165-BC05-74385ECBF771']";
    }

    public static String businessUnit_SelectAllBtn(String data)
    {
       return "//a[text()='"+data+"']";
    }

    public static String EP_projectLink()
    {
        return "//div[@id='control_DE6A7B48-B355-48A7-AA3E-8475171708AF']/div[1]";
    }

    public static String EP_projectTab()
    {
       return "//div[@id='control_301410A3-9118-4FCA-93B7-4C2A90320266']";
    }

    public static String frequency()
    {
        return "//div[@id='control_0189D12C-09E0-4D04-964C-34044E11A982']";
    }

    public static String EP_engagementPurposeTab()
    {
        return "//div[@id='control_36951962-3063-4DA2-9846-ED7137AFC783']";
    }

    public static String purposeOfEngagement(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String methodOfEngagement()
    {
        return "//div[@id='control_5D729CA2-07EF-4E0A-B491-1E223A263474']";
    }

    public static String methodOfEngagementSelect(String data)
    {
        return "//ul[@class='jstree-children']//a[text()='Project Update']";
    }

    public static String personResponsibleTab()
    {
        return "//div[@id='control_C2B7C6FA-10FC-4593-BD27-6869D1790758']";
    }

    public static String EP_SaveBtn()
    {
        return "//div[@id='btnSave_form_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F']//div[text()='Save']";
    }

    public static String participantsTab()
    {
       return "//div[text()='Participants']";
    }

    public static String stakeholders(String data)
    {
        return "//div[@id='control_86E5207D-ECEA-4F6D-9680-376A0EB4C2FE']//a[text()='"+data+"']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String entitiesArrow()
    {
        return "//div[@id='control_7524420C-1E4E-49C7-AAF6-C87B97ECEFCF']//a[text()='DVT JHB (ITAC)']/../i";
    }

    public static String EP_entities(String data)
    {
        return "(//div[@id='control_7524420C-1E4E-49C7-AAF6-C87B97ECEFCF']//a[text()='"+data+"']/..//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String engagementsPlan_tab()
    {
        return "//div[@class='tabpanel_move_content']//div[text()='Engagement Plan']";
    }

    public static String selectEngagementPlanRecord()
    {
        return "//div[@id='control_3E81F0BD-F14C-4379-A2EC-610D9F4DE5A9']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr";
    }

    public static String engagementPlanProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F']";
    }

    public static String addAndEngagementPlan()
    {
        return "//div[@id='control_8919BFBB-95E9-404E-9328-BECDFF24726A']//div[text()='Add an engagement plan']";
    }

    public static String navigate_socialSuatainability()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static String navigate_socialSustainability()
    {
        return "//label[text()='Social Sustainability']";
    }

    public static String navigate_complaintsGrievances()
    {
        return "//label[text()='Complaints & Grievances']";
    }

    public static String grievanceProcess_flow()
    {
       return "//div[@id='btnProcessFlow_form_A38BD6B3-FAD6-47B5-8124-8F7DBEC01739']";
    }

    public static String grievanceTitle()
    {
        return "//div[@id='control_2640F649-7C15-4A4A-A49D-46D00B350FC0']//input";
    }

    public static String grievanceBusinessUnitTab()
    {
        return "//div[@id='control_6A2CB420-D98E-4181-9519-1658C80C4EE5']";
    }

    public static String grievanceSummary()
    {
        return "//div[@id='control_051ED5F3-EF25-4BED-BA5A-12FE10DD3877']//textarea";
    }

    public static String receivedByTab()
    {
        return "//div[@id='control_7198DB52-AF78-41DB-ADE2-D8C9044868A6']";
    }

    public static String receivedBySelect(String data)
    {
        return "//ul[@class='jstree-container-ul jstree-children']//a[contains(text(),'"+data+"')]";
    }

    public static String individualEtnity()
    {
        return "//div[@id='control_51BF378D-98C2-47AD-AF1A-22AFCB271228']";
    }

    public static String individualEntity()
    {
        return "//div[@id='control_51BF378D-98C2-47AD-AF1A-22AFCB271228']";
    }

    public static String individualEntitySelect(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String grievanceReference()
    {
        return "//div[@id='control_C3FF2F54-1F11-440F-877C-E179E880EB5F']//input";
    }

    public static String grievantNameDD()
    {
        return "//div[@id='control_3E5174D9-094B-4422-8BC5-A0C3CB60B129']";
    }

    public static String grievantNameArrow()
    {
        return "//a[text()='DVT JHB (ITAC)']/../i[@class='jstree-icon jstree-ocl']";
    }

    public static String grievantNameSelect(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])[1]";
    }

    public static String grievantLocationTab()
    {
        return "//div[@id='control_E6FA4852-121D-41FE-BC5A-D20212EC2190']";
    }

    public static String grievantLocaionSelct(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String responsibility()
    {
        return "//div[@id='control_F9C1A84D-4E33-45BC-9C36-2BF4E8BDBC38']";
    }

    public static String responsibilitySelct(String data)
    {
        return "(//ul[@class='jstree-container-ul jstree-children']//a[contains(text(),'"+data+"')])[2]";
    }

    public static String grievance_SaveBtn()
    {
       return "//div[@id='btnSave_form_A38BD6B3-FAD6-47B5-8124-8F7DBEC01739']//div[text()='Save']";
    }

    public static String receiptionDate()
    {
       return "//div[@id='control_61CCCAF9-261B-421E-9682-A1EACFB89ACB']//input";
    }

    public static String regCompletedSelct(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String homeBtn()
    {
        return "//i[@class='size-five icon land access and resettlement cool grey module iso-icon row']";
    }           

    public static String right_Arrows()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String grievance_tab()
    {
        return "//div[text()='Grievances']";
    }

    public static String grievanceProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_A38BD6B3-FAD6-47B5-8124-8F7DBEC01739']";
    }

    public static String grievanceRecord()
    {
        return "//div[@id='control_310658C6-8D55-4419-A635-89E685F26BC7']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr[1]";
    }

    public static String navigate_IncidentManagement()
    {
        return "//label[text()='Incident Management']";
    }

    public static String incidentProcess_flow()
    {
       return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String incidentTitle()
    {
        return "//div[@id='control_E9C32F4B-AB3C-4ABB-B8E6-E5D5E34F139B']//input";
    }

    public static String incidentDescription()
    {
        return "//div[@id='control_E887FEB2-18C9-444E-A023-B05D5F08BC28']//textarea";
    }

    public static String incidentOccuredTab()
    {
        return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']";
    }

    public static String incidentOccuredSelect(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String specificLocationTab()
    {
        return "//div[@id='control_0E9F8B1D-C36C-4999-9185-A5658E72A9DB']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String pintoMap()
    {
        return "//div[@id='control_82B50C4C-22B2-46DB-8F8D-677EC2CA7D33']//div[@class='c-chk']";
    }

    public static String linktoProject()
    {
        return "//div[@id='control_26DC542D-8D63-4388-B320-03B9C9ED7C88']//div[@class='c-chk']";
    }

    public static String impactTypeDD()
    {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']";
    }

    public static String impactatypeSelectAll()
    {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//b[@class='select3-all']";
    }

    public static String incidentTypeDD()
    {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']";
    }

    public static String incidentTypeSelectAll()
    {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']//b[@class='select3-all']";
    }

    public static String dateOfOccurrence()
    {
        return "//div[@id='control_A68454D1-B0FB-4EB7-B861-2AF37ACAC8DF']//input";
    }

    public static String reportedDate()
    {
        return "//div[@id='control_991FEA22-9C36-4EC7-B385-744259EB6599']//input";
    }

    public static String timeOfOccurrence()
    {
        return "(//span[@class='timeEntry-control'])[1]";
    }

    public static String reportedTime()
    {
        return "(//span[@class='timeEntry-control'])[2]";
    }

    public static String externalParties()
    {
        return "//div[@id='control_D086CF05-958D-4916-ADC3-BD45703467A5']//div[@class='c-chk']";
    }

    public static String externalPartiesSelect(String data)
    {
        return "(//a[text()='"+data+"']/..//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String externalPartiesArrow()
    {
        return "(//a[text()='DVT JHB (ITAC)']/../i[@class='jstree-icon jstree-ocl'])[1]";
    }

    public static String actionTaken()
    {
       return "//div[@id='control_58E573F1-1421-4B47-B9F9-68668A0AA36A']//textarea";
    }

    public static String reportedBy()
    {
        return "//div[@id='control_0F48D804-ECD1-4700-B480-72338CD49E6F']//div[@class='c-chk']";
    }

    public static String incidentOwner()
    {
        return "//div[@id='control_4DC8AFD7-836E-4681-9FB8-99FAF564054C']";
    }

    public static String linktoProjectSelect(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String incidentOwnerSelect(String data)
    {
        return "//ul[@class='jstree-container-ul jstree-children']//a[contains(text(),'"+data+"')]";
    }

    public static String incident_SaveBtn()
    {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[text()='Save']";
    }

    public static String projectDD()
    {
        return "//div[@id='control_1D125078-AF69-492D-887E-0BE99AF4D528']";
    }

    public static String relationshipOwner()
    {
        return "//div[@id='control_2945EC6A-6C7E-4C6B-8AB2-6707FCFA5DF9']";
    }

    public static String relatedIncidents_tab()
    {
        return "//div[text()='Related Incidents']";
    }

    public static String incidentRecord()
    {
        return "//div[@id='control_AE8EC219-4756-40BA-8773-68B10FE425BA']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr[1]";
    }

    public static String incidentProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String homeIcon()
    {
        return "//i[@class='icon land access and resettlement cool grey size-six module iso-icon row']";
    }

    public static String navigate_socialInitiatives()
    {
       return "//label[text()='Social Initiatives']";
    }

    public static String socialInitiativesProcess_flow()
    {
        return "//div[@id='btnProcessFlow_form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']";
    }

    public static String socialInitiativesBusinessUnitTab()
    {
        return "//div[@id='control_1B017CB2-B482-4AE4-AC1E-3E589A354761']";
    }

    public static String businessUnit_Select(String data)
    {
        return "//a[text()='"+data+"']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String project_Title()
    {
        return "//div[@id='control_5CD5C423-3559-4155-A152-86D25E294254']//input";
    }

    public static String TypeOfInitiative()
    {
       return "//div[@id='control_2C68539F-B3A7-4844-A919-CCA67BB70A53']";
    }

    public static String TypeOfInitiativeArrow()
    {
        return "//a[text()='Environmental Initiatives']/../i";
    }

    public static String TypeOfInitiativeSelect(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String commencementDate()
    {
        return "//div[@id='control_A02E4959-B4FD-4733-BFCD-8153C6F7CD70']//input";
    }

    public static String deliveryDate()
    {
       return "//div[@id='control_FA763176-78D8-437B-8E60-F14EC6FD89D5']//input";
    }

    public static String approvedBudget()
    {
        return "//div[@id='control_5E293AF0-02B6-4085-81A8-839A648886CD']//input[@type='number']";
    }

    public static String location_DD()
    {
        return "//div[@id='control_3A232C9E-B084-4A78-BED5-9FC568351142']";
    }

    public static String location_Select(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String responsiblePerson_Select(String data)
    {
        return "//ul[@class='jstree-container-ul jstree-children']//a[contains(text(),'"+data+"')]";
    }

    public static String responsiblePerson_DD()
    {
        return "//div[@id='control_C1F24402-51E4-4F49-9520-00D62284121D']";
    }

    public static String socialInitiative_SaveBtn()
    {
        return "//div[@id='btnSave_form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']//div[text()='Save']";
    }

    public static String initiatives_tab()
    {
        return "//div[text()='Initiatives']";
    }

    public static String InitiativesRecord()
    {
        return "//div[@id='control_BEEA2C5C-D12B-4585-BB9E-8A220737D2C3']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr[1]";
    }

    public static String beneficiries_Tab()
    {
        return "//div[text()='Beneficiaries']";
    }

    public static String stakeholderEntityPanel()
    {
        return "//span[text()='Stakeholder Entity Beneficiaries']";
    }

    public static String add_Btn()
    {
        return "//div[@id='control_8DD073E3-7338-4CBD-A351-0BA69F5A0BCC']//div[text()='Add']";
    }

    public static String entityBeneficiary()
    {
        return "//div[@id='control_B95D7401-05C0-45B6-9608-CD3EBE4274AB']";
    }

    public static String entityBeneficiary_Select(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String beneficiarySave()
    {
        return "//div[@id='control_8DD073E3-7338-4CBD-A351-0BA69F5A0BCC']//div[text()='Save']";
    }

    public static String addInitiative()
    {
       return "//div[@id='control_E181E293-8467-44CC-91D6-5B2D09CBD666']//div[text()='Add an initiative']";
    }

    public static String navigateStakeholderEntity()
    {
        return "//label[contains(text(),'Stakeholder Entity')]";
    }

    public static String navigate_commitments()
    {
        return "//label[text()='Commitments']";
    }

    public static String commitmentsProcess_flow()
    {
        return "//div[@id='btnProcessFlow_form_8541B36E-740A-4367-94DC-BF0661305F0E']";
    }

    public static String commitments_SaveBtn()
    {
        return "//div[@id='btnSave_form_8541B36E-740A-4367-94DC-BF0661305F0E']//div[text()='Save']";
    }

    public static String commitments_tab()
    {
        return "(//div[text()='Commitments'])[1]";
    }

    public static String commitmentsRecord()
    {
        return "//div[@id='control_91B6644B-A674-499F-B3D6-A2B39278BEB2']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr[1]";
    }

    public static String addACommitment()
    {
        return "//div[@id='control_DDF2EE65-8CF7-4105-A624-EF4696F4A796']//div[text()='Add a commitment']";
    }

    public static String socialStatus()
    {
        return "//div[@id='control_8BDD72E9-E054-413A-A0DF-F357AAE8C7B2']";
    }

    public static String StakeholderInterest()
    {
        return "//div[@id='control_31C7E767-EBB3-4298-91F8-6D2BB2087E27']";
    }

    public static String StakeholderInterest_select(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String StakeholderInfluence()
    {
        return "//div[@id='control_1925A4AA-1738-4B62-AEBE-52B8F6C31096']";
    }

    public static String StakeholderInfluence_select(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }

    public static String StakeholderSupport()
    {
        return "//div[@id='control_60E6161A-A29E-4010-B97C-EF370B791A18']";
    }

    public static String StakeholderSupport_select(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String save()
    {
        return "(//div[@id='btnSave_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']//div[contains(text(),'Save')])[3]";
    }

    public static String selectEntityRecord()
    {
        return "//div[@class='k-grid-content k-auto-scrollable']//tr[2]";
    }

    public static String vulnerability_tab()
    {
        return "//div[text()='Vulnerability']";
    }

    public static String SI_Entities_AddButtton()
    {
        return "//div[@id='control_EE0F548B-BD30-4478-B62D-05A394FE49A3']//div[text()='Add']";
    }

    public static String SI_EntityName()
    {
        return "//div[@id='control_9CC0D6E7-9FCE-48E5-B28D-C1BCDB97281D']";
    }

    public static String SI_EntityName_select(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String SI_Position()
    {
        return "//div[@id='control_DE361E8C-B66C-4402-B695-58413A1CA2BA']";
    }

    public static String SI_Position_select(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String SI_save()
    {
        return "(//div[@id='btnSave_form_DF1172BD-686F-4776-A54F-2498D008B515']//div[contains(text(),'Save')])[3]";
    }

    public static String se_save()
    {
        return "//div[@id='control_7016173C-BF8B-419B-8C46-D81EAA9AEC45']//div[text()='Save']";
    }

    public static String grievantName_Dropdown() {
        return "//div[@id='control_D64C196F-C0A3-4554-B859-61E42F789D3B']";
    }
    
//    public static String recordSaved_popup() {
//        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])[2]";
//    }
    public static String recordSaved_popup1() {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])[1]";
    }
    public static String recordNoChanges_Saved_popup() {
        return "(//div[@id='divMessage']//div[contains(text(),'Record has no changes to save')])[1]";
    }
}
