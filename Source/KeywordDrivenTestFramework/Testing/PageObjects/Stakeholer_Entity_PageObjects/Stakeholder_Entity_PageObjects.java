/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author 
 */
public class Stakeholder_Entity_PageObjects extends BaseClass {

    
    public static String Record_Number;
    public static String Window_1;

    public static String getWindow()
    {
        return Window_1;
    }

    public static void setWindow(String Window)
    {
        Window_1 = Window;
    }

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }
     public static String MaskBlock() {
        return"//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone() {
        return"//div[@class='ui inverted dimmer']";
    }       
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
   
    public static String navigate_Stakeholders(){
        return "//label[text()='Stakeholder Management']";
    }
    
    public static String navigate_StakeholderEntity(){
        return "//label[text()='Stakeholder Entity']";
    }
    
    public static String SE_add(){
        return "//div[text()='Add']";
    }
    
    public static String SE_ProfileTab(){
        return "//li[@id='tab_AC924995-C8F8-41F9-BC68-BB066147CC79']//div[contains(text(),'Profile')]";
    }
    
//    public static String processFlowStatus(String phase)
//    {
//    return "(//div[text()='"+phase+"'])[2]/parent::div";
//    }
    
    public static String SE_processflow(){
        return "//div[@id='btnProcessFlow_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }
    
    public static String SE_EntityType_dropdown(){
        return "//div[@id='control_121C3080-4A13-4AC2-8640-F2380C230CDC']//li";
    }
    
    public static String SE_EntityType_dropdownValue(String value)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='"+value+"']";
    }  
    
    public static String SE_name(){
        return "//div[@id='control_6B36E56B-4BD2-4A16-AD58-94FE1883EFE2']/div/div//input";
    }
    
    public static String SE_knownas(){
        return "//div[@id='control_5126FC78-97E6-49AA-A6D0-327CD4FD2CC5']/div/div//input";
    }
    
    public static String SE_industry_dropdown(){
        return "//div[@id='control_425BD26A-5DDB-485A-BA7F-E8D2E51C4BEA']";
    }
    
    public static String SE_industry_select(String text){
        return "//a[text()='"+text+"']";
    }
    
    public static String SE_desc(){
        return "//div[@id='control_4AEFCBE1-7C06-4528-BB6B-CFD298C47AA1']/div/div//textarea";
    }
    
    public static String SE_owner_dropdown(){
        return "//div[@id='control_A01F1D1A-45BF-4A6B-B2C2-88046BDAFDA1']";
    }
    
    public static String SE_owner_select(String text){
        return "//a[contains(text(),'"+text+"')]";
    }
    
    public static String SE_categories(String text){
        return "//div[@id='control_BDB3E74D-818E-4A51-8443-3F30BA7A472A']//a[contains(text(),'"+text+"')]/i[1]";
    }
    
    public static String SE_business_unit_selectall(){
        return "//div[@id='control_931D1181-0EA6-4EBF-BAA9-B497F5793EC0']//b[@original-title='Select all']";
    }
    
    public static String SE_impact_types_selectall(){
        return "//div[@id='control_962A59FE-78C1-4FCE-B40E-CAC2A8124522']//b[@original-title='Select all']";
    }
   
    public static String SE_savetocontinue(){
        return "//div[@id='control_9E597588-6E9E-4EB7-A607-CC0E20503899']//div[text()='Save and continue']";
    }
    
    public static String SE_saveSupportDocs(){
        return "//div[@id='control_6810CB58-13EE-4E0C-909D-DAE1B6392594']//div[text()='Save supporting documents']";
    }
    
    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }
    
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    
    public static String SE_SocialStatusPanel() {
        return "//div[@id='control_7E1D1634-E831-421B-882E-6C9146CA8CA5']//span[contains(text(),'Social Status')]";
    }
    
    public static String SE_EntityStatusActive() {
        return "//div[@id='control_37166D69-F3C9-482A-BCAF-F39E8271382E']//li[contains(text(),'Active')]";
    }
    
    public static String SE_TabScrollRight() {
        return "(//div[@class='tabpanel_tab_content']/div)[2]";
    }
    
    public static String SE_SupportDocTab(){
        return "//li[@id='tab_144D92B6-733C-4F37-8572-6F268883A0EC']//div[contains(text(),'Supporting Documents')]";
    }
    
    public static String SE_SupportDocLinkDoc()
    {
        return "//div[@id='control_BFA08DA7-DF6E-4B10-A435-4EBD13654902']//b[@original-title='Link to a document']";
    }
    
    public static String Medical_SurveillanceConsentFormLinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    
    public static String Medical_SurveillanceurlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String Medical_SurveillanceurlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }
    
    //Entinty Information
    public static String entityInformationTab() {
        return "//li[@id='tab_337B1D56-1774-4C9D-8515-CC886C55C1FB']";
    }

    public static String primaryContactNo() {
        return "(//div[@id='control_F58CA39A-8E24-4A56-987E-5B84412F0A9C']//input)[1]";
    }

    public static String secondaryContactNo() {
        return "(//div[@id='control_5FB377EB-8769-48EA-9981-11E3CBF7BEE4']//input)[1]";
    }

    public static String emailAddress() {
        return "(//div[@id='control_E95B004E-1283-4AD5-85C0-2AA11196F34D']//input)[1]";
    }
    
    public static String streetOne() {
        return "(//div[@id='control_C30F3943-2351-490A-A865-7D2337A1F7C5']//input)[1]";
    }
     
    public static String streetTwo() {
        return "(//div[@id='control_95D0CC4B-878A-48CE-8BF1-5149BF5D5FD6']//input)[1]";
    }
    
     public static String se_PostalCode()
    {
        return "//div[@id='control_99DCFFAC-34D1-4291-9154-5B3B7DC59D1B']/div/div/input";
    }
     
    public static String se_Location()
    {
        return "//div[@id='control_B64B2E96-473B-4207-8FCE-C7DAB53F8834']//li";
    }
    
     public static String se_Location_select(String text)
    {
        return "(//a[text()='" + text + "'])[2]";
    }
         
    public static String se_Status()
    {
        return "//div[@id='control_AEDC76EE-6EA0-48C6-91D0-A2CE27CEF60D']//li";
    }
    
    public static String se_Status_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }
     
    public static String mainContact() {
        return "(//div[@id='control_DB611D04-5A6A-485B-B7FD-3E7468577C4D']//input)[1]";
    }
     
    public static String mainContactPhone() {
        return "(//div[@id='control_1588E4E5-AAB4-4254-8D7D-0ED15A0CC591']//input)[1]";
    }
    
    public static String mainContactEmail() {
        return "(//div[@id='control_D7FD3FAD-217D-47F0-B28F-C2B1AAFF57B3']//input)[1]";
    } 
    
     public static String se_MainContactIndividual()
    {
        return "//div[@id='control_1FE052A1-221D-426B-B9E1-46569A2BA8EF']//li";
    }
    
     public static String se_MainContactIndividual_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }
    
    public static String address() {
        return "//div[@id='control_7A4E044D-6ACD-4B15-A059-EBF7A1947AAF']//textarea";
    }

    public static String correspondenceAddress() {
        return "//div[@id='control_FA9DF216-979C-4C30-98B6-3E6E7961F8BC']//textarea";
    }

    public static String website() {
        return "(//div[@id='control_D2683D07-2857-4D9C-8EF6-588B6625B2EE']//input)[1]";
    }

    public static String SE_save() {
        return "(//div[@id='btnSave_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']//div[contains(text(),'Save')])[3]";
    }

    public static String leftArrowTab() {
        return "//div[@class='tabpanel_left_scroll icon chevron left tabpanel_left_scroll_disabled']";
    }

//    public static String rightArrowTab() {
//        return "//div[@class='tabpanel_right_scroll icon chevron right']";
//    }

     public static String rightArrowTab() {
        return "(//div[@class='tabpanel_tab_content']/div)[2]";
    }
    
    //Analysis 
    public static String analysisTab() {
        return "//li[@id='tab_879C9CCB-D07A-44E8-8654-0C4503DACF50']";
    }
    
    public static String StakeholderinterestDD() {
        return "//div[@id='control_4F51BB5C-34D0-42B8-9324-66AC1198CF45']";
    }
    
    public static String StakeholderinterestSelect(String text){
        return "//a[contains(text(),'"+text+"')]";
    }
    
    public static String StakeholderinfluenceDD() {
        return "//div[@id='control_F72B39E0-9AEB-436C-8732-F06C20862FF0']";
    }
    
    public static String StakeholderinfluenceSelect(String text){
        return "(//a[contains(text(),'"+text+"')])[2]";
    }

    public static String StakeholdersupportDD() {
        return "//div[@id='control_6307BAB8-A857-4489-8F4E-B90B68B812B2']";
    }
    
    public static String StakeholdersupportSelect(String text){
        return "//a[contains(text(),'"+text+"')]";
    }
            
    public static String GuidelinesPanel() {
        return "//div[@id='control_DC0A11DE-559B-4C80-9521-7264990DCB9B']//span[contains(text(),'Guidelines')]";
    }   
    
    public static String Engageactivelywiththisstakeholder() {
        return "//div[@id='control_F1A1447B-C528-427D-96BF-15363B72C139']";
    }
          
    public static String Positivesupportisgivenfromthisstakeholder() {
        return "//div[@id='control_13A65898-B183-43DD-9FAD-5321AACCE15B']";
    }   
       
    public static String Comments() {
        return "//div[@id='control_31C00096-86E4-448A-8925-DEC2C7CD3995']//textarea";
    }
    
    public static String ImpactonstakeholderDD() {
        return "//div[@id='control_E42EE104-4F95-427F-B2EF-54A143506F6B']";
    }
    
    public static String ImpactonstakeholderExpandChevron(String value, String index) {
        return "(//div[contains(@class,'transition visible')]//li[@title='"+value+"']//i[@class='jstree-icon jstree-ocl'])["+index+"]";
    }
    
    public static String ImpactonstakeholderSelect(String text){
        return "//a[contains(text(),'"+text+"')]";
    }
    
    public static String ImpactonstakeholderLabel() {
        return "//div[@id='control_2E6EC549-FF1A-439E-A894-DA4C4836E990']";
    }
    
    public static String SE_SaveBtn() {
        return "//div[@id='btnSave_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }
    
    // actions
    public static String action_Processflow() {
        return "//div[@id='btnProcessFlow_form_F72A20BD-FF84-43A1-8729-31F4F84F23C5']";
    }
    
    public static String processFlowStatus(String phase)
    {
    return "(//div[text()='"+phase+"'])[2]/parent::div";
    }
    
    public static String processFlowStatusChild(String phase)
    {
    return "(//div[text()='"+phase+"'])[3]/parent::div";
    }
    
    public static String action_description() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }
    
    public static String departmentResponsibleDropDown() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }
     
    public static String departmentResponsibleDropDownValue(String value) {
        return "(.//a[text()='"+value+"'])[2]";
    }
        
    public static String actionDueDate() 
    {
        return ".//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
    
    public static String StakeholderEntity_Save() 
    {
        return ".//div[@id='btnSave_form_F72A20BD-FF84-43A1-8729-31F4F84F23C5']";
    }
    
    
        
    //Upload Hyperlink Documents
    public static String supportDocumentsTab() {
        return "//li[@id='tab_144D92B6-733C-4F37-8572-6F268883A0EC']//div[contains(text(),'Supporting Documents')]";
    }
    public static String SE_uploadLinkbox() {
        return "//div[@id='control_BFA08DA7-DF6E-4B10-A435-4EBD13654902']//b[@class='linkbox-link']";
    }
    public static String LinkURL(){
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    public static String urlTitle(){
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }
    public static String urlAddButton(){
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }
    
    //Engagements
    public static String engagementsTab() {
        return "//li[@id='tab_3942DBC8-B12A-4A79-97F6-4ED66D494909']";
    }
    public static String addEngagementsButton() {
        return "//div[@id='control_A50A638E-99DE-44B8-A7B5-2BF8A948862A']";
    }
    public static String processFlow() {
        return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }
    
     public static String si_engagementDate()
    {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }
        public static String businessUnitTab() {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//li";
    }
    public static String businessUnit_GlobalCompany() {
        return "//a[contains(text(),'Global Company')]";
    }
//    public static String businessUnit_SelectAll() {
//        return "(//span[@class='select3-arrow']//b[@class='select3-all'])[3]";
//    }
    
    public static String businessUnit_SelectAll()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//b[@original-title='Select all']";
    }
    public static String expandGlobalCompany() {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'Global Company')]//..//i)[1]";
    }
    public static String expandSouthAfrica(){
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'South Africa')]//..//i)[1]";
    }
    public static String businessUnitSelect(String text) {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'" + text + "')]/i)[1]";
    }
    public static String specificLocation() {
        return "(//div[@id='control_0501A692-460C-4DB5-9EAC-F15EE8934113']//input)[1]";
    }
    public static String projectLink() {
        return "//div[@id='control_29AB36D5-E83F-43EF-AFF5-F7353A5353E9']/div[1]";
    }
    public static String projectTab() {
        return "//div[@id='control_963F5190-1317-42C1-AD7A-B277FCBA7101']//li";
    }
    public static String anyProject(String text) {
        return "//a[contains(text(),'" + text + "')]";
    }
    public static String ShowMapLink() {
        return "//div[@id='control_6723362D-F9E2-457C-B125-13246382C3F7']/div[1]";
    }
    public static String engagementDate() {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }
    public static String communicationWithTab() {
       return "//div[@id='control_45EA783F-D6C7-452F-B628-C3C91C7AB4B5']";
    }
    public static String communicationWith_SelectAll() {
        return "//div[@id='control_45EA783F-D6C7-452F-B628-C3C91C7AB4B5']//b[@class='select3-all']";
    }
    public static String communicationUsers() {
        return "//a[contains(text(),'IsoMetrix Users')]";
    }
    public static String usersTab() {
        return "//div[@id='control_CD33B817-1DF7-4499-A055-2A70F29398FC']";
    }
    public static String userOption(String text){
        return "(//a[contains(text(),'"+ text +"')]/i)[1]";
    }
    public static String stakeholdersTab() {
        return "//div[@id='control_79D83FF1-6A75-4E3C-9555-ADEF3FFBFA07']";
    }
    public static String anyStakeholders(String text) {
        return "(//a[contains(text(),'" + text + "')]/i)[1]";
    }
    public static String Stakeholders_SelectAll() {
        return "//div[@id='control_79D83FF1-6A75-4E3C-9555-ADEF3FFBFA07']//b[@class='select3-all']";
    }
    public static String entitiesTab() {
        return "//div[@id='control_4C008128-D6F8-4D73-9AF1-FBED1B1C1029']";
    }
    public static String entities(String data) {
        return "(//a[contains(text(),'" + data + "')]/i)[1]";
    }
    public static String expandInPerson() {
        return "(//a[contains(text(),'In-person engagements')]//..//i)[1]";
    }
    public static String expandOtherForms() {
        return "(//a[contains(text(),'Other forms of engagement')]//..//i)[1]";
    }
    public static String anyEngagementMethod(String data) {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }
    public static String engagementMethodTab() {
       return "//div[@id='control_4A471537-8229-4E54-A86C-DCEB99BA24D0']";
    }
    public static String engagementPurposeTab() {
        return "//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']//li";
    }
    public static String anyEngagementPurpose(String data) {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }
    public static String engagementTitle() {
        return "(//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input)[1]";
    }
    public static String engagementDescription() {
        return "(//div[@id='control_1C19AE65-23A1-4ADC-A631-D9273FC0CE9F']//textarea)[1]";
    }
    public static String engagementPlan() {
        return "(//div[@id='control_F082EFDC-2D10-478A-AB94-BD4A3FB4FE4B']//textarea)[1]";
    }

    public static String managementApprovalActionCheckBox() {
        return "(//div[@id='control_523CF0A9-803B-44F2-8572-BED13C022A03']/div)[1]";
    }

    public static String stakeholderApprovalAction() {
        return "(//div[@id='control_D0FFF631-1DA5-4BD3-A1C8-75A7527E6A34']/div)[1]";
    }
    public static String responsiblePersonTab() {
        return "//div[@id='control_213251A2-010A-4BBF-A65A-A1FC8C6F7033']//li";
    }
    
    public static String actionsresponsiblePersonTab() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }
    
            
    public static String numberOfAttendees() {
        return "(//div[@id='control_3D6CBB44-8CCF-4C7F-9150-D5DDD630995E']//input)[1]";
    }
    public static String anyResponsiblePerson(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

     public static String actionsanyResponsiblePerson(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])[2]";
    }
     
     public static String impacttypes_selectall()
    {
        return "//div[@id='control_18DD1597-B800-4D1B-B063-A2E539BB3B8A']//b[@original-title='Select all']";
    }
    
    public static String contactEnquiryOrTopic()
    {
        return "//div[@id='control_2D1B5E8D-BBF2-448A-9765-F03FA8C31019']//li";
    }
    
    public static String contactEnquiryOrTopicValue(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }
    
     public static String location()
    {
        return "//div[@id='control_F703A144-D0B6-4D4D-B5E2-D4E186427A43']//li";
    }
    
    public static String locationValue(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + text + "']";
    }
    
    public static String Engagement_save() {
        return "(//div[@id='btnSave_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']//div[contains(text(),'Save')])[3]";
    }

    public static String entitiesCloseDropdown() {
        return "//div[@id='control_4C008128-D6F8-4D73-9AF1-FBED1B1C1029']//b[@class='select3-down drop_click']";
    }
    public static String projectTitle(){
        return "IsoMetrix";
    }

    public static String searchTab() {
        return "(//div[contains(text(),'Search')])[5]";
    }
    public static String searchButton() {
        return "(//div[contains(text(),'Search')])[6]";
    }

    public static String engagementsGridView() {
        return "(//div[@id='control_89186F28-56D9-423C-9FA4-8D688243B982']//div//table)[3]";
    }
    //FR2
    public static String members_tab(){
        return "//div[text()='Members']";
    }
    
    public static String member_add(){
        return "(//div[text()='Add'])[2]";
    }
    
    public static String member_name_dopdown(){
        return "(//div[@id='control_623A6228-2A56-4954-91D6-D2E07B56E612']//li)[1]";
    }
    
    public static String member_name_select(String text){
        return "(//a[text()='"+text+"'])[2]";
    }
    
    public static String member_position(){
        return "//div[@id='control_8C0CA237-7663-407A-A383-71A76D340D97']//li";
    }
    
     public static String member_Position_select(String text){
        return "(//a[text()='"+text+"'])";
    }
     
//    public static String member_save(){
//        return "(//div[text()='Save'])[3]";
//    }
    
    public static String member_save(){
        return "//div[@id='btnSave_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }
    
    public static String si_fname(){
        return "//div[@id='control_E186B9E5-4102-409D-8F57-7355938C09D9']/div/div/input";
    }
    
    public static String si_lname(){
        return "//div[@id='control_A9D1A3E8-C561-452A-A1F4-7BCB496B365F']/div/div/input";
    }
    
    public static String si_knownas(){
        return "//div[@id='control_BED0557B-BBC2-46C1-B571-BE60A267F0EA']/div/div/input";
    }
    
    public static String si_title_dropdown(){
        return "//div[@id='control_28C03054-D663-431B-9F65-38BE54617019']//li";
    }
    
    public static String si_title(String text){
        return "//a[text()='"+text+"']";
    }
    
     public static String si_relationshipOwner_dropdown()
    {
        return "//div[@id='control_4BC61A3A-EC52-4BEA-807E-B70C75D5B421']//li";
    }

     public static String si_relationshipOwner_dropdownValue(String text)
    {
        return "//a[text()='" + text + "']";
    }
     
    public static String actions_tab(){
        return "//div[text()='Actions']";
    }
    
    public static String actions_add(){
        return "//div[@id='control_6AECC604-74CD-4CA2-8014-252301E5A6E8']//div[text()='Add']";
    }
    
    public static String actions_pf(){
        return "//div[@id='btnProcessFlow_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }
    
    public static String actions_pcn(){
        return "//div[@id='control_B37A5BE9-D18E-4D38-97C8-A79B0143F45A']/div/div/input";
    }
    
    public static String actions_scn(){
        return "//div[@id='control_5FB377EB-8769-48EA-9981-11E3CBF7BEE4']/div/div/input";
    }
    
    public static String actions_address(){
        return "//div[@id='control_7A4E044D-6ACD-4B15-A059-EBF7A1947AAF']/div/div/textarea";
    }
    
    public static String actions_caddress(){
        return "//div[@id='control_FA9DF216-979C-4C30-98B6-3E6E7961F8BC']/div/div/textarea";
    }
    
    public static String actions_website(){
        return "//div[@id='control_D2683D07-2857-4D9C-8EF6-588B6625B2EE']/div/div/input";
    }
    
//    public static String actions_Save(){
//        return "(//div[text()='Save'])[2]";
//    }
    
    public static String actions_Save(){
        return "//div[@id='btnSave_form_F72A20BD-FF84-43A1-8729-31F4F84F23C5']";
    }
    
    public static String si_processflow(){
        return "//div[@id='btnProcessFlow_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']";
    }
    
    public static String si_designation(){
        return "//div[@id='control_DE0171C4-DDEA-47BD-A5D5-F4DF639EC9E2']/div/div/input";
    }
    
    public static String si_dob(){
        return "//div[@id='control_501DB50D-5488-43AF-91D3-B74D77CC09A3']//input";
    }
    
    public static String si_age(){
        return "//div[@id='control_1BC13F9E-47D7-436E-A5BB-97E88B8D86F5']/div/input";
    }
    
    public static String si_idnum(){
        return "//div[@id='control_5A680F16-7C29-49C2-9698-68C906C1F35F']/div/div/input";
    }
    
    public static String si_gender_dropdown(){
        return "//div[@id='control_BC0B3729-5DA7-493E-BA90-653F5C3E6151']";
    }
    
    public static String si_gender_select(String text){
        return "//a[text()='"+text+"']";
    }
    
    public static String si_nationality_dropdown(){
        return "//div[@id='control_447AC363-4DFF-4821-AB92-6695191EC822']";
    }
    
    public static String si_nationality_select(String text){
        return "//a[text()='"+text+"']";
    }
    
    public static String profile_tab(){
        return "//div[@id='control_38AB48FE-5C8D-401B-948C-9985EF810CED']//div[text()='Profile']";
    }
    
    public static String si_stakeholdercat_selectall(){
        return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']//b[@original-title='Select all']";
    }
    
    public static String si_stakeholdercat(String text){
        return "//a[text()='"+text+"']/i[1]";
    }
    
    public static String si_appbu_selectall(){
        return "//div[@id='control_4CFB2165-708B-4D55-9988-9CDCB5487291']//b[@original-title='Select all']";
    }
    
    public static String si_appbu_expand(String text){
        return "//a[text()='"+text+"']/../i";
    }
    
    public static String si_appbu(String text){
        return "//a[text()='"+text+"']/i[1]";
    }
    
    public static String si_impacttypes_selectall(){
        return "//div[@id='control_65F1B5F4-17B9-48FE-817D-B27F54AB360E']//b[@original-title='Select all']";
    }
    
    public static String si_impacttypes(String text){
        return "//a[text()='"+text+"']/i[1]";
    }
    public static String si_locationmarker(){
        return "//div[@id='control_6A7B2112-2526-41F1-893D-2702E81144D7']//div[@class='icon location mark mapbox-icons']";
    }
    
    public static String location_tab(){
        return "//div[text()='Location']";
    }
    
    public static String si_location(){
        return "jozi3.PNG";
    }
    public static String si_save(){
        return "(//div[text()='Save'])[2]";
    }
    public static String contractorSupplierManager_Tab() {
        return "//li[@id='tab_5649976E-D3E2-4B5D-8E83-DECD2901F3A0']//div[contains(text(),'Contractor or Supplier Manager')]";
    }
    public static String questionnaires_Panel() {
        return "//span[contains(text(),'Questionnaire')]";
    }
    public static String questionnaires_add() {
        return "//div[@id='control_7517949D-9F94-4E11-8C20-9CE0EF740490']//div[contains(text(),'Add')]";
    }

    public static String questionnaireProcessFlow() {
        return "//div[@id='btnProcessFlow_form_7B9DCBC8-678E-4ED9-8CF5-C5AA78BED2D8']";
    }

    public static String documents_Panel() {
        return "//span[contains(text(),'Documents')]";
    }

    public static String documents_add() {
        return "//div[@id='control_1D721996-A61A-40FD-91E8-7FAFDF3B5039']//div[contains(text(),'Add')]";
    }

    public static String documents_Input() {
        return "(//div[@id='control_4EC7578A-B88B-4442-AA32-754B27F4FDF3']//input)[1]";
    }

    public static String documentsUploaded_Dropdown() {
        return "//div[@id='control_F3FF8316-9847-4F33-ACCE-CEDA5D02BE01']//li";
    }

    public static String documentsUploaded_Dropdown_Option(String data) {
        return "(//a[text()='" + data + "'])[1]";
    }

    public static String outcome_Dropdown_Option(String data) {
        return "(//a[text()='" + data + "'])[1]";
    }

    public static String outcome_Dropdown() {
        return "//div[@id='control_6BF2BC67-11B6-45D8-9A90-E7A8FB55DDAE']//li";
    }

    public static String dateVerifiedTab() {
        return "(//div[@id='control_1FD49B28-3FF0-4931-9C73-2E3D2D7AA68E']//span//input)[1]";
    }

    public static String commentsTab() {
        return "(//div[@id='control_1FF7D5F1-8209-4446-ABE0-D0D89B11FF22']//textarea)[1]";
    }

    public static String documents_Save() {
        return "(//div[@id='btnSave_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']//div[contains(text(),'Save')])[3]";
    }

    public static String supporting_tab(){
        return "//div[text()='Supporting Documents']";
    }
    
    public static String linkbox(){
        return "//div[@id='control_1F0A25D4-9905-4913-BC61-B1EED48CCCC6']//b[@original-title='Link to a document']";
    }
  
    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }
    
    public static String si_savetocontinue(){
        return "//div[text()='Save supporting documents']";
    }
    
    public static String se_createnewindividual(){
        return "//div[text()='Create a new individual']";
    }
    
    public static String si_rightarrow(){
        return "//div[@id='control_38AB48FE-5C8D-401B-948C-9985EF810CED']//div[@class='tabpanel_right_scroll icon chevron right']";
    }
    
    public static String sik_rightarrow(){
        return "rightarrow.PNG";
    }
    
    public static String ri_ename(){
        return "//div[@class='optionsPanel firstPanel primary']//td[text()='Entity name']/../td[5]/input";
    }
//    public static String ri_esearch(){
//        return "//div[text()='Search']";
//    }
    
     public static String ri_esearch(){
        return "//div[@id='btnActApplyFilter']";
    }
    
    public static String ri_eselect(String text){
        return "(//div[text()='"+text+"'])[1]";
    }
    public static String ri_related(){
        return "(//div[text()='Related Incidents'])[1]";
    }
    
    public static String ri_relatedPinned(){
        return "//div[@id='control_AE8EC219-4756-40BA-8773-68B10FE425BA']//span[@class='pin-filter icon pushpin transition visible pinned']";
    }
    
    public static String ri_record(){
        return "//div[@id='control_AE8EC219-4756-40BA-8773-68B10FE425BA']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr[1]";
    }
    public static String ri_wait(){
        return "(//div[text()='Incident Management '])[1]";
    }

    public static String supportDocumentsTab_CQ() {
        return "//li[@id='tab_88BC6C8F-E230-4853-BE1A-9E90200423E0']//div[contains(text(),'Supporting Documents')]";
    }
    public static String uploadLinkbox() {
        return "(//div[contains(text(),'Supporting Documents')]//..//div[@class='linkbox-options']//b[@class='linkbox-link'])[2]";
    }
    public static String saveToContinueButton(){
        return "//div[@id='control_D4D50C09-26C5-410D-9549-2CC4BE86A134']//div[contains(text(),'Save to continue')]";
    }
    public static String saveSupportingDocuments(){
        return "//div[@id='btnSave_form_7B9DCBC8-678E-4ED9-8CF5-C5AA78BED2D8']";
    }

    public static String companyType_Dropdown() {
        return "//div[@id='control_D8B07754-94EC-4FB2-859C-B738B6EAFD91']//li";
    }

    public static String companyType_Option(String data) {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String relevantEntity_RegNo() {
        return "(//div[@id='control_D6E2582F-135F-475C-8180-054CA159CFCC']//input)[1]";
    }

    //Tabs
    public static String companyInformationTab() {
        return "//li[@id='tab_2AED1AEA-A885-4D65-8C46-A4E0FDD8A034']";
    }
    public static String businessPartnerTab() {
       return "//li[@id='tab_ECC116DB-F2F9-49D9-82C8-122332F6CCC8']";
    }
    public static String companyCertificatesTab() {
       return "//li[@id='tab_73A0738E-B7E2-4F10-9CE7-EF3C31E48A89']";
    }
    public static String safetyHealthIssuesTab() {
       return "//li[@id='tab_44F21890-1F73-40D6-BCEF-0696F8566A3A']";
    }
    public static String healthSafeyEnvironmentTab() {
       return "//li[@id='tab_B01371BE-B0A1-4F0F-A195-7DD5D7BC264E']";
    }
    public static String additionalCompanyInformationTab() {
       return "//li[@id='tab_25BDF161-DD62-438F-B244-F3BF5FD82B72']";
    }
    public static String businessPartnerRiskRankingTab() {
       return "//li[@id='tab_CA730638-F9B1-4552-AE71-1892F034122D']";
    }
    
    //Data
    public static String mainArea() {
        return "(//div[@id='control_349C41BC-5C52-4311-B0AD-5203F0596F74']//input)[1]";
    }
    public static String operatingUnit() {
        return "(//div[@id='control_7236A83F-F382-487E-A660-2213CFC22BB0']//input)[1]";
    }
    public static String qualityManagement_Dropdown() {
        return "//div[@id='control_1575F13D-D625-43F4-AAF6-3462C227AD3A']//li";
    }

    public static String qualityManagement_Option(String data) {
        return "(//div[@id='control_3AA7ED92-601B-477C-9BF2-A248733FD534']/../../../../../../../..//a[text()='" + data + "'])[1]";
    }

    public static String environmentalManagement_Dropdown() {
        return "//div[@id='control_749B58E4-1696-44A2-A2B5-6544169861E1']//li";
    }

    public static String environmentalManagement_Option(String data) {
         return "(//div[@id='control_0D0FD72F-3263-4222-82C9-5964A0D96B95']/../../../../../../../..//a[text()='" + data + "'])[2]";
    }

    public static String COID_Act_Dropdown() {
        return "//div[@id='control_2BF947B0-C0CE-4F29-BEB7-EEAD413BCABF']//li";
    }

    public static String COID_Act_Option(String data) {
        return "(//div[@id='control_D8A26477-3F38-4CA0-A72D-19E94C7B0F3A']/../../../../../../../..//a[text()='" + data + "'])[3]";
    }

    public static String COID_RegNo() {
       return "(//div[@id='control_D70BA03D-C157-42B0-9C6E-4F1A62106056']//input)[1]";
    }

    public static String rightArrowButton() {
        return "//div[@id='control_5554718D-1C5B-401C-AE1E-6ABF12EDB801']//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String legalRegister_Dropdown() {
        return "//div[@id='control_AC471A15-AE17-44CA-9330-9110BEAA186E']//li";
    }

    public static String legalRegister_Option(String data) {
        return "(//div[@id='control_3C1ADF6C-2A91-4289-B91B-FBA0B3C43FE4']/../../../../../../../..//a[text()='" + data + "'])[4]";
    }

    public static String healthSafetyPlan_Dropdown() {
        return "//div[@id='control_3C62267D-11AA-4E60-AEB8-7F89010D81AE']";
    }

    public static String healthSafetyPlan_Option(String data) {
        return "(//div[@id='control_204F364D-A254-4AA9-BE1C-0089EE76B00F']/../../../../../../../..//a[text()='" + data + "'])[5]";
    }

    public static String rentedOwnedLeased_Dropdown() {
        return "//div[@id='control_CC9B150A-E4AF-47F4-82A2-A191DBC23DDC']//li";
    }

    public static String rentedOwnedLeased_Option(String data) {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String furnishAnnualNettProfit() {
        return "(//div[@id='control_86D35ED9-FA92-4695-8B43-9CFBE975017E']//input)[1]";
    }

    public static String questionnaire_Save() {
        return "//div[@id='btnSave_form_7B9DCBC8-678E-4ED9-8CF5-C5AA78BED2D8']//div[text()='Save']";
    }

    public static String scopeOfWork() {
        return "(//div[@id='control_32D49E9F-7654-44D8-89AB-9A24D0E343A4']//textarea)[1]";
    }
    
    public static String report(){
        return "//div[@id='btnReports']";
    }
    
    public static String view_reports(){
        return "//span[@title='View report ']";
    }

    public static String full_Reports(){
        return "//span[@title='Full report ']";
    }
    
    public static String view_wait(){
        return "//div[text()='Entity report']";
    }
    
    public static String full_wait(){
        return "//div[text()='Entity status']";
    }
    
//    public static String popup_conf(){
//        return "//div[@id='btnConfirmYes']";
//    }
//    
    public static String popup_conf(){
        return "//div[@title='Continue']";
    }
    
    
    public static String stakeholder_Array(String data){
        return "(//a[text()='" + data + "']/i[1])";
    }

    public static String anyStakeholders2(String text,int counter) {
        return "(//a[contains(text(),'" + text + "')]/i[1])[ " + counter + " ]";
    }
    public static String se_locationmarker(){
        return "//div[@id='control_D451A141-6811-4844-9A2C-501A338A05C7']//div[@class='icon location mark mapbox-icons']";
    }
    public static String inspection_RecordSaved_popup() {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }
    public static String recordSaved_popup() {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])[2]";
    }
    public static String failed(){
       return "(//div[@id='txtAlert'])[2]";
   }
    
    public static String email_notification(){
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Stakeholder Entity ')][contains(text(),'has been added')])[1]";
    }
    
    public static String actions_email_notification(){
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Stakeholder Entity Actions')][contains(text(),'has been logged')])[1]";
    }
    
    public static String SE_AnalysisTab() {
        return "//li[@id='tab_879C9CCB-D07A-44E8-8654-0C4503DACF50']//div[contains(text(),'Stakeholder Analysis')]";
    }
    
    public static String SE_TopicIssueAssessmentPanel() {
        return "//div[@id='control_43968C1B-65B3-43EC-8AF7-5F14D2382B42']//span[contains(text(),'Topic/Issue Assessment')]";
    }
    
    public static String SE_TopicIssueAssessmentAdd() {
        return "//div[@id='control_791664E9-F4D5-4CBB-9BAA-B187310C3D6F']//div[@id='btnAddNew']";
    }
    
    public static String SE_TopicIssueDD() {
        return "(//div[@id='control_359049FA-9B56-4D59-A670-93070530E0E4'])[1]";
    }
    
    public static String expandChevronTree(String value, String index)
    {
        return "(//div[contains(@class,'transition visible')]//li[@title='"+value+"']//i[@class='jstree-icon jstree-ocl'])["+index+"]";
    }  
    
    public static String singleSelectDropDownByXpath(String value, String index)
    {
        return "(.//a[text()='"+value+"'])["+index+"]/i[1]";
    }
    
    public static String singleSelectDropDownByXpath1(String value, String index)
    {
        return "(.//a[text()='"+value+"'])["+index+"][1]";
    }
    
    public static String singleSelectDropDownByXpath2(String value, String index)
    {
        return "(.//a[text()='"+value+"'])["+index+"]";
    }
    
    public static String SE_InfluenceDD() {
        return "(//div[@id='control_46284CB6-8E0D-4AF6-BD22-0AF4AA6E7759'])[1]";
    }
    
    public static String SE_InterestDD() {
        return "(//div[@id='control_63C66781-07ED-4EB2-8A2F-CAED92ED118C'])[1]";
    }
    
    public static String SE_TopicIssueAssessmentSaveAll() {
        return "//div[@id='control_791664E9-F4D5-4CBB-9BAA-B187310C3D6F']//div[@id='btnSaveAll']";
    }
    
    public static String IsoMetrixlogo() {
        return "//div[text()='Solutions Templates - QA']";
    }
    
//    public static String navigate_EHS()
//    {
//        return "//label[contains(text(),'Environment, Health & Safety')]";
//    }
//
//    public static String navigate_Stakeholders()
//    {
//        return "//label[text()='Stakeholder Management']";
//    }

    public static String navigate_StakeholderIndividual()
    {
        return "//label[text()='Stakeholder Individual']";
    }

    public static String search()
    {
        return "(//div[@id='searchOptions']//input[@class='txt border'])[1]";
    }
    
    public static String searchbutton()
    {
        return "//div[@id='btnActApplyFilter']";
    }
    
    public static String SI_Record()
    {
        return "(//div[text()='Abel Mantoa (Percy)'])[1]";
    }
    
    public static String SI_EntitiesTab()
    {
        return "//li[@id='tab_B82C2B7B-17B4-489B-A8EB-B3C93477CBBE']//div[contains(text(),'Entities')]";
    }
    
    public static String SI_RecordPosition1()
    {
        return "(//li[text()=' Brother'])[1]";
    }
    
    public static String SI_RelatedEntitiesRecord(String value)
    {
        return "(//a[text()='"+value+"'])[1]";
    }  
    
    public static String member_Position_Update(String text){
        return "(//a[text()='"+text+"'])[1]";
    }
    
    public static String SI_RelatedEntitiesSave()
    {
        return "(//div[@id='btnSaveAll']";
    }
    
    public static String SE_Record()
    {
        return "(//div[text()='Company_OS 13.3'])[1]";
    }
    
    //Associated Entities
    public static String associatedentities_tab()
    {
        return "//li[@id='tab_A07B0D3C-A829-4D3F-B4E3-1BC4FF780063']//div[text()='Associated Entities']";
    }
    
    public static String associatedentities_add()
    {
        return "//div[@id='control_90C85E93-A691-4C58-982F-39EEE9A0BC53']//div[text()='Add']";
    }
    
    public static String group_name_dopdown(){
        return "(//div[@id='control_F2EBECAD-5135-4844-B579-0A02F8CE3738']//li)[1]";
    }
    
    public static String entity_name_select(String text){
        return "(//a[text()='"+text+"'])[1]";
    }
    
    public static String group_position(){
        return "//div[@id='control_D50B12E5-3EBB-4095-AA89-FFC23B7AC247']//li";
    }
    
     public static String group_Position_select(String text){
        return "(//a[text()='"+text+"'])[1]";
    }
     
//    public static String member_save(){
//        return "(//div[text()='Save'])[3]";
//    }
    
    public static String group_save(){
        return "//div[@id='btnSave_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }
    
    public static String entity_search()
    {
        return "(//input[@class='txt border'])[2]";
    }
    
    public static String Entity_RecordSelect(String text){
        return "(//div[text()='"+text+"'])[1]";
    }
    
    public static String SE_AssociatedEntitiesRecord(String value)
    {
        return "(//li[text()='"+value+"'])[1]";
    } 
    
    public static String se_createnewentity(){
        return "//div[text()='Add new entity']";
    }
    
    public static String se_processflow(){
        return "//div[@id='btnProcessFlow_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }
    
    public static String Orders_Panel() {
        return "//span[contains(text(),'Orders')]";
    }
    
    public static String Orders_add() {
        return "//div[@id='control_D0274203-59E0-4BD0-8D74-E760964E515E']//div[contains(text(),'Add')]";
    }
    
    public static String Orders_PF() {
        return "//div[@id='btnProcessFlow_form_AC3617FE-8383-45BB-8611-EA8CA705B2C1']";
    }
    
    public static String Orders_OrderNo() {
        return "(//div[@id='control_BE92E94B-74CE-4BF6-B70E-31CE21A1422B']//input)[1]";
    }
    
    public static String Orders_StatusDD() {
        return "//div[@id='control_F50C9133-D3BE-45C0-88F6-4FCCCE4BEE4D']";
    }
    
    public static String Orders_Status_Dropdown_Option(String data) {
        return "(//a[text()='" + data + "'])[1]";
    }
    
    public static String Orders_ApprovedbyDD() {
        return "//div[@id='control_060F866E-28F3-4F44-817C-76780546B4ED']";
    }
    
    public static String Orders_Approvedby_Dropdown_Option() {
        return "//li[@title='1 Administrator']//a";
    }
    

    public static String Orders_dateapproved() {
        return "(//div[@id='control_010013A1-86AE-4EB6-AC9F-847A74A5055F']//span//input)[1]";
    }
    
    public static String Orders_Scopeofworktab() {
        return "//li[@id='tab_B0CCF67B-84FF-4F61-8964-163DE258781C']";
    }

    public static String MultiselectexpandChevronTree(String value, String index)
    {
        return "//ul[@class='select3-results jstree jstree-10 jstree-default jstree-checkbox-selection jstree-loading']//li[@title='"+value+"']//i[@class='jstree-icon jstree-ocl']["+index+"]";
    }

    public static String MultiselectSelect(String value, String index)
    {
        return "//ul[@class='select3-results jstree jstree-10 jstree-default jstree-checkbox-selection jstree-loading']//a[text()='"+value+"']["+index+"]//i[@class='jstree-icon jstree-checkbox']";
    }
    
    public static String Orders_Project() {
        return "(//div[@id='control_202D2497-A69E-4769-8980-4367637F0BB8']//input)[1]";
    }
    
    public static String Orders_Projectdescription() {
        return "(//div[@id='control_F42D5C79-1D18-448A-AA61-A9AAD5479950']//textarea)[1]";
    }
    
    public static String Orders_Startdate() {
        return "(//div[@id='control_90D93B64-68B5-4748-897A-498F7C8DE4BA']//input)[1]";
    }
    
    public static String Orders_Enddate() {
        return "(//div[@id='control_3A59A41B-41D0-498E-BD56-1D56B52C07B9']//input)[1]";
    }
    
    public static String Orders_ContractortypeDD() {
        return "//div[@id='control_3063404C-690E-4175-BB7F-E3A4D99DDF9C']";
    }
    
    public static String Orders_Contractortype_Option(String data) {
        return "(//a[text()='" + data + "'])/i[1]";
    }
    
    public static String Orders_Save() {
        return "//div[@id='btnSave_form_AC3617FE-8383-45BB-8611-EA8CA705B2C1']";
    }
    
    public static String Orders_riskassessmentTab() {
        return "//li[@id='tab_E4162FF2-2775-4F7B-8613-E192022F9B7E']";
    }
    
    public static String Orders_riskassessmentadd() {
        return "//div[@id='control_2466C37B-B25C-4DF6-B32D-6F623F2AC781']//div[contains(text(),'Add')]";
    }
    
    public static String RiskassessmentPF() {
        return "//div[@id='btnProcessFlow_form_7F5090E0-7D49-4B93-962C-FA8DF119B3D6']";
    }
    
    public static String RiskassessmentCapturerLogger() {
        return "//div[@id='control_8EAF69D1-B750-4545-BD49-99E898FA1971']//li[@title='3 Logger']";
    }
    
    public static String RiskassessmentRiskrankingdate() {
        return "//div[@id='control_90DA037B-D4F2-4862-BF60-34485B5C0DB6']//input";
    }
    
    public static String RiskassessmentDescriptionscope() {
        return "(//div[@id='control_85DB80BB-F2BD-41B8-901C-338EBCF53047']//textarea)[1]";
    }
    
    public static String RiskassessmentRelatedDocs() {
        return "//li[@id='tab_ECF8B2EB-29EF-40E6-AD13-AE2CEF63F5CE']";
    }
    
    public static String RiskassessmentRelatedDocsLinkDoc() {
        return "//div[@id='control_315F609A-FA2C-44FE-A010-149E750E725E']//b[@original-title='Link to a document']";
    }
    
    public static String RiskassessmentLinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    
    public static String RiskassessmentURLTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }
    
    public static String RiskassessmenturlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }
    
    public static String RiskassessmentRiskRankingTab() {
        return "//li[@id='tab_41A55F7A-7F5A-4C4C-8959-007F572FA3D7']";
    }
    
    public static String Riskassessment1BlastingDD() {
        return "//div[@id='control_E81E2C37-3D9A-46A5-932D-30B82C0F97D5']";
    }
    
    public static String Riskassessment1BlastingYes() {
        return "(//a[text()='Yes'])[1]";
    }
    
    public static String Riskassessment2CommunityDD() {
        return "//div[@id='control_0D4428C1-5B20-4EF6-B7C1-E139F54B03F1']";
    }
    
    public static String Riskassessment2CommunityYes() {
        return "(//a[text()='Yes'])[2]";
    }
    
    public static String Riskassessment3WorkingDD() {
        return "//div[@id='control_03562577-C7CA-4F51-A506-03D9C5CCC74B']";
    }
    
    public static String Riskassessment3WorkingYes() {
        return "(//a[text()='Yes'])[3]";
    }
    
    public static String Riskassessment4DischargingDD() {
        return "//div[@id='control_7170CA96-6A82-4F56-B465-414D4ECAFF43']";
    }
    
    public static String Riskassessment4DischargingYes() {
        return "(//a[text()='Yes'])[4]";
    }
    
    public static String Riskassessment5IsolationDD() {
        return "//div[@id='control_719E9CD9-8ECE-480B-A1FF-3A4145A1D251']";
    }
    
    public static String Riskassessment5IsolationYes() {
        return "(//a[text()='Yes'])[5]";
    }
    
    public static String Riskassessment6EnvironmentalDD() {
        return "//div[@id='control_675EACE3-43C1-43BB-8023-F369E6A48A99']";
    }
    
    public static String Riskassessment6EnvironmentalYes() {
        return "(//a[text()='Yes'])[6]";
    }
    
    public static String Riskassessment7ExcavationDD() {
        return "//div[@id='control_B54DC2A1-EEA5-408D-8A4D-9681E0AE2835']";
    }
    
    public static String Riskassessment7ExcavationYes() {
        return "(//a[text()='Yes'])[7]";
    }
    
    public static String Riskassessment8ExplosionsLabel() {
        return "//div[@id='control_25E1CE00-A9D6-4888-BBF8-7542B66D8101']";
    }
    
    public static String Riskassessment8ExplosionsDD() {
        return "//div[@id='control_430EA317-2642-4033-917C-C9F84B153DCB']";
    }
    
    public static String Riskassessment8ExplosionsYes() {
        return "(//a[text()='Yes'])[8]";
    }
    
    public static String Riskassessment9ExposureDD() {
        return "//div[@id='control_615370F8-8BE7-4FA5-B8AB-C998C21E5753']";
    }
    
    public static String Riskassessment9ExposureYes() {
        return "(//a[text()='Yes'])[9]";
    }
    
    public static String Riskassessment10ExposureDD() {
        return "//div[@id='control_1FDB5742-B172-4B76-9B79-D51831B3F146']";
    }
    
    public static String Riskassessment10ExposureYes() {
        return "(//a[text()='Yes'])[10]";
    }
    
    public static String Riskassessment11ExposureDD() {
        return "//div[@id='control_E5C69DB4-3356-4D40-A1CF-ED0A22E3A2B7']";
    }
    
    public static String Riskassessment11ExposureYes() {
        return "(//a[text()='Yes'])[11]";
    }
    
    public static String Riskassessment12FailureDD() {
        return "//div[@id='control_10BB4310-729E-41ED-91E6-AD0C5A025DA5']";
    }
    
    public static String Riskassessment12FailureYes() {
        return "(//a[text()='Yes'])[12]";
    }
    
    public static String Riskassessment13FallDD() {
        return "//div[@id='control_8EB91FD7-1AFE-4AED-88A4-D23A28275E25']";
    }
    
    public static String Riskassessment13FallYes() {
        return "(//a[text()='Yes'])[13]";
    }
    
    public static String Riskassessment14FiresDD() {
        return "//div[@id='control_582061CF-6561-4EAA-A84D-AB71BBBB6736']";
    }
    
    public static String Riskassessment14FiresYes() {
        return "(//a[text()='Yes'])[14]";
    }
    
    public static String Riskassessment15ExposureDD() {
        return "//div[@id='control_A008DFAE-C99E-41A2-A745-D533DF85F135']";
    }
    
    public static String Riskassessment15ExposureYes() {
        return "(//a[text()='Yes'])[15]";
    }
    
    public static String Riskassessment16GasDD() {
        return "//div[@id='control_AF195D70-A84F-4D15-AC04-6106329FE689']";
    }
    
    public static String Riskassessment16GasYes() {
        return "(//a[text()='Yes'])[16]";
    }
    
    public static String Riskassessment17HazardousDD() {
        return "//div[@id='control_B4947CCC-FAA4-49F7-ADFA-1413A7292A50']";
    }
    
    public static String Riskassessment17HazardousYes() {
        return "(//a[text()='Yes'])[17]";
    }
    
    public static String Riskassessment18HoistingDD() {
        return "//div[@id='control_491A8153-CA9A-4CAF-95A7-845006CD51C8']";
    }
    
    public static String Riskassessment18HoistingYes() {
        return "(//a[text()='Yes'])[18]";
    }
    
    public static String Riskassessment19ExposureDD() {
        return "//div[@id='control_3D75CC26-1018-4B3A-92A1-DD747F82EDF7']";
    }
    
    public static String Riskassessment19ExposureYes() {
        return "(//a[text()='Yes'])[19]";
    }
    
    public static String Riskassessment20IlluminationLabel() {
        return "//div[@id='control_96932818-2EE3-4437-AADD-E9214D450BAA']";
    }
    
    public static String Riskassessment20IlluminationDD() {
        return "//div[@id='control_595905BB-E828-4EED-9996-659D7AF61F8E']";
    }

    public static String Riskassessment20IlluminationYes() {
        return "(//a[text()='Yes'])[20]";
    }
    
    public static String Riskassessment21DamsDD() {
        return "//div[@id='control_B5FA73AE-39A0-4BA3-ADCD-763F6D7CD3DE']";
    }
    
    public static String Riskassessment21DamsYes() {
        return "(//a[text()='Yes'])[21]";
    }
    
    public static String Riskassessment22MaterialDD() {
        return "//div[@id='control_2B788994-283B-4712-AAA3-03F7092E3577']";
    }
    
    public static String Riskassessment22MaterialYes() {
        return "(//a[text()='Yes'])[22]";
    }
    
    public static String Riskassessment23MobileDD() {
        return "//div[@id='control_620A496C-63CB-461D-8E70-9DEC5C04E88E']";
    }
    
    public static String Riskassessment23MobileYes() {
        return "(//a[text()='Yes'])[23]";
    }
    
    public static String Riskassessment24NoiseDD() {
        return "//div[@id='control_39C2DD45-590A-48F4-B3E1-180E7ADDBED5']";
    }
    
    public static String Riskassessment24NoiseYes() {
        return "(//a[text()='Yes'])[24]";
    }
    
    public static String Riskassessment25StoredDD() {
        return "//div[@id='control_BBE6B2F1-E1D9-4B0C-AE05-A1AEFFA45593']";
    }
    
    public static String Riskassessment25StoredYes() {
        return "(//a[text()='Yes'])[25]";
    }
    
    public static String Riskassessment26TransportDD() {
        return "//div[@id='control_FC63AB3B-5AA6-4B03-ADD6-27F3B314724C']";
    }
    
    public static String Riskassessment26TransportYes() {
        return "(//a[text()='Yes'])[26]";
    }
    
    public static String Riskassessment27WorkingDD() {
        return "//div[@id='control_B8254163-C1B6-48F3-87AD-8A1D6B244482']";
    }
    
    public static String Riskassessment27WorkingYes() {
        return "(//a[text()='Yes'])[27]";
    }
    
    public static String Riskassessment28WorkingDD() {
        return "//div[@id='control_7FB87551-26A2-4C24-AF47-512AE2F5799F']";
    }
    
    public static String Riskassessment28WorkingYes() {
        return "(//a[text()='Yes'])[28]";
    }
    
    public static String Riskassessment29CarryingDD() {
        return "//div[@id='control_E716912C-9DDE-4C6B-BE15-90D7BE9416B7']";
    }
    
    public static String Riskassessment29CarryingYes() {
        return "(//a[text()='Yes'])[29]";
    }
    
    public static String Riskassessment30ElectricalDD() {
        return "//div[@id='control_6CD4EF41-14B3-4786-BF05-B9E620ED4BCC']";
    }
    
    public static String Riskassessment30ElectricalYes() {
        return "(//a[text()='Yes'])[30]";
    }
    
    public static String Riskassessment31GeneralDD() {
        return "//div[@id='control_0D364D94-E2F7-4A08-818D-B6A83E58FAE4']";
    }
    
    public static String Riskassessment31GeneralYes() {
        return "(//a[text()='Yes'])[31]";
    }
    
    public static String Riskassessment32HighwallDD() {
        return "//div[@id='control_86C6FFCE-BBA1-44D2-A616-9F4165566E20']";
    }
    
    public static String Riskassessment32HighwallYes() {
        return "(//a[text()='Yes'])[32]";
    }
    
    public static String Riskassessment33EmergencyLabel() {
        return "//div[@id='control_71A34CD6-42F5-4AF4-8F0F-35EC35E51273']";
    }
    
    public static String Riskassessment33EmergencyDD() {
        return "//div[@id='control_1BA39AB8-1331-4559-AF35-0E686E0D8A19']";
    }
    
    public static String Riskassessment33EmergencyYes() {
        return "(//a[text()='Yes'])[33]";
    }
    
    public static String Riskassessment34UtilisingDD() {
        return "//div[@id='control_963E1119-2D0A-41DE-AADA-2C5683952616']";
    }
    
    public static String Riskassessment34UtilisingYes() {
        return "(//a[text()='Yes'])[34]";
    }
    
    public static String Riskassessment35ToxicDD() {
        return "//div[@id='control_D4D76E72-EFFF-46DC-BE54-6ED80431C3F2']";
    }
    
    public static String Riskassessment35ToxicYes() {
        return "(//a[text()='Yes'])[35]";
    }
    
    public static String Riskassessment36ElectricDD() {
        return "//div[@id='control_1BDB6D99-30D8-4054-898C-9FFBA453A378']";
    }
    
    public static String Riskassessment36ElectricYes() {
        return "(//a[text()='Yes'])[36]";
    }
    
    public static String Riskassessment37WorkingDD() {
        return "//div[@id='control_923CB509-F61C-46FC-8466-9314BA397F0F']";
    }
    
    public static String Riskassessment37WorkingYes() {
        return "(//a[text()='Yes'])[37]";
    }
    
    public static String Riskassessment38OxygenDD() {
        return "//div[@id='control_D1EC916F-5677-4057-9D73-3B6F5C207EF4']";
    }
    
    public static String Riskassessment38OxygenYes() {
        return "(//a[text()='Yes'])[38]";
    }
    
    public static String Riskassessment39ExposureDD() {
        return "//div[@id='control_C983D89C-0A98-48DC-874A-A14E081B6F38']";
    }
    
    public static String Riskassessment39ExposureYes() {
        return "(//a[text()='Yes'])[39]";
    }
    
    public static String Riskassessment40WinchesDD() {
        return "//div[@id='control_80D3817F-B048-483A-AB89-2C0C35D558A0']";
    }
    
    public static String Riskassessment40WinchesYes() {
        return "(//a[text()='Yes'])[40]";
    }
    
    public static String Riskassessment41TrackboundDD() {
        return "//div[@id='control_D40A6ADE-A23A-4DD6-B7A1-5CD7BDD662C3']";
    }
    
    public static String Riskassessment41TrackboundYes() {
        return "(//a[text()='Yes'])[41]";
    }
    
    public static String Riskassessment42ExposureDD() {
        return "//div[@id='control_B59C46C9-F16A-43A2-B727-6DC9DBDEFD8C']";
    }
    
    public static String Riskassessment42ExposureYes() {
        return "(//a[text()='Yes'])[42]";
    }
    
    public static String Riskassessment43ExposureDD() {
        return "//div[@id='control_864FDACA-4D92-4734-AFED-0114BF3FA80A']";
    }
    
    public static String Riskassessment43ExposureYes() {
        return "(//a[text()='Yes'])[43]";
    }
    
    public static String Riskassessment44UsageLabel() {
        return "//div[@id='control_8D325055-0545-4C28-8F6D-C02314A873F7']";
    }
    
    public static String Riskassessment44UsageDD() {
        return "//div[@id='control_8FA55F6A-9BFC-43A7-A60B-BAA7732FF65D']";
    }
    
    public static String Riskassessment44UsageYes() {
        return "(//a[text()='Yes'])[44]";
    }

    public static String Riskassessment45ExposureDD() {
        return "//div[@id='control_7B463461-F43B-4CEE-BE6C-442DC2711FB2']";
    }
    
    public static String Riskassessment45ExposureYes() {
        return "(//a[text()='Yes'])[45]";
    }
    
    public static String Riskassessment46WorkingDD() {
        return "//div[@id='control_E1C9D404-6111-45E7-A4F6-38062C537F81']";
    }
    
    public static String Riskassessment46WorkingYes() {
        return "(//a[text()='Yes'])[46]";
    }
    
    public static String Riskassessment47InadequateDD() {
        return "//div[@id='control_22AFC23C-0AC1-4C19-96CC-3B7E5BC1BB7C']";
    }
    
    public static String Riskassessment47InadequateYes() {
        return "(//a[text()='Yes'])[47]";
    }
    
    public static String Riskassessment48TransportationDD() {
        return "//div[@id='control_B29A0341-4C58-4021-B091-7F9100D47833']";
    }
    
    public static String Riskassessment48TransportationYes() {
        return "(//a[text()='Yes'])[48]";
    }

    public static String Riskassessment49PerformingDD() {
        return "//div[@id='control_6D0484D1-E4BD-4596-A08E-79F9EF0627E0']";
    }
    
    public static String Riskassessment49PerformingYes() {
        return "(//a[text()='Yes'])[49]";
    }
    
    public static String RiskassessmentLikelihoodriskexposurecriteriaDD() {
        return "//div[@id='control_B5C6710C-8125-483D-B2C2-EE45900D2ADD']";
    }
    
    public static String RiskassessmentLikelihoodriskexposurecriteriaSelect(String value) {
        return ".//a[text()='"+value+"']";
    }

    public static String RiskassessmentNoemployeesDD() {
        return "//div[@id='control_0626E3E2-444B-4E50-986E-02A7A110F5B9']";
    }
    
    public static String RiskassessmentNoemployeesSelect(String value) {
        return ".//a[text()='"+value+"']";
    }
    
    public static String RiskassessmentDurationworkDD() {
        return "//div[@id='control_66E09ED3-2A7F-4816-BE7F-8ADD9F6B4E1E']";
    }
    
    public static String RiskassessmentDurationworkDDSelect(String value) {
        return ".//a[text()='"+value+"']";
    }
    
    public static String RiskassessmentSave() {
        return "//div[@id='btnSave_form_7F5090E0-7D49-4B93-962C-FA8DF119B3D6']";
    }
    
    public static String Orders_Fleetmanagementtab() {
        return "//li[@id='tab_0CE51886-DD1C-492E-A9EB-3BE425E55EC2']";
    }
    
    public static String Orders_Fleetmanagementadd() {
        return "//div[@id='control_5ECBE260-6D53-4E0C-A206-BAAA150BAC85']//div[contains(text(),'Add')]";
    }
    
    public static String FleetmanagementPF() {
        return "//div[@id='btnProcessFlow_form_1F7D4C38-EE03-4F83-815A-94FD64BFC9C4']";
    }
    
    public static String Vehicletypemake() {
        return "//div[@id='control_C662CD0F-B707-4A9B-AB16-476005E74C1F']//input";
    }
    
    public static String RegNo() {
        return "//div[@id='control_25C0418F-B6DF-4845-B713-E1CD10250FC6']//input";
    }
    
    public static String Licenseexpirydate() {
        return "//div[@id='control_9826F1CB-B2D2-4460-84BD-E9F986CD8173']//input";
    }
    
    public static String FleetmanagementSave() {
        return "//div[@id='btnSave_form_1F7D4C38-EE03-4F83-815A-94FD64BFC9C4']";
    }
    
    public static String Orders_ToolsandEquipmenttab() {
        return "//li[@id='tab_BDBF788A-DDC1-4A05-AD49-8140495D0875']";
    }
    
    public static String Orders_ToolsandEquipmentadd() {
        return "//div[@id='control_3849FD5B-7DEC-44B2-BA0F-6774192E3E51']//div[contains(text(),'Add')]";
    }
    
    public static String ToolsandEquipmentPF() {
        return "//div[@id='btnProcessFlow_form_4A401000-E42A-4AFD-B9C8-8A1A21187C73']";
    }
    
    public static String ToolsandEquipmentRelatedDocsTab() {
        return "//li[@id='tab_C6CEB37F-9428-4AD8-8C7A-5A392EC3A40C']";
    }
    
    public static String ToolsandEquipmentDocsLinkDoc() {
        return "//div[@id='control_68F3E7FD-324B-4813-A5C0-EC1B0F255D96']//b[@original-title='Link to a document']";
    }
    
    public static String ToolsandEquipmentEquipmentDetailTab() {
        return "//li[@id='tab_482104C7-9CE1-463F-BAEB-0E9F9DE2E272']";
    }
    
    public static String Descriptionofequipment() {
        return "//div[@id='control_5F36A2D2-2304-48BB-BB1A-574EFBF50B10']//input";
    }
    
    public static String Serialnumber() {
        return "//div[@id='control_A06DDAE0-E6E1-4634-83EE-720FBCC0D5DE']//input";
    }
    
    public static String Calibrationcertificate() {
        return "//div[@id='control_D2553CED-57A8-4462-85FB-1B340D285213']//input";
    }
    
    public static String Loadtestcertificate() {
        return "//div[@id='control_181B8961-36F5-44FF-934A-4D9112C4B82F']//input";
    }
    
    public static String Equipmenttagged() {
        return "//div[@id='control_23D637B7-8E17-4264-9F36-7D4F8CFF843B']//input";
    }
    
    public static String Nameoftaggingauthority() {
        return "//div[@id='control_A41C8F72-9CCC-4B36-BEE0-C08AD40C3A50']//input";
    }
    
    public static String Preuseinspectionchecklist() {
        return "//div[@id='control_F9BF0F42-F94D-4FC4-B251-2A47A8AB49CA']//input";
    }
    
    public static String Logbook() {
        return "//div[@id='control_41CA8904-10E0-4B7F-ABEF-E720E71C8BB0']//input";
    }
    
    public static String ToolsandEquipmentEquipmentSave() {
        return "//div[@id='btnSave_form_4A401000-E42A-4AFD-B9C8-8A1A21187C73']";
    }
    
    public static String Orders_ChemicalRegistertab() {
        return "//li[@id='tab_DBF988FE-FF7F-4113-BD71-FF5B78B83CA6']";
    }
    
    public static String Chemicalstheybringontosite() {
        return "//div[@id='control_7C39C09C-183D-4074-9A22-D0DB4318E7D6']//b[@original-title='Select all']";
    }
    
    public static String ChemicalsContractorwillbeexposedto() {
        return "(//div[@title='Methane'])[1]";
    }
    
    public static String HazardousChemicalRegisterRecordClosed() {
        return "//*[@id=\"form_67D12C4F-9715-42CB-8583-738A005007A8\"]/div[1]/i[2]";
    }
    
    public static String TabScrollRight() {
        return "(//div[@class='tabpanel_tab_content']/div)[2]";
    }
    
    public static String Orders_PermissiontoWorktab() {
        return "//li[@id='tab_22CDF283-7455-450C-A45C-C2F8D2573A1A']";
    }
    
    public static String Orders_PermissiontoWorkadd() {
        return "//div[@id='control_E46D1FE3-3FAA-4DB0-A3E1-06CE9A9CEFC8']//div[contains(text(),'Add')]";
    }
    
    public static String PermissiontoWorkPF() {
        return "//div[@id='btnProcessFlow_form_446929D1-893C-47E0-BCB9-30C0C1D7A70B']";
    }
    
    public static String PermissiontoWorkRoleDD() {
        return "//div[@id='control_928D5A28-956F-4C01-B43A-EB08F0157406']";
    }
    
    public static String Role_Dropdown_Option(String data) {
        return "(//a[text()='" + data + "'])[1]";
    }
    
    public static String PermissiontoWorkResponsiblePersonDD() {
        return "//div[@id='control_6AFD8447-DD45-4E0B-9885-558782D0DB69']";
    }
    
    public static String ResponsiblePerson_Dropdown_Option(String data) {
        return "(//a[text()='" + data + "'])[1]";
    }
    
    public static String CommentsText() {
        return "//div[@id='control_E9196EB7-D01F-479C-9C61-9C60F7ACD48D']//textarea";
    }
    
    public static String PermissiontoWorkRoleSave() {
        return "//div[@id='btnSave_form_446929D1-893C-47E0-BCB9-30C0C1D7A70B']";
    }
    
    public static String SignoffDD() {
        return "//div[@id='control_DAE7D09B-B8AE-41D8-B834-F2E347322FD9']";
    }
    
    public static String SignoffDD_Option() {
        return "(//a[text()='Yes'])[1]";
    }
    
    public static String SignoffDate() {
        return "//div[@id='control_FA67DC5D-68BF-4A12-86E2-0CC7B93C9F41']//input";
    }
    
    public static String Orders_Evaluationstab() {
        return "//li[@id='tab_08CD4FD0-85F1-4810-95EF-95346F58A250']";
    }
    
    public static String Orders_Evaluationsadd() {
        return "//div[@id='control_75AE308C-4204-44CC-8A14-A2167C9827C9']//div[contains(text(),'Add')]";
    }
    
    public static String contractor_checklistPF() {
        return "//div[@id='btnProcessFlow_form_0AE7D7E4-12B9-4159-83E3-5DF8D4F49CD4']";
    }
    
    public static String contractor_checklistStartDate() {
        return "//div[@id='control_BB124A7C-02A5-47A5-A918-BBDF0C1AA35B']//input";
    }
    
    public static String contractor_checklistEndDate() {
        return "//div[@id='control_C23CB71D-B301-4EB4-84E1-65BF4642B711']//input";
    }
    
    public static String contractor_checklistDD() {
        return "//div[@id='control_2A842B0C-8258-421F-997F-388DE02D9335']";
    }
    
    public static String contractor_checklist_Option(String data) {
        return "(//a[text()='" + data + "'])[1]";
    }
    
    public static String checklist_PersonconductingchecklistDD() {
        return "//div[@id='control_D3BD3B30-B5B7-49DE-BD8D-CF9BE72E62C0']";
    }
    
    public static String checklist_PersonconductingchecklistOption(String data) {
        return "(//a[text()='" + data + "'])[1]";
    }
    
    public static String checklist_SaveandContinue() {
        return "//div[@id='control_76766175-90DD-44DE-839F-D28FFEE16394']";
    }
    
    public static String checklist_StartBtn() {
        return "//div[@id='btnChecklist_form_0AE7D7E4-12B9-4159-83E3-5DF8D4F49CD4']//div[text()='START']";
    }
    
    public static String checklist_Findingstab() {
        return "//li[@id='tab_FDC1580F-0B58-4FE3-9EAE-E6F7620B4961']";
    }
    
    public static String checklist_SupportDocstab() {
        return "//li[@id='tab_B3FA9131-ADFF-4EC6-8E41-82D15253E6B9']";
    }
    
    public static String ChildTabScrollRight() {
        return "//div[@id='control_EC720084-72FD-4AAE-BCFF-F56B7130B40C']//div[@class='tabpanel_right_scroll icon chevron right']";

    }
    
    
    
}
