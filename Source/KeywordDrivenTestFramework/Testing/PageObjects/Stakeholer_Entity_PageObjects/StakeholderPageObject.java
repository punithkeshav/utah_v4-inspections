/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author EMashishi
 */
public class StakeholderPageObject {

    public static String Record_Number;
    public static String Window_1;

    public static String getWindow()
    {
        return Window_1;
    }

    public static void setWindow(String Window)
    {
        Window_1 = Window;
    }

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }
    //new
    public static String entityNameXapth() {
        return "//div[@id='control_6B36E56B-4BD2-4A16-AD58-94FE1883EFE2']//input[@language]";
    }
    
     public static String anyClickOptionCheckXpath(String option) {
        return "//a[text()='" + option + "']//../i[@class='jstree-icon jstree-checkbox']";
    }


    public static String industryDropdownXpath() {
        return "//div[@id='control_425BD26A-5DDB-485A-BA7F-E8D2E51C4BEA']";
    }

    public static String entityDescriptionXapth() {
        return "//div[@id='control_4AEFCBE1-7C06-4528-BB6B-CFD298C47AA1']//textarea";
    }
    
    public static String relationShipOwenerDropdownXpath() {
        return "//div[@id='control_A01F1D1A-45BF-4A6B-B2C2-88046BDAFDA1']";
    }
    
    public static String saveToContinueXpath() {
        return "//div[text()='Save to continue']";
    }

}
