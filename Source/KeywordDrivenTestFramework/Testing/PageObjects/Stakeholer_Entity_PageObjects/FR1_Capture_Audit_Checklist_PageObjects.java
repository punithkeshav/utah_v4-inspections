/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
/**
 *
 * @author Delwin.Horsthemke
 */
public class FR1_Capture_Audit_Checklist_PageObjects 
{
    //FR1_Capture_Ausit_Checklist_MainScenario
    public static String Audits()
    {
        return "//label[contains(text(),'Audits')]";
    }
    
    public static String Audit_Checklist()
    {
        return "//label[contains(text(),'Audit Checklist')]";
    }
    
    public static String Audit_ChecklistAdd()
    {
        return "//div[@id='btnActAddNew']";
    }
    
    public static String Audit_ChecklistPF()
    {
        return "//div[@id='btnProcessFlow_form_1B0044DE-2C55-488C-9850-75C417EED9C8']";
    }
    
    public static String Audit_ProjectTick()
    {
        return "//div[@id='control_4A79FD7F-CD2B-4E66-AF12-B25FDC205CB1']//div[@class='c-chk']";
    }
    
    public static String Audit_ProjectLabel()
    {
        return "//div[@id='control_A66E7D59-81A2-48A2-90BD-44BCF4C699D4']";
    }
    
    public static String Audit_RelatedStakeholderLabel()
    {
        return "//div[@id='control_8F2DD7A8-4CEB-446A-A70D-BC49C6EE2132']";
    }
    
    public static String Audit_BUDD()
    {
        return "//div[@id='control_B1ED7656-E00D-4A2E-B3FF-93E068F7D531']";
    }
    
    public static String Audit_BUExpandChevron1()
    {
        return "(//div[contains(@class,'transition visible')]//li[@title='Global Company']//i[@class='jstree-icon jstree-ocl'])[\"+index+\"]";
    }
    
    public static String Audit_BUExpandChevron2()
    {
        return "(//div[contains(@class,'transition visible')]//li[@title='South Africa']//i[@class='jstree-icon jstree-ocl'])[\"+index+\"]";
    }
    
    public static String Audit_BUSelect()
    {
        return "//a[text()='Victory Site']";
    }
    
    public static String Audit_ProjectDD()
    {
        return "//div[@id='control_6380EE87-B589-48A4-BE47-2B255AA00E5C']";
    }
    
    public static String Audit_ProjectSelect()
    {
        return "//a[text()='Security upgrade']";
    }
    
    public static String Audit_ImpactTypeDD()
    {
        return "//div[@id='control_61C03BE8-24FD-4378-9AAA-0DA65D03E82E']";
    }
    
    public static String Audit_ImpactTypeSelectAll()
    {
        return "//div[@id='control_61C03BE8-24FD-4378-9AAA-0DA65D03E82E']//b[@original-title='Select all']";
    }
    
    public static String Audit_TitleXpath()
    {
        return "//div[@id='control_FEB3D889-9079-4781-943C-D2CA02A5A5F4']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String Audit_ScopeXpath()
    {
        return "//div[@id='control_9C956F79-A9C9-470F-975D-4923F2DB6E7A']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String Audit_TypeDD()
    {
        return "//div[@id='control_2C70ED4C-D256-4B45-B76F-7E916644A513']";
    }
    
    public static String Audit_TypeSupplier()
    {
        return "(//a[text()='Supplier']/i[1])";
    }
    
    public static String Audit_TypeInternal()
    {
        return "(//a[text()='Internal']/i[1])";
    }
    
    public static String Audit_TypeExternal()
    {
        return "(//a[text()='External']/i[1])";
    }
    
    public static String Audit_TypeDDArrow()
    {
        return "//div[@id='control_2C70ED4C-D256-4B45-B76F-7E916644A513']//b[@class='select3-down drop_click']";
    }
    
    public static String Audit_RelateStakeDD()
    {
        return "//div[@id='control_5E74E964-4873-4B1D-8162-4EE4532A3FE8']";
    }
    
    public static String Audit_RelateStakeSelect()
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='AJ Energy Solutions']";
    }
    
    public static String Audit_RelateProtocolDD()
    {
        return "//div[@id='control_2C3B9625-8F0F-44C8-8CB9-8C6600FB57E0']";
    }
    
    public static String Audit_RelateProtocolSelect()
    {
        return "//a[text()='Combined Audit 2018']";
    }
    
    public static String Audit_ProcessActivityExpand1()
    {
        return "(//div[@id='control_6B5F3505-F436-4ED1-B8B1-38D8DB086FC2']//li//a[contains(text(),'Exploration')]//..//i)[1]";
    }
    
    public static String Audit_ProcessActivitySelect()
    {
        return "(//a[text()='Field data collection']/i[1])";
    }
    
    public static String Audit_ManagerDD()
    {
        return "//div[@id='control_8F83D7C1-3D82-47CE-8A8A-BCB3A0A5248E']";
    }
    
    public static String Audit_Manager2Manager()
    {
        return "//a[text()='2 Manager']";
    }
    
    public static String Audit_AuditeeDD()
    {
        return "//div[@id='control_CCD4D19C-9ADA-49A2-9824-9BFD60753453']";
    }
    
    public static String Audit_Auditee4Viewer()
    {
        return "(//a[text()='4 Viewer'])[2]";
    }
    
    public static String Audit_PersonConductingAuditDD()
    {
        return "//div[@id='control_DC3EE96D-3BB3-4B26-8D65-33EA527EFA79']";
    }
    
    public static String Audit_Auditee3Logger()
    {
        return "(//a[text()='3 Logger'])[3]";
    }
    
    public static String Audit_StartDate()
    {
        return "//div[@id='control_C58B4570-0BCC-4016-955F-BC39F5A7CBD9']//input";
    }
    
    public static String Audit_EndDate()
    {
        return "//div[@id='control_155B5250-AFF6-4EE7-A2B2-200427F133C8']//input";
    }
    
    public static String Audit_Introduction()
    {
        return "//div[@id='control_9AAF2CD3-5F45-4560-962B-1E4A39280565']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String Audit_Objective()
    {
        return "//div[@id='control_EE073962-0DB4-4FD6-B88F-95B13A10D358']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String Audit_SupportDocsTab()
    {
        return "//li[@id='tab_7960880A-D0C6-4353-832D-D46245B6D262']";
    }
    
    public static String Audit_SavetoContinue()
    {
        return "//div[@id='control_BEB32BC0-2756-4428-BB99-E27FC206BA8F']";
    }
    
    public static String Audit_SubmitAudit()
    {
        return "//div[@id='control_51BDBB20-08B1-49CD-B9DA-117FF964C58E']";
    }
    
    public static String Audit_SearchBtn()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Audit_ChecklistOpenRecord()
    {
        return "(//div[text()='DH Audit Title Automation'])[1]";
    }

    public static String Audit_StatusLabel()
    {
        return "//div[@id='control_F9A50A20-2E8A-4364-902B-D9671A42A403']";
    }
    
    public static String Audit_StatusPlanning()
    {
        return "//div[@id='control_E8EBE770-3C9A-4EB6-93A4-35BF3D60C7EF']//li[@title='Planning ']";
    }
    
    public static String Audit_ProposeNewDatesLabel()
    {
        return "//div[@id='control_E703CEEE-DC64-4CCA-9CB7-D95D99F8A95F']";
    }
    
    public static String Audit_ScheduleApprovedLabel()
    {
        return "//div[@id='control_65EF6FE9-86E6-44C1-8D9D-E6DEC11911E1']";
    }
    
    public static String Audit_TeamTab() 
    {
        return "//li[@id='tab_E77E9785-96A3-4FF3-B479-DE189E9C7C43']";
    }
    
    public static String Audit_RisksIncidentsTab() 
    {
        return "//li[@id='tab_25B02C7C-46B9-46DA-8A2E-C89FE47A5ED2']";
    }
    
    public static String Audit_ActionsTab() 
    {
        return "//li[@id='tab_28BD8830-BEF9-4B53-A10A-0D98C0B23E09']";
    }
    

    public static String office_URL() {
    return "https://www.office.com";
    }
    
    public static String office_signin() {
    return ".//a[contains(text(),'Sign in')]";
    }
    
    public static String office_email_id(){
    return ".//input[@type='email']";
    }
    
    public static String email_next_btn(){
    return ".//input[@value='Next']";
    }
    
    public static String office_password(){
    return "//input[@type='password']";
    }
    
    public static String office_signin_btn(){
    return "//input[@value='Sign in']";
    }
    
    public static String office_No_btn(){
    return "//input[@value='No']";
    }
    
    public static String outlook_icon(){
    return ".//div[@id='ShellMail_link_text']";
    }
    
    public static String inbox_chevron_expand(){
    return "//i[@class='ms-Button-icon _1IolX_6rKX93IRLO1O_oCP _3o738zmfzs1fXK1kxpiX5 _1OPSsVxfk_GWTnwo-KIMYX flipForRtl icon-60']";
    }
    
    public static String system_mail_folder(){
    return "//span[text()='System Mail']";
    }
    
    public static String email_notification(){
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Audit Checklist')][contains(text(),'has been logged')])[1]";
    }
    
    public static String ActionsRecordNumber_xpath() {
    return "(//div[@id='form_DB1AFD7D-40B5-47B3-A866-45A3E2F86848']//div[contains(text(),'- Record #')])[1]";
    }
    
    public static String linkBackToRecord_Link(){
    return ".//a[@data-auth='NotApplicable']";
    }
    
    public static String Username() {
    return "//input[@id='txtUsername']";
    }

    public static String Password() {
    return "//input[@id='txtPassword']";
    }

    public static String LoginBtn() {
    return "//div[@id='btnLoginSubmit']";
    }
    
    //FR1_02_Schedule_Audit_Checklist_MainScenario
    public static String Audit_ScheduleApprovedTick() 
    {
        return "//div[@id='control_5467B726-978A-457D-BE3C-6A1BCAA9DDAD']";
    }
    
    public static String Audit_ScheduleApprovedPanel() 
    {
        return "//div[@id='control_7DA04152-1CBD-446B-89EC-678C1CDA05E0']";
    }
    
    public static String Audit_ApprovedBy()
    {
        return "//div[@id='control_90184175-2CC0-4182-A5B8-57542A33786F']//li[@title='2 Manager']";
    }
    
    public static String Audit_DateApproved()
    {
        return "//div[@id='control_3B2C50D6-337E-447F-BE17-A0A877B94130']//input[@data-role='datepicker']";
    }
    
    public static String Audit_StatusScheduled()
    {
        return "//div[@id='control_E8EBE770-3C9A-4EB6-93A4-35BF3D60C7EF']//li[@title='Scheduled']";
    }
    
    public static String Audit_SaveBtn()
    {
        return "//div[@id='btnSave_form_1B0044DE-2C55-488C-9850-75C417EED9C8']";
    }
    
    public static String Audit_SubmitauditplanningBtn()
    {
        return "//div[@id='control_BC2D3B83-36C0-4596-ACD0-899571582DB8']";
    }
    
    public static String Audit_StartAuditBtn()
    {
        return "//div[@id='control_A024BB31-1AA2-4637-9E45-4AF551634E02']";
    }
    
    //FR1_Capture_Audit_Checklist_OptionalScenario
    public static String Audit_SupportDocsLinkDoc()
    {
        return "//div[@id='control_A9D01542-73BD-4025-BD39-FE3AB4B97BA1']//b[@original-title='Link to a document']";
    }
    
    public static String Audit_ConsentFormLinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    
    public static String Audit_urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }
    
    public static String Audit_urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }
    
    //FR2_Capture_New_Dates_MainScenario
    public static String Audit_ProposeNewDatesTick() 
    {
        return "//div[@id='control_700E32B0-1735-45D2-BAC4-DD98C85091DF']";
    }
    
    public static String Audit_NewProposedDatesPanel() 
    {
        return "//div[@id='control_10047691-08A8-408D-9A56-5E55A6ACAA8B']";
    }
    
    public static String Audit_SuggestedStartDate() 
    {
        return "//div[@id='control_4AA4E37C-AAA9-4C34-87A9-F70E6CCA1EEC']//input";
    }
    
    public static String Audit_SuggestedEndDate() 
    {
        return "//div[@id='control_5CB8F6B6-2F53-4102-9340-CE64C72E37CD']//input";
    }
    
    public static String Audit_Comments() 
    {
        return "//div[@id='control_BC062ECD-9E04-40C3-8EF4-0B4007B9DEB0']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String email_NewDatesnotification()
    {
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Audit Checklist')][contains(text(),'new dates proposed')])[1]";
    }
    
    public static String Audit_Logout()
    {
    return "//*[@id=\"default-page\"]/body/div/div[1]/div[3]/div[2]/a[3]/i";
    }
    
    public static String Audit_SignInAgainBtn()
    {
    return "//div[@id='btnIsometrixSigninagain']";
    }

    //FR2_02_Manager_Approves_Audit_Checklist_MainScenario
    public static String Audit_StatusSearchDD()
    {
    return "(//span[@class='select3-arrow']//b)[5]";
    }
    
    public static String Audit_StatusSearchPlanning()
    {
    return "(//a[text()='Planning']/i[1])";
    }
    
    public static String Audit_AuditManagerSearchDD()
    {
    return "(//span[@class='select3-arrow']//b)[20]";
    }
    
    public static String Audit_StatusSearch2Manager()
    {
    return "(//a[text()='2 Manager']/i[1])";
    }

     public static String Audit_ChecklistApproveOpenRecord()
    {
        return "(//div[text()='DH Audit New Dates Automation'])[1]";
    }
     
    public static String Audit_AcceptProposedDates()
    {
        return "//div[@id='control_30FEA922-FA7C-4572-A387-44875026F2A3']";
    }
    
    public static String Audit_SubmitNewDates()
    {
        return "//div[@id='control_27CA5F1A-F9FA-4FD4-8F42-ACF7DBCF0DB7']";
    }
    
    public static String Audit_ViewFilterBtn()
    {
        return "//div[@id='btnActFilter']";
    }
    
    public static String Audit_StatusSearchScheduled()
    {
    return "(//a[text()='Scheduled']/i[1])";
    }
    
    //FR3_Capture_Audit_Team_MainScenario
    public static String Audit_TeamAddBtn()
    {
    return "//div[@id='btnAddNew']";
    }
    
    public static String Audit_TeamFullNameDD()
    {
    return "//div[@id='control_74F3ABB3-D328-4733-BFC4-9D151C8478D3']";
    }
    
    public static String Audit_TeamFullNameSelect()
    {
    return "//a[text()='Abel Mantoa (Percy)']";
    }
    
    public static String Audit_TeamExperience()
    {
    return "//div[@id='control_9D9BAC87-D7A1-4552-A9B0-08F2ACCC4157']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String Audit_TeamStartDate()
    {
        return "//div[@id='control_B1A6F86B-56C1-4100-81F6-F9DE01644413']//input";
    }
    
    public static String Audit_TeamEndDate()
    {
        return "//div[@id='control_0957E548-2540-4095-98A0-A8C30DB9FB72']//input";
    }
    
    public static String Audit_Hours()
    {
        return "//div[@id='control_CEE2D645-02D7-4931-9474-A2D4706B148B']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String Audit_SaveTick()
    {
        return "(//div[@class='icon checkmark icon ten six grid-icon-save'])[1]";
    }
    
    public static String Audit_OpenRecord()
    {
        return "//div[@id='control_8634EC1E-2840-40A4-9426-7555DC876807']//i[@class='icon edit on paper icon ten six grid-icon-edit grid-icon-edit-active'][1]";
    }
    
    public static String Audit_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_A28AF4DB-68CF-43DF-88F7-4748627E083C']";
    }

    public static String Audit_email_Audit_Team()
    {
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Audit Team')][contains(text(),'has been logged')])[1]";
    }
    
    //FR4_View_Related_Risks_MainScenario
    public static String Audit_RelatedRiskPanel()
    {
    return "//div[@id='control_8C49CD4F-7506-42D1-91FC-0AFA05BC1F16']//i[@class='toggle']";
    }
    
    public static String Audit_RelatedRiskRecord()
    {
    return "(//div[@label='Related Risk Linked to Business Unit and Impact Type']//div[text()='Global Company -> South Africa -> Victory Site'])[1]";
    }
    
    public static String Audit_RelatedRiskStatusOpen()
    {
    return "//div[@id='control_87EEE5E5-7E0D-4E0B-A183-DD2E5613D0ED']//li[text()='Open']";
    }
    
    //FR5_View_Related_Incidents_MainScenario
    public static String Audit_RelatedIncidentsPanel()
    {
    return "//div[@id='control_73831A00-432A-4C5A-8BFC-D1B6609C82B9']//i[@class='toggle']";
    }
    
    public static String Audit_RelatedIncidentsRecord()
    {
    return "(//div[@label='Related Incidents Linked to Business Unit and Impact Type']//div[text()='Global Company -> South Africa -> Victory Site'])[1]";
    }
    
    public static String Audit_RelatedIncidentStatusOpen()
    {
    return "//div[@id='control_B1667955-9616-471C-BBEC-14F334A90E65']//li[text()='Open']";
    }
    
    //FR6_Start_Audit_Checklist_MainScenario
    public static String Audit_StartAuditBtn1()
    {
        return "//div[@id='control_A024BB31-1AA2-4637-9E45-4AF551634E02']";
    }
    
    public static String Audit_RequestSignOffBtn()
    {
        return "//div[@id='control_AFB7FD68-58DE-4E99-9CB8-80475DD346FA']";
    }
    
    public static String Audit_FindingsTab()
    {
        return "//li[@id='tab_28113C52-C055-4AAC-82D6-E05232AA7086']";
    }
    
    public static String Audit_StatusInProgress()
    {
        return "//div[@id='control_E8EBE770-3C9A-4EB6-93A4-35BF3D60C7EF']//li[@title='In progress']";
    }
    
    //FR6_02_Capture_Checklist_MainScenario
    public static String Audit_ChecklistStartBtn()
    {
        return "//div[@id='btnChecklist_form_1B0044DE-2C55-488C-9850-75C417EED9C8']";
    }
    
    public static String Audit_ChecklistSaveCurrectSectionBtn()
    {
        return "//div[@id='btnSave_checklist_1B0044DE-2C55-488C-9850-75C417EED9C8'][2]";
    }
    
    public static String Audit_ChecklistCombinedAuditISO9001DD()
    {
        return "//div[@id='control_B8A936DB-7C51-4DFE-ABDF-CEE0F440BD70']";
    }
    
    public static String Audit_ChecklistCombinedAuditISO9001Yes()
    {
        return "//a[text()='Yes']";
    }
    
    public static String Audit_ChecklistQ1Comment()
    {
        return "//div[@id='control_E897BBBA-A58C-4500-8D0D-055DA3976C0C']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String Audit_ChecklistCloseBtn()
    {
        return "//*[@id=\"form_checklist_1B0044DE-2C55-488C-9850-75C417EED9C8\"]/div[1]/i[2]";
    }
    
    public static String Audit_ChecklistContinueBtn()
    {
        return "//div[@id='btnChecklist_form_1B0044DE-2C55-488C-9850-75C417EED9C8']//div[text()='Continue']";
    }
    
    public static String Audit_ChecklistStatusInProgress()
    {
        return "//div[@class='checklistProgressBody transition visible']//div[text()='In progress']";
    }
    
    public static String Audit_ChecklistNextBtn()
    {
        return "//div[@id='btnSave_checklist_1B0044DE-2C55-488C-9850-75C417EED9C8'][3]";
    }
    
    public static String Audit_ChecklistCombinedAudit2ndTabQ1DD()
    {
        return "//div[@id='control_55F31EA8-21FA-44C4-9860-3E6ED8CB1FF6']";
    }
    
    public static String Audit_ChecklistCombinedAudit2ndTabQ1Yes()
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='Yes']";
    }
    
    public static String Audit_ChecklistCombinedAudit2ndTabQ2DD()
    {
        return "//div[@id='control_80053806-5CDD-4AB4-B105-2E3BF2A5F337']";
    }
    
    public static String Audit_ChecklistCombinedAudit2ndTabQ2Yes()
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='Yes']";
    }
    
    public static String Audit_ChecklistCombinedAudit2ndTabQ3DD()
    {
        return "//div[@id='control_FDE0D942-7CAC-4C0D-A0A3-CEEFA9253A1F']";
    }
    
    public static String Audit_ChecklistCombinedAudit2ndTabQ3Yes()
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='Yes']";
    }
    
    public static String Audit_Checklist2ndTabQ2Comment()
    {
        return "//div[@id='control_E4250502-218A-4596-8208-9CA916CC7E5F']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String Audit_ChecklistFinishBtn()
    {
        return "//div[@id='btnSave_checklist_1B0044DE-2C55-488C-9850-75C417EED9C8'][1]";
    }
    
    public static String Audit_ChecklistViewBtn()
    {
        return "//div[@id='btnChecklist_form_1B0044DE-2C55-488C-9850-75C417EED9C8']//div[text()='View']";
    }
    
    public static String Audit_ChecklistStatusComplete()
    {
        return "//div[@class='checklistProgressBody transition visible']//div[text()='Complete']";
    }
    
    //FR7_Capture_Audit_Checklist_Findings_MainScenario
    public static String Audit_ChecklistFindingsAdd()
    {
        return "//div[@id='btnAddNew']";
    }
    
    public static String Audit_ChecklistFindingsProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_D0DC2467-E0D2-4AB2-BC47-B8987BA93E91']";
    }
    
    public static String Audit_ChecklistFindingsDescription()
    {
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String Audit_ChecklistFindingsOwnerDD()
    {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']";
    }
    
    public static String Audit_ChecklistFindingsOwnerSelect()
    {
        return "//a[text()='2 Manager']";
    }
    
    public static String Audit_ChecklistFindingsRiskSourceDD()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']";
    }
    
    public static String Audit_ChecklistFindingsRiskSourceSelect()
    {
        return "(//a[text()='Access control hazards']/i[1])";
    }
    
    public static String Audit_ChecklistFindingsClassLabel()
    {
        return "//div[@id='control_2F7CA8F4-8ABD-4773-8F47-D22C0171E44D']";
    }
    
    public static String Audit_ChecklistFindingsClassDD()
    {
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']";
    }
    
    public static String Audit_ChecklistFindingsClassSelect()
    {
        return "//a[text()='Minor Non-conformance']";
    }
    
    public static String Audit_ChecklistFindingsRiskDD()
    {
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']";
    }
    
    public static String Audit_ChecklistFindingsRiskSelect()
    {
        return "//a[text()='test']";
    }
    
    public static String Audit_ChecklistFindingsSavetoContinue()
    {
        return "//div[@id='control_E0B5417B-8F24-4E1D-A4DB-8C68192F6F7B']";
    }
    
    public static String Audit_ChecklistFindingsStatusOpen()
    {
        return "//div[@id='control_763B9B77-A105-47D2-8AED-10F4D0E482DE']//li[text()='Open']";
    }
    
    public static String Audit_ChecklistFindingsCloseBtn()
    {
        return "//*[@id=\"form_D0DC2467-E0D2-4AB2-BC47-B8987BA93E91\"]/div[1]/i[2]";
    }
    
    public static String Findings_email_notification(){
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Audit Checklist Findings')][contains(text(),'has been opened')])[1]";
    }
    
    //FR9_Capture_Audit_Checklist_Actions_MainScenario
    public static String Audit_ChecklistActionsTab()
    {
        return "//li[@id='tab_28BD8830-BEF9-4B53-A10A-0D98C0B23E09']";
    }
    
    public static String Audit_ChecklistActionsAddBtn()
    {
        return "//div[@id='btnAddNew']";
    }
    
    public static String Audit_ChecklistActionsProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_190D982F-D333-4122-8ACE-9A342B8359BC']";
    }
    
    public static String Audit_ChecklistActionsDescription()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String Audit_ChecklistActionsBUDD()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']";
    }
    
    public static String Audit_ActionsBUExpandChevron1()
    {
        return "//div[contains(@class,'transition visible')]//li[@title='Global Company']//i[@class='jstree-icon jstree-ocl']";
    }
    
    public static String Audit_ActionsBUExpandChevron2()
    {
        return "//div[contains(@class,'transition visible')]//li[@title='South Africa']//i[@class='jstree-icon jstree-ocl']";
    }
    
    public static String Audit_ActionsBUSelect()
    {
        return "(//a[text()='Victory Site'])[2]";
    }
    
    public static String Audit_ChecklistActionsRespPersonDD()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']";
    }
    
    public static String Audit_ChecklistActionsRespPersonSelect()
    {
        return "(//a[text()='2 Manager'])[5]";
    }
    
    public static String Audit_ChecklistActionsDueDate()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
    
    public static String Audit_ChecklistActionsSaveBtn()
    {
        return "//div[@id='btnSave_form_190D982F-D333-4122-8ACE-9A342B8359BC']";
    }
    
    public static String Audit_ChecklistActionStatus()
    {
        return "//div[@id='control_FEEDA1FF-BDED-41F2-A970-81E75BE28850']//li[@title='To be initiated']";
    }
    
    public static String Audit_ChecklistActionAddFeedbackBtn()
    {
        return "//div[@id='control_523DE899-F173-4121-9AFC-0E792D0F9F34']";
    }
    
    public static String Audit_ChecklistActionAddExtensionBtn()
    {
        return "//div[@id='control_A3C5064B-E3A1-49DE-81FA-D171B497D175']";
    }
    
    public static String Actions_email_notification(){
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Audit Checklist Actions')][contains(text(),'has been logged')])[1]";
    }
    
    //FR10_Capture_Request_Sign_Off_MainScenario
    public static String Audit_ChecklistRequestSignOffBtn()
    {
        return "//div[@id='control_AFB7FD68-58DE-4E99-9CB8-80475DD346FA']";
    }
    
    public static String Audit_StatusAwaitingsignoff()
    {
        return "//div[@id='control_E8EBE770-3C9A-4EB6-93A4-35BF3D60C7EF']//li[@title='Awaiting sign off']";
    }
    
    public static String AwaitingSignOff_email_notification(){
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Audit Checklist')][contains(text(),'awaiting sign off')])[1]";
    }
    
    //FR11_Capture_Sign_Off_Approval_MainScenario
    public static String Audit_ChecklistSignOffTab()
    {
        return "//li[@id='tab_76DBDDF6-5B4C-4FF3-B5A6-511A77402477']";
    }
    
    public static String Audit_ChecklistSignOffSubmodule()
    {
        return "//div[@id='control_EDE28D9B-722E-4786-8CCB-FFA228880432']";
    }
    
    public static String Audit_ChecklistSignOffAuditRatingDD()
    {
        return "//div[@id='control_523C9133-C853-43A4-98DD-71FE64BB1BEF']";
    }
    
    public static String Audit_ChecklistSignOffAuditRatingSelect()
    {
        return "//a[text()='Satisfactory']";
    }
    
    public static String Audit_ChecklistSignOffConclusion()
    {
        return "//div[@id='control_A57685F6-1561-4F09-8570-7FF89ED157F3']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String Audit_ChecklistSignOffSubmoduleAdd()
    {
        return "//div[@id='control_EDE28D9B-722E-4786-8CCB-FFA228880432']//div[@id='btnAddNew']";
    }
    
    public static String Audit_ChecklistSignOffProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_2A91B334-A8CC-4FD6-8D7D-01BC35649F02']";
    }
    
    public static String Audit_ChecklistSignOffAuditorTypeDD()
    {
        return "//div[@id='control_F8F35C1B-6B3C-4885-9834-C64C5DDA4555']";
    }
    
    public static String Audit_ChecklistSignOffAuditorTypeSelect()
    {
        return "//a[text()='Audit manager']";
    }
    
    public static String Audit_ChecklistSignOffAuditorDD()
    {
        return "//div[@id='control_DEA38CFE-258E-4507-BE92-180D8536EF89']";
    }
    
    public static String Audit_ChecklistSignOffAuditorSelect()
    {
        return "(//a[text()='2 Manager'])[2]";
    }
    
    public static String Audit_ChecklistSignOffDD()
    {
        return "//div[@id='control_AC075E37-7F8F-4DEC-8DF0-02DC8D70BDFB']";
    }
    
    public static String Audit_ChecklistSignOffYes()
    {
        return "(//a[text()='Yes'])[5]";
    }
    
    public static String Audit_ChecklistSignOffComments()
    {
        return "//div[@id='control_5FC74CF8-0BE4-459D-9599-A35927F43D28']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    
    public static String Audit_ChecklistSignOffEmail2Manager()
    {
        return "(//a[text()='2 Manager'])[1]/i[1]";
    }
    
    public static String Audit_ChecklistSignOffSaveBtn()
    {
        return "//div[@id='btnSave_form_2A91B334-A8CC-4FD6-8D7D-01BC35649F02']";
    }
    
    public static String Audit_ChecklistSignOffCloseBtn()
    {
        return "//*[@id=\"form_2A91B334-A8CC-4FD6-8D7D-01BC35649F02\"]/div[1]/i[2]";
    }
    
    public static String Audit_ChecklistTab()
    {
        return "//li[@id='tab_959C9B3A-42F9-4296-BB2F-8B98634AE570']";
    }
    
    public static String Audit_StatusComplete()
    {
        return "//div[@id='control_E8EBE770-3C9A-4EB6-93A4-35BF3D60C7EF']//li[@title='Completed']";
    }
    
    public static String SignOffAccepted_email_notification(){
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Audit Checklist')][contains(text(),'has been signed off')])[1]";
    }
    
    //FR11_Capture_Sign_Off_Approval_AlternateScenario
    public static String Audit_ChecklistSignOffNo()
    {
        return "(//a[text()='No'])[5]";
    }
    
    public static String SignOffRejected_email_notification(){
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Audit Checklist')][contains(text(),'has been rejected')])[1]";
    }
    
    public static String Audit_StatusInprogress()
    {
        return "//div[@id='control_E8EBE770-3C9A-4EB6-93A4-35BF3D60C7EF']//li[@title='In progress']";
    }
    
    //FR13_View_Audit_Checklist_Registers_Reports
    public static String Audit_ChecklistReportsIcon()
    {
        return "//div[@id='btnReports']";
    }
    
    public static String Audit_ChecklistsearchBtn()
    {
    return "//div[@id='btnActApplyFilter']";
    }
    
    public static String Audit_ChecklistviewReport()
    {
    return "//span[@title='View report ']";
    }
    
    public static String Audit_ChecklistcontinueButton()
    {
    return "//div[@title='Continue']";
    }
    
    public static String Audit_ChecklistexportDropdownMenu()
    {
    return "//img[@id='viewer_ctl05_ctl04_ctl00_ButtonImgDown']";
    }
    
    public static String Audit_ChecklistexportWord()
    {
    return "//a[@title='Word']";
    }
    
    public static String Audit_ChecklistfullReport()
    {
    return "//span[@title='Full report ']";
    }
    
}
