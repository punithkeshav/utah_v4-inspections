/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author smabe
 */
public class EngagementPlan_TestSuit extends BaseClass
{

    static TestMarshall instance;

    public EngagementPlan_TestSuit()

    {
        ApplicationConfig appConfig = new ApplicationConfig();
        //TestMarshall.currentEnvironment = Enums.Environment.ANG;
    }

     @Test
    public void EngagementPlan_Regression_CH05() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\Engagement Plan - Regression Ch05.xlsx", Enums.BrowserType.Chromeheadless);
        instance.runKeywordDrivenTests();
    }
    
    
    @Test
    public void EngagementPlan_Regression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\Engagement Plan - Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void EngagementPlan_Regression_tranning() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\Engagement Plan - Regression tranning.xlsx", Enums.BrowserType.Chromeheadless);
        instance.runKeywordDrivenTests();
    }


    @Test
    public void FR1_CaptureEngagementPlan_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix  Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR1-Capture Engagement Plan Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_CaptureEngagementPlan_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR1-Capture Engagement Plan Alternate scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_CaptureEngagementPlan_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR1-Capture Engagement Plan Alternate scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_CaptureEngagementPlan_AlternateScenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR1-Capture Engagement Plan Alternate scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_CaptureEngagementPlan_AlternateScenario4() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR1-Capture Engagement Plan Alternate scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_CaptureEngagementPlan_AlternateScenario5() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR1-Capture Engagement Plan Alternate scenario 5.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_CaptureEngagementPlan_AlternateScenario6() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR1-Capture Engagement Plan Alternate scenario 6.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_CaptureEngagementPlan_AlternateScenario7() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR1-Capture Engagement Plan Alternate scenario 7.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_CaptureEngagementPlan_AlternateScenario8() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR1-Capture Engagement Plan Alternate scenario 8.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_CaptureEngagementPlan_OptionalScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR1-Capture Engagement Plan Optional scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UCEDP_01_02_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\UC EDP 01-02 Capture Attendees  Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UCEDP_01_03_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\UC EDP 01-03 Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_EngagementPlanActions_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR2-Capture Engagement Plan Actions  Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR3_EngagementScheduled_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR3-Engagement Scheduled Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR4_DeleteEngagementPlan_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan\\FR4-Delete Engagement Plan  Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

}
