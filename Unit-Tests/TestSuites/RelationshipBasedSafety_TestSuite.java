/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;
import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author smabe
 */
public class RelationshipBasedSafety_TestSuite extends BaseClass {

    static TestMarshall instance;

    public RelationshipBasedSafety_TestSuite() 
    
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        //TestMarshall.currentEnvironment = Enums.Environment.ANG;
    }

         @Test
    public void RelationshipBasedSafety_Regression_Ch05() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Relationship Based Safety\\Relationship Based Safety - Regression Ch05.xlsx\\", Enums.BrowserType.Chromeheadless);
        instance.runKeywordDrivenTests();
    }
    
    
       @Test
    public void RelationshipBasedSafety_Regression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Relationship Based Safety\\Relationship Based Safety - Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
         @Test
    public void RelationshipBasedSafety_Regression_QA() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Relationship Based Safety\\Relationship Based Safety - Regression - QA.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
    
    @Test
    public void FR1_CaptureARelationshipBasedSafetyRecordForSLPObservation_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix  Test Pack");
        instance = new TestMarshall("TestPacks\\Relationship Based Safety\\FR1-Capture a Relationship Based Safety record for SLP Observation - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_CaptureARelationshipBasedSafetyRecordForSLPObservation_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Relationship Based Safety\\FR1-Capture a Relationship Based Safety record for SLP Observation - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      @Test
    public void FR1_CaptureARelationshipBasedSafetyRecordForSLPObservation_AlternateScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Relationship Based Safety\\FR1-Capture a Relationship Based Safety record for SLP Observation - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
    @Test
    public void FR2_CaptureARelationshipBasedSafetyRecordForVFLObservation_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Relationship Based Safety\\FR2-Capture a Relationship Based Safety record for VFL Observation - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR2_CaptureARelationshipBasedSafetyRecordForVFLObservation_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Relationship Based Safety\\FR2-Capture a Relationship Based Safety record for VFL Observation- Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
      
   
  
    @Test
    public void FR3_CaptureSafetyLeadershipActions_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Relationship Based Safety\\FR3-Capture Safety Leadership Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR4_EditRelationshipBasedSafetyRecord_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Relationship Based Safety\\FR4-Edit Relationship Based Safety Record - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
       @Test
    public void FR4_EditRelationshipBasedSafetyRecord_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Relationship Based Safety\\FR4-Edit Relationship Based Safety Record - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void FR5_DeleteRelationshipBasedSafetyRecord_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Relationship Based Safety\\FR5-Delete Relationship Based Safety Record - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
   
    

}

