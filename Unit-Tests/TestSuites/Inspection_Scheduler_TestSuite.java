/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;
import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author smabe
 */
public class Inspection_Scheduler_TestSuite extends BaseClass {

    static TestMarshall instance;

    public Inspection_Scheduler_TestSuite() 
    
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        //TestMarshall.currentEnvironment = Enums.Environment.ANG;
    }

    
    
          @Test
    public void Inspection_Scheduler_Execution_Ch05() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\Inspection Scheduler - Regression Ch05.xlsx\\", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
       @Test
    public void Inspection_Scheduler_Execution() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\Inspection Scheduler - Regression.xlsx\\", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
        
    @Test
    public void Inspection_Scheduler() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\S5_InspectionsScheduler_Beta.xlsx\\", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR1-Capture Inspection Scheduler - Main Scenario
    @Test
    public void FR1_Capture_Inspection_Scheduler_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR1-Capture Inspection Scheduler - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
      //FR1-Capture Inspection Scheduler - Alternate Scenario 1
    @Test
    public void FR1_Capture_Inspection_Scheduler_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR1-Capture Inspection Scheduler - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2_Capture_Inspection_Scheduler_Recurrence_MainScenario
    @Test
    public void FR2_Capture_Inspection_Scheduler_Recurrence_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR2-Capture Inspection Scheduler Recurrence - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR2_Capture_Inspection_Scheduler_Recurrence_AlternateScenario1
    @Test
    public void FR2_Capture_Inspection_Scheduler_Recurrence_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR2-Capture Inspection Scheduler Recurrence - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
       //FR2_Capture_Inspection_Scheduler_Recurrence_AlternateScenario2
    @Test
    public void FR2_Capture_Inspection_Scheduler_Recurrence_AlternateScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR2-Capture Inspection Scheduler Recurrence - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
       //FR2_Capture_Inspection_Scheduler_Recurrence_AlternateScenario3
    @Test
    public void FR2_Capture_Inspection_Scheduler_Recurrence_AlternateScenario3() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR2-Capture Inspection Scheduler Recurrence - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
        //FR2_Capture_Inspection_Scheduler_Recurrence_AlternateScenario4
    @Test
    public void FR2_Capture_Inspection_Scheduler_Recurrence_AlternateScenario4() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR2-Capture Inspection Scheduler Recurrence - Alternate Scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
       //FR2_Capture_Inspection_Scheduler_Recurrence_AlternateScenario5      
    @Test
    public void FR2_Capture_Inspection_Scheduler_Recurrence_AlternateScenario5() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR2-Capture Inspection Scheduler Recurrence - Alternate Scenario 5.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //  FR2_Capture_Inspection_Scheduler_Recurrence_AlternateScenario6
    @Test
    public void FR2_Capture_Inspection_Scheduler_Recurrence_AlternateScenario6() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR2-Capture Inspection Scheduler Recurrence - Alternate Scenario 6.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //  FR2_Capture_Inspection_Scheduler_Recurrence_OptionalScenario
    
     @Test
    public void FR2_Capture_Inspection_Scheduler_Recurrence_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR2-Capture Inspection Scheduler Recurrence - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
   
        //UC_INS_06_02_Mark_an_Inspection_Scheduler_as_inactive_MainScenario
    @Test
    public void UC_INS_06_02_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\UC_INS_06_02_Mark_an_Inspection_Scheduler_as_inactive - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

      //FR3_View_Inspections_Main_Scenario
    @Test
    public void FR3_Edit_Inspection_Scheduler_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR3_Edit Inspection Scheduler  - Main - Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR4_Edit_Inspection_Scheduler_Recurrence_MainScenario
    @Test
    public void FR4_Edit_Inspection_Scheduler_Recurrence_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR4_Edit_Inspection_Scheduler_Recurrence - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR5_Delete_Inspection_Scheduler_MainScenario
    @Test
    public void FR5_Delete_Inspection_Scheduler_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR5_Delete_Inspection_Scheduler - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
     //FR6_Delete_Inspection_Scheduler_Recurrence_MainScenario
    @Test
    public void FR6_Delete_Inspection_Scheduler_Recurrence_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR6_Delete_Inspection_Scheduler_Recurrence - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR7_View_Inspections_MainScenario
    @Test
    public void FR7_View_Inspections_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspection Scheduler\\FR7_View_Inspections - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    

}

