/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author smabe
 */
public class BaselineDataCollection_TestSuit extends BaseClass
{

    static TestMarshall instance;

    public BaselineDataCollection_TestSuit()

    {
        ApplicationConfig appConfig = new ApplicationConfig();
        //TestMarshall.currentEnvironment = Enums.Environment.ANG;
    }

    //Ch05
    @Test
    public void BaselineDataCollection_Regression_Ch05() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\Baseline Data Collection - Regression Ch05.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void BaselineDataCollection_Regression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\Baseline Data Collection - Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
     @Test
    public void BaselineDataCollection_Regression_QA() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\Baseline Data Collection - Regression - QA.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      @Test
    public void BaselineDataCollection_Regression_training() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\Baseline Data Collection - Regression training.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_CaptureBaselineDataCollection_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix  Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\FR1-Capture Baseline Data Collection Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_CaptureBaselineDataCollection_OptionalScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\FR1-Capture Baseline Data Collection Optional Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_CaptureBaselineDataCollection_OptionalScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\FR1-Capture Baseline Data Collection Optional Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_CaptureBaselineDataCollectionSurvey_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\FR2-Capture Baseline Data Collection Survey Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_CaptureBaselineDataCollectionSurvey_AlternateScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\FR2-Capture Baseline Data Collection Survey Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR3_CaptureTheSurvey_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\FR3-Capture the Survey Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR3_CaptureTheSurvey_OptionalScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\FR3-Capture the Survey Optional Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR3_CaptureTheSurvey_OptionalScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\FR3-Capture the Survey Optional Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR3_CaptureTheSurvey_OptionalScenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\FR3-Capture the Survey Optional Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR4_CaptureActions_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\FR4-Capture Actions Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR5_UpdateStatusToComplete_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\FR5-Update Status to Complete  Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR6_DeleteBaselineDataCollectionRecord_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Baseline Data Collection\\FR6-Delete Baseline Data Collection Record  Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

}
