/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author LDisemelo
 */
public class Project_Management_TestSuite {

    static TestMarshall instance;

    public Project_Management_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }
    
    
    @Test
    public void ProjectManement_RegressionSuite() throws FileNotFoundException 
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\Project Manement Regression Suite.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   

       @Test
    public void Ch05_RegressionSuite() throws FileNotFoundException {
        Narrator.logDebug("Isometrix  - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\ProjectManement_Ch05RegressionSuite.xlsx", Enums.BrowserType.Chromeheadless);
        instance.runKeywordDrivenTests();
    } 
    //IsomQa Regression Full Execution
  
    
    @Test
    public void FR1_Capture_Project_Management_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR1 - Capture Project Management Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }    
    
    @Test
    public void FR1_Capture_Project_Management_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR1 - Capture Project Management Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
     @Test
    public void FR1_Capture_Project_Management_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR1 - Capture Project Management Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
   @Test
    public void FR2_Review_Registered_Project_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR2 Review Registered Project Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
     //FR2_Review_Registered_Project_AlternateScenario()
    @Test
    public void FR2_Review_Registered_Project_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR2 Review Registered Project Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
     //UCPJM_0202_Reviewing_Registered_Project_MainScenario
    @Test
    public void UCPJM_0202_Reviewing_Registered_Project_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\UCPJM_0202 Reviewing Registered Project MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
     //UCPJM_0202_Reviewing_Registered_Project_AlternateScenario
    @Test
    public void UCPJM_0202_Reviewing_Registered_Project_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\UCPJM_0202 Reviewing Registered Project Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
    @Test
    public void FR3_Approve_Project_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR3-Approve Project Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
     @Test
    public void FR3_Approve_Project_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR3-Approve Project Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      @Test
    public void UC_PJM_03_02_Approve_Project_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\UC PJM 03-02 Approve the Project Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void FR4_Capture_Forecasted_Budget_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR4- Capture Forecasted Budget Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void FR5_Capture_Project_Actual_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR5-Capture Project Actual Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR6_Capture_Energy_and_GHG_Emissions_Savings_MainScenario 
    @Test
    public void FR6_Capture_ECO2Man_Project_Savings_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR6-Capture ECO2Man Project Savings Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void FR7_Capture_Energy_and_GHG_Emissions_Savings_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR7-Capture Energy and GHG Emissions Savings Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_PJM_07_02_Approve_Record_As_An_ECO2Man_Indicator() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\UC PJM 07-02: Approve Record as an ECO2Man indicator Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR8_Capture_Project_Actions_MainScenario()   throws FileNotFoundException {
        Narrator.logDebug("Isometrix  - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR8-Capture Actions Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void FR9_Edit_Project_Management_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR9-Edit Project Management  Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

   //FR10_Delete_Project_Management_MainScenario
    @Test
    public void FR10_Delete_Project_Management_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR10-Delete Project Management  Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
   //FR11_View_Linked_Engagements_MainScenario
    @Test
    public void FR11_View_Linked_Engagements_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR11 View Linked Engagements Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      //FR12_View_Dashboards_MainScenario
    @Test
    public void FR12_View_Dashboards_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management\\FR12_View_Dashboards_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
 
    
    
}
