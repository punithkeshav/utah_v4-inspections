/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;
/**
 *
 * @author LDisemelo
 */
public class S4_Inspections_TestSuite extends BaseClass
{

    static TestMarshall instance;

    public S4_Inspections_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.MTN;
    }
    
    
      @Test
    public void Inspections_Regression_Traning() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\Inspections_Regression - traning site.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
       @Test
    public void Inspections_Regression() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\Inspections_Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
        @Test
    public void Inspections_Regression_QA() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\Inspections_Regression - QA.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void Inspections_Regression_Ch05() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\Inspections_Regression Ch05.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 

    //FR1 Register an Inspection - Main Scenario
     @Test
    public void FR1_Capture_Inspection_MainScenario () throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR1-Capture Inspection - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
    
   // FR1 Register an Inspection Alternate Scenario 1
     @Test
    public void FR1_Capture_Inspection_AlternateScenario1 () throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR1-Capture Inspection - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
      // FR1 Register an Inspection Alternate Scenario 2
     @Test
    public void FR1_Capture_Inspection_AlternateScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR1-Capture Inspection - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
     // FR1 Register an Inspection Alternate Scenario 3
     @Test
    public void FR1_Capture_Inspection_AlternateScenario3() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR1-Capture Inspection - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
      // FR1 Register an Inspection Alternate Scenario 4
     @Test
    public void FR1_Capture_Inspection_AlternateScenario4() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR1-Capture Inspection - Alternate Scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
    
      @Test
    public void FR1_Capture_Inspection_AlternateScenario5() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR1-Capture Inspection - Alternate Scenario 5.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
    
      @Test
    public void FR1_Capture_Inspection_AlternateScenario6() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR1-Capture Inspection - Alternate Scenario 6.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
    
    // FR1 Register an Inspection Alternate Oprtional Scenario
     @Test
    public void FR1_Capture_Inspection_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR1-Capture Inspection - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
    
    //FR2- Capture Inspection Checklist  - Main Scenario
     @Test
    public void FR2_Capture_Inspection_Checklist_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR2 - Capture Inspection Checklist - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
      
    //FR2- Capture Inspection Checklist  - Alternate scenario:
     @Test
    public void FR2_Capture_Inspection_Checklist_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR2 - Capture Inspection Checklist - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
    
     @Test
    public void UC_ISP_02_02_Capture_Checklist_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\UC ISP 02 02 Capture Checklist - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void UC_ISP_02_02_Capture_Checklist_OptionaScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\UC ISP 02 02 Capture Checklist - Optional Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void UC_ISP_02_02_Capture_Checklist_OptionaScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\UC ISP 02 02 Capture Checklist - Optional Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
    
    //UC ISP 02-02 Capture Checklist - Main Scenario
     @Test
    public void UC_ISP01_02_CaptureChecklist_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\UC ISP 02 02 Capture Checklist - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
     //UC ISP 02-02 Capture Checklist - Alternate Scenario 1
     @Test
    public void UC_ISP01_02_CaptureChecklist_AlternativeScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\UC ISP 02 02 Capture Checklist - Alternate Scenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
    @Test
    //UC ISP 02-02 Capture Checklist - Alternate Scenario 2
      public void UC_ISP01_02_CaptureChecklist_AlternativeScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\UC ISP 02 02 Capture Checklist - Alternate Scenario2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //UC_ISP01-02Capture Checklist - Optional Scenario
     @Test
    public void UC_ISP01_02_CaptureChecklist_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\UC_ISP01-02Capture Checklist - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   

    //UC ISP 02-03: Capture Checklist Non-Compliance Intervention  - Main Scenario
     @Test
    public void UC_ISP_02_03_Capture_Checklist_Non_Compliance_Intervention_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\UC ISP 02-03 Capture Checklist Non-Compliance Intervention  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
   // UC ISP 02-03: Capture Checklist Actions - Main Scenario
     @Test
    public void UC_ISP_02_03_Capture_Checklist_Actions_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\UC ISP 02-03 Capture Checklist Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
    //FR3 Capture Inspection Checklist Actions  - Main Scenaio
     @Test
    public void FR3_Capture_Inspection_Actions_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR3 - Capture Inspection Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
    //FR4- Capture Inspection Checklist Non-Compliance Intervention Main Scenario
       @Test
    public void FR4_Capture_Inspection_Checklist_NonCompliance_Intervention_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR4- Capture Inspection Checklist Non-Compliance Intervention Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
    //FR5_View_Supporting_Docuemts_MainScenario
     @Test
    public void FR5_View_Supporting_Docuemts_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR5-View Inspections Report - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
    
      @Test
    public void FR5_View_Supporting_Docuemts_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR5-View Inspections Report - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
   
    //FR6_Request_Validation_MainScenario
     @Test
    public void FR6_Request_Validation_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR6- Request Validation - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
    
        //FR6_Request_Validation_AlternateScenario
     @Test
    public void FR6_Request_Validation_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR6- Request Validation - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
    //FR7- Capture Validation - Main Scenario
       @Test
    public void FR7_Capture_Validation_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR7- Capture Validation - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
    
     //FR7- Capture Validation Alternate Scenario
       @Test
    public void FR7_Capture_Validation_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR7- Capture Validation Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
     //FR8-Edit Inspection - Main Scenario
       @Test
    public void FR8_Edit_Inspection_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR8-Edit Inspection - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
     //FR9-Delete Inspection  - Main Scenario
       @Test
    public void FR9_Delete_Inspection_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR9-Delete Inspection  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 

      //FR10- Inspection automatically goes overdue - Alternate Scenario
       @Test
    public void FR10_Inspection_Automatically_Goes_Overdue_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR10- Inspection automatically goes overdue - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
      //FR11-View Dashboards - Main Scenario
       @Test
    public void FR11_View_Dashboards_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Inspections\\FR11-View Dashboards - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
}
