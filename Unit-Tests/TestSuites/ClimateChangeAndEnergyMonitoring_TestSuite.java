/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMabe
 */
public class ClimateChangeAndEnergyMonitoring_TestSuite {

    static TestMarshall instance;

    public ClimateChangeAndEnergyMonitoring_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }
    
    
    @Test
    public void Climate_Change_And_Energy_Monitoring_RegressionSuite() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\Climate Change and Energy Monitoring Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   
    
     @Test
    public void Climate_Change_And_Energy_Monitoring_RegressionSuite_training() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\Climate Change and Energy Monitoring Regression training.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 

    //ISom Dev Regression Full Execution
    
       @Test
    public void ISomDev_RegressionSuite() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
       @Test
    public void Ch05_RegressionSuite() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
       @Test
    public void IsomQa_RegressionSuite() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
    
    @Test
    public void FR1_Capture_Climate_Change_And_Energy_Monitoring_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\FR1-Capture Climate Change and Energy Monitoring - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }    
    
    @Test
    public void FR1_Capture_Climate_Change_And_Energy_Monitoring_OptionalScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\FR1-Capture Climate Change and Energy Monitoring - Optional Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
     @Test
    public void FR1_Capture_Climate_Change_And_Energy_Monitoring_OptionalScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\FR1-Capture Climate Change and Energy Monitoring - Optional Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
     @Test
    public void FR1_Capture_Climate_Change_And_Energy_Monitoring_OptionalScenario3() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\FR1-Capture Climate Change and Energy Monitoring - Optional Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
  
  
    @Test
    public void FR2_CaptureClimateChangeEnergyMeasurement_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\FR2-Capture Climate Change Energy Measurement Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
    @Test
    public void FR2_CaptureClimateChangeEnergyMeasurement_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\FR2-Capture Climate Change Energy Measurement Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
   
    
    @Test
    public void FR3_Capture_Findings_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\FR3-Capture Findings - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
    //FR4_Capture_Project_Actual_MainScenario 
    @Test
    public void FR4_EditClimateChangeAndEnergy_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\FR4-Edit Climate Change and Energy - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    

    @Test
    public void FR5_Delete_Climate_Change_And_Energy_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Monitoring\\FR5-Delete Climate Change and Energy  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
  
    

    
    
}
