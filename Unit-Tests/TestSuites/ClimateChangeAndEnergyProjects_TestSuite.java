/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMabe
 */
public class ClimateChangeAndEnergyProjects_TestSuite {

    static TestMarshall instance;

    public ClimateChangeAndEnergyProjects_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }
    
    
    //S5_2_Project Manement_RegressionSuite ProjectSite
    @Test
    public void S5_2_ProjectManement_RegressionSuite() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\S5_2_ProjectManement_RegressionSuite.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }   

    //ISom Dev Regression Full Execution
    
       @Test
    public void ISomDev_RegressionSuite() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Projects\\S5_2_ProjectManement_IsomDevRegressionSuite.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
    //Ch05 Regression Full Execution
       @Test
    public void Ch05_RegressionSuite() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\S5_2_ProjectManement_Ch05RegressionSuite.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    //IsomQa Regression Full Execution
       @Test
    public void IsomQa_RegressionSuite() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\S5_2_ProjectManement_IsomQARegressionSuite.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
    //FR1_Capture_Project_Management_MainScenario
    @Test
    public void FR1_Capture_Project_Management_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR1_Capture_Project_Management_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }    
    
    //FR1_Capture_Project_Management_OptionalScenario
    @Test
    public void FR1_Capture_Project_Management_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR1_Capture_Project_Management_OptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
    //FR1_Capture_Project_Management_AlternateScenario
    @Test
    public void FR1_Capture_Project_Management_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR1_Capture_Project_Management_AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR2_Review_Registered_Project_MainScenario()
    @Test
    public void FR2_Review_Registered_Project_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR2_Review_Registered_Project_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
     //FR2_Review_Registered_Project_AlternateScenario()
    @Test
    public void FR2_Review_Registered_Project_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR2_Review_Registered_Project_AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
     //UCPJM_0202_Reviewing_Registered_Project_MainScenario
    @Test
    public void UCPJM_0202_Reviewing_Registered_Project_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\UCPJM_0202_Reviewing_Registered_Project_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
     //UCPJM_0202_Reviewing_Registered_Project_AlternateScenario
    @Test
    public void UCPJM_0202_Reviewing_Registered_Project_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\UCPJM_0202_Reviewing_Registered_Project_AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
    //FR3_Capture_Forecasted_Budget_MainScenario
    @Test
    public void FR3_Capture_Forecasted_Budget_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR3_Capture_Forecasted_Budget_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
    //FR4_Capture_Project_Actual_MainScenario 
    @Test
    public void FR4_Capture_Project_Actual_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR4_Capture_Project_Actual_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
   //FR5_Capture_ECO2Man_Project_Savings_MainScenario 
    @Test
    public void FR5_Capture_ECO2Man_Project_Savings_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR5_Capture_ECO2Man_Project_Savings_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR6_Capture_Energy_and_GHG_Emissions_Savings_MainScenario 
    @Test
    public void FR6_Capture_Energy_and_GHG_Emissions_Savings_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR6_Capture_Energy_and_GHG_Emissions_Savings_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //UC PJM 06-02_Approve_Record_as_an_ECO2Man_indicator_MainScenario
    
    @Test
    public void UCPJM_0602_ApproveRecordasanECO2Manindicator_MainScenario() throws FileNotFoundException{
       Narrator.logDebug("Isometrix - V5.2 - Test Pack");
       instance = new TestMarshall("TestPacks\\Project Management v5.2\\UC PJM 06-02_Approve_Record_as_an_ECO2Man_indicator_MainScenario.xlsx", Enums.BrowserType.Chrome);
       instance.runKeywordDrivenTests();
        
    }
        
     //FR7_Approve_Project_MainScenario 
    @Test
    public void FR7_Approve_Project_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR7_Approve_Project_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR7_Approve_Project_AlternateScenario 
    @Test
    public void FR7_Approve_Project_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR7_Approve_Project_AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR8_Capture_Project_Actions_MainScenario  
    @Test
    public void FR8_Capture_Project_Actions_MainScenario()   throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR8_Capture_Project_Actions_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR9_Edit_Project_Management_MainScenario  
    @Test
    public void FR9_Edit_Project_Management_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR9_Edit_Project_Management_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

   //FR10_Delete_Project_Management_MainScenario
    @Test
    public void FR10_Delete_Project_Management_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR10_Delete_Project_Management_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
   //FR11_View_Linked_Engagements_MainScenario
    @Test
    public void FR11_View_Linked_Engagements_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR11_View_Linked_Engagements_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      //FR12_View_Dashboards_MainScenario
    @Test
    public void FR12_View_Dashboards_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Project Management v5.2\\FR12_View_Dashboards_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
}
