/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author smabe
 */
public class JobRiskAssessment_TestSuit extends BaseClass
{

    static TestMarshall instance;

    public JobRiskAssessment_TestSuit()

    {
        ApplicationConfig appConfig = new ApplicationConfig();
        //TestMarshall.currentEnvironment = Enums.Environment.ANG;
    }

    //Ch05
    @Test
    public void JobRiskAssessment_Regression_Ch05() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\\\.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void JobRiskAssessment_Regression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\Job Risk Assessment Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      @Test
    public void JobRiskAssessment_Regression_training() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void FR1_Capture_Job_Risk_Assessment_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR1-Capture Job Risk Assessment - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_Job_Risk_Assessment_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR1-Record A Job Safety Analysis - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_Job_Risk_Assessment_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR1-Record A Job Safety Analysis - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_Job_Risk_Assessment_OptionalScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR1-Record A Job Safety Analysis - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_Capture_Task_Information_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR2 Capture Task Information - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_Capture_Task_Information_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR2 Capture Task Information - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_Capture_Task_Information_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR2 Capture Task Information - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_Capture_Task_Information_AlternateScenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR2 Capture Task Information - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR3_Capture_JRA_Team_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR3- Capture Hazard  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR4_Capture_Task_Information_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR4-Capture Unwanted Events  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR4_Capture_Task_Information_OptionalScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR4-Capture Task Information  - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR5_Capture_Hazard_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR5-Capture Hazard  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR6_Capture_Unwanted_Events_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR6 - Capture Unwanted Events - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR7_Capture_Controls_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR7-Capture Controls  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR7_Capture_Controls_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR7-Capture Controls  - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR7_Capture_Controls_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR7-Capture Controls  - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Update_Work_Execution_Tasks_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR8-Update Work Execution Tasks   - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Update_Work_Execution_Tasks_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR8-Update Work Execution Tasks   - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Update_Work_Execution_Tasks_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR8-Update Work Execution Tasks   - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Update_Work_Execution_Tasks_AlternateScenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR8-Update Work Execution Tasks   - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR9_Capture_Work_Execution_Specification_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR9-Capture Work Execution Specification - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR10_Sign_off_the_Change_Log_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR10-Sign off the Change Log - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR11_Capture_Actions_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR11-Capture Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR12_Edit_Job_Risk_Assessment_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR12 - Edit Job Risk Assessment - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR13_Delete_Job_Risk_Assessment_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Risk Assessment\\FR13-Delete Job Risk Assessment   - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

   

}
