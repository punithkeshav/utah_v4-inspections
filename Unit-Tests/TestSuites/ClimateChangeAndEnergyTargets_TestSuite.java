/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMabe
 */
public class ClimateChangeAndEnergyTargets_TestSuite {

    static TestMarshall instance;

    public ClimateChangeAndEnergyTargets_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }

    
     @Test
    public void ClimateChangeAndEnergyTargets_RegressionSuite_Ch05() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Targets\\Climate Change and Energy Targets Regression Ch05.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void ClimateChangeAndEnergyTargets_RegressionSuite() throws FileNotFoundException {
    Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Targets\\Climate Change and Energy Targets Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
      @Test
    public void ClimateChangeAndEnergyTargets_RegressionSuite_tanning() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Targets\\Climate Change and Energy Targets Regression tanning.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 


    @Test
    public void FR1_Capture_ClimateChangeAndEnergyTargets_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Targets\\FR1-Capture Climate Change and Energy Targets - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR2_Review_Registered_Project_MainScenario()
    @Test
    public void FR2_Mineral_Production_Targets_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Targets\\FR2- Capture Mineral Production Targets - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
  
    @Test
    public void FR3_Edit_Climate_Change_and_Energy_Targets_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Targets\\FR3-Edit Climate Change and Energy Targets   - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
    //FR4_Capture_Project_Actual_MainScenario 
    @Test
    public void FR4_Delete_Climate_Change_and_Energy_Targets_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Climate Change and Energy Targets\\FR4-Delete Climate Change and Energy Targets - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
   
    
}
