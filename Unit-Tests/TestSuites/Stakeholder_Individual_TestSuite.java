/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author RNagel
 */
public class Stakeholder_Individual_TestSuite extends BaseClass {

    static TestMarshall instance;

    public Stakeholder_Individual_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.MTN;

        //*******************************************
    }

    
       //Stakeholder Individual Regresssion
      
    
     @Test
    public void StakeholderIndividual_Regresssion() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\Stakeholder_Regresssion.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
    
    
        @Test
    public void StakeholderIndividual_Regresssion1_traning_site() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder_Individual_Regresssion_1_traning_site.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
     @Test
    public void StakeholderIndividual_Regresssion2_traning_site() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder_Individual_Regresssion_2_traning_site.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    

     @Test
    public void StakeholderIndividual_Regresssion1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder_Individual_Regresssion_1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
     @Test
    public void StakeholderIndividual_Regresssion2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder_Individual_Regresssion_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
       @Test
    public void StakeholderIndividual_Regresssion_Ch05() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\Stakeholder_Regresssion Ch05.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
    
    //S4_StakeholderIndividual_Beta
    @Test
    public void S4_StakeholderIndividual_Beta() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\S4_Stakeholder Individual_Beta.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }    

    //ISom-Qa_SA
      @Test
    public void S4_StakeholderIndividual_ISom_Qa_SA() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\S4_Stakeholder Individual_IsomQa.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  

     //ch05
      @Test
    public void S4_StakeholderIndividual_ch05() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\S4_Stakeholder Individual_ch05.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
      //ISom-DEV
      @Test
    public void S4_StakeholderIndividual_IsomDev() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\S4_Stakeholder Individual_IsomDev.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  

    //FR1-Capture_StakeholdeFR1_Capture_Stakeholder_Individual_MainScenarior_Individual - MainScenario
    @Test
    public void FR1_Capture_Stakeholder_Individual_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR1-Capture_Stakeholder_Individual - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR1-Capture_Stakeholder_Individual - AlternateScenario1
    @Test
    public void FR1_Capture_Stakeholder_Individual_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR1-Capture_Stakeholder_Individual - AlternateScenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR1-Capture_Stakeholder_Individual - OptionalScenario
    @Test
    public void FR1_Capture_Stakeholder_Individual_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR1-Capture_Stakeholder_Individual - OptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //UC_STA01_02_UpdateStakeholderDetails_MainScenario
    @Test
    public void UC_STA01_02_UpdateStakeholderDetails_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\UC_STA01_02_UpdateStakeholderDetails_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //UC_STA01_02_UpdateStakeholderDetails_AlternateScenario1
     @Test
    public void UC_STA01_02_UpdateStakeholderDetails_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\UC_STA01_02_UpdateStakeholderDetails_AlternateScenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2_CaptureStakeholderAnalysis_MainScenario
    @Test
    public void FR2_CaptureStakeholderAnalysis_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR2_CaptureStakeholderAnalysis_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
   //FR2_Capture_Stakeholder_OptionalScenario
    
    @Test
    public void FR2_CaptureStakeholderAnalysis_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR2_Capture_Stakeholder_OptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
 //UC_STA02_02_CaptureTopicIssueAssessment_MainScenario 
    @Test
    public void UC_STA02_02_CaptureTopicIssueAssessment_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\UC_STA02_02_CaptureTopicIssueAssessment_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR3_Capture_and_or_UpdateRelatedStakeholders_MainScenario 
    @Test
    public void FR3_Capture_and_or_UpdateRelatedStakeholders_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR3_Capture_and_or_UpdateRelatedStakeholders_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR3_Capture_and_or_UpdateRelatedStakeholders_AlternateScenario
    @Test
    public void FR3_Capture_and_or_UpdateRelatedStakeholders_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR3-Capture Related Stakeholder - AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR4_CaptureRelatedGroups_MainScenario 
    @Test
    public void FR4_CaptureRelatedGroups_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR4_CaptureRelatedGroups_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR4_CaptureRelatedGroups_AlternateScenario
    @Test
    public void FR4_CaptureRelatedGroups_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR4_CaptureRelatedGroups_AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR5_CaptureUpdateVulnerability_MainScenario
    @Test
    public void FR5_CaptureUpdateVulnerability_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR5_CaptureUpdateVulnerability_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR5_CaptureUpdateVulnerability_AlternateScenario_1
    @Test
    public void FR5_CaptureUpdateVulnerability_AlternateScenario_1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR5_CaptureUpdateVulnerability_AlternateScenario_1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR5_CaptureUpdateVulnerability_AlternateScenario_2
    @Test
    public void FR5_CaptureUpdateVulnerability_AlternateScenario_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR5_CaptureUpdateVulnerability_AlternateScenario_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR6_AddViewEngagements_MainScenario
    @Test
    public void FR6_AddViewEngagements_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR6_View_RelatedAssessment_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR6_AddViewEngagements_AlternateScenario
    @Test
    public void FR6_AddViewEngagements_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR6_AddViewEngagements_AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR7_AddViewEngagementPlan_MainScenario
    @Test
    public void FR7_AddViewEngagementPlan_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR7_AddViewEngagementPlan_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR7_AddViewEngagementPlan_OptionalScenario
     @Test
    public void FR7_AddViewEngagementPlan_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR7_AddViewEngagementPlan_OptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     //FR8_AddViewCommitments_MainScenario
    @Test
    public void FR8_AddViewCommitments_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR8_AddViewCommitments_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR8_AddViewCommitments_AlternateScenario
    @Test
    public void FR8_AddViewCommitments_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR8_AddViewCommitments_AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR9_AddViewGrievances_MainScenario
    @Test
    public void FR9_AddViewGrievances_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR9_View_Grievances_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR10_CaptureActions_MainScenario
    @Test
    public void FR10_CaptureActions_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR10_CaptureActions_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR11_EditStakeholderIndividual_MainScenario
    @Test
    public void FR11_EditStakeholderIndividual_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR11_EditStakeholderIndividual_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR12_DeleteStakeholderIndividual_MainScenario 
    @Test
    public void FR12_DeleteStakeholderIndividual_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR12_DeleteStakeholderIndividual_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR13_ViewStakeholderIndividualReport_MainScenario
    @Test
    public void FR13_ViewStakeholderIndividualReport_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR13_ViewStakeholderIndividualReport_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR14_ViewStakeholderIndividualDashboard_MainScenario
    @Test
    public void FR14_ViewStakeholderIndividualDashboard_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Stakeholder Individual\\FR14_ViewStakeholderIndividualDashboard_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
}